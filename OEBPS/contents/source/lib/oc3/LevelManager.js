function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function LevelManager() {

    var self = this;
    //var parts = window.location.search.substr(1).split("&");

    var parts,
        cookieParts = getCookie('parts');

    if(cookieParts){
        parts = [cookieParts];
    } else {
        parts = window.location.search.substr(1).split("&");
        setCookie('parts', parts);

        //location.href = location.href.split('?')[0];
    }


    var $_GET = {};
    for (var i = 0; i < parts.length; i++) {
        var temp = parts[i].split("=");
        $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
    }
    this.config = ($_GET['config'] ? $_GET['config'] : 'levels/default/index.xml');
    var req = /[^\/]*$/;
    this.route = this.config;
    this.isLocalMode = this.route.substr(this.route.length - 2, this.route.length) == 'js';
    this.route = this.route.replace(req, '');
    var levelValidator = new LevelValidator();
    this.updateConfig = function(url, callback) {
        var parts = url.split("&");
        var $_GET = {};
        for (var i = 0; i < parts.length; i++) {
            var temp = parts[i].split("=");
            $_GET[decodeURIComponent(temp[0])] = decodeURIComponent(temp[1]);
        }
        var newConfig = ($_GET['config'] ? $_GET['config'] : 'levels/default/index.xml');
        if (newConfig == self.config) return false;
        self.config = newConfig;
        var req = /[^\/]*$/;
        self.route = self.config;
        self.isLocalMode = self.route.substr(self.route.length - 2, self.route.length) == 'js';
        self.route = self.route.replace(req, '');
        self.loadConfig(callback);
    };
    this.loadConfig = function(callback) {
        if (this.isLocalMode) {
            importJS(this.config, 'xml', function(xml) {
                var data = xml.replace(/\<\/br\>/g, '\n');
                if (levelValidator.validateXML(data)) callback($.parseXML(data));
                xml = null;
            });
        } else {
            try {
                $.ajax({
                    type: "GET",
                    url: this.config,
                    dataType: "text",
                    success: function(data) {
                        //alert(data);
                        //alert($.parseXML(data));
                        if (levelValidator.validateXML(data)) callback($.parseXML(data));
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        throw new Error('Ошибка загрузки уровня');
                    }
                });
            } catch (error) {
                if (error.message.indexOf("Failed to execute 'send' on 'XMLHttpRequest'") != -1) {
                    //alert('XML-уровень нельзя загрузить локально. Используйте .js');
                }
            }
        }
    };
    this.initRoute = function(xmlRoot) {
        var xmlRoute = $(xmlRoot).attr('route');
        this.route = (typeof xmlRoute == 'undefined') ? this.route : xmlRoute;
    };
    this.getRoute = function(val) {
        return ((val != '' && typeof val != 'undefined') ? (this.route + val) : val)
    };
}

function LevelValidator() {
    this.validateXML = function(txt) {
        if (window.ActiveXObject) {
            this.onSuccess();
        } else if (document.implementation.createDocument) {
            try {
                var text = txt;
                //alert('got something');
                //alert(txt);
                var parser = new DOMParser();
                var xmlDoc = parser.parseFromString(text, "application/xml");
            } catch (err) {
                this.onError(err.message);
                return false;
            }
            if (xmlDoc.getElementsByTagName("parsererror").length > 0) {
                checkErrorXML(xmlDoc.getElementsByTagName("parsererror")[0]);
                this.onError(xt);
                return false;
            } else {
                this.onSuccess();
            }
        } else {
            //alert('Browser cannot handle XML validation');
        }
        return true;
    };
    this.onError = function(errorMessage) {
        var err = new Error('\nУровень содержит следующие ошибки:\n' + errorMessage.replace('This page contains the following errors:', ''));
        err.name = 'Ошибка синтаксиса xml';
        throw err;
        return false;
    };
    this.onSuccess = function() {
        //alert('Level succesfully loaded!');
        return true;
    };

    function checkErrorXML(x) {
        xt = "";
        h3OK = 1;
        checkXML(x);
    }

    function checkXML(n) {
        var l, i, nam;
        nam = n.nodeName;
        if (nam == "h3") {
            if (h3OK == 0) {
                return;
            }
            h3OK = 0;
        }
        if (nam == "#text") {
            xt = xt + n.nodeValue + "\n";
        }
        l = n.childNodes.length;
        for (i = 0; i < l; i++) {
            checkXML(n.childNodes[i]);
        }
    }
}