function ExternalInterface() {
    var self = this;
    this.init = function() {
        window.addEventListener("message", self.receiveMessage, false);
    };
    this.receiveMessage = function(event) {
        console.log(event.data);
        if (event.data == 'destroy') {
            self.destructor.call();
        } else if (event.data == 'resize') {
            self.resize.call();
        } else {
            self.update.call(event.data.replace(/.*\?/, ''));
        }
    };
    this.destructor = function() {};
    this.resize = function() {
        console.log('call resize!');
        window.location.href = window.location.href;
    };
    this.update = function(val) {};
}
window.externalInterface = new ExternalInterface();
externalInterface.init();