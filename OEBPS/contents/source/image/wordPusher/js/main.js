$(window).load(function () {

    $(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

    Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    function reorient(e) {
        window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	/*Глобальные переменные**/

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    if(isMobile) Kinetic.pixelRatio = 1;

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var scrollBar = null;

    var dragging = false;

	var aspectRatio = 0; //widescreen 16:9

    var defaultStageWidth = 1024;
    var defaultStageHeight = 576;

	var wordCollisionArray = [];

	//Отступ всего контента от сцены
	var marginLeft = 60;
    var marginTop = 60;

    var contentMargin = {x: 5, y: 5};

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 20;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

	var isHorizontal;

	var answerLayerClip = {};

    var mainTitle = null;

    var colorSheme = null;
    var skinManager = null;

    var resultScreen = null;

    var levelManager = new LevelManager();

    var mainFont = 'mainFont';

    var adaptMode = false;

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
	var contentScrollLayer = new Kinetic.Layer();
    var answerLayer = new Kinetic.Layer();  
    var dragLayer = new Kinetic.Layer();
    var resultLayer = new Kinetic.Layer();
	var imageContainer = new Kinetic.Layer();

	stage.add(backgroundLayer);
	stage.add(contentLayer);
	stage.add(contentScrollLayer);
    stage.add(answerLayer);
    stage.add(dragLayer);
    stage.add(resultLayer);
	stage.add(imageContainer);

	/*Группы**/
	var textGroup = new Kinetic.Group();
    var answerGroup = new Kinetic.Group();
	var answerGroupContainer = new Kinetic.Group();

	contentLayer.add(textGroup);
	answerLayer.add(answerGroupContainer);

    answerGroupContainer.add(answerGroup);

	$(window).on('resize', fitStage);

    function fitStage(){
        scaleScreen($(window).width(),  $(window).height());
    }

    function initAspectRatio(){
        if($(window).width() / $(window).height() < 1.6){
            aspectRatio = 1;
            defaultStageWidth = 1024;
            defaultStageHeight = 768;

            stage.width(defaultStageWidth);
            stage.width(defaultStageHeight);
        }
    }

    function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        stage.draw();
    }

    function initWindowSize(){      
        scaleScreen($(window).width(), $(window).height());
    }

	function isColliding(firstRectangle, secondRectangle){
		var result = false;
	 
		if(!(firstRectangle.bottom < secondRectangle.top ||
			firstRectangle.top > secondRectangle.bottom ||
			firstRectangle.left > secondRectangle.right ||
			firstRectangle.right < secondRectangle.left))

				result = true;

		return result;
	 }

    skinManager = new SkinManager('assets/skin', function(){
        initAspectRatio();
        initWindowSize();

        setTimeout(initApplication, 300);
    });

    function initApplication(){
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    }

    function createWord(x, y, string, fontSize, answerString, spacer){

    	var wordShape = new Kinetic.Shape({
    		x: x,
    		y: y,
        	drawFunc: regularDrawFunction
	      });

        wordShape.spacer = spacer;

    	wordShape.text = string;

    	wordShape.linkedAnswer = null;

    	wordShape.lock = false;

        wordShape.glowed = false;

    	wordShape.setText = function(textString){
    		wordShape.text = textString;
    		contentLayer.draw();

    		var context = contentLayer.getContext()._context;
    		context.font = fontSize + "px " + mainFont;

    		wordShape.width(context.measureText(textString).width);

    		textGroup.recalculateStrings();
    		contentLayer.draw();
    	}

    	wordShape.setAnswer = function(answer){
    		if(!wordShape.lock){
	    		if(wordShape.linkedAnswer!=null){
	    			wordShape.linkedAnswer.show();
	    			wordShape.linkedAnswer.resetPosition();
	    		}
	    		wordShape.linkedAnswer = answer;
	    		wordShape.setText(answer.getValue());
	    	}
    	}

    	wordShape.setEmpty = function(){
    		wordShape.setText('...');
    	}

    	wordShape.checkAnswer = function(){
            if(wordShape.answer.indexOf('~') != -1){
                var tempArr = wordShape.answer.split('~');
                var isCorrect = false;
                for(var i = 0; i < tempArr.length; i++){
                    if(tempArr[i] == wordShape.text){
                        isCorrect = true;
                        break;
                    }
                }

                return isCorrect;
            }
    		return wordShape.text == wordShape.answer;
    	}

        wordShape.setWrong = function(){
            wordShape.glowed = true;

            wordShape.color = 'red';
        }

    	wordShape.glow = function(){
            if(wordShape.glowed) return;

            if(!wordShape.lock){
                wordShape.glowed = true;

                wordShape.color = colorSheme.glowedWord.hover;;                
                //contentLayer.drawScene();
            }
        }

    	wordShape.unGlow = function(){
            if(!wordShape.glowed) return;
    		if(!wordShape.lock){
	    		wordShape.color = 'black';
                wordShape.glowed = false;
                //contentLayer.drawScene();
	    		//contentLayer.draw();
	    	}
    	}

    	wordShape.setLock = function(){
    		wordShape.lock = true;
    	}

    	if(answerString!=''){ //add to collision group
    		wordShape.text = '...';
    		wordShape.answer = answerString;

    		wordShape.on(events[isMobile].mouseover, function(){
                if(!dragging){
                    wordShape.glow();
                    contentLayer.drawScene();
                }
            });

    		wordShape.on(events[isMobile].mouseout, function(){
                if(!dragging){
                    wordShape.unGlow();
                    contentLayer.drawScene();
                }
            });

    		wordShape.on(events[isMobile].click, function(){
	    		if(wordShape.linkedAnswer!=null){
                    if(answerMode == 'move'){
    	    			wordShape.linkedAnswer.show();
    	    			wordShape.linkedAnswer.resetPosition();
                    }

		    		wordShape.linkedAnswer = null;

		    		wordShape.setEmpty();
	    		}
	    	})

    		wordShape.getBounds = function(){
    			return {
    				left: wordShape.getAbsolutePosition().x,
    				right: wordShape.getAbsolutePosition().x + wordShape.width() * stage.scaleX(),
    				top: wordShape.getAbsolutePosition().y - fontSize,
    				bottom: wordShape.getAbsolutePosition().y}
    		}

    		wordCollisionArray.push(wordShape);
    	}

    	wordShape.color = 'black';

    	var context = contentLayer.getContext()._context;
    	context.font = fontSize + "px " + mainFont;

    	wordShape.width(context.measureText(wordShape.text).width);

        //console.log(context.font, wordShape.width(), wordShape.text);

    	function regularDrawFunction(context) {
		    context._context.font = fontSize + "px " + mainFont;
		    context._context.fillStyle = 'rgba(255, 255, 255, 0)';
		    //context._context.fillStyle = 'black';

		    context.beginPath();
		    context.rect(0, 0 - fontSize, context._context.measureText(this.text).width, fontSize);
			
			context.fill();
			context._context.fillStyle = this.color;
			context.fillText(this.text, 0, 0);

			context.closePath();

			context.fillShape(this);
        }

		return wordShape;
    }

    function createText(x, y, text, maxWidth){

        /*var textRect = new Kinetic.Rect({
            x: 0,
            y: -30,
            width: maxWidth,
            height: 400,
            fill: 'black'
        });

        textGroup.add(textRect);*/

    	textGroup.x(x);
    	textGroup.y(y);

    	maxWidth -= x;

    	textGroup.answerArray = [];

    	textGroup.wordArray = [];

        text = text.replace(/#[^#]+#/gi, function(word){
            textGroup.answerArray.push(word.substr(1, word.length - 2));
            return '#'
        });
        text = text.replace(/\n\r/g, ' ');

    	var wordArray = text.split(' ');

    	var dx = 0;
    	var dy = 0;

    	var answerCounter = 0;

    	var stringWidth = 0;

    	var answerString = '';

		var spacer = 10;
        var questionArray;

        var reg = /#/g;
        var match;

    	for(var i = 0; i < wordArray.length; i++){
    		if(wordArray[i] == '') continue;

            questionArray = [];

            while((match = reg.exec(wordArray[i])) != null) {
                questionArray.push(match.index);
            }

            if(questionArray.length > 0){
                //var subWords = wordArray[i].split('#');

                var subSpacer = 0;

                var word = null;

                var subWord = '';

                for(var j = 0; j < wordArray[i].length; j++){
                    if(wordArray[i][j] == '#'){
                        if(subWord != ''){
                            word = createWord(dx, dy, subWord, 25, '', 0);

                            dx += word.width();
                            stringWidth += word.width();
                            subWord = '';

                            textGroup.add(word);

                            if(stringWidth > maxWidth){
                                dy += 30;

                                word.x(0);
                                word.y(dy);

                                dx = word.width();

                                stringWidth = word.width();
                            }
                        }
                        word = createWord(dx, dy, '...', 25, textGroup.answerArray[answerCounter], 0);
                        
                        answerCounter++;

                        dx += word.width();
                        stringWidth += word.width();

                        textGroup.add(word);0

                        if(stringWidth > maxWidth){
                            dy += 30;

                            word.x(0);
                            word.y(dy);

                            dx = word.width();

                            stringWidth = word.width();
                        }
                    } else{
                        subWord += wordArray[i][j];
                    }
                }

                if(subWord != ''){
                    word = createWord(dx, dy, subWord, 25, '', 0);

                    dx += word.width();
                    stringWidth += word.width();
                    subWord = '';
                    textGroup.add(word);

                    if(stringWidth > maxWidth){
                        dy += 30;

                        word.x(0);
                        word.y(dy);

                        dx = word.width();

                        stringWidth = word.width();
                    }
                }

                word.spacer = spacer;
                dx += spacer;
                stringWidth += spacer;

                if(stringWidth > maxWidth){
                    dy += 30;

                    word.x(0);
                    word.y(dy);

                    dx = word.width();

                    stringWidth = word.width() + spacer;
                }
            }else{                
        		var word = createWord(dx, dy, wordArray[i], 25, '', spacer);

        		dx += word.width() + spacer;
        		stringWidth += word.width() + spacer;

        		if(stringWidth > maxWidth){
                    dy += 30;

                    word.x(0);
                    word.y(dy);

                    dx = word.width() + spacer;

                    stringWidth = word.width() + spacer;

                    /*if(i+1<wordArray.length){
                        if(wordArray[i+1] == ',' || wordArray[i+1] == '.' || wordArray[i+1] == ')' || wordArray[i+1] == '-'){
                            word.x(dx);
                            word.y(dy);

                            dx += word.width() + spacer;

                            stringWidth += word.width() + spacer;
                        }
                    }*/
        		}
                textGroup.add(word);
            }

            textGroup.height(dy + 60);    		
    	}

        textGroup.scrollBar = false;

        textGroup.showScrollBar = function(){
            var clip = contentLayer.getClip();

            if(clip.height <= textGroup.height()){
                if(!textGroup.scrollBar){
                    scrollBar = createScrollBar(defaultStageWidth - 30, 10, 20, 250, textGroup, contentLayer);
                    $(document).on("mousewheel", wheelScroll);

                    contentLayer.draw();
                    textGroup.scrollBar = true;
                } else{
                    scrollBar.refresh();
                }
            }
        }

    	textGroup.recalculateStrings = function(){
    		var wordCollection = textGroup.children;

    		var stringWidth = 0;

    		var dx = 0;
    		var dy = 0;

    		var spacer = 10;

    		for(var i = 0; i < wordCollection.length; i++){

    			wordCollection[i].x(dx);
    			wordCollection[i].y(dy);

    			dx += wordCollection[i].width() + wordCollection[i].spacer;
	    		stringWidth += wordCollection[i].width() + wordCollection[i].spacer;

                if(stringWidth > maxWidth){
                    dy += 30;

                    wordCollection[i].x(0);
                    wordCollection[i].y(dy);

                    dx = wordCollection[i].width() + wordCollection[i].spacer;

                    stringWidth = wordCollection[i].width() + wordCollection[i].spacer;

    	    		/*if(i+1<wordCollection.length){

                        if(wordCollection[i+1].text == ',' || wordCollection[i+1].text == '.' || wordCollection[i+1].text == ')' || wordCollection[i+1].text == '-'){
                            wordCollection[i].x(dx);
                            wordCollection[i].y(dy);

                            dx += wordCollection[i].width() + wordCollection[i].spacer;

                            stringWidth += wordCollection[i].width() + wordCollection[i].spacer;
                        }
                    }*/
                }
    		}

            textGroup.height(dy + 60);

            //textGroup.showScrollBar();

            scrollBar.refresh();

            /*if(scrollBar == null){
                var clip = contentLayer.getClip();

                //if(clip.height < textGroup.height()){
                    //console.log('out');
                    scrollBar = createScrollBar(stage.getWidth() - 30, 10, 20, 250, textGroup, contentLayer);
                    contentLayer.draw();
                    $(document).on("mousewheel", wheelScroll);
                //}
            }*/
    	}

    	contentLayer.draw();
    }

    function createAnswer(x, y, answer, fontSize){
        var answerRectGroup = new Kinetic.Group({
            x: x,
            y: y,
            draggable: true
        });

        var answerSheme = colorSheme.answerRect;

        var answerText = new Kinetic.Text({
            x: 5,
            y: 5,
            text: answer,
            fontFamily: mainFont,
            fontSize: fontSize,
            fill: answerSheme.fontColor
        })

        var answerRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: answerText.width()+10,
            height: answerText.height()+10,
            fill: answerSheme.color,
            stroke: answerSheme.stroke,
            cornerRadius: answerSheme.cornerRadius
        })

        answerRectGroup.defaultX = x;
        answerRectGroup.defaultY = y;

        answerRectGroup.answer = answer;

        answerRectGroup.on(events[isMobile].mouseover, hoverPointer);
        answerRectGroup.on(events[isMobile].mouseout, hoverPointer);

        answerRectGroup.setDefaultPosition = function(x, y){
            answerRectGroup.defaultX = x;
            answerRectGroup.defaultY = y;

            answerRectGroup.x(x);
            answerRectGroup.y(y);
        }

        answerRectGroup.getBounds = function(){         
            return {
                left: answerRectGroup.getAbsolutePosition().x,
                right: answerRectGroup.getAbsolutePosition().x + answerRect.width() * stage.scaleX(),
                top: answerRectGroup.getAbsolutePosition().y,
                bottom: answerRectGroup.getAbsolutePosition().y + answerRect.height() * stage.scaleY()
            };
        }

        answerRectGroup.getColliding = function(){
            var wordClip = contentLayer.getClip();
            var firstRectangle = answerRectGroup.getBounds();
            var secondRectangle;

            for(var i = 0; i < wordCollisionArray.length; i++){
                secondRectangle = wordCollisionArray[i].getBounds();
                if(secondRectangle.left > (wordClip.x + wordClip.width * stage.scaleX()) || secondRectangle.top > (wordClip.y + wordClip.height * stage.scaleY())) continue;
                if(isColliding(firstRectangle, secondRectangle)) return wordCollisionArray[i];
            }

            return false;
        }

        answerRectGroup.getColliding.process();

        answerRectGroup.resetPosition = function(){
            answerRectGroup.x(answerRectGroup.defaultX);
            answerRectGroup.y(answerRectGroup.defaultY);

            answerLayer.draw();
        }
        answerRectGroup.getValue = function(){
            return answerRectGroup.answer;
        }
        answerRectGroup.hover = function(){
            answerRect.stroke(colorSheme.glowedWord.stroke);
            //answerLayer.draw();
        }
        answerRectGroup.unHover = function(){
            answerRect.stroke(answerSheme.stroke);
            //answerLayer.draw();
        }

        answerRectGroup.width(answerRect.width());
        answerRectGroup.height(answerRect.height());

        answerRectGroup.add(answerRect);
        answerRectGroup.add(answerText);

        answerRectGroup.on(events[isMobile].down, onDragStart);
        answerRectGroup.on('dragmove', onDragMove);
        answerRectGroup.on('dragend', onDragEnd);

        answerRectGroup.collidingWord = false;

        function onDragStart(evt){
            /*answerLayer.setClip({
                x: 0,
                y: 0,
                width: defaultStageWidth,
                height: defaultStageHeight
            });

            var answerCollection = answerGroup.children;

            for(var i = 0; i < answerCollection.length; i++){
                if(answerCollection[i] != answerRectGroup) answerCollection[i].hide();
            }*/

            dragging = true;

            answerRectGroup.x(answerRectGroup.getAbsolutePosition().x / stage.scaleX());
            answerRectGroup.y(answerRectGroup.getAbsolutePosition().y / stage.scaleY());

            answerRectGroup.moveTo(dragLayer);

            dragLayer.draw();
            answerLayer.draw();

            answerRectGroup.startDrag();
        }

        function onDragEnd(evt){
            answerRectGroup.moveTo(answerGroup);
            answerRectGroup.resetPosition();

            if(answerRectGroup.collidingWord){
                 answerRectGroup.collidingWord.setAnswer(answerRectGroup);
                 answerRectGroup.unHover();
                 if(answerMode == 'move') answerRectGroup.hide();
                 //answerRectGroup.resetPosition();
                 answerRectGroup.collidingWord.unGlow();

                 contentLayer.draw();
            }

            /*answerLayer.setClip(answerLayerClip);

            var answerCollection = answerGroup.children;

            for(var i = 0; i < answerCollection.length; i++){
                if(answerCollection[i] != answerRectGroup) answerCollection[i].show();
            }*/

            answerLayer.draw();
            dragLayer.draw();
            contentLayer.draw();

            dragging = false;
        }
        function onDragMove(evt){
            var colliding = answerRectGroup.getColliding();
            var contentChange = false;

            if(answerRectGroup.collidingWord){
                if(answerRectGroup.collidingWord.glowed) contentChange = true;
                answerRectGroup.collidingWord.unGlow();                
            }

            if(colliding){

                if(!colliding.glowed) contentChange = true;
                
                answerRectGroup.hover();
                colliding.glow();
            } else{
                answerRectGroup.unHover();
            }

            if(contentChange) contentLayer.batchDraw();


            answerRectGroup.collidingWord = colliding;
                        
        }

        return answerRectGroup;
    }

    function buildAnswerPanel(x, y, width, height, answerArray){

        answerGroup.x(x + 20);
        answerGroup.y(y + 20);

        answerLayerClip = {
            x: answerGroup.x() - 3,
            y: answerGroup.y() - 3,
            //y: $(window).height() / stage.scaler - (aspectRatio == 1 ? 220 : 190) + 20,
            width: width + 6,
            height: height + 6
        }

        answerLayer.setClip(answerLayerClip);

        var backgroundRectangle = new Kinetic.Rect({
            x: x,
            y: y,
            fill: colorSheme.experimentPanel.backgroundColor,
            stroke: colorSheme.experimentPanel.stroke,
            cornerRadius: colorSheme.experimentPanel.cornerRadius,
            strokeWidth: 0.8
        });

        backgroundRectangle.opacity(colorSheme.experimentPanel.opacity)

        contentScrollLayer.add(backgroundRectangle);

        var dx = 0;
        var dy = 0;

        var verticalSpacer = 10;
        var horizontalSpacer = 10;

        var maxWidth = answerLayerClip.width - x - 60;

        var answerHash = [];

        var tempArr;

        for(var i = 0; i < answerArray.length; i++){
            if(answerArray[i].indexOf('~') != -1){
                tempArr = answerArray[i].split('~');
                for(var j = 0; j < tempArr.length; j++){
                    if(j == 0){
                        answerArray[i] = tempArr[j];
                    } else{
                        answerArray.push(tempArr[j]);
                    }
                }
            }
        }

        shuffle(answerArray);

        for(i = 0; i < answerArray.length; i++){

            if(typeof answerHash[answerArray[i]] == 'undefined'){
                answerHash[answerArray[i]] = 1;
            } else continue;

            var answer = createAnswer(0, 0, answerArray[i], 25);

            var position = dx + answer.width() + horizontalSpacer;

            if(position > maxWidth){
                dx = 0;
                position = answer.width() + horizontalSpacer;
                dy += answer.height() + verticalSpacer;
            }

            answer.setDefaultPosition(dx, dy);

            answerGroup.add(answer);

            dx = position;
        }

        answerGroupContainer.height(dy + answer.height() + verticalSpacer);

        backgroundRectangle.width(answerLayerClip.width - 6)
        backgroundRectangle.height(130);

        answerLayer.draw();

        //if(answerLayerClip.height < answerGroupContainer.height()){
        var scrollbar = createScrollBar(
                $(window).width() / stage.scaler - 45,
                backgroundRectangle.y() + 10,
                20,
                backgroundRectangle.height() - 20,
                answerGroupContainer,
                answerLayer,
                contentScrollLayer);

        scrollbar.refresh();
        //}   
    }

    function xmlLoadHandler(xml){

        initImageContainer();

        resultScreen = initResultScreen();

        var root = $(xml).children('component');

        if(root.length == 0){
            root = $(xml).children('task').children('set').eq(0);
            adaptMode = true;
            if(root.length == 0){
                throw new Error('XML имеет неверный формат!');
            }
        }

        levelManager.initRoute(root);

        var title = root.children('title').text();
        var question = root.children('question').text();

        mainTitle = createHead(title, question);

        var textData = $(root).children('text');

        var imageSource = $(textData).attr('image');

        var imageMargin = (typeof imageSource != 'undefined' && imageSource != '') ? ((($(window).width() / stage.scaler) - 50) * 1/3) : 35;

        var contentClip = {
            x: imageMargin,
            y: mainTitle.height() + 5,
            width: ($(window).width() / stage.scaler) - imageMargin,
            height: ($(window).height() / stage.scaler) - 195 - mainTitle.height()
        }

        contentLayer.setClip(contentClip);

        answerLayerClip = {
            x: 25,
            //y: $(window).height() / stage.scaler - (aspectRatio == 1 ? 220 : 190),
            y: contentClip.y + contentClip.height + 5,
            width: $(window).width() / stage.scaler,
            height: 95
        }

        answerLayer.setClip(answerLayerClip);

    	var wrongWords = root.children('wrongWords').text().split('#');

        var mode = $(root).attr('mode');

        if(adaptMode){
            mode = $(xml).children('task').attr('mode');
        }

        answerMode = (typeof mode !== 'undefined' && mode !== false) ? mode : 'move';

        backgroundLayer.add(mainTitle);

        if(typeof imageSource != 'undefined'){
            var imgObj = new Image();

            imgObj.onload = function(){

                var imagePadding = 10;
                
                var tipImage = new Kinetic.Image({
                    x: imagePadding,
                    y: contentClip.y,
                    width: imgObj.width,
                    height: imgObj.height,
                    image: imgObj
                });

                var tempRect = new Kinetic.Rect({
                    x: tipImage.x(),
                    y: tipImage.y(),
                    width: imageMargin - imagePadding * 3,
                    height: contentClip.height - (imagePadding << 1),
                    fill: 'black'
                });

                var scaler = calculateAspectRatioFit(
                    imgObj.width,
                    imgObj.height,
                    tempRect.width(),
                    tempRect.height()
                );

                tipImage.scale({
                    x: scaler,
                    y: scaler
                });

                var imageWidth = tipImage.width() * scaler;
                var imageHeight = tipImage.height() * scaler;

                if(imageWidth < tempRect.width()) tipImage.x((tempRect.width() >> 1) - (imageWidth >> 1) + imagePadding);
                if(imageHeight < tempRect.height()) tipImage.y((tempRect.height() >> 1) - (imageHeight >> 1) + contentClip.y);

                //backgroundLayer.add(tempRect);
                backgroundLayer.add(tipImage);
                backgroundLayer.draw();

                tipImage.on(events[isMobile].click, function(){
                    imageContainer.zoomImage(imgObj);
                })
            }

            imgObj.src = levelManager.getRoute(imageSource);
        }

        //createText(50, 90, textData.text(), contentLayer.getClip().width - contentLayer.getClip().x - 260);
        createText(contentClip.x, contentClip.y + 30, textData.text(), contentClip.width + contentClip.x - 85);

    	for(var i = 0; i < wrongWords.length; i++){
    		if(wrongWords[i] != '') textGroup.answerArray.push(wrongWords[i]);
    	}

        //buildAnswerPanel(30, answerLayerClip.y + 10, textGroup.answerArray);
    	buildAnswerPanel(
            10,
            answerLayerClip.y + 10,
            answerLayerClip.width - 25,
            80,
            textGroup.answerArray
        );

        //textGroup.showScrollBar();

        scrollBar = createScrollBar(
            $(window).width() / stage.scaler - 45,
            contentClip.y + 5,
            20,
            contentClip.height,
            textGroup,
            contentLayer);
        //$(document).on("mousewheel", wheelScroll);

        scrollBar.refresh();

    	createButton((($(window).width() / stage.scaler) >> 1) - 60, ($(window).height() / stage.scaler) - 35 - screenMarginY, 120, 30, 'Ответить', answerButtonHandler);

    	/*var columns = root.children('table').children('column');
    	var wrongWords = root.children('wrongWords').text();

    	buildColumns(columns, wrongWords);

    	var resetButton = createButton(690, 450, 120, 30, 'Сброс', clear);
    	resetButton.id('reset');

    	createButton(820, 450, 120, 30, 'Ответить', answerButtonHandler);*/
    }

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
     */
    function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: mainTitle.height() + mainTitle.fontSize() * 0.2 + padding,
                stroke: headSheme.questionTitle.stroke
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

    function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

    function wheelScroll(evt){
    	scrollBar.scrollBy(-evt.originalEvent.wheelDelta/12);
    }

    function createScrollBar(x, y, width, height, contentGroup, clippedLayer){
        var scrollObject = new Kinetic.Group();

        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Kinetic.Rect({
           x: x,
           y: y,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11 + y;

        var vscroll = new Kinetic.Group({
            x: x + 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart*stage.scaleY()){
                    newY = scrollStart*stage.scaleY();
                }
                else if(newY > scrollEnd*stage.scaleY()) {
                    newY = scrollEnd*stage.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                }
            }
          });

          var scrollCircle = new Kinetic.Group();

          var backgroundCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 10,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          }

          var scrollTopArrowGroup = new Kinetic.Group({
            x: x,
            y: y
          })

          var scrollTopArrow = new Kinetic.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Kinetic.Group({
            x: x,
            y: y + height
          });

          var scrollBotArrow = new Kinetic.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Kinetic.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          contentScrollLayer.add(scrollObject);

          contentScrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            contentScrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            contentScrollLayer.drawScene();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
          }

          scrollObject.refresh = function(){
            var clip = clippedLayer.getClip();

            //vscrollArea.y(clip.y + 5);
            //vscrollArea.height(clip.height - 10);

            //scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            //scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            //scrollTopArrowGroup.y(clip.y + 5);
            //scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            //contentGroup.y(0);
            //vscroll.y(scrollStart);

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
                scrollTopArrowGroup.hide();
                scrollBotArrowGroup.hide();
            } else{
                scrollObject.show();
                scrollTopArrowGroup.show();
                scrollBotArrowGroup.show();
            }

            scrollObject.resetScroll();

            //contentScrollLayer.draw();
            //clippedLayer.batchDraw();
          }

          clippedLayer.batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.batchDraw()
          }
          return scrollObject;
    }

    function answerButtonHandler(evt){
    	var isCorrect = true;

    	for(var i = 0; i < wordCollisionArray.length; i++){
    		if(!wordCollisionArray[i].checkAnswer()){
    			isCorrect = false;
    			wordCollisionArray[i].setWrong();
    			wordCollisionArray[i].setLock();
    		}
    	}

        resultScreen.invoke(isCorrect);

    	//showResultLabel($(window).width() / stage.scaler - 360, answerLayerClip.y + answerLayerClip.height + 40, isCorrect);

    	var button = evt.target.getParent();

    	button.setText('Ещё раз');
    	button.off(events[isMobile].click);
    	button.on(events[isMobile].click, clear);
    	backgroundLayer.draw();
        contentLayer.drawScene();
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback){
        var buttonSheme = colorSheme.button;

    	var buttonGroup = new Kinetic.Group({ x:x, y:y });
    	var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
	        x: 0,
	        y: 0,
	        width: width,
	        height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
	      });

    	var label = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: 2,
	        width: buttonRectangle.width(),
	        height: buttonRectangle.height(),
	        text: textLabel,
            fontSize: 24,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
	      });

        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mouseout, resetPointer);

    	/*Определяет новую текстовую метку кнопки
    	@param {string} value новое значение метки**/
    	buttonGroup.setText = function(value){
    		label.text(value);
    		backgroundLayer.draw();
    	};

    	buttonGroup.add(buttonRectangle);
    	buttonGroup.add(label);
    	backgroundLayer.add(buttonGroup);
    	backgroundLayer.draw();
    	
    	buttonGroup.on(events[isMobile].click, callback);
    	/*label.on("click", function(evt){ //костыль вместо полноценного потока событий
    		evt.targetNode = buttonRectangle;
    		buttonRectangle.fire('click', evt);
    	});*/
		return buttonGroup;
    }

    function showResultLabel(x, y, isWin){
    	var label = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	backgroundLayer.draw();
    }

    function initResultScreen(){
        var resultScreen = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var _modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            opacity: 0.8
        });

        //console.log(_modalRect);

        _modalRect.hide();

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 340,
            height: 240,
            fill: 'white',
            stroke: "black",
            strokeWidth: 0.7
        });

        var resultLabel = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            text: '',
            fontSize: 40,
            fontFamily: mainFont,
            fill: 'black'
        });

        var imgObj = new Image();

        var resultButton;

        imgObj.onload = function(){

            resultButton = new Kinetic.Sprite({
                x: (backgroundRect.width() >> 1) - 70,
                y: backgroundRect.height() - 160,
                image: imgObj,
                animations: {
                    correct: [
                      // x, y, width, height
                      0,0,140,140
                    ],
                    wrong: [
                      // x, y, width, height
                      140,0,140,140
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            resultScreen.on(events[isMobile].click, function(){

                resultScreen.x((($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1));
                resultScreen.y((($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1));

                resultScreen.scale({
                    x: 1,
                    y: 1
                })

                var outTween = new Kinetic.Tween({
                    node: resultScreen,
                    scaleX: 0,
                    scaleY: 0,
                    x: (defaultStageWidth >> 1),
                    y: ((defaultStageHeight - screenMarginY) >> 1),
                    duration: 0.3,
                    easing: Kinetic.Easings.EaseInOut
                });

                _modalRect.hide();

                outTween.onFinish = function(){             
                    resultScreen.hide();
                }

                outTween.play();
            });


            resultScreen.add(resultButton);

            resultLayer.draw();
        }

        imgObj.src = skinManager.getGraphic(colorSheme.resultButton.iconSource);

        resultLayer.add(_modalRect);
        resultScreen.add(backgroundRect);
        resultScreen.add(resultLabel);

        resultScreen.width(backgroundRect.width());
        resultScreen.height(backgroundRect.height());

        resultScreen.hide();

        resultLayer.add(resultScreen);
        resultLayer.draw();

        resultScreen.centerLabel = function(){
            resultLabel.x((backgroundRect.width() >> 1) - (resultLabel.width() >> 1));
            resultLabel.y((resultButton.y() >> 1) - ((resultLabel.height() + resultLabel.fontSize() * 0.2) >> 1));
        }

        resultScreen.setWin = function(){
            //backgroundRect.fill('green');
            //resetButton.fill(colorSheme.resultButton.correctColor);
            resultButton.animation("correct");
            resultLabel.text('Правильно!');
        }

        resultScreen.setLoose = function(){
            //backgroundRect.fill('red');
            //resetButton.fill(colorSheme.resultButton.wrongColor);
            resultButton.animation("wrong");
            resultLabel.text('Неправильно!');
        }

        resultScreen.invoke = function(isWin){
            if(isWin){
                resultScreen.setWin();
            } else{
                resultScreen.setLoose();
            }

            resultScreen.centerLabel();

            resultScreen.x(defaultStageWidth >> 1);
            resultScreen.y((defaultStageHeight - screenMarginY) >> 1);

            resultScreen.scale({
                x: 0,
                y: 0
            })

            var inTween = new Kinetic.Tween({
                node: resultScreen,
                scaleX: 1,
                scaleY: 1,
                x: (($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1),
                y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1),
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            _modalRect.show();
            resultScreen.show();

            inTween.play();
        }

        return resultScreen;
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

    function initImageContainer(){
        var image = new Kinetic.Image({
          x: 20,
          y: 20
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler
        });

        var modalGroup = new Kinetic.Group();

        modalRect.opacity(0.8);

        image.on(events[isMobile].mouseover, hoverPointer);
        image.on(events[isMobile].mouseout, resetPointer);

        image.hide();
        modalRect.hide();

        modalGroup.add(modalRect);
        modalGroup.add(image);

        imageContainer.add(modalGroup);

        imageContainer.zoomImage = function(imageObj){

            image.setImage(imageObj);

            var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight-40);

            image.width(imageObj.width * scaler);
            image.height(imageObj.height * scaler);

            if(!image.isVisible()){
                image.x(defaultStageWidth >> 1);
                image.y(defaultStageHeight >> 1);

                image.scale({
                    x: 0,
                    y: 0
                })

                var inTween = new Kinetic.Tween({
                    node: image,
                    scaleX: 1,
                    scaleY: 1,
                    x: (($(window).width() / stage.scaler) >> 1) - (image.width() >> 1),
                    y: (($(window).height() / stage.scaler) >> 1) - (image.height() >> 1),
                    duration: 0.4,
                    easing: Kinetic.Easings.EaseInOut
                })
                inTween.onFinish = function(){
                    //modalRect.show();
                }

                modalRect.show();

                image.show();
                inTween.play();
            }

            modalGroup.on(events[isMobile].click, zoomOut);

            imageContainer.draw();
        }

        function zoomOut(evt){

            var outTween = new Kinetic.Tween({
                node: image,
                scaleX: 0,
                scaleY: 0,
                x: defaultStageWidth>>1,
                y: defaultStageHeight>>1,
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            outTween.onFinish = function(){
                image.hide();
                imageContainer.draw();
            }

            modalRect.hide();

            outTween.play();
        }
    }

});