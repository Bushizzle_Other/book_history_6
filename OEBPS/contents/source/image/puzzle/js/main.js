$(window).load(function () {

  Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

  $(document).on('touchmove touch tap',function(e) {
      e.preventDefault();
  });

  function reorient(e) {
      window.location.href = window.location.href;
  }

  window.onorientationchange = reorient;

	/*Глобальные переменные**/

  var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
  var events = [{}, {}];

  var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

  var screenMarginX = 0;
  var screenMarginY = isIOS ? 0 : 0;

  events[0].click = 'click';
  events[1].click = 'tap';
  events[0].down = 'mousedown';
  events[1].down = 'touchstart';
  events[0].up = 'mouseup';
  events[1].up = 'touchend';
  events[0].move = 'mousemove';
  events[1].move = 'touchmove';
  events[0].mouseover = 'mouseover';
  events[1].mouseover = 'mouseover';
  events[0].mouseout = 'mouseout';
  events[1].mouseout = 'mouseout';

	/*Глобальный флаг, характеризующий движение мыши с зажатой кнопкой по канве**/
	var moving = false;
	/*Отпустили ли левую кнопку мыши над узлом**/
	var isMouseUpOnNode = false;
	/*Узел из которого в данный момент рисуется связь**/
	var currentNode = null;
	/*Текущие связи, заполняются пользователем**/
	var connections = [];
	/*Связи-ответы, берутся из xml**/
	var answers = [];
	/*Отрисовываемая в данный момент линия**/
	var currentLine = null;

  var horizontalPieces = 6;
  var verticalPieces = 4;

  var contentMargin = {x: 5, y: 5};

  var pieceWidth = 0;
  var pieceHeight = 0;

  var puzzleList = [];

  var currentPuzzle = -1;

  var puzzleCount = 0;

  var puzzleBorderWidth = 0;
  var puzzleStrokeColor = 'black';

  var isFullScreen = false;

  var backgroundRectangle = null;

  var correctCount = 0;
  var tileCount = horizontalPieces * verticalPieces;

	var aspectRatio = 0; //widescreen 16:9

  var defaultStageWidth = 1024;
  var defaultStageHeight = 576;

  /*var pictureMaxWidth = defaultStageWidth - 20;
  var pictureMaxHeight = defaultStageHeight - 65;*/

  var pictureMaxWidth = 0;
  var pictureMaxHeight = 0;

  var nextButton = null;
  var descriptionButton = null;
  var resultLabel = null;

  var titleGroup = null;

  var levelManager = new LevelManager();

  var descriptionContainer = null;

  var mobileScaler = isMobile ? 1.5 : 1;

  var isGameFinished = false;

  var mainFont = 'mainFont';

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

  var contentLayer = new Kinetic.Layer();
  var backgroundLayer = new Kinetic.Layer();
  var descriptionLayer = new Kinetic.Layer();
  var contentScrollLayer = new Kinetic.Layer();
  var dragLayer = new Kinetic.Layer();
  var imageContainer = new Kinetic.Layer();

  stage.add(backgroundLayer);
  stage.add(descriptionLayer);
  stage.add(contentScrollLayer);
  stage.add(contentLayer);
  stage.add(dragLayer);
  stage.add(imageContainer);

  var puzzleGroup = new Kinetic.Group();

  contentLayer.add(puzzleGroup);
  
	$(window).on('resize', fitStage);

  function fitStage(){
    scaleScreen($(window).width(),  $(window).height());
  }

  function initAspectRatio(){
    if($(window).width() / $(window).height() < 1.6){
      aspectRatio = 1;
      defaultStageWidth = 1024;
      defaultStageHeight = 768;

      stage.width(defaultStageWidth);
      stage.width(defaultStageHeight);
    }
  }

  function scaleScreen(width, height){

      width -= screenMarginX;
      height -= screenMarginY;

      var scalerX = width / defaultStageWidth;
      var scalerY = height / defaultStageHeight;
      var scaler = Math.min(scalerX, scalerY);
  
      stage.scaleX(scaler);
      stage.scaleY(scaler);

      stage.scaler = scaler;

      stage.width(width);
      stage.height(height);

      stage.draw();
  }

  function initWindowSize(){    
    scaleScreen($(window).width(), $(window).height());
  }

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var pictureLayer = new Kinetic.Layer();
	var nodeLayer = new Kinetic.Layer();
	var achivementLayer = new Kinetic.Layer();
	var lineLayer = new Kinetic.Layer();
	var pointLayer = new Kinetic.Layer();

	stage.add(backgroundLayer);

	//createBackground();
	//createButton();

 var skinManager = null;
 var colorSheme = null;

 var preLoader = null;

 skinManager = new SkinManager('assets/skin', function(){
    colorSheme = skinManager.colorSheme;
    preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));
    levelManager.loadConfig(xmlLoadHandler);
  });

    function checkForWin(){
      if(correctCount == tileCount){

        var tileTween = new Kinetic.Tween({
          node: puzzleGroup,
          opacity: 0,
          duration: 3
        })

        tileTween.play();

        puzzleList[currentPuzzle].resultImage.show();

        var resultImageTween = new Kinetic.Tween({
          node: puzzleList[currentPuzzle].resultImage,
          opacity: 1,
          duration: 3
        })

        resultImageTween.onFinish = function(){
          if(currentPuzzle != listCount - 1){
            //console.log(backgroundRectangle.y(), backgroundRectangle.height());
            //showResult(false, backgroundRectangle.x() + ((backgroundRectangle.width() > 420) ? ((backgroundRectangle.width() >> 1) - 210) : 0 ), backgroundRectangle.y() + backgroundRectangle.height() - 30);
            showResult(false, backgroundRectangle.x() + backgroundRectangle.width() - 30, backgroundRectangle.y() + (backgroundRectangle.height() >> 1));
          } else{
            showResult(true, backgroundRectangle.x() + backgroundRectangle.width() - 30, backgroundRectangle.y() + (backgroundRectangle.height() >> 1));
          }
        };

        tileTween.play();
        resultImageTween.play();
      }
    }

    function showResult(reachEnd, x, y){
      backgroundLayer.moveToTop();
      isGameFinished = true;
      //nextButton.showHere(x, y);
      resultLabel.showHere(backgroundRectangle.x(), backgroundRectangle.y());
      //descriptionButton.showHere(x - (backgroundRectangle.width() - (descriptionButton.radius << 1) - 10), y);
      //if(puzzleList[currentPuzzle].result == '') descriptionButton.hide();
      //if(reachEnd){
      refreshButton.showHere(x - (backgroundRectangle.width() >> 1) - 35 * mobileScaler, backgroundRectangle.y() + backgroundRectangle.height() - 35 * mobileScaler);
        //nextButton.hide();
      //}

      if(reachEnd) refreshButton.setClear();

      backgroundLayer.draw();
    }

    function hideResult(){
      nextButton.hide();
      resultLabel.hide();
      refreshButton.hide();
      descriptionButton.hide();
      backgroundLayer.draw();
    }

    function buildPuzzle(x, y, imageObj) {
        var piecesArray = new Array();
        var imageWidth = imageObj.width;
        var imageHeight = imageObj.height;

        pieceWidth = Math.floor((imageWidth / horizontalPieces) / 8) * 8;
        pieceHeight = Math.floor((imageHeight / verticalPieces) / 8) * 8;

        // >128 ???
        /*pieceWidth = 128;
        pieceHeight = 128;*/

        var puzzleSheme = colorSheme.puzzleRect;

        imageWidth = pieceWidth * horizontalPieces;
        imageHeight = pieceHeight * verticalPieces;

        var resultImage = new Kinetic.Image({
          x: x,
          y: y,
          width: imageWidth,
          height: imageHeight,
          image: imageObj,
          opacity: 0
        });

        resultImage.hide();

        resultImage.on(events[isMobile].click, function(){
          if(!isGameFinished) return;
          imageContainer.zoomImage(resultImage);
        })

        puzzleList[currentPuzzle].resultImage = resultImage;

        backgroundRectangle = new Kinetic.Rect({
          x: x,
          y: y,
          stroke: puzzleSheme.stroke,
          strokeWidth: puzzleSheme.strokeWidth,
          fill: puzzleSheme.color,
          width: imageWidth,
          height: imageHeight
        });

        puzzleGroup.add(backgroundRectangle);

         for(i=0;i<horizontalPieces;i++){
             piecesArray[i]=new Array();
              for(j=0;j<verticalPieces;j++){
                 piecesArray[i][j] = new Object();
                 piecesArray[i][j].right = Math.floor(Math.random()*2);
                 piecesArray[i][j].down = Math.floor(Math.random()*2);
                 if(j>0){
                    piecesArray[i][j].up=1-piecesArray[i][j-1].down;        
                }
                if(i>0){
                    piecesArray[i][j].left=1-piecesArray[i-1][j].right;        
                }
                piecesArray[i][j].shape = createPuzzleTile(x, y, i, j, piecesArray[i][j], imageObj);

                puzzleGroup.add(piecesArray[i][j].shape);

                piecesArray[i][j].shape.cache({
                  x: - 32,
                  y: - 32,
                  width: piecesArray[i][j].shape.width() + 64,
                  height: piecesArray[i][j].shape.height() + 64//,
                  //drawBorder: true
                });

                piecesArray[i][j].shape.initTween();
              }
          }

          contentLayer.add(resultImage);
          contentLayer.draw();
    }
    function setupPuzzle(image){

        var scaler = calculateAspectRatioFit(image.width, image.height, pictureMaxWidth, pictureMaxHeight);

        image.width = Math.floor(image.width * scaler);
        image.height = Math.floor(image.height * scaler);

        var positionX = 29;
        //var positionY = aspectRatio == 1 ? 90 : 80;

        positionY = titleGroup.height() + 5;

        if(image.width < Math.floor(pictureMaxWidth)) positionX = ($(window).width() / stage.scaler >> 1) - (image.width >> 1);
        if(image.height < Math.floor(pictureMaxHeight)) positionY = ((pictureMaxHeight - positionY) >> 1 ) - (image.height >> 1) + positionY;

        image.scaler = scaler;

        buildPuzzle(positionX, positionY, image);
     }

    function setHeader(title, question){
      titleGroup.setValue(title, question);
      titleGroup.show();
      backgroundLayer.draw();
    }

    function createPuzzleTile(x, y, i, j, tileObj, imageObj){
      var PuzzleTile = new Kinetic.Shape({
          x: x + pieceWidth * i - 64,
          y: y + pieceHeight * j - 64,
          drawFunc: function(i,j,pieceWidth,pieceHeight,tileObj){
              return function(context) {
                   var offsetX = 32;
                   var offsetY = 32;
                   var x8 = pieceWidth / 8;
                   var y8 = pieceHeight / 8;
                    context.beginPath();
                    context.moveTo(offsetX, offsetY);
                    if(j!=0){
                        context.lineTo(offsetX+3*x8,offsetY);
                        if(tileObj.up==1){
                            context.quadraticCurveTo(offsetX+2*x8,offsetY-2*y8,offsetX+4*x8,offsetY-2*y8);
                            context.quadraticCurveTo(offsetX+6*x8,offsetY-2*y8,offsetX+5*x8,offsetY);    
                        }
                        else{
                            context.quadraticCurveTo(offsetX+2*x8,offsetY+2*y8,offsetX+4*x8,offsetY+2*y8);
                            context.quadraticCurveTo(offsetX+6*x8,offsetY+2*y8,offsetX+5*x8,offsetY);
                      }
                  }

                    context.lineTo(offsetX+8*x8,offsetY);

                    if(i!=horizontalPieces-1){
                        context.lineTo(offsetX+8*x8,offsetY+3*y8);
                        if(tileObj.right==1){
                            context.quadraticCurveTo(offsetX+10*x8,offsetY+2*y8,offsetX+10*x8,offsetY+4*y8);
                            context.quadraticCurveTo(offsetX+10*x8,offsetY+6*y8,offsetX+8*x8,offsetY+5*y8);
                        }
                        else{
                          context.quadraticCurveTo(offsetX+6*x8,offsetY+2*y8,offsetX+6*x8,offsetY+4*y8);
                            context.quadraticCurveTo(offsetX+6*x8,offsetY+6*y8,offsetX+8*x8,offsetY+5*y8);
                      }
                  }
                  context.lineTo(offsetX+8*x8,offsetY+8*y8);
                    if(j!=verticalPieces-1){
                      context.lineTo(offsetX+5*x8,offsetY+8*y8);
                      if(tileObj.down==1){
                            context.quadraticCurveTo(offsetX+6*x8,offsetY+10*y8,offsetX+4*x8,offsetY+10*y8);
                            context.quadraticCurveTo(offsetX+2*x8,offsetY+10*y8,offsetX+3*x8,offsetY+8*y8);
                        }
                        else{
                          context.quadraticCurveTo(offsetX+6*x8,offsetY+6*y8,offsetX+4*x8,offsetY+6*y8);
                            context.quadraticCurveTo(offsetX+2*x8,offsetY+6*y8,offsetX+3*x8,offsetY+8*y8);
                      }
                  }
                    context.lineTo(offsetX,offsetY+8*y8);
                    if(i!=0){
                      context.lineTo(offsetX,offsetY+5*y8);
                      if(tileObj.left==1){
                            context.quadraticCurveTo(offsetX-2*x8,offsetY+6*y8,offsetX-2*x8,offsetY+4*y8);
                            context.quadraticCurveTo(offsetX-2*x8,offsetY+2*y8,offsetX,offsetY+3*y8);
                        }
                        else{
                          context.quadraticCurveTo(offsetX+2*x8,offsetY+6*y8,offsetX+2*x8,offsetY+4*y8);
                            context.quadraticCurveTo(offsetX+2*x8,offsetY+2*y8,offsetX,offsetY+3*y8);
                      }
                  }
                  context.lineTo(offsetX,offsetY);

                  //context._context.shadowColor = '#00ff00';
                  //context._context.shadowBlur = this._blur;
                  //context._context.shadowOffsetX = 0;
                  //context._context.shadowOffsetY = 0;

                  context.fillStrokeShape(this);
                  }
          }(i,j,pieceWidth,pieceHeight,tileObj),
              width: pieceWidth + 64,
              height: pieceHeight + 64,
              fillPatternImage: imageObj,
              fillPatternRepeat: 'no-repeat',
              fillPatternX: 32,
              fillPatternY: 32,              
              fillPatternScale: {x: imageObj.scaler, y: imageObj.scaler},
              fillPatternOffsetX: (pieceWidth * i) / imageObj.scaler,
              fillPatternOffsetX: (pieceWidth * i) / imageObj.scaler,
              fillPatternOffsetY: (pieceHeight * j) / imageObj.scaler,
              stroke: puzzleStrokeColor,
              strokeWidth: puzzleBorderWidth,
              lineCap: "round"
        });

        PuzzleTile.on(events[isMobile].mouseover, hoverPointer);
        PuzzleTile.on(events[isMobile].mouseout, resetPointer);

        PuzzleTile.isDraggable = false;

        PuzzleTile._blur = 0;

        PuzzleTile.defaultX = PuzzleTile.x();
        PuzzleTile.defaultY = PuzzleTile.y();

        PuzzleTile.initTween = function(){
          var tween = new Kinetic.Tween({
            node: PuzzleTile, 
            duration: 1,
            x: x + Math.random() * (pieceWidth * (horizontalPieces - 1) - 64),
            y: y + Math.random() * (pieceHeight * (verticalPieces - 1) - 64),
            easing: Kinetic.Easings.Linear
          });
          tween.onFinish = function(){
            PuzzleTile.isDraggable = true;
          };
          //console.log(imageObj.width - PuzzleTile.getTilePosition().x);

          setTimeout(function(){
            tween.play();
          }, 2000);
        }


        PuzzleTile.setCorrect = function(){
          PuzzleTile.x(PuzzleTile.defaultX);
          PuzzleTile.y(PuzzleTile.defaultY);

          PuzzleTile.isDraggable = false;

          PuzzleTile._blur = 20;

          /*PuzzleTile.filters([Kinetic.Filters.Brighten]);

          var tweenIn = new Kinetic.Tween({
            node: PuzzleTile, 
            duration: 0.6,
            filterBrightness: 10,
            easing: Kinetic.Easings.EaseInOut
          });

          var tweenOut = new Kinetic.Tween({
            node: PuzzleTile,
            duration: 0.6,
            blurRadius: 0,
            easing: Kinetic.Easings.EaseInOut
          });

          tweenIn.onFinish = function(){
            tweenOut.play();
          };

          //tweenIn.play();

          PuzzleTile.filterBrightness(100);
          contentLayer.bathDraw();*/

          contentLayer.draw();

          var velocity = 0.5;
          var speed = 10;

          /*var tweenInterval = setInterval(function(){
            if(PuzzleTile._blur > 0){
              PuzzleTile._blur -= speed;
              speed += velocity;

            } else{
              clearInterval(tweenInterval);
            }

            contentLayer.draw();
            dragLayer.draw();
          }, 200);*/

          PuzzleTile.moveToBottom();

          PuzzleTile.getParent().draw();

          correctCount++;

          checkForWin();
        }

        PuzzleTile.on(events[isMobile].down, function(){
          if(PuzzleTile.isDraggable){
            PuzzleTile.moveToTop();
            //PuzzleTile.moveTo(dragLayer);
            //contentLayer.draw();
            PuzzleTile.startDrag();            
          }
          //console.log(PuzzleTile.getTilePosition().x);
        });

        PuzzleTile.on('dragend', function(){
          if(PuzzleTile.x() > PuzzleTile.defaultX - 20 && PuzzleTile.x() < PuzzleTile.defaultX + 20 &&
            PuzzleTile.y() > PuzzleTile.defaultY - 20 && PuzzleTile.y() < PuzzleTile.defaultY + 20){
              PuzzleTile.setCorrect();
          }
          //this.moveTo(puzzleGroup);          
          //dragLayer.draw();
          //puzzleGroup.draw();
        })

        return PuzzleTile;
    }

    function showNextList(){
      if(currentPuzzle != listCount - 1){
        if(currentPuzzle >= 0){
          puzzleGroup.removeChildren();
          puzzleGroup.opacity(1);
          puzzleList[currentPuzzle].resultImage.destroy();

          correctCount = 0;

          hideResult();

          contentLayer.draw();
        }

        currentPuzzle++;

        setHeader(puzzleList[currentPuzzle].title, puzzleList[currentPuzzle].question);

        pictureMaxHeight = ($(window).height() / stage.scaler) - titleGroup.height() - 5;

        //descriptionContainer.setValue(puzzleList[currentPuzzle].description);
        //descriptionLayer.draw();

        setupPuzzle(puzzleList[currentPuzzle]);
      } else{
        //console.log('App reach end');
      }
    }

    function setupPuzzleList(imageCollection){

      listCount = imageCollection.length;

      var imageLoadCounter = listCount;

      imageCollection.each(function(index){
        var imageObj = new Image();

        imageObj.src = levelManager.getRoute($(this).attr('src'));

        imageObj.index = index;
        imageObj.title = $(this).attr('title');
        imageObj.question = $(this).attr('question');
        imageObj.result = levelManager.getRoute($(this).attr('result'));
        imageObj.taskSource = $(this).attr('taskSource');
        imageObj.description = $(this).text();
        imageObj.label = $(this).attr('label');

        if(typeof imageObj.result == 'undefined') imageObj.result = '';
        if(typeof imageObj.taskSource == 'undefined') imageObj.taskSource = '';
        if(typeof imageObj.label == 'undefined') imageObj.label = '';

        imageObj.onload = function(){

          //imageObj.width = this.width;
          //imageObj.height = this.height

          puzzleList[imageObj.index] = this;

          imageLoadCounter--;

          if(imageLoadCounter == 0) initApplication();
        }
      })
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            }

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
                backgroundLayer.draw();
            }

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        }

        imgObj.src = 'assets/preLoader.png'

        return preloaderObject;
    }

    /*Вызывается по окончанию загрузки данных. Инициирует данными глобальные переменные а также элементы интерфейса**/
    function xmlLoadHandler(xml){

      var root = $(xml).children('component');

      levelManager.initRoute(root);

      var imageCollection = $(root).children('image');

      isFullScreen = ($(root).attr('isFullScreen') == 'yes');
      verticalPieces = +$(root).attr('verticalTileCount');
      horizontalPieces = +$(root).attr('horizontalTileCount');

      tileCount = horizontalPieces * verticalPieces;

      puzzleStrokeColor = $(root).attr('borderColor');
      puzzleBorderWidth = +$(root).attr('borderWidth');

      initAspectRatio();

      if(isFullScreen) initWindowSize();

      pictureMaxWidth = $(window).width() / stage.scaler - 58;
      //pictureMaxHeight = $(window).height() / stage.scaler - titleGroup.height() - 5;

      /*pieceWidth = Math.floor((imageWidth / horizontalPieces) / 8) * 8;
      pieceHeight = Math.floor((imageHeight / verticalPieces) / 8) * 8;*/

      //pictureMaxWidth = Math.floor(($(window).width() / stage.scaler - 58) / 80) * 80;
      //pictureMaxHeight = Math.floor(($(window).height() / stage.scaler - 118) / 80) * 80;

      //setupPuzzle(imageCollection.eq(1).attr('src'));

      setupPuzzleList(imageCollection);
	}

  function initApplication(){
      preLoader.hidePreloader();
      
      initHeader();

      /*initDescriptionContainer(
        (($(window).width() / stage.scaler) >> 1) - 300,
        titleGroup.height() + 5,
        600,
        ($(window).height() / stage.scaler) - titleGroup.height() - 15);*/

      showNextList();

      resultLabel = initResultLabel(300, aspectRatio == 1 ? 690 : 510);

      //nextButton = createButton(600, aspectRatio == 1 ? 690 : 510, 120, 30, 'Дальше', showNextList);
      refreshButton = createButton(600, aspectRatio == 1 ? 690 : 510, 120 * mobileScaler, 30 * mobileScaler, 'Далее', showNextList);
      //nextButton = createRoundButton(600, aspectRatio == 1 ? 690 : 510, 25, showNextList, '→', 'bold', 46, -20, -28);
      nextButton = createRoundButton(600, aspectRatio == 1 ? 690 : 510, 25, toTaskMode, '→', 'bold', 46, -20, -28);
      descriptionButton = createRoundButton(600, aspectRatio == 1 ? 690 : 510, 25, showToolTip, 'i', 'italic', 64, -9, -32);

      backgroundLayer.add(refreshButton);

      //backgroundLayer.draw();

      refreshButton.hide();

      initImageContainer();
  }

  function toTaskMode(){
    window.location.href = puzzleList[currentPuzzle].taskSource;
  }

  function initDescriptionContainer(x, y, width, height){
    descriptionContainer = new Kinetic.Group({
      x: x,
      y: y,
      width: width,
      height: height
    });

    var descriptionSheme = colorSheme.descriptionContainer;

    descriptionContainer.nextButton = new Kinetic.Group({
      x: x + (width >> 1) - 70 * mobileScaler,
      y: y + height - 45 * mobileScaler,
      width: 140 * mobileScaler,
      height: 37 * mobileScaler,
    })

    var btnRect = new Kinetic.Rect({
      x: 0,
      y: 0,
      width: descriptionContainer.nextButton.width(),
      height: descriptionContainer.nextButton.height(),
      fill: colorSheme.button.gradientBackgroundColorStart,
      stroke: colorSheme.button.stroke,
      strokeWidth: colorSheme.button.strokeWidth
    });

    var btnLabel = new Kinetic.Text({
      x: 0,
      y: (descriptionContainer.nextButton.height() >> 1) - 10 * mobileScaler,
      width: descriptionContainer.nextButton.width(),
      fontSize: 20 * mobileScaler,
      fill: colorSheme.button.labelColor,
      fontFamily: mainFont,
      text: 'Играть',
      align: 'center'
    });

    descriptionContainer.nextButton.add(btnRect);
    descriptionContainer.nextButton.add(btnLabel);

    descriptionContainer.nextButton.on(events[isMobile].click, function(){
      descriptionContainer.hideContainer(function(){
        setTimeout(function(){
          setupPuzzle(puzzleList[currentPuzzle]);
        }, 100);
      });
    });

    descriptionLayer.setClip({
      x: x,
      y: y,
      width: width,
      height: height - descriptionContainer.nextButton.height() - 15 * mobileScaler
    })

    var padding = 15;
    var scrollMargin = 35 * mobileScaler;

    var backgroundRect = new Kinetic.Rect({
      x: x,
      y: y,
      width: descriptionContainer.width(),
      height: descriptionContainer.height(),
      fill: descriptionSheme.color,
      stroke: descriptionSheme.stroke,
      strokeWidth: descriptionSheme.strokeWidth
    });

    var descriptionHolder = new Kinetic.Group({
      x: padding,
      y: 0
    });

    var descriptionLabel = new Kinetic.Text({
       x: 0,
       y: 0,
       width: descriptionContainer.width() - (padding << 1) - scrollMargin - 5,
       fill: 'black',
       fontFamily: mainFont,
       text: 'Temp text',
       fontSize: 20 * mobileScaler
    });

    descriptionHolder.add(descriptionLabel);

    descriptionHolder.height(0);

    //x, y, width, height, contentGroup, clippedLayer

    descriptionContainer.scroll = createScrollBar(
      descriptionContainer.x() + descriptionContainer.width() - scrollMargin,
      descriptionContainer.y() + 10,
      scrollMargin - 10,
      descriptionContainer.height() - 20,
      descriptionHolder,
      descriptionLayer
    )

    descriptionContainer.scroll.refresh();

    descriptionContainer.setValue = function(value){
      descriptionLabel.setText(value);
      descriptionHolder.height(descriptionLabel.height() + descriptionLabel.fontSize() * 0.1);
      descriptionContainer.scroll.refresh();
      descriptionContainer.show();
      backgroundRect.show();
      descriptionContainer.nextButton.show();
    }

    descriptionContainer.hideContainer = function(callback){
      descriptionHolder.height(0);
      descriptionContainer.scroll.refresh();
      descriptionContainer.hide();
      backgroundRect.hide();
      descriptionContainer.nextButton.hide();
      descriptionLayer.draw();
      backgroundLayer.draw();

      callback();
    }
    descriptionContainer.add(descriptionHolder);
    descriptionLayer.add(descriptionContainer);

    backgroundLayer.add(backgroundRect);
    backgroundLayer.add(descriptionContainer.nextButton);

    backgroundLayer.moveToBottom();
    descriptionLayer.moveToTop();
  }

  function showToolTip(){
      $('#helpLoader').load(puzzleList[currentPuzzle].result);
      $('#helpLoader').hide();

      $('#backBtn').click(function(){
          hideToolTip();
      });

      $('body').css('overflow', 'visible');
      $('html').css('overflow', 'visible');

      $('#stageContainer').hide();
      $('#domContainer').show();
      $('#helpLoader').show();
  }

  function hideToolTip(){
    $('body').css('overflow', 'hidden');
    $('html').css('overflow', 'hidden');

    $('#stageContainer').show();
    $('#domContainer').hide();
    $('#helpLoader').hide();
  }

  function _initHeader(){
      var padding = 20;

      titleGroup = new Kinetic.Group({
        x: contentMargin.x,
        y: contentMargin.y
      })

      var mainTitle = new Kinetic.Text({
          x: padding,
          y: padding * 0.65,
          text: '',
          fontFamily: mainFont,
          fontSize: 24 * mobileScaler,
          width: $(window).width() / stage.scaler,
          fill: 'black',
          fontStyle: 'bold'
      });

      var mainTitleRect = new Kinetic.Rect({
          x: - contentMargin.x - 2,
          y: - contentMargin.y - 2,
          fill: '#E7E7E7',
          width: mainTitle.width() + 4,
          height: mainTitle.height() + mainTitle.fontSize() * 0.2,
          stroke: '#BCB9B3'
      });

      titleGroup.add(mainTitleRect);
      titleGroup.add(mainTitle);

      var questionLabel = new Kinetic.Text({
          x: mainTitle.x(),
          y: mainTitle.y() + mainTitle.height() + 10,
          text: '',
          fontFamily: mainFont,
          fontSize: 16 * mobileScaler,
          fill: 'black',
          width: mainTitle.width()
      });

      mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

      titleGroup.add(questionLabel);

      titleGroup.height(mainTitleRect.height());

      titleGroup.setValue = function(header, question){
        mainTitle.text(header);
        questionLabel.text(question);
      }

      backgroundLayer.add(titleGroup);

      titleGroup.hide();

      backgroundLayer.draw();
    }

    function initHeader(){

        title = '';
        question = '';

        var padding = 10;

        var headSheme = colorSheme.header;

        titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: 24 * mobileScaler,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        //if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: 16 * mobileScaler,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: mainTitle.height() + mainTitle.fontSize() * 0.2 + padding,
                stroke: headSheme.questionTitle.stroke
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        //}

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        titleGroup.setValue = function(header, question){
          mainTitle.text(header);
          questionLabel.text(question);
        }

        backgroundLayer.add(titleGroup);

        titleGroup.hide();

        backgroundLayer.draw();

        return titleGroup;
    }

  function createButton(x, y, width, height, textLabel, callback, sheme){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 20 * mobileScaler,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.showHere = function(x, y){
          buttonGroup.x(x);
          buttonGroup.y(y);
          
          buttonGroup.show();
        }

        buttonGroup.setClear = function(){
          label.text('Ещё раз');
          buttonGroup.off(events[isMobile].click, callback);
          buttonGroup.on(events[isMobile].click, clear)
        }

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        //backgroundLayer.add(buttonGroup);
        //backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mouseout, resetPointer);

        return buttonGroup;
    }

    function createRoundButton(x, y, radius, callback, textIcon, fontStyle, fontSize, dx, dy){

      var buttonGroup = new Kinetic.Group({ x:x, y:y });
      var buttonRectangle = new Kinetic.Circle({ //прямоугольная область
          x: 0,
          y: 0,
          radius: radius,
          fillLinearGradientStartPoint: {x: radius >> 1, y: 0},
          fillLinearGradientEndPoint: {x: radius >> 1, y: radius},
          fillLinearGradientColorStops: [0, '#FFC23F', 1, '#FF9200']
        });

      var label = new Kinetic.Text({ //текстовая метка
          x: dx,
          y: dy,
          text: textIcon,
          fontSize: fontSize,
          fontFamily: mainFont,
          fill: 'black',
          fontStyle: fontStyle
        });

      buttonGroup.radius = radius;

      buttonGroup.on(events[isMobile].mouseover, hoverPointer);
      buttonGroup.on(events[isMobile].mouseout, resetPointer);

      /*Определяет новую текстовую метку кнопки
      @param {string} value новое значение метки**/
      buttonGroup.setText = function(value){
        label.text(value);
        backgroundLayer.draw();
      };

      buttonGroup.setClear = function(){
        buttonGroup.off(events[isMobile].click);
        buttonGroup.setText('Ещё раз');
        refreshButton.show();
        nextButton.hide();

        backgroundLayer.draw();

        buttonGroup.on(events[isMobile].click, clear);
      }

      buttonGroup.showHere = function(x, y){
        buttonGroup.x(x);
        buttonGroup.y(y);
        
        buttonGroup.show();
      }

      buttonGroup.add(buttonRectangle);
      buttonGroup.add(label);

      backgroundLayer.add(buttonGroup);

      buttonGroup.hide();

      backgroundLayer.draw();
      
      buttonGroup.on(events[isMobile].click, callback);
      /*label.on("click", function(evt){ //костыль вместо полноценного потока событий
        evt.targetNode = buttonRectangle;
        buttonRectangle.fire('click', evt);
      });*/
    return buttonGroup;
    }

    function initResultLabel(x, y){
      var resultGroup = new Kinetic.Group({
        x: x,
        y: y
      });

      var padding = 10;

      var label = new Kinetic.Text({ //текстовая метка
          x: padding,
          y: padding,
          text: 'Молодец! Всё верно!',
          fontSize: 24 * mobileScaler,
          fontFamily: mainFont,
          fill: 'white'
      });

      var labelRect = new Kinetic.Rect({
        x: 0,
        y: 0,
        width: 100,
        height: 50,
        fill: 'black',
        opacity: 0.7
      })

      resultGroup.showHere = function(x, y){

        imageContainer.moveToTop();

        if(puzzleList[currentPuzzle].label == ''){
          resultGroup.hide();
          return;
        }

        resultGroup.x(x);
        resultGroup.y(y);

        label.text(puzzleList[currentPuzzle].label);
        label.width(backgroundRectangle.width() - (padding << 1));

        labelRect.width(backgroundRectangle.width());
        labelRect.height(label.height() + label.fontSize() * 0.1 + (padding << 1));

        if(puzzleList[currentPuzzle] != '') resultGroup.show();
      }

      resultGroup.add(labelRect);
      resultGroup.add(label);

      resultGroup.hide();

      backgroundLayer.add(resultGroup);
      backgroundLayer.draw();

      return resultGroup;
    }

function initImageContainer(){
      var image = new Kinetic.Image({
      x: 20,
      y: 20
    });

    var label = new Kinetic.Text({
      x: 20,
        y: 20
    });

    var rect = new Kinetic.Rect({
      x: 20,
        y: 20
    });

    var modalRect = new Kinetic.Rect({
      x: 0,
      y: 0,
      fill: 'black',
      width: $(window).width() / stage.scaler,
      height: $(window).height() / stage.scaler
    });

    var modalGroup = new Kinetic.Group();

    modalRect.opacity(0.8);

    image.on(events[isMobile].mouseover, hoverPointer);
    image.on(events[isMobile].mouseout, resetPointer);

    image.hide();
    modalRect.hide();
    rect.hide();
    label.hide();

    modalGroup.add(modalRect);
    modalGroup.add(image);
    modalGroup.add(rect);
    modalGroup.add(label);

    imageContainer.add(modalGroup);

    imageContainer.zoomImage = function(linkedImage){
      var imageObj = linkedImage.getImage();
      //var objGroup = linkedImage.getParent().getParent();

      image.setImage(imageObj);

      var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight - 40 - screenMarginY);

      image.width(imageObj.width*scaler);
      image.height(imageObj.height*scaler);

      image.x(((defaultStageWidth-40)>>1)-(image.width()>>1))

      modalGroup.on(events[isMobile].click, zoomOut);

      if(!image.isVisible()){
        image.x(defaultStageWidth >> 1);
          image.y(defaultStageHeight >> 1);

          image.scale({
            x: 0,
            y: 0
          })

          var inTween = new Kinetic.Tween({
            node: image,
            scaleX: 1,
            scaleY: 1,
            x: ($(window).width() / stage.scaler >> 1) - (image.width() >> 1),
                    y: ($(window).height() / stage.scaler >> 1) - (image.height() >> 1),
          duration: 0.4,
          easing: Kinetic.Easings.EaseInOut
          });

          inTween.onFinish = function(){
            //modalRect.show();

            /*if( typeof objGroup.label != 'undefined' ){
            //label.setAttrs(objGroup.label.getAttrs());
            rect.setAttrs(objGroup.labelRect.getAttrs());

                        label.fontSize(objGroup.label.fontSize());
                        label.fontFamily(objGroup.label.fontFamily());
                        label.fill(objGroup.label.fill());
                        label.text(objGroup.label.text());

                        label.width(image.width()*image.scaleX() - 10);

            label.x(image.x() + 10)
            label.y((image.y() + image.height() * image.scaleX()) - (label.height() + label.fontSize() * 0.1));

            rect.x(image.x());
            rect.y(label.y());

            rect.width(label.width() + 10);
                        rect.height(label.height() + 2);

            rect.show();
            label.show();

            imageContainer.draw();
          }*/
          };

                modalRect.show();
        
        image.show();
          inTween.play();
      }

      imageContainer.draw();
    }

    function zoomOut(evt){
          
      modalRect.hide();

        var outTween = new Kinetic.Tween({
          node: image,
          scaleX: 0,
          scaleY: 0,
          x: defaultStageWidth>>1,
          y: defaultStageHeight>>1,
        duration: 0.4,
        easing: Kinetic.Easings.EaseInOut
        });

      rect.hide();
      label.hide();

        outTween.onFinish = function(){
          image.hide();
        }
        outTween.play();
      }
  }

	function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

	    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

	    return ratio;
	 }

    function clear(evt){
      window.location.href = window.location.href;
    }

    /*Перемешивает элементы массива o
    * @param {array} o исходный массив
    * @return {array} o массив с перемешанными элементами
    */
	function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

  function hoverPointer(){
      $('body').css('cursor', 'pointer');
  }
  function resetPointer(){
      $('body').css('cursor', 'default');
  }

  function createScrollBar(x, y, width, height, contentGroup, clippedLayer){
        var scrollObject = new Kinetic.Group();
        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Kinetic.Rect({
           x: x,
           y: y,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11 + y;

        var vscroll = new Kinetic.Group({
            x: x + 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart*stage.scaleY()){
                    newY = scrollStart*stage.scaleY();
                }
                else if(newY > scrollEnd*stage.scaleY()) {
                    newY = scrollEnd*stage.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                }
            }
          });

            if(isMobile){

                contentGroup._scrollBarDefaultY = contentGroup.y();

                //contentGroup.draggable(true);

                contentGroup.dragBoundFunc(function(pos){
                    var newY = pos.y;
                    //console.log(newY);
                    //console.log(contentGroup.height());

                    if(newY >= scrollStart){
                        newY = scrollStart + 20;
                    }
                    else if(newY < (- contentGroup.height() + contentGroup._scrollBarDefaultY)) {
                        newY = (- contentGroup.height() + contentGroup._scrollBarDefaultY);
                    }
                    return {
                        x: this.getAbsolutePosition().x,
                        y: newY
                    }
                });

                contentGroup.on("dragmove", function(){
                    var clip = clippedLayer.getClip();

                    /*var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
                    var dy = speed * (vscroll.getPosition().y - scrollStart);
                    contentGroup.y(dy + scrollObject.dy);*/

                    var percents =  1 / Math.floor(Math.abs(contentGroup.height() / (contentGroup.y() - contentGroup._scrollBarDefaultY)));

                    if(percents < 1){
                        vscroll.y(scrollStart + percents * clip.height);
                    } else{
                        vscroll.y(scrollEnd);
                    } 
                    console.log(percents, contentGroup.height(), contentGroup.y(), contentGroup._scrollBarDefaultY);
                    contentScrollLayer.batchDraw();
                    //vscroll.y(scrollStart + dy )
                });
            }

          var scrollCircle = new Kinetic.Group({
            x: (width >> 1) - 9.5,
            y: 0
          });

          var backgroundCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: width >> 1,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: backgroundCircle.radius() * 0.6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          }

          var scrollTopArrowGroup = new Kinetic.Group({
            x: x,
            y: y
          })

          var scrollTopArrow = new Kinetic.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Kinetic.Group({
            x: x,
            y: y + height
          });

          var scrollBotArrow = new Kinetic.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Kinetic.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          contentScrollLayer.add(scrollObject);

          contentScrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            contentScrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            contentScrollLayer.drawScene();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollToContentOn = function(dy){
            if(!vscroll.isVisible()) return;
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!

            var scrollPosition = scrollStart - (dy / speed);

            if(scrollPosition > scrollEnd) scrollPosition = scrollEnd;

            vscroll.y(scrollPosition);

            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
            scrollTopArrowGroup.hide();
            scrollBotArrowGroup.hide();
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
            scrollTopArrowGroup.show();
            scrollBotArrowGroup.show();
          }

          scrollObject.refresh = function(){
            var clip = clippedLayer.getClip();

            //vscrollArea.y(clip.y + 5);
            //vscrollArea.height(clip.height - 10);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            scrollTopArrowGroup.y(clip.y + 5);
            scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            //contentGroup.y(0);
            vscroll.y(scrollStart);

            console.log(clippedLayer.getClip().height, contentGroup.height())

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
            } else{
                scrollObject.show();
            }

            contentScrollLayer.draw();
          }

          clippedLayer.batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.batchDraw()
          }
          return scrollObject;
    }
});