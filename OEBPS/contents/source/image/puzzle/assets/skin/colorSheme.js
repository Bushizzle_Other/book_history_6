var colorSheme = {
	"scrollBar":{
		"trackColor": "#87D0E7",
		"arrowColor": "#87D0E7",
		"innerSliderColor": "#87D0E7",
		"outerSliderColor": "#87D0E7"
	},
	"bottomPanel":{
		"color": "#00A8D4",
		"stroke":"#00A8D4"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#00A8D4",
		"strokeWidth": 1.5,
		"cornerRadius": 0
	},
	"puzzleRect":{
		"fill": "white",		
		"stroke": "#7fd3e9",
		"strokeWidth": 1.5
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C",
		"iconSource": "resultMark.png"
	},
	"object":{
		"color": "white",
		"stroke": "#c8c4bd",
		"strokeWidth": 0.5,
		"cornerRadius": 0
	},
	"player":{
		"backgroundRectColor": "black",
		"trackBackroundColor": "#969696",
		"trackColor": ["#46A6D1", "#46A6D1"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"descriptionContainer":{
		"color": "white",
		"stroke": "#7fd3e9",
		"strokeWidth": 1.5
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#7FD3E9",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#7FD3E9"
		}
	}
}