$(window).load(function () {

	$(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

    function reorient(e) {
      window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

	var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';

	/*Глобальные переменные**/

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var aspectRatio = 0; //widescreen 16:9

	var defaultStageWidth = 1024;
	var defaultStageHeight = 576;

	var maxSize = 0;

	var contentMargin = {x: 5, y: 5};

	//Отступ всего контента от сцены
	var marginLeft = 60;
    var marginTop = 60;

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 20;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var contentContainer = null;

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

	var isHorizontal;

	var pictureData = [];

	var root = '';

	var mainTitle = null;

	var resultScreen = null;

	var levelManager = new LevelManager();

	var tipCollection = [];

	var sizeLabel = null;

	var mainFont = 'mainFont';

	var colorSheme = null;

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
	var pictureLayer = new Kinetic.Layer();
	var tipLayer = new Kinetic.Layer();
	var dragLayer = new Kinetic.Layer();
	var imageContainer = new Kinetic.Layer();
	var resultLayer = new Kinetic.Layer();

	stage.add(backgroundLayer);
	stage.add(contentLayer);
	stage.add(pictureLayer);
	stage.add(tipLayer);
	stage.add(dragLayer);
	stage.add(imageContainer);
	stage.add(resultLayer);

	/*Группы**/
	var tableGroup = new Kinetic.Group();
	var answerGroup = new Kinetic.Group();
	var dropContainerGroup = new Kinetic.Group();

	contentLayer.add(tableGroup);
	contentLayer.add(answerGroup);
	//contentLayer.add(dropContainerGroup);

	var preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));

	$(window).on('resize', fitStage);

	function fitStage(){
		scaleScreen($(window).width(),  $(window).height());
	}

	function initAspectRatio(){
		if($(window).width() / $(window).height() < 1.6){
			aspectRatio = 1;
			defaultStageWidth = 1024;
			defaultStageHeight = 768;

			stage.width(defaultStageWidth);
			stage.width(defaultStageHeight);
		}
	}

	function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        stage.draw();
    }

	function initWindowSize(){		
		scaleScreen($(window).width(), $(window).height());
	}

	function isColliding(firstRectangle, secondRectangle){
		var result = false;
	 
		if(!(firstRectangle.bottom < secondRectangle.top ||
			firstRectangle.top > secondRectangle.bottom ||
			firstRectangle.left > secondRectangle.right ||
			firstRectangle.right < secondRectangle.left))

				result = true;

		return result;
	 }

	function createAnswerBlock(x, y, width, height, label){
		var answerBlock = new Kinetic.Group({x:x, y:y, draggable:true, dragOnTop: false});

		answerBlock.defaultX = x;
		answerBlock.defaultY = y;

		answerBlock.getValue = function(){
			return label.text();
		}

		answerBlock.resetPosition = function(){
			answerBlock.x(answerBlock.defaultX);
			answerBlock.y(answerBlock.defaultY);

			contentLayer.draw();
		}

		x = y = 0;

		var rect = new Kinetic.Rect({
			x: x,
			y: y,
			width: width,
			height: height,
			stroke: 'black',
			strokeWidth: 0.5,
			fillLinearGradientStartPoint: {x:width/2, y:0},
	        fillLinearGradientEndPoint: {x:width/2,y:height},
	        fillLinearGradientColorStops: [0, '#E9FFB1', 1, '#7DAD55']
		});
		
		label.x(x);
		label.y(y);

		answerBlock.on('dragstart', function(evt) {
			answerBlock.moveToTop();
		});
		answerBlock.on('dragend', function(evt) {
			var node = evt.dragEndNode;
			var firstRectangle = { left:node.x(), right:node.x()+maxItemWidth, top:node.y(), bottom:node.y()+maxItemHeight } //danger height!!!!11
			var secondRectangle;
			var tableBoxArray = tableGroup.getChildren();

			for(var i = 0; i < tableBoxArray.length; i++){
				if(tableBoxArray[i].isLocked) continue;
				secondRectangle = { left:tableBoxArray[i].x(), right:tableBoxArray[i].x()+maxItemWidth, top:tableBoxArray[i].y(), bottom:tableBoxArray[i].y()+maxItemHeight }

				if(isColliding(firstRectangle, secondRectangle)){

					tableBoxArray[i].setValue(node);

					if(answerMode=='copy'){
						answerBlock.resetPosition();
					} else{
						answerBlock.hide();
					}
					
					contentLayer.draw();

					return;
				}
			}
			answerBlock.resetPosition();
		});

	    answerBlock.add(rect);
	    answerBlock.add(label);
	    answerGroup.add(answerBlock);

	    contentLayer.draw();
	}

	function createTableBlock(x, y, width, height, type, value, answer){
		var tableBlock = new Kinetic.Group({
			x: x,
			y: y
		});

		var fontStyle = 'italic';

		if(value=='active'){
			tableBlock.answer = answer;
			tableBlock.isLocked = false;
			value = '';
		} else{
			fontStyle = 'bold'
			tableBlock.isLocked = true;
		}

		x = y = 0;

		var radius = 15;
		var drawFunction = center;

		switch(type){
			case "topLeft": drawFunction = topLeft;
				break;
			case "botLeft": drawFunction = botLeft;
				break;
			case "botRight": drawFunction = botRight;
				break;
			case "topRight": drawFunction = topRight;
				break;
		}

		var rect = new Kinetic.Shape({
		    drawFunc: drawFunction,
		    fill: 'white',
		    stroke: 'black',
		    strokeWidth: 1,
		});

		var blockLabel = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: value,
	        fontSize: xmlFontSize,
	        fontFamily: mainFont,
	        fill: 'black',
	        align: 'center',
	        fontStyle: fontStyle,
	        width: width
	    });

	    tableBlock.check = function(){
	    	return (answer.indexOf(tableBlock.value)!=-1)
	    }

		tableBlock.setValue = function(answerBlock){
			if(answerMode != 'copy'){

				if('answerBlock' in tableBlock){
					tableBlock.answerBlock.show();
					tableBlock.answerBlock.resetPosition();
				}

			tableBlock.answerBlock = answerBlock;

			}

			blockLabel.text(answerBlock.getValue());
			tableBlock.name('answered');
			tableBlock.value = answerBlock.getValue();

			contentLayer.draw();
		}

		tableBlock.setWrong = function(){
			blockLabel.fill('red');
			contentLayer.draw();
		}

		tableBlock.add(rect);
		tableBlock.add(blockLabel);
		tableGroup.add(tableBlock);
		contentLayer.draw();

		function topRight(context) {
			context.beginPath();
			context.moveTo(x, y);
			context.lineTo(x + width - radius, y);
			context.quadraticCurveTo(x + width, y, x + width, y + radius, radius);
			context.lineTo(x + width, y + height);
			context.lineTo(x, y + height);
			context.lineTo(x, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function topLeft(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x + radius, y);
			context.quadraticCurveTo(x, y, x, y + radius, radius);
			context.lineTo(x, y + height);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
		function botLeft(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);			
			context.lineTo(x, y + height-radius);
			context.quadraticCurveTo(x, y + height, x+radius, y + height, radius);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function botRight(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);			
			context.lineTo(x, y + height);
			context.lineTo(x+width-radius, y + height);			
			context.quadraticCurveTo(x + width, y + height, x + width, y + height - radius, radius);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function center(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);			
			context.lineTo(x, y + height);
			context.lineTo(x + width, y + height);
			context.lineTo(x + width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }

	}

    skinManager = new SkinManager('assets/skin', function(){
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    });

    function xmlLoadHandler(xml){

    	initAspectRatio();

    	initWindowSize();

    	resultScreen = initResultScreen();

    	//preLoader.showPreloader();

    	root = $(xml).children('component');

    	levelManager.initRoute(root);

    	preloadPictures(root.children('cells').children('cell'));
    }

    function initApplication(){
    	fontSize = $(root).attr('text_size');

    	xmlFontSize = (typeof fontSize !== 'undefined' && fontSize !== false) ? fontSize : 22;

    	var title = root.children('title').text();
    	var question = root.children('question').text();

    	/*$('#mainTitle').html(mainTitle);
    	$('#question').html(question);*/

    	mainTitle = createHead(title, question);

    	backgroundLayer.add(mainTitle);

    	backgroundLayer.draw();

    	var cells = root.children('cells').children('cell');

    	buildCells(5, mainTitle.height() + 5, cells, $(window).width() / stage.scaler - 15, $(window).height() / stage.scaler - mainTitle.height() - 45 - screenMarginY);

    	createButton((($(window).width() / stage.scaler) >> 1) - 60, $(window).height() / stage.scaler - 35 - screenMarginY, 120, 30, 'Ответить', answerButtonHandler);

    	initImageContainer();

    	preLoader.hidePreloader();
    }

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
	 */
	function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        //imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: questionLabel.height() + questionLabel.fontSize() * 0.1 + (padding << 1),
                stroke: headSheme.questionTitle.stroke,
                strokeWidth: headSheme.questionTitle.strokeWidth
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

    function buildCells(marginX, marginY, cells, avaibleSpaceW, avaibleSpaceH){
    	contentContainer = new Kinetic.Group({
    		x: marginX,
    		y: marginY
    	});

    	var tempRect = new Kinetic.Rect({
    		x: marginX,
    		y: marginY,
    		width: avaibleSpaceW,
    		height: avaibleSpaceH,
    		fill: 'black'
    	});

    	//contentLayer.add(tempRect);
    	//contentLayer.draw();

    	maxSize = optimizeSquareSize(avaibleSpaceW >> 1, avaibleSpaceH, cells.length);

    	var gridX = Math.floor(((avaibleSpaceW >> 1) + 110 / stage.scaler) / maxSize);
    	var gridY = Math.floor(avaibleSpaceH / maxSize);

    	var spacerX = 15;
    	var spacerY = 15;

    	var mainContainerSize = Math.min((avaibleSpaceW >> 1) - 110 / stage.scaler, avaibleSpaceH - 50);

    	var j = 0;

    	var dx = 0;
    	var dy = 0;

    	maxSize -= Math.max(spacerX, spacerY);

    	var mainContainer = buildMainContainer(0, 0, mainContainerSize, cells);

    	var cellPadding = mainContainerSize + spacerX;

    	cells.each(function(i){

    		buildDropContainer(dx + cellPadding, dy, maxSize, i, $(this).children('title').text());

    		j++;

    		if(j == gridX){
    			j = 0;
    			dy += maxSize + spacerY;
    		}

    		dx = j * (maxSize + spacerX);
    	})

    	var actualCellWidth = gridX * maxSize + (gridX - 1) * spacerX;

    	contentContainer.add(mainContainer);
    	contentContainer.add(dropContainerGroup);

    	contentLayer.add(contentContainer)

    	var totalHeight = gridY * maxSize + (gridY - 1) * spacerY;
    	var totalWidth = cellPadding + actualCellWidth;

    	if(totalWidth < avaibleSpaceW) contentContainer.x(contentContainer.x() + ((avaibleSpaceW >> 1) - (totalWidth >> 1)));
    	//if(totalHeight < avaibleSpaceH) contentContainer.y(contentContainer.y() + ((avaibleSpaceH >> 1) - (totalHeight >> 1)))
    	//if(mainContainerSize < avaibleSpaceH) mainContainer.y( - contentContainer.y() + marginY + ((avaibleSpaceH >> 1) - (mainContainerSize >> 1)));

    	var maxActualHeight = Math.max(totalHeight, mainContainerSize);
    	if(maxActualHeight < avaibleSpaceH) contentContainer.y(contentContainer.y() + ((avaibleSpaceH >> 1) - (maxActualHeight >> 1)));

    	for(i = 0; i < tipCollection.length; i++){
    		tipCollection[i].align();
    	}

    	mainContainer.initPictures();

    	tipLayer.batchDraw();

    	//console.log(totalHeight < avaibleSpaceH);

    	//if(totalHeight < avaibleSpaceH) contentContainer.y((avaibleSpaceH >> 1) - (totalHeight >> 1) + marginY)
    }

    function optimizeSquareSize(width, height, n){

    	var interval = [0, Math.sqrt((width * height) / n)];

    	for(var i = interval[1]; i >= interval[0]; i--){
    		if(borderRule(i)) return i;
    	}

    	return 0;

    	function borderRule(S_star){
    		return (Math.floor(width/S_star) * Math.floor(height/S_star) >= n);
    	}
    }

    function buildMainContainer(x, y, size, cells){
    	var containerGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	});

    	var containerSheme = colorSheme.container;

    	var imageObj = new Image();

    	containerGroup.isRootConainer = true;

    	containerGroup.pictureArray = [];

    	//imageObj.onload = function(){

		var image = new Kinetic.Rect({
			x: 0,
			y: 0,
			width: size,
			height: size,
			fill: colorSheme.container.outRect.color,
			stroke: colorSheme.container.outRect.stroke,
			strokeWidth: colorSheme.container.outRect.strokeWidth
		});

		var innerWidth = size * 0.90;
		var innerHeight = size * 0.80;

		var containerRect = new Kinetic.Rect({
			x: (size - innerWidth) >> 1,
			y: 10,
			width: innerWidth,
			height: innerHeight,
			fill: colorSheme.container.innerRect.color,
			stroke: colorSheme.container.innerRect.stroke,
			strokeWidth: colorSheme.container.innerRect.strokeWidth
		});

		containerGroup.contentArea = containerRect;

		var panelPadding = 30;
		var panelHeight = size - containerRect.height() - containerRect.y() - panelPadding;

		var buttonPanel = buildNavigationPanel(containerRect.x(), containerRect.height() + containerRect.y() + (panelPadding >> 1), containerGroup, panelHeight, containerRect.width());

		var tipSize = containerRect.height() * 1/6;

		containerGroup.tip = createImageTip(
			containerRect.x(),
			containerRect.y() + containerRect.height() - tipSize,
			containerRect.width(),
			tipSize,
			containerGroup);

		containerGroup.add(image);
		containerGroup.add(containerRect);
		containerGroup.add(buttonPanel);

		//contentLayer.add(containerGroup);
		//contentLayer.draw();

		containerGroup.initPictures = function(){
			var isTop = true;

			for(var i = 0; i < pictureData.length; i++){
				for(var j = 0; j < pictureData[i].pictures.length; j++){
					var currentPicture = createPicture(containerGroup.x() + containerRect.x() + contentContainer.x(), containerGroup.y() + containerRect.y() + contentContainer.y(), pictureData[i].pictures[j], pictureData[i].pictures[j].tip, i, containerRect.width(), containerRect.height());
					//pictureLayer.add( currentPicture );

					currentPicture.hide();

					//if(!isTop) currentPicture.hide();

					//if(isTop) isTop = false;

					containerGroup.pictureArray.push(currentPicture);
					currentPicture.setContainer(containerGroup);
				}
			}

			containerGroup.pictureArray = shuffle(containerGroup.pictureArray)

			for(i = 0; i < containerGroup.pictureArray.length; i++){
				pictureLayer.add( containerGroup.pictureArray[i] );
			}

			containerGroup.pictureArray[i - 1].show();

			containerGroup.currentPictureIndex = i - 1;

			pictureLayer.batchDraw();

			containerGroup.tip.setValue(containerGroup.pictureArray[containerGroup.currentPictureIndex]);
		}

		//containerGroup.currentPictureIndex = 0;

		containerGroup.next = function(){

			if(containerGroup.pictureArray.length > 0){

				var newIndex = containerGroup.currentPictureIndex + 1;

				if(newIndex > (containerGroup.pictureArray.length - 1)) newIndex = 0;

				containerGroup.pictureArray[newIndex].show();
				containerGroup.pictureArray[containerGroup.currentPictureIndex].hide();
				containerGroup.tip.setValue(containerGroup.pictureArray[newIndex]);

				tipLayer.batchDraw();
				pictureLayer.draw();

				containerGroup.currentPictureIndex = newIndex;

			}
		}
		containerGroup.prev = function(){

			if(containerGroup.pictureArray.length > 0){

				var newIndex = containerGroup.currentPictureIndex - 1;

				if(newIndex < 0) newIndex = containerGroup.pictureArray.length - 1;

				containerGroup.pictureArray[newIndex].show();
				containerGroup.pictureArray[containerGroup.currentPictureIndex].hide();
				containerGroup.tip.setValue(containerGroup.pictureArray[newIndex]);

				tipLayer.batchDraw();
				pictureLayer.draw();

				containerGroup.currentPictureIndex = newIndex;

			}
		}

		containerGroup.showNextPicture = function(){
			if(containerGroup.pictureArray.length == 1) return;

			//var newIndex = containerGroup.currentPictureIndex > 0 ? containerGroup.currentPictureIndex - 1 : 0;

			//newIndex = containerGroup.currentPictureIndex;

			/*containerGroup.currentPictureIndex--;

			//console.log(newIndex, containerGroup.currentPictureIndex, containerGroup.pictureArray.length);

			

			containerGroup.pictureArray[containerGroup.currentPictureIndex].show();

			console.log(containerGroup.pictureArray.length - 1);

			console.log(containerGroup.pictureArray);*/

			/*console.log(containerGroup.currentPictureIndex);

			console.log(containerGroup.pictureArray);

			containerGroup.prevIndex = containerGroup.currentPictureIndex;

			if(containerGroup.pictureArray.length > 0){
				containerGroup.prev();
			} else{
				containerGroup.next();
			}*/

			/*console.log(containerGroup.currentPictureIndex);

			console.log(containerGroup.pictureArray);

			//containerGroup.currentPictureIndex--;*/

			//containerGroup.pictureArray[containerGroup.currentPictureIndex - 1].show();

			//console.log(containerGroup.pictureArray[containerGroup.currentPictureIndex - 1].index);

			//containerGroup.currentPictureIndex = containerGroup.prevIndex - 1;
		}
	//}

    	//imageObj.src = 'assets/box_mc.png'

    	return containerGroup
    }

    function buildDropContainer(x, y, size, id, labelText){
    	//console.log(id, labelText)
    	var containerGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	});

    	var imageObj = new Image();

    	containerGroup.isRootConainer = false;

    	containerGroup.pictureArray = [];
    	containerGroup.answerID = id;

    	containerGroup.contentArea = null;

    	containerGroup.getBounds = function(){
    		return {left: containerGroup.x() + contentContainer.x(), top: containerGroup.y() + contentContainer.y(), right: containerGroup.x() + contentContainer.x() + size, bottom: containerGroup.y() + contentContainer.y() + size}
    	}

    	containerGroup.pushPicture = function(pictureGroup){

    		if(containerGroup.pictureArray.length != 0) containerGroup.pictureArray[containerGroup.currentPictureIndex].hide();

    		//pictureGroup.scaleImage(size * 0.95, size * 0.78);
    		pictureGroup.scaleImage(containerGroup.contentArea.width(), containerGroup.contentArea.height());

    		pictureGroup.x(containerGroup.x() + contentContainer.x() + containerGroup.contentArea.x());
    		pictureGroup.y(containerGroup.y() + contentContainer.y() + containerGroup.contentArea.y());

    		pictureGroup.defaultX = pictureGroup.x();
    		pictureGroup.defaultY = pictureGroup.y();

    		pictureGroup.centerImage(containerGroup.contentArea.width(), containerGroup.contentArea.height());

    		pictureGroup.recalculateImagePanel();

    		pictureGroup.swapContainer(containerGroup);

    		containerGroup.tip.setValue(pictureGroup);

    		tipLayer.batchDraw();
    		pictureLayer.batchDraw();
    	}

    	//imageObj.onload = function(){
    		/*var image = new Kinetic.Image({
			  x: 0,
			  y: 0,
			  image: imageObj,
			  width: size,
			  height: size
			});*/

		var image = new Kinetic.Rect({
			x: 0,
			y: 0,
			width: size,
			height: size,
			fill: colorSheme.container.outRect.color,
			stroke: colorSheme.container.outRect.stroke,
			strokeWidth: colorSheme.container.outRect.strokeWidth
		});

		var innerWidth = size * 0.90;
		var innerHeight = size * 0.80;

		var containerRect = new Kinetic.Rect({
			x: (size - innerWidth) >> 1,
			y: 10,
			width: innerWidth,
			height: innerHeight,
			fill: colorSheme.container.innerRect.color,
			stroke: colorSheme.container.innerRect.stroke,
			strokeWidth: colorSheme.container.innerRect.strokeWidth
		});
		var actualPanelHeight = size - containerRect.height() - containerRect.y();
		var panelPadding = actualPanelHeight * 0.3;
		var panelHeight = actualPanelHeight - panelPadding;

		containerGroup.contentArea = containerRect;

		var buttonPanel = buildNavigationPanel(containerRect.x(), containerRect.y() + containerRect.height() + (panelPadding >> 1), containerGroup, panelHeight, containerRect.width());

		var label = new Kinetic.Text({
			x: buttonPanel.arrowWidth + containerRect.x(),
			y: 0,
			text: labelText,
			fontSize: panelHeight,
	        fontFamily: mainFont,
	        fill: 'black',
	        align: 'center',
	        width: (containerRect.width() - (buttonPanel.arrowWidth << 1))
		});	

		label.y(containerRect.y() + containerRect.height());

		if(label.height() > actualPanelHeight){
			//label.fontSize(actualPanelHeight);
			var stringCount = Math.ceil(label.height() / label.fontSize());
			if(stringCount < 0 ) stringCount = 1;
			label.fontSize(actualPanelHeight / stringCount);
		}

		if(label.height() < actualPanelHeight){
			label.y(label.y() + ((actualPanelHeight >> 1) - (label.height() >> 1)));
		} else{			
			label.height(actualPanelHeight);
		}
		
		var tipSize = containerRect.height() * 1/6;

		containerGroup.tip = createImageTip(
			containerRect.x(),
			containerRect.y() + containerRect.height() - tipSize,
			containerRect.width(),
			tipSize,
			containerGroup);

		containerGroup.add(image);
		containerGroup.add(containerRect);
		containerGroup.add(label);
		containerGroup.add(buttonPanel);

		dropContainerGroup.add(containerGroup);
		//contentLayer.draw();
		//tipLayer.draw();

		containerGroup.pictureArray = [];

		containerGroup.currentPictureIndex = - 1;

		containerGroup.width(image.width() * image.scaleX());
		containerGroup.height(image.height() * image.scaleY());

		containerGroup.next = function(){

			if(containerGroup.pictureArray.length > 1){

				var newIndex = containerGroup.currentPictureIndex + 1;

				if(newIndex > (containerGroup.pictureArray.length - 1)) newIndex = 0;

				containerGroup.pictureArray[newIndex].show();
				containerGroup.tip.setValue(containerGroup.pictureArray[newIndex]);
				containerGroup.pictureArray[containerGroup.currentPictureIndex].hide();

				pictureLayer.draw();
				tipLayer.batchDraw();

				containerGroup.currentPictureIndex = newIndex;
			}
		}
		containerGroup.prev = function(){

			if(containerGroup.pictureArray.length > 1){

				var newIndex = containerGroup.currentPictureIndex - 1;					

				if(newIndex < 0) newIndex = containerGroup.pictureArray.length - 1;

				containerGroup.pictureArray[newIndex].show();
				containerGroup.tip.setValue(containerGroup.pictureArray[newIndex]);
				containerGroup.pictureArray[containerGroup.currentPictureIndex].hide();

				pictureLayer.draw();
				tipLayer.batchDraw();

				containerGroup.currentPictureIndex = newIndex;

			}
		}
    	//}

    	//imageObj.src = 'assets/box_mc.png'
    }

    function createImageTip(x, y, width, height, container){
    	var tipGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	});

    	var tipSheme = colorSheme.tip;

    	tipGroup.container = container;

    	var labelPadding = height * 0.3;
    	var zoomPadding = height * 0.2;

    	tipGroup.zoomButton = createZoomButton(width - height + (zoomPadding >> 1), (zoomPadding >> 1), height - zoomPadding, tipGroup);

    	var labelHeight = height - labelPadding;

    	var tipLabel = new Kinetic.Text({ //текстовая метка
	        x: labelPadding >> 1,
	        y: 0,
	        text: '',
	        fontSize: height - labelPadding,
	        fontFamily: mainFont,
	        fill: tipSheme.fontColor,
	        align: 'left',
	        width: width - height - (labelPadding >> 1)
	    });

	    tipLabel.maxSymbolCount = Math.ceil((width - height - (labelPadding >> 1)) / tipLabel.fontSize());
	    console.log(tipLabel.maxSymbolCount);

	    if(tipLabel.height() > labelHeight) tipLabel.height(labelHeight);

	    var tipLabelDy = (height >> 1) - (tipLabel.height() >> 1);

	    if(tipLabelDy > 0) tipLabel.y(tipLabelDy);

		var labelRect = new Kinetic.Rect({
    		x: 0,
    		y: 0,
    		width: width,
    		height: height,
    		fill: tipSheme.color,
    		opacity: tipSheme.opacity
    	});

    	tipCollection.push(tipGroup);

    	tipGroup.hide();

    	tipGroup.align = function(){
    		tipGroup.x(tipGroup.container.x() + contentContainer.x() + tipGroup.x());
    		tipGroup.y(tipGroup.container.y() + contentContainer.y() + tipGroup.y());
    	}

    	tipGroup.setValue = function(image){
    		tipGroup.linkedImage = image;

    		if(image == null){
    			tipGroup.hide();
    			return;
    		}

    		if(!tipGroup.isVisible()) tipGroup.show();

    		tipLabel.text(shorterText(image.tip, tipLabel.fontFamily(), tipLabel.fontSize(), tipLabel.width()));

    		if(tipLabel.height() > labelHeight) tipLabel.height(labelHeight);

		    var tipLabelDy = (height >> 1) - (tipLabel.height() >> 1);

		    if(tipLabelDy > 0) tipLabel.y(tipLabelDy);
    	}

    	tipGroup.add(labelRect);
    	tipGroup.add(tipLabel);
    	tipGroup.add(tipGroup.zoomButton);

		tipLayer.add(tipGroup);

    	return tipGroup;
    }

    function shorterText(text, fontFamily, fontSize, maxWidth){
    	var result = '';

    	if(sizeLabel == null){
    		sizeLabel = new Kinetic.Text({
    			x: 0,
    			y: 0
    		});
    	};

    	sizeLabel.fontSize(fontSize);
    	sizeLabel.fontFamily(fontFamily);
    	sizeLabel.text('');

    	for(var i = 0; i < text.length; i++){
    		result += text[i];
    		sizeLabel.text(result);
    		if(sizeLabel.width() > maxWidth){
    			result = result.substr(0, result.length - 3) + '...';
    			break;
    		}
    	}

    	return result;
    }

    function preloadPictures(cells){
    	var loadCounter = 0;

    	$(cells).each(function(index){
    		var title = $(this).children('title').text();
    		var object = $(this).children('object');

    		pictureData[index] = {
    			title: title,
    			pictures: []
    		}

    		$(object).each(function(objIndex){
    			loadCounter++;

	    		var imageObj = new Image();

	    		imageObj.text = $(this).text();
	    		imageObj.index = index;

	    		imageObj.onload = function(){
	    			var image = new Kinetic.Image({
	    				x: 0,
	    				y: 0,
	    				image: imageObj
	    			});

	    			image.tip = imageObj.text;

	    			//console.log(imageObj, this.index);

	    			pictureData[this.index].pictures.push(image);

	    			loadCounter--;

	    			if(loadCounter == 0) onPicturesLoaded();
	    		}

	    		imageObj.src = levelManager.getRoute($(this).attr('picture'));
    		})
    	});
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            }

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
                backgroundLayer.draw();
            }

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        }

        imgObj.src = 'assets/preLoader.png'

        return preloaderObject;
    }

    function onPicturesLoaded(){
    	initApplication();
    }

    function createPicture(x, y, image, tooltip, answerID, maxWidth, maxHeight){
    	//var imageObj = new Image();
    	var imageGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	});

    	imageGroup.answerID = answerID;

    	imageGroup.defaultX = x;
    	imageGroup.defaultY = y;

    	function onDragStart(evt){
    		imageGroup.dragFlag = true;

    		imageGroup.defaultW = imageGroup.image.width() * imageGroup.image.scaleX();
    		imageGroup.defaultH = imageGroup.image.height() * imageGroup.image.scaleY();

    		/*imageGroup.x(pointer.x - ((imageGroup.image.width() * imageGroup.image.scaleX()) >> 1) + imageGroup.image.x());
    		imageGroup.y(pointer.y - ((imageGroup.image.height() * imageGroup.image.scaleY()) >> 1) + imageGroup.image.y());*/

    		imageGroup.scaleImage(maxSize/2, maxSize/2);
    		imageGroup.hideImagePanel();
    		//imageGroup.recalculateImagePanel();

    		var pointer = getRelativePointerPosition();

    		imageGroup.x(pointer.x - imageGroup.image.x() - ((imageGroup.image.width() * imageGroup.image.scaleX()) >> 1));
    		imageGroup.y(pointer.y - imageGroup.image.y() - ((imageGroup.image.height() * imageGroup.image.scaleY()) >> 1));

    		//imageGroup.container.showNextPicture();

    		imageGroup.moveTo(dragLayer);
    		pictureLayer.draw();

    		imageGroup.startDrag();
    	}

    	function dragEnd(){
    		imageGroup.moveTo(pictureLayer);
    		dragLayer.draw();

    		imageGroup.showImagePanel();

    		var collidingContainer = imageGroup.getColliding();
    		if(collidingContainer){
    			collidingContainer.pushPicture(imageGroup);
    		} else{
    			imageGroup.resetPosition();
    		}

    		pictureLayer.draw();

    		imageGroup.dragFlag = false;
    	}

    	function onDragEnd(evt){
    		if(imageGroup.dragFlag) dragEnd();
    	}

    	imageGroup.getBounds = function(){
    		return {left: imageGroup.x(), top: imageGroup.y(), right: imageGroup.x() + imageGroup.image.width() * imageGroup.image.scaleX(), bottom: imageGroup.y() + imageGroup.image.height() * imageGroup.image.scaleY()};
    	}

    	imageGroup.checkAnswer = function(){
    		return imageGroup.answerID == imageGroup.container.answerID;
    	}

    	imageGroup.getColliding = function(){
    		var containerCollection = dropContainerGroup.children;
    		var firstRectangle = imageGroup.getBounds();
    		var secondRectangle;

    		for(var i = 0; i < containerCollection.length; i++){
    			if(containerCollection[i] == imageGroup.container) continue;
    			secondRectangle = containerCollection[i].getBounds();

    			if(isColliding(firstRectangle, secondRectangle)) return containerCollection[i];
    		}

    		return false;
    	}

    	imageGroup.resetPosition = function(){
    		imageGroup.x(imageGroup.defaultX);
    		imageGroup.y(imageGroup.defaultY);
    		imageGroup.scaleImage(imageGroup.defaultW, imageGroup.defaultH);
    		if(!imageGroup.container.isRootConainer) imageGroup.recalculateImagePanel();
    	}

    	imageGroup.setContainer = function(newContainer){
    		imageGroup.container = newContainer;
    	}

    	imageGroup.swapContainer = function(newContainer){
    		var position = $.inArray(imageGroup, imageGroup.container.pictureArray);
    		if ( ~position ) imageGroup.container.pictureArray.splice(position, 1);
    			
			if(imageGroup.container.pictureArray.length != 0){
				if(imageGroup.container.currentPictureIndex > 0){
					imageGroup.container.currentPictureIndex--;
				} else{
					imageGroup.container.currentPictureIndex++;
				}
				imageGroup.container.pictureArray[imageGroup.container.currentPictureIndex].show();
				imageGroup.container.tip.setValue(imageGroup.container.pictureArray[imageGroup.container.currentPictureIndex]);
			} else{
				imageGroup.container.currentPictureIndex = -1;
				imageGroup.container.tip.setValue(null);
			}

    		newContainer.pictureArray.push(imageGroup);
    		newContainer.currentPictureIndex++;    		

    		imageGroup.setContainer(newContainer);

    		tipLayer.batchDraw();
    	}

    	imageGroup.scaleImage = function(maxWidth, maxHeight){
    		var scaler = calculateAspectRatioFit(imageGroup.image.width(), imageGroup.image.height(), maxWidth, maxHeight);

    		imageGroup.image.scaleX(scaler);
    		imageGroup.image.scaleY(scaler);
    	}

    	imageGroup.centerImage = function(containerWidth, containerHeight){
    		var dx = 0;
			var dy = 0;

			//CAUTION!!! DANGER AREA!
			if(imageGroup.image.width() * imageGroup.image.scaleX() < containerWidth){
				dx = (containerWidth >> 1) - ((imageGroup.image.width() * imageGroup.image.scaleX()) >> 1);
			}
			if(imageGroup.image.height() * imageGroup.image.scaleY() < containerHeight){
				dy = (containerHeight >> 1) - ((imageGroup.image.height() * imageGroup.image.scaleY()) >> 1);
			}

			imageGroup.image.x(dx);
			imageGroup.image.y(dy);
    	}

    	imageGroup.dragFlag = false;

    	imageGroup.image = null;

    	//imageObj.onload = function(){
		/*var image = new Kinetic.Image({
		  x: 0,
		  y: 0,
		  image: imageObj,
		  width: imageObj.width,
		  height: imageObj.height
		});*/

		imageGroup.image = image;
		imageGroup.tip = tooltip;

    	imageGroup.image.on(events[isMobile].down, onDragStart);	    	
    	imageGroup.on(events[isMobile].up, function(){
    		if(imageGroup.dragFlag){
    			dragEnd();
    		}
    	});
    	imageGroup.on('dragend', onDragEnd);

		imageGroup.scaleImage(maxWidth, maxHeight);

		imageGroup.centerImage(maxWidth, maxHeight);

		var label = new Kinetic.Text({ //текстовая метка
	        x: 10,
	        y: 0,
	        text: tooltip,
	        //fontSize: Math.floor(xmlFontSize * 0.78),
	        fontSize: xmlFontSize,
	        fontFamily: mainFont,
	        fill: 'white',
	        align: 'left',
	        width: image.width() * image.scaleX() - 30
	    });

	    var textHeight = label.height();

		label.height(image.height() * image.scaleY() * 0.13);

		label.x(label.x() + image.x());

		var labelRect = new Kinetic.Rect({
    		x: label.x() - 10,
    		y: image.height() * image.scaleX() - image.height() * image.scaleY() * 0.13 + image.y(),
    		width: label.width() + 30,
    		height: label.height(),
    		fill: 'grey'
    	});

    	var dy = (label.height() >> 1) - (textHeight >> 1);

    	if(dy > 0){
    		label.y(labelRect.y() + dy);
    	} else{
    		label.y(labelRect.y());
    	}

    	labelRect.opacity(0.5);

    	imageGroup.labelRect = labelRect;
    	imageGroup.label = label;

		//imageGroup.zoomButton = createZoomButton(image.x() + image.width() * image.scaleY() - 25, label.y() - 2, image);

		imageGroup.add(image);
		//imageGroup.add(labelRect);
		//imageGroup.add(label);

			//pictureLayer.draw();		
    	//}

    	//imageObj.src = imageSource;

    	//pictureLayer.draw(); //DANGER

    	imageGroup.hideImagePanel = function(){
    		imageGroup.label.hide();
    		imageGroup.labelRect.hide();
    		//imageGroup.zoomButton.hide();
    	}

    	imageGroup.showImagePanel = function(){
    		imageGroup.label.show();
    		imageGroup.labelRect.show();
    		//imageGroup.zoomButton.show();
    	}

    	imageGroup.recalculateImagePanel = function(){
    		imageGroup.label.width(imageGroup.image.width()*imageGroup.image.scaleX() - 30);
    		imageGroup.label.height(imageGroup.image.height()*imageGroup.image.scaleY() * 0.13);

    		imageGroup.label.x(imageGroup.image.x() + 10);

    		imageGroup.label.y(imageGroup.image.y() + imageGroup.image.height()*imageGroup.image.scaleX() - imageGroup.label.height() + 2);
    		
    		imageGroup.labelRect.y(imageGroup.label.y() - 2);
    		imageGroup.labelRect.x(imageGroup.label.x() - 10);
    		imageGroup.labelRect.width(imageGroup.label.width() + 30);
    		imageGroup.labelRect.height(imageGroup.label.height())

    		//imageGroup.zoomButton.x(imageGroup.image.width()*imageGroup.image.scaleY() - 25 + imageGroup.image.x());
    		//imageGroup.zoomButton.y(imageGroup.label.y() - 2);
    	}

    	imageGroup.setWrong = function(){
    		imageGroup.image.stroke('red');
    		imageGroup.image.strokeWidth(5);
    	}

    	return imageGroup;
    }    

    function buildNavigationPanel(x, y, container, panelSize, containerWidth){
    	var panel = new Kinetic.Group({x: x, y: y});

    	var leftArrow = createArrowButton(0, 0, 'prev', prevHandler, panelSize);
    	var rightArrow = createArrowButton(0, 0, 'next', nextHandler, panelSize);

    	//leftArrow.x(0);
    	rightArrow.x(containerWidth - rightArrow.width());

    	panel.add( leftArrow );
    	panel.add( rightArrow );

    	panel.container = container

    	function nextHandler(){
    		panel.container.next();
    	}
    	function prevHandler(){
    		panel.container.prev();
    	}

    	panel.arrowWidth = rightArrow.width();

    	return panel;
    }

    function createArrowButton(x, y, type, handler, panelSize){
    	var imageObj = new Image();
    	var buttonGroup = new Kinetic.Group({x: x, y: y});

    	var graphicSize = {
    		x: 35,
    		y: 47
    	}

    	var scaler = calculateAspectRatioFit(graphicSize.x, graphicSize.y, panelSize, panelSize);

    	buttonGroup.width(graphicSize.x * scaler);
    	buttonGroup.height(graphicSize.y * scaler); 

    	imageObj.onload = function(){
    		var arrowButton = new Kinetic.Sprite({
				x: 0,
				y: 0,
				image: imageObj,
				animation: type,
				animations: {
					prev: [
					  // x, y, width, height
					  0, 0, graphicSize.x, graphicSize.y
					],
					next: [
					  // x, y, width, height
					  graphicSize.x, 0, graphicSize.x, graphicSize.y
					]
				},
				frameRate: 7,
				frameIndex: 0,
				scale:{
					x: scaler,
					y: scaler
				}
			});

			buttonGroup.on(events[isMobile].click, handler);
			buttonGroup.on(events[isMobile].mouseover, hoverPointer);
			buttonGroup.on(events[isMobile].mouseout, resetPointer);

			buttonGroup.add(arrowButton);
			contentLayer.draw();
    	}

    	imageObj.src = skinManager.getGraphic(colorSheme.navigationArrow)

    	return buttonGroup;
    }

    function buildColumns(columns, wrongWords){
    	var wordArray = [];
    	var titles = [];
    	var answers = [];

    	var currentRow;
    	var currentString;
    	var premadeLabel;
    	var rowStrArray = [];

    	colCount = columns.length;
    	rowCount = columns.eq(0).children('row').length;

    	var answerColCount = isHorizontal ? colCount : 2;

    	var horizontalElementsCount = isHorizontal ? colCount : colCount + answerColCount;

    	var horizontalAnswerIndentSpace = (answerColCount-1) * horizontalAnswerSpacer;

    	var maxWidth = (defaultStageWidth - horizontalElementsCount * textIndentX - answerGroupSpacer - marginLeft*2 - (isHorizontal ? 0 : horizontalAnswerIndentSpace)) / horizontalElementsCount;
    	var maxHeight = 0;

    	var premadeLabel;

    	columns.each(function(colIndex){

    		currentRow = $(this).children('row');
    		answers[colIndex] = [];
    		titles[colIndex] = [];

    		currentRow.each(function(rowIndex){

    			currentString = $(this).text();

    			if(currentString[0]=='#'){

    				currentString = currentString.substr(1, currentString.length-1);
    				
					rowStrArray = currentString.split('~');
					var strCount = rowStrArray.length>1?rowStrArray.length-1:1;

    				answers[colIndex][rowIndex] = rowStrArray;

					for(var i = 0; i<strCount; i++){
						if(!(rowStrArray[i] in wordArray)){
							premadeLabel = new Kinetic.Text({ //текстовая метка
						        x: 0,
						        y: 0,
						        text: rowStrArray[i],
						        fontSize: xmlFontSize,
						        fontFamily: mainFont,
						        fill: 'black',
						        align: 'center',
						        width: maxWidth
						    });

						    if(premadeLabel.height()>maxHeight) maxHeight = premadeLabel.height();


							wordArray[rowStrArray[i]] = premadeLabel;
						}
					}

					titles[colIndex][rowIndex] = 'active';

    			} else{
    				titles[colIndex][rowIndex] = currentString;
    			}
    		})
    	});

    	if(wrongWords!=''){
    		wrongWords = wrongWords.substr(1, wrongWords.length-1);
    		wrongArray = wrongWords.split('#');
    		for(i = 0; i<wrongArray.length; i++){
    			if(!(wrongArray[i] in wordArray)){
					premadeLabel = new Kinetic.Text({ //текстовая метка
				        x: 0,
				        y: 0,
				        text: wrongArray[i],
				        fontSize: xmlFontSize,
				        fontFamily: mainFont,
				        fill: 'black',
				        align: 'center',
				        width: maxWidth
				    });

				    if(premadeLabel.height()>maxHeight) maxHeight = premadeLabel.height();


					wordArray[wrongArray[i]] = premadeLabel;
				}
    		}
    	}

    	maxItemWidth = maxWidth+textIndentX;
    	maxItemHeight = maxHeight+textIndentY;

    	createTable(marginLeft+(horizontalAnswerIndentSpace>>1), marginTop, titles, answers);

    	var answerBoxesPosition = {};

    	answerBoxesPosition.x = isHorizontal? marginLeft : (marginLeft + colCount*maxItemWidth + answerGroupSpacer);
    	answerBoxesPosition.y = isHorizontal? marginTop+rowCount*maxItemHeight + answerGroupSpacer : marginTop;

    	buildAnswerBoxes(answerBoxesPosition.x, answerBoxesPosition.y, wordArray, answerColCount);
    }

    function buildAnswerBoxes(x, y, wordArray, boxColCount){
    	var dy = y;
    	var dx = x;
    	var counter = 0;
    	for(label in wordArray){

    		createAnswerBlock(dx, dy, maxItemWidth, maxItemHeight, wordArray[label]);

    		counter++;

    		if(counter != boxColCount){
    			dx += maxItemWidth+horizontalAnswerSpacer;
    		} else{
    			counter = 0;
    			dx = x;
    			dy += maxItemHeight+verticalAnswerSpacer;
    		}
    	}
    }

    function createTable(x, y, titles, answers){

    	for(var i = 0; i<colCount; i++){
    		for(var j = 0; j<rowCount; j++){

    			createTableBlock(x+maxItemWidth*i, y+maxItemHeight*j, maxItemWidth, maxItemHeight, calculateBoxType(i, j, colCount-1, rowCount-1), titles[i][j], answers[i][j])

    		}
    	}
    }

    function calculateBoxType(i, j, colCount, rowCount){
    	if(i==0&&j==0){
    		return 'topLeft';
    	}
    	if(j==0&&i==colCount){
    		return 'topRight'
    	}
    	if(j==rowCount&&i==0){
    		return 'botLeft';
    	}
    	if(j==rowCount&&i==colCount){
    		return 'botRight';
    	}

    	return 'center';
    }

    function answerButtonHandler(evt){
    	var pictureCollection = pictureLayer.children;
    	var isCorrect = true;

    	for(var i = 0; i<pictureCollection.length; i++){
    		if(!pictureCollection[i].checkAnswer()){
    			isCorrect = false;
    			pictureCollection[i].setWrong();
    			pictureLayer.draw();
    		}
    	}

    	resultScreen.invoke(isCorrect);

    	//showResultLabel($(window).width() / stage.scaler - 390, $(window).height() / stage.scaler - 45 - screenMarginY, isCorrect);

    	var button = evt.targetNode.getParent();

    	button.setText('Ещё раз');
    	button.off(events[isMobile].click);
    	button.on(events[isMobile].click, clear);
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback, sheme){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 26,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        backgroundLayer.add(buttonGroup);
        backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mouseout, resetPointer);

        return buttonGroup;
    }

    function showResultLabel(x, y, isWin){
    	var label = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	backgroundLayer.draw();
    }

    function createZoomButton(x, y, height, linkedTip){
    	var zoomButtonGroup = new Kinetic.Group({x: x, y: y})
		var imageObj = new Image();

		zoomButtonGroup.linkedTip = linkedTip

        imageObj.onload = function() {
			var zoomButton = new Kinetic.Image({
				x: 0,
				y: 0,
				image: imageObj,
				width: height,
				height: height		
			});

			zoomButtonGroup.on(events[isMobile].click, function(){
				imageContainer.zoomImage(this.linkedTip.linkedImage.image);
			})

			zoomButtonGroup.on(events[isMobile].mouseover, hoverPointer);
			zoomButtonGroup.on(events[isMobile].mouseout, resetPointer);

			zoomButtonGroup.add(zoomButton);

			//linkedImage.getParent().add(zoomButtonGroup);
			//pictureLayer.draw();

			tipLayer.draw();
		}
		imageObj.src = skinManager.getGraphic(colorSheme.zoomButton);

		return zoomButtonGroup;
	}

function initImageContainer(){
		var tipSheme = colorSheme.tip;

    	var image = new Kinetic.Image({
		  x: 20,
		  y: 20
		});

		var label = new Kinetic.Text({
			x: 20,
		  	y: 20,
		  	fill: tipSheme.fontColor,
		  	fontFamily: mainFont,
		  	fontSize: 30
		});

		var rect = new Kinetic.Rect({
			x: 20,
		  	y: 20,
		  	fill: tipSheme.color,
		  	opacity: tipSheme.opacity
		});

		var modalRect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: 'black',
			width: $(window).width() / stage.scaler,
			height: $(window).height() / stage.scaler
		});

		var modalGroup = new Kinetic.Group();

		modalRect.opacity(0.8);

		image.on(events[isMobile].mouseover, hoverPointer);
    	image.on(events[isMobile].mouseout, resetPointer);

		image.hide();
		modalRect.hide();
		rect.hide();
		label.hide();

		modalGroup.add(modalRect);
		modalGroup.add(image);
		modalGroup.add(rect);
		modalGroup.add(label);

		imageContainer.add(modalGroup);

		imageContainer.zoomImage = function(linkedImage){
			var imageObj = linkedImage.getImage();
			var objGroup = linkedImage.getParent();

			image.setImage(imageObj);

			var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth, defaultStageHeight);

			image.width(imageObj.width * scaler);
			image.height(imageObj.height * scaler);

			image.x(((defaultStageWidth)>>1)-(image.width()>>1))

			modalGroup.on(events[isMobile].click, zoomOut);

			if(!image.isVisible()){
				image.x(defaultStageWidth >> 1);
		    	image.y((defaultStageHeight - screenMarginY) >> 1);

		    	image.scale({
		    		x: 0,
		    		y: 0
		    	})

		    	var inTween = new Kinetic.Tween({
		    		node: image,
		    		scaleX: 1,
		    		scaleY: 1,
		    		x: ($(window).width() / stage.scaler >> 1) - (image.width() >> 1),
		    		y: ($(window).height() / stage.scaler >> 1) - (image.height() >> 1),
					duration: 0.4,
					easing: Kinetic.Easings.EaseInOut
		    	})
		    	inTween.onFinish = function(){
		    		//modalRect.show();

		    		if( typeof objGroup.label != 'undefined' ){
						//label.setAttrs(objGroup.label.getAttrs());
						//rect.setAttrs(objGroup.labelRect.getAttrs());

						label.width(image.width()*image.scaleX() - 10);

						label.text(objGroup.tip);

						label.x(image.x() + 10)
						label.y((image.y() + image.height()*image.scaleX()) - label.height() - 5);

						rect.x(image.x());
						rect.y(label.y());

						rect.width(label.width() + 10);
						rect.height(label.height() + 20);

						rect.show();
						label.show();

						imageContainer.draw();
					}
		    	};

		    	modalRect.show();
				
				image.show();
		    	inTween.play();
			}

			imageContainer.draw();
		}

		function zoomOut(evt){
					
			modalRect.hide();

	    	var outTween = new Kinetic.Tween({
	    		node: image,
	    		scaleX: 0,
	    		scaleY: 0,
	    		x: defaultStageWidth>>1,
	    		y: defaultStageHeight>>1,
				duration: 0.4,
				easing: Kinetic.Easings.EaseInOut
	    	});

			rect.hide();
			label.hide();

	    	outTween.onFinish = function(){
		    	image.hide();
			}

	    	outTween.play();
		}
    }

    function initResultScreen(){
        var resultScreen = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            opacity: 0.8
        });

        modalRect.hide();

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 340,
            height: 240,
            fill: 'white',
            stroke: "black",
            strokeWidth: 0.7
        });

        var resultLabel = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            text: '',
            fontSize: 40,
            fontFamily: mainFont,
            fill: 'black'
        });

        var imgObj = new Image();

        var resultButton;

        imgObj.onload = function(){

            resultButton = new Kinetic.Sprite({
                x: (backgroundRect.width() >> 1) - 70,
                y: backgroundRect.height() - 160,
                image: imgObj,
                animations: {
                    correct: [
                      // x, y, width, height
                      0,0,140,140
                    ],
                    wrong: [
                      // x, y, width, height
                      140,0,140,140
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            resultScreen.on(events[isMobile].click, function(){

                resultScreen.x((($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1));
                resultScreen.y((($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1));

                resultScreen.scale({
                    x: 1,
                    y: 1
                })

                var outTween = new Kinetic.Tween({
                    node: resultScreen,
                    scaleX: 0,
                    scaleY: 0,
                    x: (defaultStageWidth >> 1),
                    y: ((defaultStageHeight - screenMarginY) >> 1),
                    duration: 0.3,
                    easing: Kinetic.Easings.EaseInOut
                });

                modalRect.hide();

                outTween.onFinish = function(){             
                    resultScreen.hide();
                }

                outTween.play();
            });


            resultScreen.add(resultButton);

            resultLayer.draw();
        }

        imgObj.src = skinManager.getGraphic(colorSheme.resultButton.iconSource);

        resultLayer.add(modalRect);
        resultScreen.add(backgroundRect);
        resultScreen.add(resultLabel);

        resultScreen.width(backgroundRect.width());
        resultScreen.height(backgroundRect.height());

        resultScreen.hide();

        resultLayer.add(resultScreen);
        resultLayer.draw();

        resultScreen.centerLabel = function(){
            resultLabel.x((backgroundRect.width() >> 1) - (resultLabel.width() >> 1));
            resultLabel.y((resultButton.y() >> 1) - ((resultLabel.height() + resultLabel.fontSize() * 0.2) >> 1));
        }

        resultScreen.setWin = function(){
            //backgroundRect.fill('green');
            //resetButton.fill(colorSheme.resultButton.correctColor);
            resultButton.animation("correct");
            resultLabel.text('Правильно!');
        }

        resultScreen.setLoose = function(){
            //backgroundRect.fill('red');
            //resetButton.fill(colorSheme.resultButton.wrongColor);
            resultButton.animation("wrong");
            resultLabel.text('Неправильно!');
        }

        resultScreen.invoke = function(isWin){
            if(isWin){
                resultScreen.setWin();
            } else{
                resultScreen.setLoose();
            }

            resultScreen.centerLabel();

            resultScreen.x(defaultStageWidth >> 1);
            resultScreen.y((defaultStageHeight - screenMarginY) >> 1);

            resultScreen.scale({
                x: 0,
                y: 0
            })

            var inTween = new Kinetic.Tween({
                node: resultScreen,
                scaleX: 1,
                scaleY: 1,
                x: (($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1),
                y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1),
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            modalRect.show();
            resultScreen.show();

            inTween.play();
        }

        return resultScreen;
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function getRelativePointerPosition() {
	    var pointer = stage.getPointerPosition();
	    var pos = stage.getPosition();
	    var offset = stage.getOffset();
	    var scale = stage.getScale();

	    return {
	        x : ((pointer.x / scale.x) - (pos.x / scale.x) + offset.x),
	        y : ((pointer.y / scale.y) - (pos.y / scale.y) + offset.y)
	    };
	}

	function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

});