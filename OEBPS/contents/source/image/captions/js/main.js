$(window).load(function () {

    hookManager.applyFix('KitkatTouchFix');

    $(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

    Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    };

    function reorient(e) {
      window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	/*Глобальные переменные**/

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var aspectRatio = 0; //widescreen 16:9

    var defaultStageWidth = 1024;
    var defaultStageHeight = 576;

    var contentMargin = {x: 5, y: 5};

	//Отступ всего контента от сцены
	var marginLeft = 60;
    var marginTop = 60;

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 20;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var tempLabel = null;

    var answerArray = [];

    var collisionAreaCollection = [];

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

    var contentImage = null;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

    var root = null;

	var isHorizontal;

	var fontSize = 10;

    var mainFont = 'mainFont';

	var fontFamily = mainFont;
	var fontColor = 'black';

    var mainTitle = null;

    var resultScreen = null;

    var colorSheme = null;
    var skinManager = null;

    var levelManager = new LevelManager();

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var areaLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
    var contentScrollLayer = new Kinetic.Layer();
    var dragLayer = new Kinetic.Layer();
	var resultLayer = new Kinetic.Layer();

	stage.add(backgroundLayer);
	stage.add(areaLayer);
	stage.add(contentLayer);
    stage.add(contentScrollLayer);
    stage.add(dragLayer);
	stage.add(resultLayer);

	/*Группы**/
	var tableGroup = new Kinetic.Group();
    var answerGroup = new Kinetic.Group();

	//contentLayer.add(tableGroup);
	//contentLayer.add(answerGroup);

	$(window).on('resize', fitStage);

    function fitStage(){
        scaleScreen($(window).width(),  $(window).height());
    }

    function initAspectRatio(){
        if($(window).width() / $(window).height() < 1.6){
            aspectRatio = 1;
            defaultStageWidth = 1024;
            defaultStageHeight = 768;

            stage.width(defaultStageWidth);
            stage.width(defaultStageHeight);
        }
    }

    function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        stage.draw();
    }

    function initWindowSize(){      
        scaleScreen($(window).width(), $(window).height());
    }

	function isColliding(firstRectangle, secondRectangle){
		var result = false;
	 
		if(!(firstRectangle.bottom < secondRectangle.top ||
			firstRectangle.top > secondRectangle.bottom ||
			firstRectangle.left > secondRectangle.right ||
			firstRectangle.right < secondRectangle.left))

				result = true;

		return result;
	 }

	function initTempLabel(fontSize, fontFamily, maxWidth){
		tempLabel = new Kinetic.Text({
			x: 0,
			y: 0,
			fontSize: fontSize,
			fontFamily: fontFamily,
			text: '',
			width: maxWidth
		})
	}

	function calculateTextSize(text){
		tempLabel.text(text);
		return {width: tempLabel.width(), height: tempLabel.height()}
	}

	function createAnswerBlock(x, y, width, height, labelText){
		var answerBlock = new Kinetic.Group({x:x, y:y, draggable:true, dragOnTop: false, width: width, height: height});

		var answerSheme = colorSheme.answerBlock;

        answerBlock.defaultX = x;
		answerBlock.defaultY = y;

		var textSize = calculateTextSize(labelText);

		var labelPosition = {x: 0, y: 0};

		//if(textSize.width < width) labelPosition.x = (width >> 1) - (textSize.width >> 1);
		if(textSize.height < height) labelPosition.y = (height >> 1) - (textSize.height >> 1);

		var label = new Kinetic.Text({
			x: labelPosition.x,
			y: labelPosition.y,
			text: labelText,
			fontFamily: mainFont,
			fontSize: 18,
			align: 'center',
			width: width,
			height: height,
			fill: answerSheme.fontColor
		});

        answerBlock.on(events[isMobile].mouseover, hoverPointer);
        answerBlock.on(events[isMobile].mouseout, resetPointer);

		answerBlock.getValue = function(){
			return label.text();
		};

		answerBlock.resetPosition = function(){
			answerBlock.x(answerBlock.defaultX);
			answerBlock.y(answerBlock.defaultY);
		};

		x = y = 0;

		var rect = new Kinetic.Rect({
			x: x,
			y: y,
			width: width,
			height: height,
			stroke: answerSheme.stroke,
			strokeWidth: answerSheme.strokeWidth,
			fill: answerSheme.color,
	        cornerRadius: answerSheme.cornerRadius
		});

        answerBlock.on(events[isMobile].down, function(evt){
            answerBlock.x(answerBlock.getAbsolutePosition().x / stage.scaleX());
            answerBlock.y(answerBlock.getAbsolutePosition().y / stage.scaleY());

            answerBlock.moveTo(dragLayer);

            contentLayer.draw();
            dragLayer.draw();

            answerBlock.startDrag();
        });

		answerBlock.on('dragend', function(evt) {

			var colliding = answerBlock.getColliding();

			if(colliding){
				colliding.setText(answerBlock);
			}

            answerBlock.moveTo(answerGroup);
            answerBlock.resetPosition();

	    	contentLayer.draw();
            dragLayer.draw();
            areaLayer.draw();
		});

		answerBlock.getBounds = function(){
			return { left:answerBlock.getAbsolutePosition().x, right:answerBlock.getAbsolutePosition().x+answerBlock.width(), top:answerBlock.getAbsolutePosition().y, bottom:answerBlock.getAbsolutePosition().y+answerBlock.height() }
		};

		answerBlock.getColliding = function(){
			var firstRectangle = answerBlock.getBounds();
			var secondRectangle;

			for(var i = 0; i < collisionAreaCollection.length; i++){
				secondRectangle = collisionAreaCollection[i].getBounds();

				if(isColliding(firstRectangle, secondRectangle)){
					return collisionAreaCollection[i];
				}
			}

			return false;
		};

	    answerBlock.add(rect);
	    answerBlock.add(label);

	    return answerBlock;
	}
	
    skinManager = new SkinManager('assets/skin', function(){
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    });

    function xmlLoadHandler(xml){

        initAspectRatio();

        initWindowSize();

        root = $(xml).children('component');

        levelManager.initRoute(root);

        var title = root.children('title').text();
        var question = root.children('question').text();

        mainTitle = createHead(title, question);

        var backgroundImage = levelManager.getRoute(root.children('background').text());

        contentImage = setBackgroundImage(15, (mainTitle.height() + 15), $(window).width() / stage.scaler - 295, ($(window).height() / stage.scaler) - screenMarginY - 155, backgroundImage);

        backgroundLayer.add( contentImage );

        resultScreen = initResultScreen();
    };

    function initApplication(){

        contentLayerClip = {
            x: $(window).width() / stage.scaler - 250,
            y: mainTitle.height() + 15,
            width: 200,
            height: ($(window).height() / stage.scaler) - screenMarginY - (mainTitle.height() + 40)
        }

        contentLayer.setClip(contentLayerClip);

        fontSize = $(root).attr('font');
        fontFamily = $(root).attr('fontFamily');
        fontColor = $(root).attr('fontColor');

        xmlFontSize = (typeof fontSize !== 'undefined' && fontSize !== false) ? fontSize : 16;

        var mode = $(root).attr('mode');

        answerMode = (typeof mode !== 'undefined' && mode !== false) ? mode : 'move';

        backgroundLayer.add(mainTitle);
        backgroundLayer.draw();

        var wrongWords = root.children('wrongWords').text();

        var areas = root.children('rectangleAreas').children('area');

        initRectangleAreas(contentImage, areas);

        initAnswerArray(wrongWords);

        buildAnswerContainer();

        createButton((($(window).width()  / stage.scaler) >> 1) - 200, $(window).height() / stage.scaler - 35 - screenMarginY, 120, 30, 'Проверить', answerButtonHandler);
    };

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
     */
    function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        };

        //imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: mainTitle.height() + mainTitle.fontSize() * 0.2 + padding,
                stroke: headSheme.questionTitle.stroke
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

    function calculateMaxLabelSize(labelArray){
    	var maxSize = {width: 0, height: 0};

    	for(var i = 0; i < labelArray.length; i++){
    		var currentSize = calculateTextSize(labelArray[i]);

    		if(currentSize.width > maxSize.width) maxSize.width = currentSize.width;
    		if(currentSize.height > maxSize.height) maxSize.height = currentSize.height;
    	}

    	return maxSize;
    }

    function buildAnswerContainer(){
    	var clip = contentLayer.getClip();
    	answerGroup = new Kinetic.Group({x: clip.x + 2, y: clip.y});

        var anwerRectPadding = 10;

        var answerContainerRect = new Kinetic.Rect({
            x: answerGroup.x() - anwerRectPadding,
            y: answerGroup.y() - anwerRectPadding,
            width: clip.width + (anwerRectPadding << 1),
            height: clip.height + (anwerRectPadding << 1),
            fill: 'white',
            stroke: 'black',
            cornerRadius: 8,
            strokeWidth: 1
        });

        var containerLimiter = new Kinetic.Line({
            points: [
                answerContainerRect.x(), answerContainerRect.y() - 5,
                answerContainerRect.x(), answerContainerRect.y() + ($(window).height() / stage.scaler) - mainTitle.height() - 5
            ],
            stroke: colorSheme.containerLimiter,
        });

        backgroundLayer.add(containerLimiter);

        //backgroundLayer.add(answerContainerRect);

        backgroundLayer.draw();

    	var maxWidth = clip.width - 4;

    	initTempLabel(18, mainFont, maxWidth);   	
    	
    	var maxSize = calculateMaxLabelSize(answerArray);

    	var dy = maxSize.height + 20;

    	var containerHeight = 0;

    	for(var i = 0; i < answerArray.length; i++){
    		answerGroup.add( createAnswerBlock(0, dy * i, maxWidth, maxSize.height + 10, answerArray[i]) );
    		containerHeight += maxSize.height + 20;
    	}

    	answerGroup.height(containerHeight);

    	contentLayer.add(answerGroup);

        var scroll = createScrollBar($(window).width() / stage.scaler - 30, clip.y - 10, 20, clip.height + 20, answerGroup, contentLayer);

        scroll.refresh();

        if(!scroll.isActive){
            answerGroup.x(answerGroup.x() + 20);
            clip.x += 20;
            contentLayer.setClip(clip);
        }

        contentLayer.draw();
    }

    function initAnswerArray(wrongWords){
        if(wrongWords == '') return;
    	wrongWords = wrongWords.replace('\n', '');
    	var wrongWordsArray = wrongWords.split('#');

    	for(var i = 0; i < wrongWordsArray.length; i++){
    		answerArray.push(wrongWordsArray[i]);
    	}    	

    	shuffle(answerArray);
    }

    function setBackgroundImage(x, y, maxWidth, maxHeight, imagePath){
        var imageGroup = new Kinetic.Group({
            x: x,
            y: y
        });

        var containerPadding = 10;

        var containerRect = new Kinetic.Rect({
            x: - containerPadding,
            y: - containerPadding,
            width: maxWidth + (containerPadding << 1),
            height: maxHeight + (containerPadding << 1),
            fill: 'white',
            stroke: 'black',
            strokeWidth: 1,
            cornerRadius: 8
        });

        //imageGroup.add(containerRect);

    	var imageObj = new Image();

	        imageObj.onload = function() {

				var image = new Kinetic.Image({
					x: 0,
					y: 0,
					image: imageObj
				});

                var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, maxWidth, maxHeight);

                image.scale({
                    x: scaler,
                    y: scaler
                })

                imageGroup.width(imageObj.width * scaler);
                imageGroup.height(imageObj.height * scaler);

                imageGroup.scaler = scaler;

                if(imageGroup.width() < maxWidth) image.x((maxWidth >> 1) - (imageGroup.width() >> 1));
                if(imageGroup.height() < maxHeight) image.y((maxHeight >> 1) - (imageGroup.height() >> 1));

                imageGroup.actualX = image.x() + imageGroup.x();
                imageGroup.actualY = image.y() + imageGroup.y();

				imageGroup.add(image);
				backgroundLayer.draw();

                initApplication();
			};
		imageObj.src = imagePath;

        return imageGroup;
    }

    function initRectangleAreas(contentImage, areaCollection){
    	var x = 0;
    	var y = 0;
    	var width = 0;
    	var height = 0;

    	var answer = '';

    	var rectangleArea;

        var areaContainer = new Kinetic.Group({
            x: contentImage.actualX,
            y: contentImage.actualY,
            scaleX: contentImage.scaler,
            scaleY: contentImage.scaler
        });

    	areaCollection.each(function(index){
    		x = (+$(this).attr('x'));
    		y = (+$(this).attr('y'));
    		width = (+$(this).attr('width'));
    		height = (+$(this).attr('height'));

    		answer = $(this).text();

    		answerArray.push(answer);

    		var rectangleArea = createRectangleArea(x, y, width, height, answer);

    		collisionAreaCollection.push(rectangleArea);

    		areaContainer.add(rectangleArea);
    	});

        areaLayer.add(areaContainer);

        areaLayer.draw();
    }

    function createRectangleArea(x, y, width, height, answer){
    	var rectangleAreaGroup = new Kinetic.Group({
    		x: x,
    		y: y,
    		width: width,
    		height: height
    	});

    	rectangleAreaGroup.answer = answer;

        var tipRect = new Kinetic.Rect({
            x: contentImage.actualX + (x * contentImage.scaler),
            y: contentImage.actualY + (y * contentImage.scaler),
            width: (rectangleAreaGroup.width() * contentImage.scaler),
            height: (rectangleAreaGroup.height() * contentImage.scaler),
            fill: 'black'
        });

    	rectangleAreaGroup.getBounds = function(){
    		return {
    			left: (tipRect.x()) * stage.scaleX(),
    			right: (tipRect.x()) * stage.scaleX() + tipRect.width() * stage.scaleX(),
    			top: (tipRect.y()) * stage.scaleY(),
    			bottom: (tipRect.y()) * stage.scaleY() + tipRect.height() * stage.scaleY()}
    	};

        //areaLayer.add(tipRect);

    	var label = new Kinetic.Text({
    		x: 0,
    		y: 3,
    		text: '',
    		fill: fontColor,
    		width: width - 5,
    		height: height,
    		fontSize: fontSize,
    		fontFamily: fontFamily,
            align: 'center'
    	});

        var checkHeightLabel = new Kinetic.Text({
            x: 0,
            y: 0,
            text: '',
            fill: fontColor,
            width: width - 5,
            fontSize: fontSize,
            fontFamily: fontFamily,
            align: 'center'
        });

        rectangleAreaGroup.value = null;

    	rectangleAreaGroup.label = label;

    	rectangleAreaGroup.setText = function(answerBlock){
            if(rectangleAreaGroup.value != null){
                rectangleAreaGroup.value.show();
                rectangleAreaGroup.value.resetPosition();
            }
    		label.text(answerBlock.getValue());
            checkHeightLabel.text(label.text());

            var labelHeight = checkHeightLabel.height() + checkHeightLabel.height() * 0.2;

            label.y(3);

            if(labelHeight < height){
                label.y((height >> 1) - (labelHeight >> 1));
            }

            rectangleAreaGroup.value = answerBlock;
            answerBlock.hide();
    	};

        rectangleAreaGroup.setWrong = function(){
            label.fill('red');
        };

    	rectangleAreaGroup.checkValue = function(){
    		return (rectangleAreaGroup.getValue() == rectangleAreaGroup.answer)
    	};

    	rectangleAreaGroup.getValue = function(){
    		return rectangleAreaGroup.label.text();
    	};

    	rectangleAreaGroup.add(label);

    	return rectangleAreaGroup;
    }

    function buildColumns(columns, wrongWords){
    	var wordArray = [];
    	var titles = [];
    	var answers = [];

    	var currentRow;
    	var currentString;
    	var premadeLabel;
    	var rowStrArray = [];

    	colCount = columns.length;
    	rowCount = columns.eq(0).children('row').length;

    	var answerColCount = isHorizontal ? colCount : 2;

    	var horizontalElementsCount = isHorizontal ? colCount : colCount + answerColCount;

    	var horizontalAnswerIndentSpace = (answerColCount-1) * horizontalAnswerSpacer;

    	var maxWidth = (defaultStageWidth - horizontalElementsCount * textIndentX - answerGroupSpacer - marginLeft*2 - (isHorizontal ? 0 : horizontalAnswerIndentSpace)) / horizontalElementsCount;
    	var maxHeight = 0;

    	var premadeLabel;

    	columns.each(function(colIndex){

    		currentRow = $(this).children('row');
    		answers[colIndex] = [];
    		titles[colIndex] = [];

    		currentRow.each(function(rowIndex){

    			currentString = $(this).text();

    			if(currentString[0]=='#'){

    				currentString = currentString.substr(1, currentString.length-1);
    				
					rowStrArray = currentString.split('~');
					var strCount = rowStrArray.length>1?rowStrArray.length-1:1;

    				answers[colIndex][rowIndex] = rowStrArray;

					for(var i = 0; i<strCount; i++){
						if(!(rowStrArray[i] in wordArray)){
							premadeLabel = new Kinetic.Text({ //текстовая метка
						        x: 0,
						        y: 0,
						        text: rowStrArray[i],
						        fontSize: xmlFontSize,
						        fontFamily: mainFont,
						        fill: 'black',
						        align: 'center',
						        width: maxWidth
						    });

						    if(premadeLabel.height()>maxHeight) maxHeight = premadeLabel.height();


							wordArray[rowStrArray[i]] = premadeLabel;
						}
					}

					titles[colIndex][rowIndex] = 'active';

    			} else{
    				titles[colIndex][rowIndex] = currentString;
    			}
    		})
    	});

    	if(wrongWords!=''){
    		wrongWords = wrongWords.substr(1, wrongWords.length-1);
    		wrongArray = wrongWords.split('#');
    		for(i = 0; i<wrongArray.length; i++){
    			if(!(wrongArray[i] in wordArray)){
					premadeLabel = new Kinetic.Text({ //текстовая метка
				        x: 0,
				        y: 0,
				        text: wrongArray[i],
				        fontSize: xmlFontSize,
				        fontFamily: mainFont,
				        fill: 'black',
				        align: 'center',
				        width: maxWidth
				    });

				    if(premadeLabel.height()>maxHeight) maxHeight = premadeLabel.height();


					wordArray[wrongArray[i]] = premadeLabel;
				}
    		}
    	}

    	maxItemWidth = maxWidth+textIndentX;
    	maxItemHeight = maxHeight+textIndentY;

    	createTable(marginLeft+(horizontalAnswerIndentSpace>>1), marginTop, titles, answers);

    	var answerBoxesPosition = {};

    	answerBoxesPosition.x = isHorizontal? marginLeft : (marginLeft + colCount*maxItemWidth + answerGroupSpacer);
    	answerBoxesPosition.y = isHorizontal? marginTop+rowCount*maxItemHeight + answerGroupSpacer : marginTop;

    	buildAnswerBoxes(answerBoxesPosition.x, answerBoxesPosition.y, wordArray, answerColCount);
    }

    function buildAnswerBoxes(x, y, wordArray, boxColCount){
    	var dy = y;
    	var dx = x;
    	var counter = 0;
    	for(label in wordArray){

    		createAnswerBlock(dx, dy, maxItemWidth, maxItemHeight, wordArray[label]);

    		counter++;

    		if(counter != boxColCount){
    			dx += maxItemWidth+horizontalAnswerSpacer;
    		} else{
    			counter = 0;
    			dx = x;
    			dy += maxItemHeight+verticalAnswerSpacer;
    		}
    	}
    }

    function createTable(x, y, titles, answers){

    	for(var i = 0; i<colCount; i++){
    		for(var j = 0; j<rowCount; j++){

    			createTableBlock(x+maxItemWidth*i, y+maxItemHeight*j, maxItemWidth, maxItemHeight, calculateBoxType(i, j, colCount-1, rowCount-1), titles[i][j], answers[i][j])

    		}
    	}
    }

    function calculateBoxType(i, j, colCount, rowCount){
    	if(i==0&&j==0){
    		return 'topLeft';
    	}
    	if(j==0&&i==colCount){
    		return 'topRight'
    	}
    	if(j==rowCount&&i==0){
    		return 'botLeft';
    	}
    	if(j==rowCount&&i==colCount){
    		return 'botRight';
    	}

    	return 'center';
    }

    function answerButtonHandler(evt){
    	var isCorrect = true;

    	for(var i = 0; i < collisionAreaCollection.length; i++){
    		if(!collisionAreaCollection[i].checkValue()){
                isCorrect = false;
                collisionAreaCollection[i].setWrong();
            }
    	}
    	//showResultLabel($(window).width() / stage.scaler - 390, $(window).height() / stage.scaler - 45 - screenMarginY);

    	resultScreen.invoke(isCorrect);

        var button = evt.target.getParent();

    	button.setText('Ещё раз');
    	button.off(events[isMobile].click);
    	button.on(events[isMobile].click, clear);

        areaLayer.drawScene();
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback){
        var buttonSheme = colorSheme.button;

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 20,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 2);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        backgroundLayer.add(buttonGroup);
        backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        //buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        //buttonGroup.on(events[isMobile].mousedown, resetPointer);

        return buttonGroup;
    }

    function showResultLabel(x, y, isWin){
    	var label = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	backgroundLayer.draw();
    }

    function wheelScroll(evt){
    	scrollBar.scrollBy(-evt.originalEvent.wheelDelta/12);
    }

    function createScrollBar(x, y, width, height, contentGroup, clippedLayer){
        var scrollObject = new Kinetic.Group();

        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Kinetic.Rect({
           x: x,
           y: y,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11 + y;

        var vscroll = new Kinetic.Group({
            x: x + 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart*stage.scaleY()){
                    newY = scrollStart*stage.scaleY();
                }
                else if(newY > scrollEnd*stage.scaleY()) {
                    newY = scrollEnd*stage.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                };
            }
          });

          var scrollCircle = new Kinetic.Group();

          var backgroundCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 10,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          };

          var scrollTopArrowGroup = new Kinetic.Group({
            x: x,
            y: y
          })

          var scrollTopArrow = new Kinetic.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Kinetic.Group({
            x: x,
            y: y + height
          });

          var scrollBotArrow = new Kinetic.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Kinetic.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          contentScrollLayer.add(scrollObject);

          contentScrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            contentScrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            contentScrollLayer.drawScene();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          };

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            };

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
            scrollBotArrowGroup.hide();
            scrollTopArrow.hide();
            scrollObject.isActive = false;
          };

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
            scrollBotArrowGroup.show();
            scrollTopArrow.show();
            scrollObject.isActive = true;
          };

          scrollObject.refresh = function(){
            var clip = clippedLayer.getClip();

            vscrollArea.y(clip.y + 5);
            vscrollArea.height(clip.height - 10);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            scrollTopArrowGroup.y(clip.y + 5);
            scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            //contentGroup.y(0);
            vscroll.y(scrollStart);

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
            } else{
                scrollObject.show();
            }

            contentScrollLayer.draw();
          };

          clippedLayer.batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.batchDraw()
          }
          return scrollObject;
    }

    function initResultScreen(){
        var resultScreen = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            opacity: 0.8
        });

        modalRect.hide();

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 340,
            height: 240,
            fill: 'white',
            stroke: "black",
            strokeWidth: 0.7
        });

        var resultLabel = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            text: '',
            fontSize: 40,
            fontFamily: mainFont,
            fill: 'black'
        });

        var imgObj = new Image();

        var resultButton;

        imgObj.onload = function(){

            resultButton = new Kinetic.Sprite({
                x: (backgroundRect.width() >> 1) - 70,
                y: backgroundRect.height() - 160,
                image: imgObj,
                animations: {
                    correct: [
                      // x, y, width, height
                      0,0,140,140
                    ],
                    wrong: [
                      // x, y, width, height
                      140,0,140,140
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            resultScreen.on(events[isMobile].click, function(){

                resultScreen.x((($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1));
                resultScreen.y((($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1));

                resultScreen.scale({
                    x: 1,
                    y: 1
                })

                var outTween = new Kinetic.Tween({
                    node: resultScreen,
                    scaleX: 0,
                    scaleY: 0,
                    x: (defaultStageWidth >> 1),
                    y: ((defaultStageHeight - screenMarginY) >> 1),
                    duration: 0.3,
                    easing: Kinetic.Easings.EaseInOut
                });

                modalRect.hide();

                outTween.onFinish = function(){             
                    resultScreen.hide();
                };

                outTween.play();
            });


            resultScreen.add(resultButton);

            resultLayer.draw();
        };

        imgObj.src = skinManager.getGraphic(colorSheme.resultButton.iconSource);

        resultLayer.add(modalRect);
        resultScreen.add(backgroundRect);
        resultScreen.add(resultLabel);

        resultScreen.width(backgroundRect.width());
        resultScreen.height(backgroundRect.height());

        resultScreen.hide();

        resultLayer.add(resultScreen);
        resultLayer.draw();

        resultScreen.centerLabel = function(){
            resultLabel.x((backgroundRect.width() >> 1) - (resultLabel.width() >> 1));
            resultLabel.y((resultButton.y() >> 1) - ((resultLabel.height() + resultLabel.fontSize() * 0.2) >> 1));
        };

        resultScreen.setWin = function(){
            //backgroundRect.fill('green');
            //resetButton.fill(colorSheme.resultButton.correctColor);
            resultButton.animation("correct");
            resultLabel.text('Правильно!');
        };

        resultScreen.setLoose = function(){
            //backgroundRect.fill('red');
            //resetButton.fill(colorSheme.resultButton.wrongColor);
            resultButton.animation("wrong");
            resultLabel.text('Неправильно!');
        };

        resultScreen.invoke = function(isWin){
            if(isWin){
                resultScreen.setWin();
            } else{
                resultScreen.setLoose();
            }

            resultScreen.centerLabel();

            resultScreen.x(defaultStageWidth >> 1);
            resultScreen.y((defaultStageHeight - screenMarginY) >> 1);

            resultScreen.scale({
                x: 0,
                y: 0
            })

            var inTween = new Kinetic.Tween({
                node: resultScreen,
                scaleX: 1,
                scaleY: 1,
                x: (($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1),
                y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1),
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            modalRect.show();
            resultScreen.show();

            inTween.play();
        }

        return resultScreen;
    }

    function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

});