/**
* We wrap all our code in the jQuery "DOM-ready" function to make sure the script runs only
* after all the DOM elements are rendered and ready to take action
*/
$(document).ready(function () {
    //var refreshTime = 20;

    var animState; // 0 - static, 1 - move to start pos, 2 - up animation,   3 - up view,   4 - move to start pos, 5 - down animation
    			   //zoom         6 - move to start pos  7 - zoom plus anim  8 - zoomed     9 - move to start pos, 10 - zoom minus anim
    			   //top to cloce 11 - move to start pos, 12 - down animation,   13 - up view,   14 - move to start pos, 15 - up animation
    var curTime;
    var timer;
    $(function () { timer = setInterval(RefreshFrame, 50); });

    function RefreshFrame() {
    	if (animState == 1) {
    		console.log(currentFrame);
            if (currentFrame == endFrame) {
            	partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
            	var arc = Math.abs(endFrame)/10;
            	curAnim = 3;//+arc%4;
            	arcFrame=endFrame;
                totalFrames = animFrames;
                animState = 2;
                endFrame = 0;
                currentFrame = totalFrames;
                $(function () { clearInterval(timer); });
                $(function () { timer = setInterval(RefreshFrame, 50); });
            }
        }
        else if (animState == 2) {
            if (currentFrame == 1) {
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
                curAnim = 1;
                totalFrames = statFrames;
                animState = 3;
                canDrag = true;
                currentFrame = arcFrame;
                endFrame = currentFrame;
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
            }
        }
        else if (animState == 4) {
            if (currentFrame == endFrame) {
            	partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
                var arc = Math.abs(endFrame)/10;
            	curAnim = 3;//+arc%4;
            	arcFrame=currentFrame;
                totalFrames = animFrames;
                animState = 5;
                endFrame = totalFrames;
                currentFrame = 0;
                $(function () { clearInterval(timer); });
                $(function () { timer = setInterval(RefreshFrame, 50); });
            }
        }
        else if (animState == 5) {
            if (currentFrame == endFrame) {
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image"); //38
                curAnim = 0;
                totalFrames = statFrames;
                animState = 0;
                canDrag = true;
                currentFrame = arcFrame;
                endFrame = currentFrame;
            }
        }
        else
        if (animState == 6) {
            if (currentFrame == endFrame) {
            	partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
            	var arc = Math.abs(endFrame)/10;
            	curAnim = 4;//7+arc%4;
            	arcFrame=endFrame;
            	totalFrames = animFrames;
                animState = 7;
                endFrame = 0;
                currentFrame = totalFrames;
                $(function () { clearInterval(timer); });
                $(function () { timer = setInterval(RefreshFrame, 50); });
            }
        }
        else if (animState == 7) {
        	if (currentFrame == 1) {
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
                curAnim = 2;
                totalFrames = statFrames;
                animState = 8;
                canDrag = true;
                currentFrame = arcFrame;
                endFrame = currentFrame;
            }
        }
        else 
        if (animState == 9) {
            if (currentFrame == endFrame) {
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
                var arc = Math.abs(endFrame)/10;
                curAnim = 4;//7+arc%4;
            	arcFrame=endFrame;
            	totalFrames = animFrames;
                animState = 10;
                endFrame = totalFrames;
                currentFrame = 0;
                $(function () { clearInterval(timer); });
                $(function () { timer = setInterval(RefreshFrame, 50); });
            }
        }
        else 
        if (animState == 10) {
        	if (currentFrame == endFrame) {
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image"); //38
                curAnim = 0;
                totalFrames = statFrames;
                animState = 0;
                canDrag = true;
                currentFrame = arcFrame;
                endFrame = currentFrame;
            }
        }
        else
        if (animState == 11) {
        	console.log(currentFrame);
            if (currentFrame == endFrame) {
               	partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
               	curAnim = 5;//11;
               	totalFrames = animFrames;
                animState = 12;
                endFrame = totalFrames;
                currentFrame = 0;
                $(function () { clearInterval(timer); });
                $(function () { timer = setInterval(RefreshFrame, 50); });
            }
        }
        else if (animState == 12) {
            if (currentFrame == endFrame) {
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
                curAnim = 2;
                totalFrames = statFrames;
                animState = 8;
                canDrag = true;
                currentFrame = 0;
                endFrame = currentFrame;
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
            }
        }
        else
        if (animState == 14) {
          	console.log(currentFrame);
            if (currentFrame == endFrame) {
               	partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
               	curAnim = 5;//11;
               	totalFrames = animFrames;
                animState = 15;
                endFrame = 0;
                currentFrame = totalFrames;
                $(function () { clearInterval(timer); });
                $(function () { timer = setInterval(RefreshFrame, 50); });
            }
        }
        else if (animState == 15) {
            if (currentFrame == 1) {
                partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
                curAnim = 1;
                totalFrames = statFrames;
                animState = 3;
                canDrag = true;
                currentFrame = 0;
                endFrame = currentFrame;
            }
        }

        refreshRender();
    }

    var curAnim;
    var arcFrame;
    var koef = 1;

    var canDrag;
    var needZoom;
    var needUp;
    
    // Tells if the app is ready for user interaction
    var ready = false,
    // Tells the app if the user is dragging the pointer
			dragging = false,
    // Stores the pointer starting X position for the pointer tracking
			pointerStartPosX = 0,
    // Stores the pointer ending X position for the pointer tracking
			pointerEndPosX = 0,
    // Stores the distance between the starting and ending pointer X position in each time period we are tracking the pointer
			pointerDistance = 0,

    // The starting time of the pointer tracking period
			monitorStartTime = 0,
    // The pointer tracking time duration
			monitorInt = 10,
    // A setInterval instance used to call the rendering function
			ticker = 0,
    // Sets the speed of the image sliding animation
			speedMultiplier = 3,
    // CanvasLoader instance variable
			spinner,

    // Stores the total amount of images we have in the sequence

            totalFrames = 40,
            statFrames = 40,
            animFrames = 40,
    // The current frame value of the image slider animation
 			currentFrame = 0,
    // The value of the end frame which the currentFrame will be tweened to during the sliding animation
			endFrame = 0;
    // We keep track of the loaded images by increasing every time a new image is added to the image slider
    var loadedImages;
    var needLoad;

    var totalPatrs = 6;
    var partFrames;
    var partPath = [];
    partPath = new Array(totalPatrs);

    partPath[0] = "img/Middle/Middle_";
    partPath[1] = "img/Top/Top_";
    partPath[2] = "img/Close/Close_";
    partPath[3] = "img/Arc_X+/Arc_X+_";
    partPath[4] = "img/Zoom_X+/Zoom_X+_";
    partPath[5] = "img/TopToClose_X+/TopToClose_X+_";
    //partPath[6] = "img/Arc_Z+/Arc_Z+_";
    
    //partPath[7] = "img/Zoom_X+/Zoom_X+_";
    
    //partPath[8] = "img/Zoom_Z-/Zoom_Z-_";
    //partPath[9] = "img/Zoom_X-/Zoom_X-_";
    //partPath[10] = "img/Zoom_Z+/Zoom_Z+_";

    //partPath[11] = "img/TopToClose_X+/TopToClose_X+_";
    
    

    function loadStartModel() {
        loadedImages = 0;
        needLoad = 40*totalPatrs;
        addSpinner();
        //spinner.hide();
        partFrames = new Array(totalPatrs);
        //alert(partFrames.length);
        console.log("Start loading images: " + loadedImages + "/" + needLoad);
        for (i = 0; i < totalPatrs; i++) {
            loadPart(i);
            console.log("i " + loadedImages + "/" + needLoad);
        }
        //        addSpinner();
        //        $("#spinner span").text("100%");
        console.log(loadedImages + "/" + needLoad);
    }

    function loadPart(partIndex) {
        if (partIndex == 2) {
            partFrames[partIndex] = new Array(animFrames)
            for (j = 0; j < animFrames; j++) {
                loadPartFrame(partIndex, j, 0)
            }
        }
        else {
            partFrames[partIndex] = new Array(statFrames)
            for (j = 0; j < statFrames; j++) {
                loadPartFrame(partIndex, j)
            }
        }
        //partFrames[partIndex][0].removeClass("previous-image"); //.addClass("current-image");
    }

    function loadPartFrame(partIndex, frameIndex) {
        var liPart = document.createElement("liPart" + partIndex);
        // Generates the image file name using the incremented "loadedImages" variable
        var imagePartName = partPath[partIndex] +(frameIndex) + ".jpg";
        //console.log(imagePartName);
        /*
        Creates a new <img> and sets its src attribute to point to the file name we generated.
        It also hides the image by applying the "previous-image" CSS class to it.
        The image then is added to the <li>.
        */
        var imagePart = $('<img>');
        imagePart.addClass("previous-image");
        imagePart.attr('src', imagePartName);
        imagePart.appendTo(liPart);
        // We add the newly added image object (returned by jQuery) to the "frames" array.
        partFrames[partIndex][frameIndex] = imagePart;
        $("#threesixty_images").append(liPart);
        $(imagePart).hide();
        $(imagePart).load(function () {
            $(imagePart).show();
            $("#spinner span").text(Math.floor(loadedImages / needLoad * 100) + "%");
            loadedImages++;
            if (loadedImages >= needLoad) {
                $("#spinner").fadeOut("slow", function () {
                    showThreesixty();
                    console.log("End loading images: " + loadedImages + "/" + needLoad);
                });
            }
        });
    }

    function rPosition(elementID, mouseX, mouseY) {
        var offset = $('#' + elementID).offset();
        var x = mouseX - offset.left;
        var y = mouseY - offset.top;
        return { 'x': x, 'y': y };
    }

    /**
    * Adds a "spiral" shaped CanvasLoader instance to the #spinner div
    */
    function addSpinner() {
        spinner = new CanvasLoader("spinner");
        spinner.setShape("rect");
        spinner.setDiameter(90);
        spinner.setDensity(90);
        spinner.setRange(1);
        spinner.setSpeed(1);
        spinner.setColor("#888888");
        // As its hidden and not rendering by default we have to call its show() method
        spinner.show();
        // We use the jQuery fadeIn method to slowly fade in the preloader
        $("#spinner").fadeIn("slow");
    };

    /**
    * Displays the images with the "swooshy" spinning effect.
    * As the endFrame is set to -720, the slider will take 4 complete spin before it stops.
    * At this point it also sets the application to be ready for the user interaction.
    */
    function showThreesixty() {
        // Fades in the image slider by using the jQuery "fadeIn" method
        $("#threesixty_images").fadeIn("slow");
        // Sets the "ready" variable to true, so the app now reacts to user interaction 
        ready = true;
        // Sets the endFrame to an initial value...
        //endFrame = -720;
        // ...so when the animation renders, it will initially take 4 complete spins.
        refreshRender();
    };

    /**
    * Renders the image slider frame animations.
    */
    function render() {
        if (currentFrame !== endFrame) {
            /*
            Calculates the 10% of the distance between the "currentFrame" and the "endFrame".
            By adding only 10% we get a nice smooth and eased animation.
            If the distance is a positive number, we have to ceil the value, if its a negative number, we have to floor it to make sure
            that the "currentFrame" value surely reaches the "endFrame" value and the rendering doesn't end up in an infinite loop.
            */
            var frameEasing = endFrame < currentFrame ? Math.floor((endFrame - currentFrame) * 0.1) : Math.ceil((endFrame - currentFrame) * 0.1);
            // Sets the current image to be hidden
            hidePreviousFrame();
            // Increments / decrements the "currentFrame" value by the 10% of the frame distance
            currentFrame += frameEasing;
            // Sets the current image to be visible
            showCurrentFrame();
        } else {
            // If the rendering can stop, we stop and clear the ticker
            window.clearInterval(ticker);
            ticker = 0;
        }
    };

    function refreshRender() {
//    	if(animState != 2)
    	{

	        var frameEasing = endFrame < currentFrame ? Math.floor((endFrame - currentFrame) * 0.1) : Math.ceil((endFrame - currentFrame) * 0.1);
	        hidePreviousFrame();
	        

	        currentFrame += frameEasing;
	        showCurrentFrame();
    	}
/*    	else
    	if(animState == 2)
    	{
	        hidePreviousFrame();
	        currentFrame -= 1;
	        showCurrentFrame();
    	}    	    		
*/    };

    /**
    * Creates a new setInterval and stores it in the "ticker"
    * By default I set the FPS value to 60 which gives a nice and smooth rendering in newer browsers
    * and relatively fast machines, but obviously it could be too high for an older architecture.
    */
    function refresh() {
        // If the ticker is not running already...
        if (ticker === 0) {
            // Let's create a new one!
            ticker = self.setInterval(render, Math.round(1000 / 60));
        }
    };

    /**
    * Hides the previous frame
    */
    function hidePreviousFrame() {
        partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
    };

    /**
    * Displays the current frame
    */
    function showCurrentFrame() {
        partFrames[curAnim][getNormalizedCurrentFrame()].removeClass("previous-image").addClass("current-image");
    };

    /**
    * Returns the "currentFrame" value translated to a value inside the range of 0 and "totalFrames"
    */
    function getNormalizedCurrentFrame() {
        var c = -Math.ceil(currentFrame % totalFrames);
        if (c < 0) c += (totalFrames - 1);
        return c;
    };

    /**
    * Returns a simple event regarding the original event is a mouse event or a touch event.
    */
    function getPointerEvent(event) {
        return event.originalEvent.targetTouches ? event.originalEvent.targetTouches[0] : event;
    };

    /**
    * Adds the jQuery "mousedown" event to the image slider wrapper.
    */

    $("#threesixty").mousedown(function (event) {

        if (canDrag) {
            // Prevents the original event handler behaciour
            event.preventDefault();
            // Stores the pointer x position as the starting position
            pointerStartPosX = getPointerEvent(event).pageX;
            // Tells the pointer tracking function that the user is actually dragging the pointer and it needs to track the pointer changes
            dragging = true;
        }
    });

    /*function checkImagesOnClick(e) {
    //var imgs = document.getElementsByClass("current-image");

    };*/

    /**
    * Adds the jQuery "mouseup" event to the document. We use the document because we want to let the user to be able to drag
    * the mouse outside the image slider as well, providing a much bigger "playground".
    */
    $(document).mouseup(function (event) {
        // Prevents the original event handler behaciour
        //        frames[getNormalizedCurrentFrame()].removeClass("current-image").addClass("previous-image");
        event.preventDefault();

        // Tells the pointer tracking function that the user finished dragging the pointer and it doesn't need to track the pointer changes anymore
        dragging = false;
        //alert(event.target);
    });

    /**
    * Adds the jQuery "mousemove" event handler to the document. By using the document again we give the user a better user experience
    * by providing more playing area for the mouse interaction.
    */
    $(document).mousemove(function (event) {
        // Prevents the original event handler behaciour
        event.preventDefault();
        // Starts tracking the pointer X position changes
        trackPointer(event);
    });

    /**
    *
    */
    $("#threesixty").live("touchstart", function (event) {
        if (canDrag) {
            //koef = 6;
            // Prevents the original event handler behaciour
            event.preventDefault();
            // Stores the pointer x position as the starting position
            pointerStartPosX = getPointerEvent(event).pageX;
            // Tells the pointer tracking function that the user is actually dragging the pointer and it needs to track the pointer changes
            dragging = true;
        }
    });

    /**
    *
    */
    $("#threesixty").live("touchmove", function (event) {
        // Prevents the original event handler behaciour
        event.preventDefault();
        // Starts tracking the pointer X position changes
        trackPointer(event);
    });

    /**
    *
    */
    $("#threesixty").live("touchend", function (event) {
        // Prevents the original event handler behaciour
        event.preventDefault();
        // Tells the pointer tracking function that the user finished dragging the pointer and it doesn't need to track the pointer changes anymore
        dragging = false;
    });

    /**
    * Tracks the pointer X position changes and calculates the "endFrame" for the image slider frame animation.
    * This function only runs if the application is ready and the user really is dragging the pointer; this way we can avoid unnecessary calculations and CPU usage.
    */
    function trackPointer(event) {
        // If the app is ready and the user is dragging the pointer...
        if (ready && dragging) {
            // Stores the last x position of the pointer
            pointerEndPosX = getPointerEvent(event).pageX;
            // Checks if there is enough time past between this and the last time period of tracking
            if (monitorStartTime < new Date().getTime() - monitorInt) {
                // Calculates the distance between the pointer starting and ending position during the last tracking time period
                pointerDistance = pointerEndPosX - pointerStartPosX;
                // Calculates the endFrame using the distance between the pointer X starting and ending positions and the "speedMultiplier" values
                endFrame = currentFrame + Math.round((totalFrames - 1) * speedMultiplier * (pointerDistance / $("#threesixty").width() / 2));
                // Updates the image slider frame animation
                refresh();
                // restarts counting the pointer tracking period
                monitorStartTime = new Date().getTime();
                // Stores the the pointer X position as the starting position (because we started a new tracking period)
                pointerStartPosX = getPointerEvent(event).pageX;
            }
        }
    };

    $('ul.roundabout-holder').roundabout({
    });

    function start() {
        //addSpinner();
        // loading the firt image in the sequence.
        loadStartModel();
        //focusedPart = -1;
    }
    start();

/*    $("#rotRight").css("opacity", "0.6");
    $("#rotLeft").css("opacity", "0.6");

    $("#rotLeft").mouseover(function (e) {
        $("#rotLeft").css("opacity", "1");
    });

    $("#rotLeft").mouseleave(function (e) {
        $("#rotLeft").css("opacity", "0.6");
        dragging = false;
    });

    $("#rotRight").mouseover(function (e) {
        $("#rotRight").css("opacity", "1");
    });

    $("#rotRight").mouseleave(function (e) {
        $("#rotRight").css("opacity", "0.6");
    });

    $("#rotLeft").mousehold(25, function () {
        if (canDrag) {
            endFrame -= 1;
            refreshRender();
        }
    });

    $("#rotRight").mousehold(25, function () {
        if (canDrag) {
            endFrame += 1;
            refreshRender();
        }
    });*/

    canDrag = true;
    animState = 0;
    curAnim = 0;

    function getEndFrame() {
        var tt = Math.round(endFrame / totalFrames) * totalFrames;
        return tt;
    }
    
    function getArcFrame() {
        //var tt = currentFrame - currentFrame % (totalFrames/4); // 4 arcs
        var tt = currentFrame - currentFrame % totalFrames;//1 arc
        console.log(tt); 
        return tt;
    }

    $("#Arrow_up").click(function (e) {
        if (animState == 0) {
        	canDrag = false;
            endFrame = getArcFrame();
            animState = 1;
        }
        else
        if (animState == 3) {
            canDrag = false;
            endFrame = getEndFrame();
            animState = 11;
        }
        else
        if (animState == 8) {
	        canDrag = false;
	        endFrame = getArcFrame();
	        animState = 9;
        }

        $(function () { clearInterval(timer); });
        $(function () { timer = setInterval(RefreshFrame, 50); });
    });

    $("#Arrow_down").click(function (e) {
        if (animState == 0) {
            canDrag = false;
            endFrame = getArcFrame();
            animState = 6;
        }
        else
    	if (animState == 3) {
    		canDrag = false;
	        endFrame = getArcFrame();
	        animState = 4;
    	}
        else
        if (animState == 8) {
   	        canDrag = false;
   	        endFrame = getArcFrame();
  	        animState = 14;
        }
        
        $(function () { clearInterval(timer); });
        $(function () { timer = setInterval(RefreshFrame, 50); });
    });
    
/*    $("#Zoom_plus").click(function (e) {
//      $("#Disassem_hover").hide();
//      $("#Disassem_disabled").show();
      if (animState == 0) {
          canDrag = false;
          endFrame = getArcFrame();
          animState = 6;
      }
      else
      if (animState == 3) {
          canDrag = false;
          //needZoom = true;
          endFrame = getEndFrame();
          animState = 11;
      }
      $(function () { clearInterval(timer); });
      $(function () { timer = setInterval(RefreshFrame, 50); });
    });
    
    $("#Zoom_minus").click(function (e) {
        if (animState == 8) {
	        canDrag = false;
	        endFrame = getArcFrame();
	        animState = 9;
        }
        $(function () { clearInterval(timer); });
        $(function () { timer = setInterval(RefreshFrame, 50); });
    });*/

});
