var colorSheme = {
	"scrollBar":{
		"trackColor": "#87D0E7",
		"arrowColor": "#87D0E7",
		"innerSliderColor": "#87D0E7",
		"outerSliderColor": "#87D0E7"
	},
	"bottomPanel":{
		"answerRect":{
			"stroke": "#00A8D4",
			"strokeWidth": 0.8,
			"color": "white"
		}
	},
	"button":{
		"gradientBackgroundColorStart": "#00A8D4",
		"gradientBackgroundColorEnd": "#00A8D4",
		"labelColor": "white",
		"stroke": "#00A8D4",
		"strokeWidth": 1,
		"cornerRadius": 0
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"keyboard":{
		"backgroundColor": "white",
		"stroke": "#46a6d1",
		"strokeWidth": 1.5,
		"cornerRadius": 0,
		"keyboardIcon": "keyIconBlue.png",
		"rectStroke": "#46a6d1",
		"rectStrokeWidth": 1.5,
		"closeIcon": "closeIconBlue.png"
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#7FD3E9",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#7FD3E9"
		}
	}
}