$(window).load(function () {

    $(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

    Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    function reorient(e) {
        window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
    events[1].click = 'tap';
    events[0].down = 'mousedown';
    events[1].down = 'touchstart';
    events[0].up = 'mouseup';
    events[1].up = 'touchend';
    events[0].move = 'mousemove';
    events[1].move = 'touchmove';
    events[0].mouseover = 'mouseover';
    events[1].mouseover = 'mouseover';
    events[0].mouseout = 'mouseout';
    events[1].mouseout = 'mouseout';
    events[0].mousedown = 'mousedown';
    events[1].mousedown = 'tap';

	/*Глобальные переменные**/

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

    var contentMargin = {x: 5, y: 5};

	var aspectRatio = 0; //widescreen 16:9

    var defaultStageWidth = 1024;
    var defaultStageHeight = 576;

	var targetBox = null;
	var targetRoot = null;

	var tooltipBox = null;

	var inputBox = null;

	var crossTileMatrix = [];

	var wordsArray = [];

	var setArray = [];

	var resultLabel = null;

	var showKeyboard = false;

	var currentSetIndex = 0;

	var currentScreenObject = null;

	var scrollBar = null;

	//Отступ всего контента от сцены
	var marginLeft = 60;
    var marginTop = 60;

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 20;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

	var isHorizontal;

    var mainTitle = null;

    var taskContainerRect = null;

    var resultPanel = null;

    var colorSheme = null;

    var levelManager = new LevelManager();

    var mobileScaler = isMobile ? 1.5 : 1;

    var keyboard = null;

    var bottomPanelGroup = null;

    var mainFont = 'mainFont';

    var preloader = null;

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
	var inputLayer = new Kinetic.Layer();  
	var contentScrollLayer = new Kinetic.Layer();
    var imageContainer = new Kinetic.Layer();   
    var resultLayer = new Kinetic.Layer();

	stage.add(contentLayer);
    stage.add(contentScrollLayer);
    stage.add(backgroundLayer);
	stage.add(inputLayer);
    stage.add(imageContainer);
	stage.add(resultLayer);

	/*Группы**/
	var scrollGroup = new Kinetic.Group();

	contentLayer.add(scrollGroup);

    $(window).on('resize', fitStage);

    function fitStage(){
        scaleScreen($(window).width(),  $(window).height());
    }

    function initAspectRatio(){
        if($(window).width() / $(window).height() < 1.6){
            aspectRatio = 1;
            defaultStageWidth = 1024;
            defaultStageHeight = 768;

            stage.width(defaultStageWidth);
            stage.width(defaultStageHeight);
        }
    }

    function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        //stage.draw();
    }

    function initWindowSize(){    
        scaleScreen($(window).width(), $(window).height());
    }

    skinManager = new SkinManager('assets/skin', function(){
        initAspectRatio();
        initWindowSize();

        setTimeout(initApplication, 300);
    });

    function initApplication(){
        preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));

        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    }

    function buildBottomPanel(x, y){
    	bottomPanelGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	});

        bottomPanelGroup.defaultY = y;
        bottomPanelGroup.expandedY = y - 145 * mobileScaler;

        var keyboardSheme = colorSheme.keyboard;

        var imgObj = new Image();

        imgObj.onload = function(){

            bottomPanelGroup.closeIcon = new Kinetic.Image({
                x: ($(window).width() / stage.scaler) - x - (50 * mobileScaler),
                y: 5 * mobileScaler,
                image: imgObj,
                width: 42 * mobileScaler,
                height: 42 * mobileScaler
            }); 

            bottomPanelGroup.add(bottomPanelGroup.closeIcon);
            
            bottomPanelGroup.closeIcon.on(events[isMobile].click, toggleKeyboard);

            bottomPanelGroup.closeIcon.hide();
        }

        imgObj.src = skinManager.getGraphic(colorSheme.keyboard.closeIcon);

        var inputPadding = 10;

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            //cornerRadius: 10,
            width: ($(window).width() / stage.scaler),
            height: ($(window).height() / stage.scaler) - y,
            opacity: 0.9,
            stroke: keyboardSheme.rectStroke,
            strokeWidth: keyboardSheme.rectStrokeWidth
        });

        bottomPanelGroup.add(backgroundRect);

        var answerRectSheme = colorSheme.bottomPanel.answerRect;

    	var answerRectangle = new Kinetic.Rect({
    		x: (($(window).width() / stage.scaler) >> 1) - (285 * mobileScaler),
    		y: inputPadding,
    		fill: answerRectSheme.color,
    		//cornerRadius: 10,
    		width: 570 * mobileScaler,
    		height: 40 * mobileScaler,
    		stroke: answerRectSheme.stroke,
            strokeWidth: answerRectSheme.strokeWidth
    	})

        bottomPanelGroup.height(answerRectangle.height());

    	//answerRectangle.opacity(0.5);

        var buttonScaler = mobileScaler;

    	var answerLabel = new Kinetic.Text({
    		x: answerRectangle.x() + 10,
    		y: answerRectangle.y() + ((answerRectangle.height() >> 1) - ((30 * mobileScaler) >> 1)),
    		text: 'Ответ: ',
    		fill: '#7f7f7f',
    		fontFamily: mainFont,
    		fontSize: 30 * mobileScaler
    	});

        var inputHeight = answerRectangle.height() * 0.8;

    	inputBox = createInputBox(x + answerLabel.x() + answerLabel.width() + 5, y + answerRectangle.y() + ((answerRectangle.height() >> 1) - (inputHeight >> 1)), 390 * mobileScaler, inputHeight);

    	if(showKeyboard) {
            keyboard = buildKeyBoard(answerRectangle.x(), inputBox.y() - 85 * mobileScaler);
            keyboard.x(answerRectangle.x() + (answerRectangle.width() >> 1) - (keyboard.width() >> 1));
            keyboard.y(answerRectangle.y() + answerRectangle.height() + 10 * mobileScaler)
            bottomPanelGroup.add(keyboard);

            bottomPanelGroup.height(bottomPanelGroup.height() + keyboard.height());

            bottomPanelGroup.toKeyboardBtn = createIconButton(
                answerRectangle.x() + answerRectangle.width() - (48 * buttonScaler) - 10,
                inputBox.y() + (inputBox.height() >> 1) - ((27 * buttonScaler) >> 1),
                48 * buttonScaler,
                27 * buttonScaler,
                colorSheme.keyboard.keyboardIcon,
                toggleKeyboard
            );
        }

        bottomPanelGroup.add(answerRectangle);
    	//bottomPanelGroup.add(keyboard);
    	bottomPanelGroup.add(answerLabel);    	

    	backgroundLayer.add(bottomPanelGroup);
        if(showKeyboard) backgroundLayer.add(bottomPanelGroup.toKeyboardBtn);
    	backgroundLayer.draw();

        inputLayer.add(inputBox);
        inputLayer.draw();

    	inputBox.initCaret();

        bottomPanelGroup.height(answerRectangle.height() + answerRectangle.y());

        initResultPanel(bottomPanelGroup.height() + y + 10);

        bottomPanelGroup.isExpanded = false;

        bottomPanelGroup.toggle = function(){
            if(!bottomPanelGroup.isExpanded){
                bottomPanelGroup.expand();
            } else{
                bottomPanelGroup.reset();
            }
        }

        bottomPanelGroup.expand = function(){
            var dy = (bottomPanelGroup.defaultY - bottomPanelGroup.expandedY);
            bottomPanelGroup.y(bottomPanelGroup.expandedY);
            backgroundRect.height(backgroundRect.height() + dy);
            inputBox.y(bottomPanelGroup.y() + answerRectangle.y() + 2);
            bottomPanelGroup.toKeyboardBtn.y(inputBox.y() + (inputBox.height() >> 1) - ((27 * buttonScaler) >> 1));

            bottomPanelGroup.closeIcon.show();

            bottomPanelGroup.isExpanded = true;
        }

        bottomPanelGroup.reset = function(){
            var dy = (bottomPanelGroup.defaultY - bottomPanelGroup.expandedY);
            bottomPanelGroup.y(bottomPanelGroup.defaultY);
            backgroundRect.height(backgroundRect.height() - dy);
            inputBox.y(bottomPanelGroup.y() + answerRectangle.y() + 2);
            bottomPanelGroup.toKeyboardBtn.y(inputBox.y() + (inputBox.height() >> 1) - ((27 * buttonScaler) >> 1));

            bottomPanelGroup.closeIcon.hide();

            bottomPanelGroup.isExpanded = false
        }
    }

    function toggleKeyboard(){
        keyboard.toggle();
        bottomPanelGroup.toggle();
        backgroundLayer.draw();
    }

    function initResultPanel(y){
        var width = 140 * mobileScaler;
        var height = 35 * mobileScaler;

        resultPanel = {
            defaultButtonX: (($(window).width() / stage.scaler) >> 1) - (width >> 1),
            extendedButtonX: (($(window).width() / stage.scaler) >> 1) - (width + 5)
        };        
        resultPanel.button = createButton(resultPanel.defaultButtonX, y, width, height, 'Проверить', answerButtonHandler);
        resultPanel.label = buildResultLabel(resultPanel.button.x() + 10, resultPanel.button.y(), width + 20, height);
        
        resultPanel.showResult = function(isWin){
            resultPanel.button.x(resultPanel.extendedButtonX);
            resultPanel.label.x(resultPanel.button.x() + (width + 10));
            resultPanel.label.showResult(isWin);
        }

        resultPanel.hideResult = function(){
            resultPanel.button.x(resultPanel.defaultButtonX);
            resultPanel.label.hide();
        }
    }

    function createTextCaret(x, y){

    	var caret = new Kinetic.Text({
    		x: x,
    		y: y,
    		text: '|',
    		fontFamily: mainFont,
    		fontSize: 30 * mobileScaler,
    		fill: 'black'
    	});

		caret.setupTweens = function(){
			var blinkIn = new Kinetic.Tween({
				node: caret,
				opacity: 1,
				easing: Kinetic.Easings.Linear,
				duration: 0.4,
				onFinish: function(){
					blinkOut.reset();
					blinkOut.play();
				}
			});

			var blinkOut = new Kinetic.Tween({
				node: caret,
				opacity: 0,
				easing: Kinetic.Easings.Linear,
				duration: 0.4,
				onFinish: function(){
					blinkIn.reset();
					blinkIn.play();
				}
			});

			blinkOut.play();
		}

		return caret;				
    }

    function buildTaskContainer(){
        taskContainerRect = new Kinetic.Rect({
            x: 5,
            y: mainTitle.height() + 5,
            height: 10,
            width: ($(window).width() / stage.scaler) - 10,
            //fill: 'white',
            cornerRadius: 8,
            stroke: '#B1ADA6',
            strokeWidth: 1
        });

        taskContainerRect.refresh = function(){
            var newClip = contentLayer.getClip();
            taskContainerRect.x(newClip.x - 5);
            taskContainerRect.height(newClip.height + 5);
            taskContainerRect.width(newClip.width + 10);
        }

        //backgroundLayer.add(taskContainerRect);
    }

    function createInputBox(x, y, width, height){

    	var inputBoxGroup = new Kinetic.Group({
    		x: x,
    		y: y,
    		width: width,
    		height: height
    	})

    	inputBoxGroup.on(events[isMobile].click, function(){
    		var pointerX = stage.getPointerPosition().x - inputBoxGroup.getAbsolutePosition().x;
    		var relativePosition = pointerX / stage.scaleX();

    		var value = inputBoxGroup.getValue();

    		var currentLimitX = 0;

    		for(var i = 0; i < value.length; i++){
    			currentLimitX += inputBoxGroup.widthArray[value[i]];
    			if(relativePosition < currentLimitX){
    				inputBoxGroup.currentLineIndex = i;
		    		inputBoxGroup.caret.x(currentLimitX - inputBoxGroup.widthArray[value[i]]);
		    		inputLayer.draw();
    				return;
    			}
    		}

    		inputBoxGroup.currentLineIndex = i;
		    inputBoxGroup.caret.x(inputBoxGroup.inputLabel.width());
    	})

    	inputBoxGroup.currentLineIndex = 0;

    	var inputRect = new Kinetic.Rect({
    		x: 0,
    		y: 0,
    		fill: 'white',
    		width: width,
    		height: height,
            //stroke: 'black',
            strokeWidth: 1,
            //cornerRadius: 7,
            //opacity: 0.5
    	})

    	inputBoxGroup.inputLabel = new Kinetic.Text({
    		x: 0,
    		y: 0,
    		text: 'А',
    		fill: '#333333',
    		fontFamily: mainFont,
    		fontSize: height
    	})

    	inputBoxGroup.widthArray = calculateAlphabetWidth(inputBoxGroup.inputLabel);

    	inputBoxGroup.symbolWidth = inputBoxGroup.inputLabel.width();

    	inputBoxGroup.inputLabel.text('');

    	inputBoxGroup.caret = createTextCaret(0, 0);

    	inputBoxGroup.add(inputRect);
    	inputBoxGroup.add(inputBoxGroup.inputLabel);
    	inputBoxGroup.add(inputBoxGroup.caret);

    	inputBoxGroup.initCaret = function(){
    		inputBoxGroup.caret.setupTweens();
    	}
    	inputBoxGroup.moveCaretRight = function(){
    		var value = inputBoxGroup.getValue();

    		if(inputBoxGroup.currentLineIndex != value.length){
    			inputBoxGroup.caret.x(inputBoxGroup.caret.x() + inputBoxGroup.widthArray[value[inputBoxGroup.currentLineIndex]]);
    			inputBoxGroup.currentLineIndex++;
    			inputLayer.draw();
    		}
    	}
    	inputBoxGroup.moveCaretLeft = function(){
    		if(inputBoxGroup.currentLineIndex != 0){
    			inputBoxGroup.caret.x(inputBoxGroup.caret.x() - inputBoxGroup.widthArray[inputBoxGroup.getValue()[inputBoxGroup.currentLineIndex - 1]]);
    			inputBoxGroup.currentLineIndex--;
    			inputLayer.draw();
    		}
    	}

    	inputBoxGroup.pushSymbol = function(symbol){
    		if((inputBoxGroup.inputLabel.width() + inputBoxGroup.symbolWidth) < inputBoxGroup.width()){

    			var value = inputBoxGroup.getValue();

    			if(inputBoxGroup.currentLineIndex == value.length ){
		    		inputBoxGroup.inputLabel.text(inputBoxGroup.inputLabel.text() + symbol);
		    		inputBoxGroup.currentLineIndex++;
		    		inputBoxGroup.caret.x(inputBoxGroup.inputLabel.width());		    		
		    	} else{
		    		var rightPart = value.substring(inputBoxGroup.currentLineIndex, value.length);
		    		var leftPart = value.substring(0, inputBoxGroup.currentLineIndex);

		    		inputBoxGroup.inputLabel.text(leftPart + symbol + rightPart);

		    		inputBoxGroup.caret.x(inputBoxGroup.caret.x() + inputBoxGroup.widthArray[symbol]);
		    		inputBoxGroup.currentLineIndex++;
		    	}

		    	inputLayer.draw();
	    	}
    	}

        inputBoxGroup.pushSymbol.process();

    	inputBoxGroup.popSymbol = function(){
    		if(inputBoxGroup.currentLineIndex>0){

    			var value = inputBoxGroup.inputLabel.text();

    			if(inputBoxGroup.currentLineIndex == value.length ){
		    		inputBoxGroup.inputLabel.text(value.slice(0, value.length-1));
		    		inputBoxGroup.currentLineIndex--;
		    		inputBoxGroup.caret.x(inputBoxGroup.inputLabel.width());
		    	} else{		    		
		    		inputBoxGroup.caret.x(inputBoxGroup.caret.x() - inputBoxGroup.widthArray[value[inputBoxGroup.currentLineIndex]]);	
		    		inputBoxGroup.inputLabel.text(value.slice(0, inputBoxGroup.currentLineIndex - 1) + value.slice(inputBoxGroup.currentLineIndex, value.length));
		    		inputBoxGroup.currentLineIndex--;
		    	}
		    	inputLayer.draw();
	    	}
    	}

    	inputBoxGroup.deleteNextSymbol = function(){

    		var value = inputBoxGroup.inputLabel.text();

    		if(inputBoxGroup.currentLineIndex!=value.length){    			
	    		inputBoxGroup.inputLabel.text(value.slice(0, inputBoxGroup.currentLineIndex) + value.slice(inputBoxGroup.currentLineIndex+1, value.length));

	    		inputLayer.draw();
    		}
    	}

    	inputBoxGroup.getValue = function(){
    		return inputBoxGroup.inputLabel.text();
    	}
    	inputBoxGroup.setValue = function(val){
    		inputBoxGroup.inputLabel.text(val);
    		//inputBoxGroup.caret.x(0);
    		inputLayer.draw();
    	}

    	inputBoxGroup.resetValue = function(){
    		inputBoxGroup.inputLabel.text('');
    		inputBoxGroup.caret.x(0);
    		inputBoxGroup.currentLineIndex = 0;
    		inputLayer.draw();
    	}

    	return inputBoxGroup;
    }

    function showSet(setIndex){
    	setArray[setIndex].object.show();
    	scrollGroup.height(setArray[setIndex].object.height());

    	currentScreenObject = setArray[setIndex].object;

    	scrollBar.refresh();

    	contentLayer.draw();
    }

    function initTask(setArray){
    	setArray.each(function(index){
    		buildSet(this);
    	})
    }

    function showBonus(callback){

        scrollBar.refresh();

    	var bonus = setArray[currentSetIndex].bonus;

    	if(bonus!=null){

            var objectOutTween = new Kinetic.Tween({
                node: setArray[currentSetIndex].object,
                opacity: 0,
                duration: 0.4
            });

            objectOutTween.onFinish = function(){
                setArray[currentSetIndex].object.opacity(1);
                setArray[currentSetIndex].object.hide();
                contentLayer.draw();
            }

            bonus.opacity(0);
    		bonus.show();

            ///////////////////////////

            var bonusTween = new Kinetic.Tween({
              node: bonus,
              opacity: 1,
              duration: 0.8
            });

            bonusTween.onFinish = function(){
                scrollGroup.height(bonus.height());

                currentScreenObject = bonus;

                scrollBar.refresh();

                callback();
            }

            objectOutTween.play();

            bonusTween.play();

            ///////////////////////////

    		//contentLayer.draw();
    	}
    }

    function buildSet(set){
    	var answer = $(set).children('string').text();
    	var object = $(set).children('object');
    	var bonus = $(set).children('bonus');

        var clip = contentLayer.getClip();

    	var screenObject = buildObject(20, clip.y, clip.width - 50 * mobileScaler, clip.height, object);

    	scrollGroup.add(screenObject);

    	var screenBonus = null;

    	if(bonus.length>0){
    		screenBonus = buildObject(20, clip.y, clip.width - 50 * mobileScaler,  clip.height, bonus);
    		screenBonus.hide();

    		scrollGroup.add(screenBonus);
    	}

    	setArray.push({
    		answer: answer.toUpperCase(),
    		object: screenObject,
    		bonus: screenBonus
    	});    	

    	screenObject.hide();

    	contentLayer.draw();
    }
    function imageZoomHandler(evt){
		var currentImg = evt.targetNode;
		imageContainer.zoomImage(currentImg.getImage());
	}

    function buildObject(x, y, maxWidth, maxHeight, object){
    	var objectGroup = new Kinetic.Group({
    		x: x,
    		y: y,
    		width: maxWidth,
    		height: maxHeight
    	});

    	var contentGroup = new Kinetic.Group({
    		x: 0,
    		y: 0
    	});

    	objectGroup.centerContent = function(){
    		var dx = objectGroup.width() < maxWidth ? (maxWidth >> 1) - (objectGroup.width() >> 1) : 0;
	    	var dy = objectGroup.height() < maxHeight ? (maxHeight >> 1) - (objectGroup.height() >> 1) : 0;

	    	contentGroup.x(dx);
	    	contentGroup.y(dy);
    	}

    	var picture = $(object).attr('picture');

    	if(typeof picture == 'undefined') picture = '';

    	var text = $(object).text();

    	if(picture == '' && text != ''){
    		var objectLabel = new Kinetic.Text({
    			x: 0,
	    		y: 0,
	    		text: text,
	    		fill: 'Black',
	    		fontFamily: mainFont,
	    		fontSize: 30
    		});

            console.log(objectLabel.width());

            if(objectLabel.width() > maxWidth) objectLabel.width(maxWidth);

    		objectGroup.width(objectLabel.width());
    		objectGroup.height(objectLabel.height());

    		objectGroup.centerContent();

    		contentGroup.add(objectLabel);
    	} else if(picture!='' && text!=''){

    		var imageObj = new Image();

	        imageObj.onload = function() {

				var image = new Kinetic.Image({
					x: 0,
					y: 0,
					image: imageObj,
					width: imageObj.width,
					height: imageObj.height,
					strokeWidth: 10
				});
				var objectLabel = new Kinetic.Text({
	    			x: 0,
		    		y: 0,
		    		text: text,
		    		fill: 'Black',
		    		fontFamily: mainFont,
		    		fontSize: 30
	    		});

	    		if(objectLabel.width() > (maxWidth * 2/3)) objectLabel.width(maxWidth * 2/3);

	    		var scaler = calculateAspectRatioFit(image.width(), image.height(), (maxWidth - objectLabel.width() - 10), maxHeight);

				image.scaleX(scaler);
				image.scaleY(scaler);

				objectLabel.x(image.width() * image.scaleX() + 10);

				objectGroup.width(image.width() * image.scaleX() + objectLabel.width());
				objectGroup.height(Math.max(image.height() * image.scaleY(), objectLabel.height()));

				objectGroup.centerContent();

				contentGroup.add(objectLabel);
				contentGroup.add(image);

                image.on(events[isMobile].click, imageZoomHandler);
                image.on(events[isMobile].mouseover, hoverPointer);
				image.on(events[isMobile].mouseout, resetPointer);

				contentLayer.draw();
			}

			imageObj.src = levelManager.getRoute(picture);
    	} else if(picture!='' && text==''){
    		var imageObj = new Image();

	        imageObj.onload = function() {

				var image = new Kinetic.Image({
					x: 0,
					y: 0,
					image: imageObj,
					width: imageObj.width,
					height: imageObj.height
				});

				var scaler = calculateAspectRatioFit(image.width(), image.height(), maxWidth, maxHeight);

				image.scaleX(scaler);
				image.scaleY(scaler);

				objectGroup.width(image.width() * image.scaleX());
				objectGroup.height(image.height() * image.scaleY());

				objectGroup.centerContent();

				contentGroup.add(image);

				image.on(events[isMobile].click, imageZoomHandler);
                image.on(events[isMobile].mouseover, hoverPointer);
                image.on(events[isMobile].mouseout, resetPointer);

				contentLayer.draw();
			}

			imageObj.src = levelManager.getRoute(picture);
    	}

    	objectGroup.add(contentGroup);

    	return objectGroup;
    }

    function xmlLoadHandler(xml){

        var root = $(xml).children('component');

        levelManager.initRoute(root);

        var title = root.children('title').text();
        var question = root.children('question').text();

        mainTitle = createHead(title, question);
        
        backgroundLayer.add(mainTitle);

        var contetnClip = {
            x: 0,
            y: mainTitle.height() + 10,
            width: $(window).width() / stage.scaler,
            //height:  $(window).height() / stage.scaler - (mainTitle.height() + 10) - (mainTitle.height() + 5) - 145
            height:  $(window).height() / stage.scaler - (mainTitle.height() + 10) - (110 * mobileScaler)
        }

        buildTaskContainer();

        contentLayer.setClip(contetnClip);

        taskContainerRect.refresh();

    	scrollBar = createScrollBar(stage.getWidth() / stage.scaler - 25 * mobileScaler, contetnClip.y + 35, 20 * mobileScaler, contetnClip.height - 40, scrollGroup, contentLayer);

    	var setArray = root.children('set');

    	initTask(setArray);

    	showSet(0);

    	//showKeyboard = $(root).attr('keyboard') == 'yes' ? true : false;

        showKeyboard = isMobile;

    	buildBottomPanel(0, contetnClip.height + contetnClip.y + 5);

        initImageContainer();
    	
    	$(document).bind('keypress', keyPressHandler);

    	$(document).unbind('keydown').bind('keydown', function (event) {
		    var doPrevent = false;
		    if (event.keyCode === 8) {

		    	deleteSymbolAndGoPrev();

		    	//crossbrowser preventDefault area
		        var d = event.srcElement || event.target;
		        if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD' || d.type.toUpperCase() === 'FILE')) 
		             || d.tagName.toUpperCase() === 'TEXTAREA') {
		            doPrevent = d.readOnly || d.disabled;
		        }
		        else {
		            doPrevent = true;
		        }	        

		    } else if(event.keyCode === 46){
		    	deleteSymbolAndGoNext();
		    } /*else{
		    	//keyPressHandler(event);
		    }*/

		    if (doPrevent) {
		        event.preventDefault();
		    } else if(event.keyCode == 37){
		    	inputBox.moveCaretLeft();
		    } else if(event.keyCode == 39){		    	
		    	inputBox.moveCaretRight();
		    }
		});

        drawApp();
    }

    function drawApp(){
        preLoader.hidePreloader();
        backgroundLayer.batchDraw();
    }

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
     */
    function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 30,
                height: 30,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        //imgObj.src = skinManager.getGraphic(colorSheme.repeatIcon);

        var mainTitle = new Kinetic.Text({
            x: 35 * mobileScaler,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: 24 * mobileScaler,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: 16 * mobileScaler,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: mainTitle.height() + mainTitle.fontSize() * 0.2 + padding,
                stroke: headSheme.questionTitle.stroke
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        } else{
            titleGroup.height(titleGroup.height() + 30);
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }


    function keyPressHandler(evt){
    	var character = validateCharacter( String.fromCharCode(event.keyCode).toUpperCase() );
    	if(character != '') inputBox.pushSymbol(character);
    }

    function validateCharacter(character){
    	var ruAlphabeth = 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ ';
    	var enAlphabeth = "QWERTYUIOP[]ASDFGHJKL;'`ZXCVBNM,.";

    	if(ruAlphabeth.indexOf(character) == -1){
    		var enIndex = enAlphabeth.indexOf(character);

    		if(enIndex != -1) return ruAlphabeth[enIndex];
    	} else{
    		return character;
    	}

    	return '';
    }

    function createSymbolBox(x, y, width, height, rootSymbol, matrixX, matrixY){
    	var boxGroup = new Kinetic.Group({ x:x, y:y });
    	var boxRectangle = new Kinetic.Rect({ //прямоугольная область
	        x: 0,
	        y: 0,
	        width: width,
	        height: height,
	        fill: 'white',
	        stroke: '#99D7FE',
	        strokeWidth: 1,
	        cornerRadius: 7
	      });
    	var symbol = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: 3,
	        text: '',
	        fontSize: 18,
	        fontFamily: mainFont,
	        fill: 'black',
	        align: 'center',
	        width: width
	    });

	    boxGroup.add(boxRectangle);
	    boxGroup.add(symbol);

	    boxGroup.on(events[isMobile].click, onSymbolBoxSelect)

	    boxGroup.isRoot = false;
	    boxGroup.isFocused = false;
	    boxGroup.isActive = false;
	    boxGroup.rootSymbol = rootSymbol;

	    boxGroup.matrixX = matrixX;
	    boxGroup.matrixY = matrixY;

	    boxGroup.setActive = function(){
	    	boxRectangle.setAttrs({
	    		fill: '',
	    		fillLinearGradientStartPoint: {x:width/2, y:0},
		        fillLinearGradientEndPoint: {x:width/2,y:height},
		        fillLinearGradientColorStops: [0, '#99D7FE', 1, '#D9F0FE']
	    	});
	    	boxGroup.isActive = true;
	    	
	        contentLayer.draw();
	    }

	    boxGroup.focusWord = function(invokerX, invokerY, forceOrientation){

	    	forceOrientation = forceOrientation || -1;

	    	var x = boxGroup.matrixX;
	    	var y = boxGroup.matrixY;

	    	var orientation;

	    	if(forceOrientation!=-1){
	    		orientation = forceOrientation;
	    	} else if(invokerX == x && invokerY == y){
	    		orientation = (typeof boxGroup.linkedWord[0]!='undefined') ? 0 : 1;
	    	} else{
	    		orientation = (invokerX == x) ? 1 : 0;
	    	}

	    	boxGroup.focusedOrientation = orientation;    	

	    	for(var i = 0; i<boxGroup.linkedWord[orientation]; i++){
	    		if(orientation==0){
	    			x = i + boxGroup.matrixX;
	    		} else{
	    			y = i + boxGroup.matrixY;
	    		}
	    		if(!crossTileMatrix[x][y].isFocused){
	    			crossTileMatrix[x][y].setActive();
	    		} else{
	    			crossTileMatrix[x][y].isActive = true;
	    		}
	    	}

	    	boxGroup.tipLink[orientation].selectTip();
	    }

	    boxGroup.setFocused = function(focusWordFlag){
	    	boxRectangle.setAttrs({
	    		fill: '',
	    		fillLinearGradientStartPoint: {x:width/2, y:0},
		        fillLinearGradientEndPoint: {x:width/2,y:height},
		        fillLinearGradientColorStops: [0, '#33DB77', 1, '#D9F0FE']
	    	});

	    	boxGroup.isFocused = true;
	    	if(focusWordFlag){
		    	if(!boxGroup.isRoot){
	    			if(targetRoot!=null) targetRoot.unFocusWord();

	    			boxGroup.rootSymbol.focusWord(boxGroup.matrixX, boxGroup.matrixY);

	    			targetRoot = boxGroup.rootSymbol;
		    	} else{

		    		if(targetRoot!=null) targetRoot.unFocusWord();

		    		boxGroup.focusWord(boxGroup.matrixX, boxGroup.matrixY);

		    		targetRoot = boxGroup;
		    	}
		    }

	        contentLayer.draw();
	    }

	    function scrollToTile(tile, orientation){
	    	var position = {x: 0, y: 0};
	    	var area = contentLayer.getClip();

	    	position.x = tile.x() + crossWordGroup.x(); //CAUTION!! DANGER TEST ZONE!!!
			position.y = tile.y() + crossWordGroup.y();

			if(orientation == 0){
				if(position.x > (area.x + area.width - 25)){
					crossWordGroup.x(crossWordGroup.x() - 50);
				} else if(position.x < (area.x + 25)){
					crossWordGroup.x(crossWordGroup.x() + 50);
				}
			} else{
				if(position.y > (area.y + area.height - 25)){
					crossWordGroup.y(crossWordGroup.y() - 50);
				} else if(position.y < (area.y + 25)){
					crossWordGroup.y(crossWordGroup.y() + 50);
				}
			}
	    }

	    boxGroup.next = function(){
	    	var orientation = targetRoot.focusedOrientation;
	    	var x = orientation == 0 ? boxGroup.matrixX+1 : boxGroup.matrixX;
	    	var y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + 1;

	    	if(typeof crossTileMatrix[x]!='undefined'){
	    		if(typeof crossTileMatrix[x][y]!='undefined'){
	    			setTargetBox(crossTileMatrix[x][y], false);
	    			scrollToTile(crossTileMatrix[x][y], orientation);
	    		}
	    	}
	    }
	    boxGroup.prev = function(){
	    	var orientation = targetRoot.focusedOrientation;
	    	var x = orientation == 0 ? boxGroup.matrixX - 1 : boxGroup.matrixX;
	    	var y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY - 1;

	    	if(!(boxGroup.matrixX == targetRoot.matrixX && boxGroup.matrixY == targetRoot.matrixY)){
	    		setTargetBox(crossTileMatrix[x][y], false);
	    		scrollToTile(crossTileMatrix[x][y], orientation);
	    	}
	    }

	    boxGroup.unFocusWord = function(){
	    	var x = boxGroup.matrixX;
	    	var y = boxGroup.matrixY;

	    	for(var i = 0; i<boxGroup.linkedWord[boxGroup.focusedOrientation]; i++){
	    		if(boxGroup.focusedOrientation==0){
	    			x = i + boxGroup.matrixX;
	    		} else{
	    			y = i + boxGroup.matrixY;
	    		}

    			if(!crossTileMatrix[x][y].isFocused){
    				crossTileMatrix[x][y].setBlank();
    			}
	    	}
	    }

	    boxGroup.setBlank = function(){
	    	boxRectangle.setAttrs({
	    		fill: 'white'
	    	});

	    	isActive = false;
	    	isFocused = false;

	    	//console.log(boxGroup)
	    	
	    	contentLayer.draw();
	    }

	    boxGroup.setUnfocused = function(){

	    	//console.log(boxGroup);

	    	if(!boxGroup.isActive){
		    	setBlank();
		    } else{
		    	boxGroup.setActive();
		    }
		    boxGroup.isFocused = false;
		    contentLayer.draw();
	    }
	    boxGroup.setWrong = function(){
	    	boxRectangle.setAttrs({
	    		fill: '#FF7B7B',
	    		stroke: '#FF0033'
	    	});
	    	
	        contentLayer.draw();
	    }

	    boxGroup.setValue = function(value){
	    	var regExp = /[А-Я, A-Z]/
	    	if(regExp.test(value)){
		    	symbol.text(value);
		    	boxGroup.next();
	    		contentLayer.draw();
	    	}
	    }

	    boxGroup.getValue = function(){
	    	return symbol.text();
	    }

	    boxGroup.clear = function(value){
	    	symbol.text('');
	    	contentLayer.draw();
	    }

	    boxGroup.setTipLink = function(orientation, tipLink){
	    	boxGroup.tipLink[orientation] = tipLink;
	    }

	    boxGroup.setRoot = function(index){
	    	boxGroup.isRoot = true;
	    	boxGroup.rootIndex = index;

	    	boxGroup.linkedWord = [];
	    	boxGroup.tipLink = [];
	    	boxGroup.focusedOrientation = 0;

	    	var index = new Kinetic.Text({ //текстовая метка
		        x: 0,
		        y: 0,
		        text: index,
		        fontSize: 12,
		        fontFamily: mainFont,
		        fill: 'black',
		        align: 'left',
		        width: width
		    });

	    	boxGroup.add(index);
	    	contentLayer.draw();
	    }

	    boxGroup.getRootIndex = function(){
	    	return boxGroup.rootIndex;
	    }

	    boxGroup.linkWord = function(orientation, length){
	    	boxGroup.linkedWord[orientation] = length;
	    }

	    boxGroup.getRootWord = function(orientation){
	    	var result = '';

	    	var x;
	    	var y;

	    	for(var i = 0; i<boxGroup.linkedWord[orientation]; i++){

	    		x = orientation == 0 ? boxGroup.matrixX+i : boxGroup.matrixX;
	    		y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + i;

	    		result += crossTileMatrix[x][y].getValue();
	    	}

	    	return result;

	    }

	    /*contentLayer.add(boxGroup);

	    contentLayer.draw();*/

	    return boxGroup;
    }

    function onSymbolBoxSelect(evt){
    	setTargetBox(evt.targetNode.getParent(), true);
    }

    function buildCrossWord(x, y, words){
    	crossWordGroup = new Kinetic.Group({x: x, y: y});
    	var wordIndex = 1;

    	words.each(function(index){
    		wordIndex = createWord(this, wordIndex);
    	})

    	tooltipBox.buildLabels();

    	contentLayer.add(crossWordGroup);
    	contentLayer.draw();
    }

    function createWord(word, wordIndex){
    	var symbolBoxWidth = 25;
    	var symbolBoxHeight = 25;

    	var marginLeft = 25;
    	var marginTop = 25;

    	var spacer = 3;

    	var isHorizontal = $(word).attr('orientation')=='down' ? false : true;

    	var firstSymbolX = +$(word).attr('x');
    	var firstSymbolY = +$(word).attr('y');

    	var toolTip = $(word).children('question').text();
    	var tipImage = $(word).children('question').attr('picture');

    	var wordString = $(word).children('string').text();

    	var dx = marginLeft;
    	var dy = marginTop;

    	var matrixX;
    	var matrixY;

    	var rootSymbol = null;

    	for(var i = 0; i<wordString.length; i++){

    		matrixX = isHorizontal ? firstSymbolX + i : firstSymbolX;
    		matrixY = isHorizontal ? firstSymbolY : firstSymbolY + i;

    		if(typeof crossTileMatrix[matrixX] == 'undefined'){
	    		crossTileMatrix[matrixX] = [];
	    	}

	    	if(typeof crossTileMatrix[matrixX][matrixY] == 'undefined'){
	    		crossTileMatrix[matrixX][matrixY] = createSymbolBox(dx + (symbolBoxWidth + spacer) * matrixX, dy + (symbolBoxHeight + spacer) * matrixY, symbolBoxWidth, symbolBoxHeight, rootSymbol, matrixX, matrixY);

	    		crossWordGroup.add(crossTileMatrix[matrixX][matrixY]);
	    	}

	    	if(i == 0){
    			rootSymbol = crossTileMatrix[matrixX][matrixY];
    			if(!rootSymbol.isRoot){
    				rootSymbol.setRoot(wordIndex);
    				wordIndex++;
    			}
    			rootSymbol.linkWord(isHorizontal ? 0 : 1, wordString.length);
    		}
    	}
    	wordsArray[wordString] = {root: rootSymbol, orientation: isHorizontal ? 0 : 1};

    	toolTip = (isHorizontal ? 'По горизонтали:\n' : 'По вертикали:\n') + rootSymbol.getRootIndex() + '. ' + toolTip;    	

    	tooltipBox.pushLabel(toolTip, rootSymbol, isHorizontal ? 0 : 1, tipImage);    	

    	return wordIndex;
    }

    function createIconButton(x, y, width, height, iconGraph, callback){
        var iconBtn = new Kinetic.Group({
            x: x,
            y: y
        });

        var imgObj = new Image();

        imgObj.onload = function(){
            var img = new Kinetic.Image({
                x: 0,
                y: 0,
                width: width,
                height: height,
                image: imgObj
            });

            iconBtn.add(img);

            iconBtn.getParent().draw();
        }

        imgObj.src = skinManager.getGraphic(iconGraph);

        iconBtn.on(events[isMobile].click, callback);
        iconBtn.on(events[isMobile].mouseover, hoverPointer);
        iconBtn.on(events[isMobile].mouseout, resetPointer);

        return iconBtn;
    }

    function createKeyboardBlock(x, y, width, height, label, handler){
        var keyboardBlock = new Kinetic.Group({x: x, y: y});

        handler = handler || keyboardBlockClickHandler;

        var keyboardSheme = colorSheme.keyboard;

        var rect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: height,
            stroke: keyboardSheme.stroke,
            fill: keyboardSheme.backgroundColor,
            cornerRadius: keyboardSheme.cornerRadius,
            strokeWidth: keyboardSheme.strokeWidth
        })

        var symbol = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 7 * mobileScaler,
            text: label,
            fontSize: 26 * mobileScaler,
            fontFamily: mainFont,
            fill: 'black',
            align: 'center',
            width: width
        });

        keyboardBlock.getValue = function(){
            return symbol.text();
        }

        keyboardBlock.on(events[isMobile].mouseover, hoverPointer);
        keyboardBlock.on(events[isMobile].mouseout, resetPointer);

        keyboardBlock.add(rect);
        keyboardBlock.add(symbol);

        keyboardBlock.on(events[isMobile].click, handler)

        function keyboardBlockClickHandler(evt){
            var target = evt.targetNode.getParent();
            inputBox.pushSymbol(target.getValue());
        }

        return keyboardBlock;
    }

    function buildKeyBoard(x, y){
        var keyboard = new Kinetic.Group({x: x, y: y});

        var keyboardSheme = colorSheme.keyboard;

        var backgroundRect = new Kinetic.Rect({
            x: - x,
            y: - 5,
            width: $(window).width() / stage.scaler,
            height: 135,
            fill: 'black',
            opacity: 0.8,
            stroke: keyboardSheme.rectStroke,
            strokeWidth: keyboardSheme.rectStrokeWidth
        });

        backgroundRect.on(events[isMobile].click, toggleKeyboard);

        //keyboard.add(backgroundRect);

        var alphabetString = 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ ';

        var keyWidth = 38 * mobileScaler;
        var keyHeight = 40 * mobileScaler;

        var padding = 5;

        var dx = 0;
        var dy = 3;

        for(var i = 0; i<alphabetString.length; i++){
            keyboard.add( createKeyboardBlock(dx, dy, keyWidth, keyHeight, alphabetString[i]) );
            dx += keyWidth + padding;
            if((i+1)%12==0){
                dy+=keyHeight + padding;
                dx = 0;
            }
        }

        keyboard.add( createKeyboardBlock(dx, dy, keyWidth, keyHeight, 'del', deleteSymbolAndGoNext) );

        dx += keyWidth + padding;

        keyboard.add( createKeyboardBlock(dx, dy, keyWidth, keyHeight, '←', deleteSymbolAndGoPrev) );

        keyboard.width(12*keyWidth);
        keyboard.height(3*keyHeight + 2*padding);

        keyboard.toggle = function(){
            if(keyboard.isVisible()){
                keyboard.hide();
            } else{
                keyboard.show();
            }

            //resultLayer.draw();
        }

        keyboard.hide();

        return keyboard;
    }

    function deleteSymbolAndGoNext(){
    	inputBox.deleteNextSymbol();
    }
    function deleteSymbolAndGoPrev(){
    	inputBox.popSymbol();
    }

    function setTargetBox(currentBox, focusWordFlag){
    	if(!currentBox.isFocused){
	    	if(targetBox != null) targetBox.setUnfocused();

	    	currentBox.setFocused(focusWordFlag);

	    	targetBox = currentBox;
	    }
    }

    function answerButtonHandler(evt){
    	var button = evt.targetNode.getParent();
    	var isWin = (setArray[currentSetIndex].answer == inputBox.getValue());

    	resultPanel.hideResult();

    	resultPanel.showResult(isWin);

    	if(!isWin){

    		resultPanel.showResult(isWin);

	    	//button.setText('Ещё раз');
	    	//button.off(events[isMobile].click);
	    	//button.on(events[isMobile].click, clear);


	    } else if((currentSetIndex + 1)<setArray.length){



            button.off(events[isMobile].click);

	    	showBonus(function(){

                button.setText('Дальше'); 

                button.on(events[isMobile].click, function(){

                    resultPanel.hideResult();

                    currentScreenObject.hide();

                    currentSetIndex++;
                    showSet(currentSetIndex);

                    inputBox.resetValue();

                    button.setText('Проверить');
                    button.off(events[isMobile].click);
                    button.on(events[isMobile].click, answerButtonHandler);

                    backgroundLayer.draw();
                });
            });
	    } else{

            button.off(events[isMobile].click);

	    	showBonus(function(){
                button.setText('Ещё раз');                
                button.on(events[isMobile].click, clear);
            });
	    }

    	backgroundLayer.draw();
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback, sheme){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 26 * mobileScaler,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        backgroundLayer.add(buttonGroup);
        backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mousedown, resetPointer);

        return buttonGroup;
    }

    function buildResultLabel(x, y, width, height){
        var resultGroup = new Kinetic.Group({
            x: x,
            y: y
        });

        var resultBackground = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fill:'white',
            //stroke: 'black',
            strokeWidth: 1.5,
            //cornerRadius: 8
          });

    	var label = new Kinetic.Text({ //текстовая метка
	        x: 0,
            y: 3,
            width: resultBackground.width(),
            height: resultBackground.height(),
            text: '',
            fontSize: 26 * mobileScaler,
            fontFamily: mainFont,
            fill: 'white',
            align: 'center'
	      });

        resultGroup.add(resultBackground);
    	resultGroup.add(label);

        resultGroup.hide();

        backgroundLayer.add(resultGroup);

    	resultGroup.showResult = function(isWin){
    		resultGroup.show();
    		label.text(isWin ? 'Правильно' : 'Неправильно');
    		resultBackground.fill(isWin ? 'green' : 'red');
    		backgroundLayer.draw();
    	}

    	return resultGroup;
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function createScrollArrow(x, y, type){
		var imageObj = new Image();

        imageObj.onload = function() {

			var arrow = new Kinetic.Sprite({
				x: x,
				y: y,
				image: imageObj,
				animation: type,
				animations: {
					up: [
					  // x, y, width, height
					  0,0, 131, 130
					],
					down: [
					  // x, y, width, height
					  131,0,131,130
					],
					left: [
					  // x, y, width, height
					  0,130,131,130
					],
					right: [
					  // x, y, width, height
					  131,130,131,130
					]
				},
				frameRate: 7,
				frameIndex: 0			
			});

			arrow.type = type;

			arrow.on(events[isMobile].click, scrollContent)

			arrowLayer.add(arrow);
			arrowLayer.draw();
		}

		imageObj.src = 'assets/scrollArrow.png';

		function scrollContent(evt){
			var direction = evt.targetNode.type;
			var speed = 30;
			var dx = 0;
			var dy = 0;

			switch(direction){
				case 'left': dx += speed;
					break;
				case 'right':  dx -= speed;
					break;
				case 'up':  dy += speed;
					break;
				case 'down': dy -= speed;
					break;
			}

			crossWordGroup.x(crossWordGroup.x() + dx);
			crossWordGroup.y(crossWordGroup.y() + dy);

			contentLayer.draw();


			//console.log('x ' + crossWordGroup.x())
		}
	}

	function buildTooltipBox(){
		var clip = tooltipBoxLayer.getClip();
		var tooltipBox = new Kinetic.Group({x: clip.x, y: clip.y});
		var horizontalSpacer = 10;
		var labelArray = [];

		var activeTooltip = null;

		tooltipBox.totalHeight = 10;
		tooltipBox.scrollBar = false;

		var rectArea = new Kinetic.Rect({
			x: clip.x - 10,
			y: clip.y - 10,
			width: clip.width + 20,
			height: clip.height + 20,
			stroke: 'black'
		});	

		tooltipBox.pushLabel = function(tipString, rootSymbol, orientation, imageSrc){
			labelArray.push({index: rootSymbol.getRootIndex(), orientation: orientation, tipString:tipString, rootSymbol:rootSymbol, image: imageSrc});
		}
		tooltipBox.buildLabels = function(){
			labelArray.sort(sortRule);
			for(var i = 0; i<labelArray.length; i++){
				labelArray[i].rootSymbol.setTipLink( labelArray[i].orientation, tooltipBox.createLabel(labelArray[i].tipString, labelArray[i].rootSymbol, labelArray[i].orientation, labelArray[i].image)  );
			}
		}

		function sortRule(a, b){
			if(a.index > b.index){
				return 1;
			} else if(a.index < b.index){
				return -1;
			} else if(a.index == b.index){
				if(a.orientation == 0){
					return -1;
				} else {
					return 1;
				}
			}

			return 0;
		}

	    tooltipBox.createLabel = function(tipString, rootSymbol, orientation, imageSource){
	    	var toolTip = new Kinetic.Group({x:0, y:tooltipBox.totalHeight})
	    	var label = new Kinetic.Text({ //текстовая метка
		        x: 30,
		        y: 0,
		        text: tipString,
		        fontSize: 20,
		        fontFamily: mainFont,
		        fill: 'black',
		        width: clip.width - 50
		    });
		    var labelRect = new Kinetic.Rect({ //текстовая метка
		        x: 5,
		        y: 0,
		        width: clip.width - 40,
		        height: label.height()+20,
		        strokeWidth: 0,
		        fill: 'white'
		    });

		    //console.log(image + ' ' + labelRect.height());

		    if(imageSource!='' && typeof imageSource != undefined){

		    	var imgObj = new Image();

				imgObj.onload = function(){
					var image = new Kinetic.Image({
					  x: 30,
					  y: label.height() + 5,
					  image: imgObj,
					  width: imgObj.width,
					  height: imgObj.height
					});

					var scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, 200, 200);

					image.scaleX(scaler);
					image.scaleY(scaler);
					image.scaler = scaler;

					image.defaultPositionX = image.x();
					image.defaultPositionY = image.y();

					image.on(events[isMobile].click, imageZoomHandler);
					image.isZoomed = false;

					labelRect.height(labelRect.height() + 200)

				    toolTip.add(labelRect);
				    toolTip.add(label);
				    toolTip.add(image);				    
			   		tooltipBox.add(toolTip);
				    tooltipBoxLayer.draw();

				    
				};
				imgObj.src = levelManager.getRoute(imageSource);

				/*Скалирует изображения по клику**/
				function imageZoomHandler(evt){
					var currentImg = evt.targetNode;
					imageContainer.zoomImage(currentImg.getImage());
				}

		    	tooltipBox.totalHeight += label.height() + 220;

		    	if(tooltipBox.totalHeight>(clip.height-20)) showScrollBar();

		    } else{
			    toolTip.add(labelRect);
			    toolTip.add(label);
			    tooltipBox.add(toolTip);
			    tooltipBoxLayer.draw();

			    tooltipBox.totalHeight += label.height() + horizontalSpacer;
		    }

		    if(tooltipBox.totalHeight>(clip.height-20)) showScrollBar();

		    toolTip.rootSymbol = rootSymbol;
		    toolTip.isFocused = false;
		    toolTip.labelRect = labelRect;

		    toolTip.on(events[isMobile].click, toolTipClickHandler);

		    toolTip.focus = function(){
		    	labelRect.setAttrs({
		    		fill: '',
		    		fillLinearGradientStartPoint: {x:labelRect.width()/2, y:0},
			        fillLinearGradientEndPoint: {x:labelRect.width()/2,y:labelRect.height()},
			        fillLinearGradientColorStops: [0, '#99D7FE', 1, '#D9F0FE']
		    	});
		    	tooltipBoxLayer.draw();

		    	toolTip.isFocused = true;
		    }
		    toolTip.unFocus = function(){
		    	labelRect.setAttrs({
		    		fill: 'white'
		    	});
		    	tooltipBoxLayer.draw();
		    	toolTip.isFocused = false;
		    }

		    toolTip.selectTip = function(){
		    	if(!toolTip.isFocused){
			    	toolTip.focus();

					if(activeTooltip!=null) activeTooltip.unFocus();

					activeTooltip = toolTip;
				}
		    }		    

		    function toolTipClickHandler(evt){
				var toolTip = evt.targetNode.getParent();
				toolTip.selectTip();

		    	setTargetBox(toolTip.rootSymbol, false);

		    	if(targetRoot!=null) targetRoot.unFocusWord();

				rootSymbol.focusWord(rootSymbol.matrixX, rootSymbol.matrixY, orientation);

				targetRoot = rootSymbol;

				centerSymbolBox(targetRoot);
			}

			return toolTip;
	    }

		backgroundLayer.add(rectArea);

		tooltipBoxLayer.add(tooltipBox);
		tooltipBoxLayer.draw();

		function showScrollBar(){
			if(!tooltipBox.scrollBar){
				createScrollBar(clip.x + clip.width-25, clip.y + 30, clip.height-60, tooltipBox);
				tooltipBox.scrollBar = true;
			}
		}
		

		return tooltipBox;
	}

	function calculateAlphabetWidth(tempLabel){
		var alphabetString = 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ ';
		var widthArray = [];

		for(var i = 0; i<alphabetString.length; i++){
			tempLabel.text(alphabetString[i]);
			widthArray[alphabetString[i]] = tempLabel.width();
		}

		return widthArray;
	}

	function wheelScroll(evt){
    	scrollBar.scrollBy(-evt.originalEvent.wheelDelta/12);
    }

    function createScrollBar(x, y, width, height, contentGroup, clippedLayer){
        var scrollObject = new Kinetic.Group();

        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Kinetic.Rect({
           x: x,
           y: y,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11 + y;

        var vscroll = new Kinetic.Group({
            x: x + 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart*stage.scaleY()){
                    newY = scrollStart*stage.scaleY();
                }
                else if(newY > scrollEnd*stage.scaleY()) {
                    newY = scrollEnd*stage.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                }
            }
          });

          var scrollCircle = new Kinetic.Group({
            x: (width >> 1) - 9.5,
            y: 0
          });

          var backgroundCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: width >> 1,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: backgroundCircle.radius() * 0.6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          }

          var scrollTopArrowGroup = new Kinetic.Group({
            x: x,
            y: y
          })

          var scrollTopArrow = new Kinetic.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Kinetic.Group({
            x: x,
            y: y + height
          });

          var scrollBotArrow = new Kinetic.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Kinetic.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          contentScrollLayer.add(scrollObject);

          contentScrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            contentScrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            contentScrollLayer.drawScene();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
            scrollTopArrowGroup.hide();
            scrollBotArrowGroup.hide();
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
            scrollTopArrowGroup.show();
            scrollBotArrowGroup.show();
          }

          scrollObject.refresh = function(){
            var clip = clippedLayer.getClip();

            vscrollArea.y(clip.y + 5);
            vscrollArea.height(clip.height - 10);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            scrollTopArrowGroup.y(clip.y + 5);
            scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            contentGroup.y(0);
            vscroll.y(scrollStart);

            console.log(clippedLayer.getClip().height, contentGroup.height());

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
            } else{
                scrollObject.show();
            }

            contentScrollLayer.draw();
          }

          clippedLayer.batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.batchDraw()
          }
          return scrollObject;
    }

	function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

    function initImageContainer(){
        var image = new Kinetic.Image({
          x: 20,
          y: 20
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler
        });

        var modalGroup = new Kinetic.Group();

        modalRect.opacity(0.8);

        image.on(events[isMobile].mouseover, hoverPointer);
        image.on(events[isMobile].mouseout, resetPointer);

        image.hide();
        modalRect.hide();

        modalGroup.add(modalRect);
        modalGroup.add(image);

        imageContainer.add(modalGroup);

        imageContainer.zoomImage = function(imageObj){

            image.setImage(imageObj);

            var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight-40);

            image.width(imageObj.width * scaler);
            image.height(imageObj.height * scaler);

            if(!image.isVisible()){
                image.x(defaultStageWidth >> 1);
                image.y((defaultStageHeight - screenMarginY) >> 1);

                image.scale({
                    x: 0,
                    y: 0
                })

                var inTween = new Kinetic.Tween({
                    node: image,
                    scaleX: 1,
                    scaleY: 1,
                    x: (($(window).width() / stage.scaler) >> 1) - (image.width() >> 1),
                    y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (image.height() >> 1),
                    duration: 0.4,
                    easing: Kinetic.Easings.EaseInOut
                })
                inTween.onFinish = function(){
                    //modalRect.show();
                }

                modalRect.show();

                image.show();
                inTween.play();
            }

            modalGroup.on(events[isMobile].click, zoomOut);

            imageContainer.draw();
        }

        function zoomOut(evt){

            var outTween = new Kinetic.Tween({
                node: image,
                scaleX: 0,
                scaleY: 0,
                x: defaultStageWidth>>1,
                y: defaultStageHeight>>1,
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            outTween.onFinish = function(){
                image.hide();
                imageContainer.draw();
            }

            modalRect.hide();

            outTween.play();
        }
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height,
                scaleX: 1 / skinManager.getScaleFactor(),
                scaleY: 1 / skinManager.getScaleFactor()
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            }

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
                //backgroundLayer.draw();
            }

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        }

        imgObj.src = skinManager.getGraphic('preLoader.png');

        return preloaderObject;
    }

});