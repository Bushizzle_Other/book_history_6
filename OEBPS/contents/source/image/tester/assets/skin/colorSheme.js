var colorSheme = {
	"scrollBar":{
		"trackColor": "#87D0E7",
		"arrowColor": "#87D0E7",
		"innerSliderColor": "#87D0E7",
		"outerSliderColor": "#87D0E7"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#00A8D4",
		"strokeWidth": 0,
		"cornerRadius": 0
	},
	"radioButtonGraphic": "radioButtonBlueSheme.png",
	"checkBoxGraphic": "checkBox.png",
	"taskBoxGraphic": "taskLabelBlueSheme.png",	
	"player":{
		"backgroundRectColor": "black",
		"trackBackroundColor": "#4B4B4B",
		"trackColor": ["white", "white"],
		"opacity": 0.7
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#dfedf5",
			"strokeColor": "#c3e7f3"
		},
		"innerRect":{
			"backgroundColor": "white",
			"strokeColor": ""
		},
		"selected":{
			"backgroundColor": "#dfedf5",
			"strokeColor": "#c3e7f3"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#7FD3E9",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#7FD3E9"
		}
	}
}