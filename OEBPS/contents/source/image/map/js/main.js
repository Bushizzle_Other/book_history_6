$(window).load(function () {

    Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

     function getInternetExplorerVersion(){
        var rv = -1;
        if (navigator.appName == 'Microsoft Internet Explorer'){
            var ua = navigator.userAgent;
            var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
            if (re.exec(ua) != null)
              rv = parseFloat( RegExp.$1 );
        }
        else if (navigator.appName == 'Netscape'){
        var ua = navigator.userAgent;
        var re  = new RegExp("Trident/.*rv:([0-9]{1,}[\.0-9]{0,})");
        if (re.exec(ua) != null)
          rv = parseFloat( RegExp.$1 );
        }
        return rv;
    }

    /*function reorient(e) {
      //window.location.href = window.location.href;

      alert('onorientationchange');
    }

    window.onorientationchange = reorient;*/
    //window.addEventListener("message", receiveMessage, false);

    externalInterface.resize = fitStage;

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    if(isMobile) Konva.pixelRatio = 1;

    var isSVG = Modernizr.svg && !(/iPhone|iPad|iPod/i.test(navigator.userAgent));
    var isSupportWorkers = Modernizr.webworkers;

    var ieVersion = getInternetExplorerVersion();

    var mainFont = 'mainFont';

    /*if(ieVersion > 8){
        isSVG = false;
    } else if(ieVersion != -1){
        window.location.href = 'browserError.html';
    }*/

    //window.location = 'browserError.html';
    var isIos = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var renderType = 1;

    //if(isMobile&&isSVG) renderType = 2; //!!!11

    //renderType = 1;

    //renderType = 2;
    
	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';

	/*Глобальные переменные**/
	
	var COLUMN_COUNT;

	/*Глобальный флаг, характеризующий движение мыши с зажатой кнопкой по канве**/
	var moving = false;	
	/*Отпустили ли левую кнопку мыши над узлом**/
	var isMouseUpOnNode = false;
	/*Узел из которого в данный момент рисуется связь**/
	var currentNode = null;
	/*Текущие связи, заполняются пользователем**/
	var connections = [];
	/*Связи-ответы, берутся из xml**/
	var answers = [];
	/*Отрисовываемая в данный момент линия**/
	var currentLine = null;

    var allowReAnswer = false;

	var contentWidth = 0;
	var contentHeight = 0;

	var zoomLevel = 0;
    var maxZoomLevel = 0.5;

	var layerCollection = [];

	var layerArray = [];

    var assetOqueue = [];

	var aspectRatio = 0; //widescreen 16:9

    var defaultStageWidth = 1024;
    var defaultStageHeight = 576;

    var defaultContentWidth = 0;
    var defaultContentHeight = 0;

    var mapMargin = 0;

    var zoomCounter = null;

    var viewContainer = null;

    var taskPanel = null;

    var taskArray = [];

    var defaultScaler = 0;

	var map = {};

    var points = null;

    var controlls = null;

    var preventActions = false;

    var answerPointHitScaler = 1.5;

    var sourceGroup = new Konva.Group();

    var isLayersEditable = true;
    var showScaleTipFlag = true;

    var maxLinkCount = 0;

    var expandedContainer = null;

    var controllsPanel = null;

    var layerPanel = null;

    var taskEnabled = true;

    var iconPadding = 5;

    var fullscreenContainer = null;

    var levelManager = new LevelManager();

    var root = '';

    var showHistory = false;

    //var mobileScaler = isMobile ? 1.5 : 1;
    var mobileScaler = 1;

    var controllsGraphic = null;

    var pointAssets = [];

    var legendSource = '';

    var sliceZeroPath = false;

    var isAppInited = false;

    var layersFolder = 'layers';

    var centerPoints = false;

    var drawMap = null;

    var defaultLayerFolder = '../map/layers';

    var levelLayerCount = 0;

    var titleScreenGroup = null;

    var cachedSceneCanvas = null;

	var stage = new Konva.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

    if(ieVersion == '11'){
        var hiddenDiv = document.createElement("div");        
        document.body.appendChild(hiddenDiv);
        hiddenDiv.classList.add("hidden");
    }

    /*$('canvas').bind('mousedown touchstart', function(e) {
        e.preventDefault();
    }*/

    //$(stage).on('selectstart', function(){alert('hallo')})
    /* = function(e) {
      e.preventDefault();
    }*/

	var draggableLayer = new Konva.Layer({
			draggable: true,
			dragBoundFunc: function(pos) {
				var newX = pos.x;
				var newY = pos.y;

				if(Math.abs(pos.x) > contentWidth - stage.width() * stage.scaleX()) newX = - (contentWidth - stage.width() * stage.scaleX());
				if(Math.abs(pos.y) > contentHeight - stage.height() * stage.scaleY()) newY = - (contentHeight - stage.height() * stage.scaleY());

				if(pos.x > 0) newX = 0;
				if(pos.y > 0) newY = 0;

                if(contentWidth < stage.width() * stage.scaleX()) newX = 0;
                if(contentHeight < stage.height() * stage.scaleY()) newY = 0;


				//if(pos.y > contentHeight - defaultStageHeight) newY = contentHeight - defaultStageHeight;

	            //var newX = pos.x < 50 ? 50 : pos.x;
	            //var newY = pos.y < 50 ? 50 : pos.y;

	            return {
	              x: newX,
	              y: newY
	            };
	          }
		});

    var answerPointLayer = new Konva.Layer();
    var controllsLayer = new Konva.Layer();
    var contentScrollLayer = new Konva.Layer();
    var tipLayer = new Konva.Layer();
    var fullscreenLayer = new Konva.Layer();
    var fullscreenScrollLayer = new Konva.Layer();
    var imageContainer = new Konva.Layer();

    stage.add(draggableLayer);
   	stage.add(answerPointLayer);
    stage.add(controllsLayer);
    stage.add(contentScrollLayer);
    stage.add(tipLayer);
    stage.add(fullscreenLayer);
    stage.add(fullscreenScrollLayer);
    stage.add(imageContainer);

    var answerPointsGroup = new Konva.Group();
    answerPointLayer.add(answerPointsGroup);

    var contentGroup = new Konva.Group();

	//$(window).on('resize', fitStage);

	function fitStage(){
		scaleScreen($(window).width(), $(window).height(), true);
        //map.rebuildContent(stage.width() * stage.scaleX(), stage.height() * stage.scaleY());
        if(zoomCounter) zoomCounter.refreshTip();
	}

	function scaleScreen(width, height, isResize){
		var marginX = 0;
		var marginY = 0;

		width -= marginX;
		height -= marginY;

		var scalerX = width/defaultStageWidth;
		var scalerY = height/defaultStageHeight;
		var scaler = Math.min(scalerX, scalerY);

        stage.scaler = scaler;

        controllsLayer.scale({
            x: scaler,
            y: scaler
        });

        contentScrollLayer.scale({
            x: scaler,
            y: scaler
        });

        fullscreenLayer.scale({
            x: scaler,
            y: scaler
        });

        fullscreenScrollLayer.scale({
            x: scaler,
            y: scaler
        });

        stage.dx = (width - defaultStageWidth * scaler)/scaler;
        stage.dy = (height - defaultStageHeight * scaler)/scaler;

        if(controlls != null){

            //controlls.fitToStage(stage.dx, stage.dy);
        }

        if(isResize){
            /*scalerX = stage.width() / width;
            scalerY = stage.height() / height;

            scaler = Math.min(scalerX, scalerY);

            draggableLayer.scale({
                x: scaler,
                y: scaler
            })*/

            resizeMap(width, height);

            return;

            //centerMap();
        }

        stage.width(width);
        stage.height(height);
	}

    function initAspectRatio(){
        if($(window).width() / $(window).height() < 1.6){
            aspectRatio = 1;
            defaultStageWidth = 1024;
            defaultStageHeight = 768;

            stage.width(defaultStageWidth);
            stage.width(defaultStageHeight);
        }
    }

	function initWindowSize(){
		scaleScreen($(window).width(), $(window).height());
	}

    var colorSheme = null;

    skinManager = new SkinManager('assets/skin', function(){
        initAspectRatio();
        initWindowSize();

        setTimeout(initApplication, 300);
    });

    function initApplication(){
        preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));
        
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    }

    function resizeMap(width, height){

        if(titleScreenGroup){
            titleScreenGroup.resizeTitle(); //rewrite this shit!
            stage.width(width);
            stage.height(height);
            return;
        }

        if(layerArray.length < 1) return;

        var scaler = calculateAspectRatioFit(defaultContentWidth, defaultContentHeight, width, height);

        if(renderType == 1) {
            contentGroup.scale({
                x: scaler,
                y: scaler
            });
        } else if(renderType == 0){
            contentGroup.sourceGroup.scale({
                x: scaler,
                y: scaler
            })
        }

        defaultScaler = scaler;

        resetZoom();

        draggableLayer.x(0);
        draggableLayer.y(0);
        answerPointLayer.x(0);
        answerPointLayer.y(0);

        centerMap(width);

        controllsPanel.onSizeChange(width, height);

        var answerCollection = answerPointsGroup.children;

        for(var i = 0; i < answerCollection.length; i++){
            answerCollection[i].x(answerCollection[i].defaultX * defaultScaler * (1 + zoomLevel));
            answerCollection[i].y(answerCollection[i].defaultY * defaultScaler * (1 + zoomLevel));

            if(centerPoints){
                answerCollection[i].x(answerCollection[i].x() - (answerCollection[i].width() >> 1));
                answerCollection[i].y(answerCollection[i].y() - (answerCollection[i].height() >> 1));
            }
        }

        fullscreenContainer.resizeContent(width, height);

        //centerMap();

        stage.width(width);
        stage.height(height);

        //drawMap();

        /*for(var i = 0; i < layerArray.length; i++){
            layerArray[i].asset
        }*/
    }

    /*Заливает фон белым цветом**/
    function createBackground(){
    	var background = new Konva.Rect({
            x: 0, 
            y: 0, 
            width: stage.getWidth(),
            height: stage.getHeight(),
            fill: "white"
        });

        backgroundLayer.add(background);
    }
    /*Конструирует кнопку "Ответ"**/
    function createButton(x, y, width, height, textLabel, callback, sheme){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Konva.Group({ x:x, y:y });
        var buttonRectangle = new Konva.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var label = new Konva.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 26,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        //backgroundLayer.add(buttonGroup);
        //backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mouseout, resetPointer);

        return buttonGroup;
    }

    function checkAnswers(evt){
    	var isWin = true;
    	var wrongBonds = connections;

    	for(var i=0; i<answers.length;i++){
    		//Если ответ содержится в связях, исключаем данную связь из набора чтобы определить неправильные связи (если они есть)
    		var search = searchBond(answers[i]);
    		//console.log(search);
    		if(search!=-1){
    			wrongBonds.splice(search, 1);      			
    		} else{//Если ответа нету в связях - отображаем его на канве
    			//showAnswerNode(rootNode, answers[rootNode][i], lineLayer);
    			drawMissedBond(answers[i]);
    			//console.log('Miss: '+answers[i]);
    			isWin = false;
    		}
    	}
    	//Перебираем оставшиеся (неправильные) связи и помечаем их красным
    	for (i = 0; i<wrongBonds.length; i++) {
    		wrongBonds[i].line.stroke('red');
    		wrongBonds[i].line.from.fillPoint('red');
    		wrongBonds[i].line.to.fillPoint('red');
    		lineLayer.draw();
    		isWin = false;
    	}
    	
    	var button = evt.targetNode;
    	button.setText('Ещё раз');
    	button.off('click');
    	button.on('click', clear);

    	showResultLabel(button.x()-180, button.y()+5, isWin);
    }
    function drawMissedBond(bond){
    	var firstNodes = nodeLayer.find('.'+bond[0]);
    	var secondNodes = nodeLayer.find('.'+bond[1]);
		for(var i = 0; i<firstNodes.length; i++){
			for(var j=0; j<secondNodes.length; j++){
				if(isBondable(firstNodes[i], secondNodes[j])){
					createBond(firstNodes[i], secondNodes[j]);
					return;
				}
			}
		}
    }
    function createBond(firstNode, secondNode){
    	currentLine = new Konva.Line({
			            points: [firstNode.x(), firstNode.y(), secondNode.x(), secondNode.y()],
			            stroke: "green",
			            strokeWidth: 4
			        });
    	lineLayer.add(currentLine);
    	lineLayer.draw();

    	firstNode.fillPoint('green');
    	secondNode.fillPoint('green');

    	firstNode.showPoint();
    	secondNode.showPoint();
    }

    function isBondable(firstNode, secondNode){
    	return ((firstNode.avaibleBond == secondNode.columnIndex) && (secondNode.avaibleBond == firstNode.columnIndex))
    }

    /*Соединяет два узла зелёной линией, указывая пропущенные пользователем связи   
	 * @param {string} rootNode главный узел (LEFT_NODE или RIGHT_NODE), из которого будет рисоваться связь
	 * @param {string} node узел-ответ, в который будет входить связь
	 */
    function showAnswerNode(rootNode, node){
    	var fromNode = nodeLayer.find('#'+rootNode+'_'+rootNode)[0];
    	var toNode = nodeLayer.find('#'+rootNode+'_'+node)[0];
    	currentLine = new Konva.Line({
			            points: [fromNode.x(), fromNode.y(), toNode.x(), toNode.y()],
			            stroke: "green",
			            strokeWidth: 4
			        });
    	lineLayer.add(currentLine);
    	lineLayer.draw();
    }	

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }    

    function zoomIn(){
        if(preventActions) return;
    	map.zoom(0.20);
    }

    function zoomOut(){
        if(preventActions) return;
    	map.zoom( - 0.20);
    }

    function createZoomButton(x, y, type, handler){
    	var zoomButton = new Konva.Group({
    		x: x,
    		y: y
    	})
        zoomButton.on(events[isMobile].mouseover, hoverPointer);
        zoomButton.on(events[isMobile].mouseout, resetPointer);

    	zoomButton.on(events[isMobile].click, handler);

        var zoomSheme = colorSheme.controllsPanel.zoomIcon;

    	//var imgObj = new Image();

    	//imgObj.onload = function(){

        var imgObj = (type == 'zoomIn' ? controllsGraphic.zoomInIcon : controllsGraphic.zoomOutIcon);

		var buttonSprite = new Konva.Image({
			x: 0,
			y: 0,
			image: imgObj,
            width: 40 * mobileScaler,
            height: 40 * mobileScaler
		});

        var hitRectangle = new Konva.Rect({
            x: - imgObj.width /2,
            y: 0,
            width: imgObj.width * 2,
            height: imgObj.height * 2,
            fill: 'black'
        });

        hitRectangle.opacity(0);

        if(type == 'zoomIn'){
            hitRectangle.y( - hitRectangle.height()/2)
        }

        zoomButton.add(buttonSprite);
		zoomButton.add(hitRectangle);
		//controllsLayer.draw();
    	//}

    	//imgObj.src = skinManager.getGraphic(type == 'zoomIn' ? zoomSheme.in : zoomSheme.out)

    	return zoomButton;
    }

    function buildZoomButtonPanel(x, y){
    	var zoomPanelGroup = new Konva.Group({
    		x: x,
    		y: y
    	})

    	var panelImage = new Image();

    	panelImage.onload = function(){
    		var panel = new Konva.Image({
    			x: 0,
    			y: 0,
    			image: panelImage,
    			width: panelImage.width,
    			height: panelImage.height    			
    		})

    		zoomPanelGroup.add(panel);
    		//zoomPanelGroup.add( createZoomButton(3, 15, 'zoomIn', zoomIn) );
    		//zoomPanelGroup.add( createZoomButton(3, 45, 'zoomOut', zoomOut) );    		
    	}

    	panelImage.src = 'assets/ui/zoomBackgroundPanel.png';

    	return zoomPanelGroup;
    }

    function createCheckBox(x, y, layer){
    	var imgObj = new Image();

    	var checkBoxGroup = new Konva.Group({
    		x: x,
    		y: y + 4
    	})

    	checkBoxGroup.layer = layer;

        imgObj = controllsGraphic.checkBox;

    	//imgObj.onload = function(){
		var checkBox = new Konva.Sprite({
			x: x,
			y: y,
			image: imgObj,
			animation: layer.isVisible ? '1' : '0',
			animations: {
				0: [
				  // x, y, width, height
				  0,0,34,34
				],
				1: [
				  // x, y, width, height
				  34,0,34,34
				]
			},
			frameRate: 7,
			frameIndex: 0
		})

		checkBox.scale({
			x: 0.5,
			y: 0.5
		})

		checkBoxGroup.toggle = function(invoker){
            if(preventActions) return;
            preventActions = true;

            invoker.listening(false);
            draggableLayer.draggable(false);

            //console.log('listening: ' + invoker.listening());

            preLoader.showPreloader();
            imageContainer.drawScene();

            setTimeout(function(){
                if(checkBox.animation() == '0'){
                    checkBox.animation('1');
                    map.showLayer(checkBoxGroup.layer, invoker);
                } else {
                    checkBox.animation('0');
                    map.hideLayer(checkBoxGroup.layer, invoker);
                }
            }, 300);
		}

		checkBoxGroup.add(checkBox);

		//controllsLayer.draw();
    	//}

    	//imgObj.src = skinManager.getGraphic(colorSheme.controllsPanel.checkBox);

    	return checkBoxGroup;
    }

    function createLayerLabel(x, y, width, layer, showScrollbar){
    	var layerLabelGroup = new Konva.Group({
    		x: x,
    		y: y
    	});

        var layerPanelSheme = colorSheme.controllsPanel.container.layerPanel;

        var marginLeft = showScrollbar ? 40 : 20;

		var layerLabel = new Konva.Text({
    		x: 5,
    		y: 5,
    		text: layer.name,
    		fontSize: 14,
    		width: width - 5 - marginLeft,
    		fontFamily: mainFont,
    		//fontStyle: 'bold',
    		fill: 'black'
    	});

    	var backgroundRectangle = new Konva.Rect({
    		x: 0,
    		y: 0,
    		width: width - marginLeft,
    		height: layerLabel.height() + 10,
    		fill: layerPanelSheme.hoverRect.backgroundColor,
            stroke: layerPanelSheme.hoverRect.stroke,
            strokeWidth: layerPanelSheme.hoverRect.strokeWidth
    	});

    	backgroundRectangle.hide();

		var checkBox = createCheckBox(130 - (marginLeft >> 2), 0, layer);

		layerLabelGroup.on(events[isMobile].click, function(){
			checkBox.toggle(layerLabelGroup);
			layerLabelGroup.drawScene();
		});

        if(!isMobile){
    		layerLabelGroup.on(events[isMobile].mouseover, function(){
                if(preventActions) return;
    			backgroundRectangle.show();
    			layerPanel.draw();
                hoverPointer();
    		});

    		layerLabelGroup.on(events[isMobile].mouseout, function(){
                if(preventActions) return;
    			backgroundRectangle.hide();
    			layerPanel.draw();
                resetPointer();
    		});
        }

        layerLabelGroup.height(backgroundRectangle.height());

		layerLabelGroup.add(backgroundRectangle);
		layerLabelGroup.add(layerLabel);
		layerLabelGroup.add(checkBox);

		return layerLabelGroup;
    }

    function initFullscreenContainer(){
        fullscreenContainer = new Konva.Group({
            x: 0,
            y: 0
        });

        var containerSheme = colorSheme.controllsPanel.container;

        var backgroundRect = new Konva.Rect({
            x:0,
            y:0,
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            fill: 'white'
        });

        var headPadding = 10;

        var headRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: backgroundRect.width(),
            height: 80,
            fill: containerSheme.header.backgroundColor
        });

        var headContainer = new Konva.Group({
            x: 0,
            y: 0
        });

        fullscreenLayer.clip = {
            x: 0,
            y: 0,
            height: backgroundRect.height() - headRect.height() - 10,
            width: backgroundRect.width() - 70
        }

        var headerLabel = new Konva.Text({
            x: 15,
            y: (headRect.height() >> 1) - 20,
            text: 'Полноэкранный режим',
            fontFamily: mainFont,
            fontSize: 40,
            fill: containerSheme.header.fontColor
        });

        headContainer.on(events[isMobile].click, function(){
            fullscreenContainer.close();
        });

        var clippedConteiner = new Konva.Group({
            x: 0,
            y: headRect.height() + 10
        });

        var contentContainer = new Konva.Group({
            x: 0,
            y: 0
        });

        clippedConteiner.setClip(fullscreenLayer.clip);

        var contentHeader = new Konva.Text({
            x: 15,
            y: headRect.height() + 5,
            fontSize: 32,
            fontFamily: mainFont,
            //fontStyle: 'bold',
            width: fullscreenLayer.clip.width - 15,
            fill: 'black',
            text: 'Sample header'
        });

        var contentImage = new Konva.Image({
          x: 15,
          y: 0
        });

        var contentText = new Konva.Text({
            x: headerLabel.x(),
            y: contentHeader.height() + 30,
            fontSize: 28,
            fontFamily: mainFont,
            width: fullscreenLayer.clip.width - headerLabel.x(),
            fill: 'black',
            text: 'Sample text'
        });

        fullscreenContainer.links = [];

        if(maxLinkCount > 0){
            var link;
            var prevLink;

            for(var i = 0; i < maxLinkCount; i++){
                link = new Konva.Text({
                    x: contentText.x(),
                    y: ((prevLink == null) ? (contentText.height() + 5 * i + contentText.y() + contentText.fontSize() * 0.2) : (prevLink.height() + prevLink.y() + prevLink.fontSize() * 0.2)),
                    text: 'teh link',
                    fontSize: contentText.fontSize(),
                    fontFamily: contentText.fontFamily(),
                    width: contentText.width(),
                    fill: containerSheme.linkColor
                });

                link.url = '';

                link.setValue = function(url, label){
                    label = label == '' ? url : label;

                    this.url = url;
                    this.text('• ' + label);
                }

                link.on(events[isMobile].mouseover, function(){
                    //this.fill('orange');
                    hoverPointer();
                    //textHolder.drawScene();
                });

                link.on(events[isMobile].mouseout, function(){
                    //this.fill('blue');
                    resetPointer();
                    //textHolder.drawScene();
                });

                link.on(events[isMobile].click, function(){
                    window.open(this.url, '_blank');
                });

                link.hide();

                fullscreenContainer.links.push(link);

                contentContainer.add(link);

                prevLink = link;
            }
        }

        function updateLinks(position, links){
            var linksHeight = 0;

            for(var i = 0; i < fullscreenContainer.links.length; i++){
                if(i < links.length){
                    fullscreenContainer.links[i].show();
                    fullscreenContainer.links[i].y(position + linksHeight);

                    fullscreenContainer.links[i].setValue(links[i].url, links[i].label);

                    linksHeight += fullscreenContainer.links[i].height() + fullscreenContainer.links[i].fontSize() * 0.2;

                    continue;
                }

                fullscreenContainer.links[i].hide();
            }

            return linksHeight;
        }

        contentContainer.width(backgroundRect.width() - (contentHeader.x() << 1))

        contentContainer.add(contentHeader);
        contentContainer.add(contentImage);
        contentContainer.add(contentText);

        contentImage.on(events[isMobile].mouseover, hoverPointer);
        contentImage.on(events[isMobile].mouseout, resetPointer);

        contentImage.hide();

        contentImage.on(events[isMobile].click, function(){
            imageContainer.zoomImage(contentImage.getImage());
        })

        fullscreenContainer.add(backgroundRect);
        headContainer.add(headRect);
        headContainer.add(headerLabel);

        fullscreenContainer.add(headContainer);

        var imgObj = new Image();

        var iconPadding = 17;

        var iconSize = headRect.height() - (iconPadding << 1);

        //imgObj.onload = function(){
        var fullscreenIcon = new Konva.Image({
            x: headRect.width() - iconSize - iconPadding,
            y: iconPadding,
            width: iconSize,
            height: iconSize,
            image: controllsGraphic.fullscreenOutIcon
        });

        headContainer.add(fullscreenIcon);
        //}

        //imgObj.src = skinManager.getGraphic(colorSheme.controllsPanel.fullscreenIcon.out);

        clippedConteiner.add(contentContainer);
        fullscreenContainer.add(clippedConteiner);

        fullscreenLayer.add(fullscreenContainer);

        fullscreenLayer.scroll = createScrollBar( //x, y, width, height, contentGroup, clippedLayer, scrollLayer, drawBackground
            ($(window).width() / stage.scaler) - 55,
            headRect.height() + 8,
            45,
            fullscreenLayer.clip.height - 5,
            contentContainer,
            clippedConteiner,
            fullscreenScrollLayer,
            false);

        fullscreenLayer.scroll.refresh();

        fullscreenContainer.hide();

        fullscreenContainer.setContent = function(
            titleText,
            headText,
            image,
            contentTextData,
            links){

            if(!titleText) titleText = '';
            if(!headText) headText = '';
            if(!image) image = null;
            if(!contentTextData) contentTextData = '';

            links = links || [];

            headerLabel.text(titleText);
            contentHeader.text(headText);
            contentText.text(contentTextData);

            if(image != null){
                contentImage.setImage(image);
                contentImage.isActive = true;

                var scaler = calculateAspectRatioFit(image.width, image.height, (contentContainer.width() >> 1), ((($(window).height() / stage.scaler) - headRect.height()) >> 1));

                contentImage.scale({
                    x: scaler,
                    y: scaler
                })

            } else{
                contentImage.isActive = false;
            }

            fullscreenContainer.alignContent(links);

            fullscreenContainer.show();

            fullscreenLayer.draw();
        }

        fullscreenContainer.resizeContent = function(newWidth, newHeight){
            console.log(backgroundRect);
            backgroundRect.width(newWidth / fullscreenLayer.scaleX());
            backgroundRect.height(newHeight / fullscreenLayer.scaleX());

            headRect.width(backgroundRect.width());

            fullscreenIcon.x(headRect.width() - iconSize - iconPadding);

            fullscreenLayer.clip = {
                x: 0,
                y: 0,
                height: backgroundRect.height() - headRect.height() - 10,
                width: backgroundRect.width() - 70
            }

            clippedConteiner.setClip(fullscreenLayer.clip);

            contentHeader.width(fullscreenLayer.clip.width - 15);
            contentText.width(fullscreenLayer.clip.width - headerLabel.x());

            console.log(fullscreenLayer.scroll.x());

            fullscreenLayer.scroll.x((newWidth / fullscreenScrollLayer.scaleX())  - 55);

            fullscreenLayer.scroll.setHeight(fullscreenLayer.clip.height - 5);
            fullscreenLayer.scroll.refresh(true);

            fullscreenScrollLayer.draw();

            /*fullscreenLayer.scroll = createScrollBar( //x, y, width, height, contentGroup, clippedLayer, scrollLayer, drawBackground
            ($(window).width() / stage.scaler) - 55,
            headRect.height() + 8,
            45,
            fullscreenLayer.clip.height - 5,
            contentContainer,
            clippedConteiner,
            fullscreenScrollLayer,
            false);*/
        }

        fullscreenContainer.alignContent = function(links){            
            var padding = 10;
            var dy = 0;

            if(contentHeader.text() != ''){
                contentHeader.show();
                contentHeader.y(dy);
                dy += contentHeader.height() + contentHeader.fontSize() * 0.2 + padding;
            } else{
                contentHeader.hide();
            }

            if(contentImage.isActive){
                contentImage.show();
                contentImage.y(dy);
                dy += contentImage.height() * contentImage.scaleY() + padding;

                var imageWidth = contentImage.width() * contentImage.scaleX();

                if(imageWidth < contentContainer.width()) contentImage.x((contentContainer.width() >> 1) - (imageWidth >> 1));
            } else{
                contentImage.hide();
            }

            if(contentText.text() != ''){
                contentText.show();
                contentText.y(dy);
                dy += contentText.height() + contentText.fontSize() * 0.2 + padding;
            } else{
                contentText.hide();
            }

            contentContainer.height(dy + updateLinks(dy, links));

            fullscreenLayer.scroll.refresh();
        }

        fullscreenContainer.close = function(){
            contentContainer.height(0);
            fullscreenLayer.scroll.refresh();
            fullscreenContainer.hide();
            fullscreenLayer.draw();
        }
    }

    function buildLayerPanel(x, y){
        var layerPanelWidth = 300;

    	var layerPanelGroup = new Konva.Group({
    		x: - layerPanelWidth,
    		y: y
    	});

        var containerSheme = colorSheme.controllsPanel.container;

        layerPanelGroup.btn = controllsPanel.layerPanelBtn;

    	layerPanelGroup.isExpanded = false;

        var headerGroup = new Konva.Group();

    	var headerRect = new Konva.Rect({
    		x: 0,
    		y: 0,
    		width: layerPanelWidth,
    		height: 40,
    		fill: containerSheme.header.backgroundColor
    	});

    	var headerLabel = new Konva.Text({
    		x: 15,
    		y: (headerRect.height() >> 1) - 9,
    		text: 'Слои',
    		fontFamily: mainFont,
    		fontSize: 18,
    		//fontStyle: 'bold',
    		fill: containerSheme.header.fontColor
    	});

    	var layerSelectionContainer = new Konva.Group({
    		x: 0,
    		y: headerRect.height()
    	})

    	var expandedRect = new Konva.Rect({
    		x: 0,
    		y: 0,
    		width: layerPanelWidth,
    		height: 180,
    		fill: 'white',
            stroke: containerSheme.stroke,
            strokeWidth: containerSheme.strokeWidth
    	})

        var expandedPosition = controllsPanel.width();

    	layerSelectionContainer.add(expandedRect);

        var dy = 3;

        var layerLabelGroup = new Konva.Group();
        var labelHolder = new Konva.Group();

        layerLabelGroup.setClip({
            x: 5,
            y: 5,
            width: layerPanelWidth,
            height: expandedRect.height() - 10
        })

        layerPanelGroup.rebuildContent = function(){
            console.log('rebuild layerPanel')
            labelHolder.removeChildren();

            var dy = 3;
            var showScrollbar = (levelLayerCount > 6);

            layerSelectionContainer.scrollable = showScrollbar;

            for(var i = 0; i < layerArray.length; i++){

                if(!layerArray[i].levelRelated) continue;

                var layerLabel = createLayerLabel(10, 5 + dy, layerPanelWidth, layerArray[i], showScrollbar);

                dy += layerLabel.height();

                labelHolder.add(layerLabel);

                layerLabel = null;
            }

            labelHolder.height(5 + dy);

            if(layerSelectionContainer.scroll) layerSelectionContainer.scroll.refresh();
        }

        var showScrollbar = (layerArray.length > 6);

    	for(var i = 0; i < layerArray.length; i++){

			var layerLabel = createLayerLabel(10, 5 + dy, layerPanelWidth, layerArray[i], showScrollbar);

            dy += layerLabel.height();

	    	labelHolder.add(layerLabel);
    	}

        labelHolder.height(5 + dy);

        layerSelectionContainer.scroll = null;

        //if(labelHolder.height() >= expandedRect.height()) layerSelectionContainer.scroll = createScrollBar(controllsPanel.width() + layerPanelWidth - 20, y + headerRect.height() + 10, 15, expandedRect.height() * 0.9, labelHolder, layerLabelGroup);
        if(showScrollbar) layerSelectionContainer.scroll = createScrollBar(controllsPanel.width() + layerPanelWidth - 20, y + headerRect.height() + 10, 15, expandedRect.height() * 0.9, labelHolder, layerLabelGroup);

        if(layerSelectionContainer.scroll != null){
            layerSelectionContainer.scrollable = true;
            layerSelectionContainer.scroll.hide();
            contentScrollLayer.draw();
        }

        layerLabelGroup.add(labelHolder);

        layerSelectionContainer.add(layerLabelGroup);

    	layerPanelGroup.add(layerSelectionContainer);

    	//layerSelectionContainer.hide();

        layerPanelGroup.inTween = null;

        layerPanelGroup.leftSidePosition = {
            expanded: expandedPosition,
            regular: - layerPanelWidth
        }

        layerPanelGroup.rightSidePosition = {
            expanded: ($(window).width() / stage.scaler) - expandedPosition - layerPanelWidth,
            regular: ($(window).width() / stage.scaler) + expandedPosition
        }

        layerPanelGroup.sidePosition = layerPanelGroup.leftSidePosition;

        layerPanelGroup.recalculatePosition = function(){
            layerPanelGroup.rightSidePosition = {
                expanded: ($(window).width() / stage.scaler) - expandedPosition - layerPanelWidth,
                regular: ($(window).width() / stage.scaler) + expandedPosition
            }
        }

        layerPanelGroup.onSideChanged = function(){
            layerPanel.sidePosition = controllsPanel.rightSideMode ? layerPanel.rightSidePosition : layerPanel.leftSidePosition;

            layerPanel.x(!layerPanel.isExpanded ? layerPanel.sidePosition.regular : layerPanel.sidePosition.expanded);

            if(layerSelectionContainer.scroll != null){
                layerSelectionContainer.scroll.x((layerPanel.sidePosition.expanded + layerPanelWidth - 20) - (controllsPanel.width() + layerPanelWidth - 20));
                //contentScrollLayer.draw();
                /*console.log('scroll ' + (layerPanel.sidePosition.expanded + layerPanelWidth - 20));
                console.log('scroll prev --> ' + (controllsPanel.width() + layerPanelWidth - 20));
                console.log(layerSelectionContainer.scroll.x());*/
            }

            //controllsLayer.draw();
        }

        layerPanelGroup.reallocateScroll = function(dy){
            if(layerSelectionContainer.scroll != null){
                layerSelectionContainer.scroll.reallocateScroll(dy);
                contentScrollLayer.draw();
            }
        }

        layerPanelGroup.getTween = function(){

            if(layerPanelGroup.inTween == null){

                layerPanelGroup.inTween = new Konva.Tween({
                    node: layerPanelGroup,
                    duration: 1,
                    x:  expandedPosition,
                    easing: Konva.Easings.Linear
                });
            }

            return layerPanelGroup.inTween;
        }

        layerPanelGroup.showContainer = function(){
            layerPanel.inTween = new Konva.Tween({
                node: layerPanel,
                duration: 0.2,
                x: layerPanel.sidePosition.expanded,
                easing: Konva.Easings.Linear
            });

            layerPanelGroup.inTween.onFinish = function(){
                if(layerSelectionContainer.scrollable){
                    layerSelectionContainer.scroll.show();
                    contentScrollLayer.draw();
                }

                //layerPanelGroup.isExpanded = true;
            }

            layerPanelGroup.getTween().play();
        }

        layerPanelGroup.hideContainer = function(){
            //layerPanelGroup.isExpanded = false;

            if(layerSelectionContainer.scrollable){
                layerSelectionContainer.scroll.hide();
                contentScrollLayer.draw();
            }

            layerPanel.outTween = new Konva.Tween({
                node: layerPanel,
                duration: 0.2,
                x: layerPanel.sidePosition.regular,
                easing: Konva.Easings.Linear
            });

            layerPanel.outTween.play();

            //layerPanelGroup.inTween.reverse();
        }

    	layerPanelGroup.togglePanel = function(){
    		if(!layerPanelGroup.isExpanded){
               expandContainer(layerPanelGroup);
               controllsPanel.glow(controllsPanel.layerPanelBtn);
    		} else{
                hideContainer(layerPanelGroup);
                controllsPanel.unGlow(controllsPanel.layerPanelBtn);
    		}
    	}

        headerGroup.on(events[isMobile].mouseover, hoverPointer);
        headerGroup.on(events[isMobile].mouseout, resetPointer);

        headerGroup.on(events[isMobile].click, layerPanelGroup.togglePanel);

    	headerGroup.add(headerRect);
    	headerGroup.add(headerLabel);

        layerPanelGroup.add(headerGroup)

    	return layerPanelGroup;
    }

    function createTaskPanel(x, y){
        var taskPanelGroup = new Konva.Group({
            x: x,
            y: y
        });

        var containerSheme = colorSheme.controllsPanel.container;

        taskPanelGroup.btn = controllsPanel.taskBtn;

        taskPanelGroup.width(400);

        taskPanelGroup.maxHeight = 438;
        taskPanelGroup.minHeight = 200;

        taskPanelGroup.rightCount = 0;
        taskPanelGroup.wrongCount = 0;
        taskPanelGroup.skipCount = 0;

        var taskPanel = new Konva.Rect({
            width: taskPanelGroup.width(),
            height: taskPanelGroup.minHeight,
            fill: 'white',
            stroke: containerSheme.stroke,
            strokeWidth: containerSheme.strokeWidth
        });

        var header = new Konva.Group({
            x: 0,
            y: 0
        });

        header.on(events[isMobile].mouseover, hoverPointer);
        header.on(events[isMobile].mouseout, resetPointer);

        header.on(events[isMobile].click, function(){
            hideContainer(taskPanelGroup);
        });

        var headerRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: taskPanelGroup.width(),
            height: 40 * mobileScaler,
            fill: containerSheme.header.backgroundColor
        });

        var headerLabel = new Konva.Text({
            x: 15,
            y: (headerRect.height() >> 1) - (9 * mobileScaler),
            text: 'Викторина',
            fontFamily: mainFont,
            fontSize: 18 * mobileScaler,
            //fontStyle: 'bold',
            fill: containerSheme.header.fontColor
        });

        var headerText = new Konva.Text({
            x: headerLabel.x(),
            y: headerRect.height() + 5,
            fontFamily: mainFont,
            //fontStyle: 'bold',
            fontSize: 16 * mobileScaler,
            text: 'Вопрос 5 из 6',
            fill: 'black'
        });

        function createResultRect(x, y, labelText){

            var padding = 20;

            var resultRect = new Konva.Group({
                x: x,
                y: y
            });

            var resultRectSheme = colorSheme.taskResultRect;

            resultRect.width(taskPanel.width() - (padding >> 1));

            var rectWidth = (resultRect.width() >> 1 ) - (padding << 1);

            var label = new Konva.Text({
                x: 0,
                y: padding,
                text: labelText,
                fontFamily: mainFont,
                fontSize: 18,
                //fontStyle: 'bold',
                align: "center",
                fill: "black",
                width: rectWidth
            })

            var labelRect = new Konva.Rect({
                x: 0,
                y: 0,
                width: rectWidth,
                height: label.height() + (padding << 1),
                fill: resultRectSheme.labelRect.color
            });

            var valueRect = new Konva.Rect({
                x: labelRect.x() + labelRect.width(),
                y: 0,
                width: rectWidth,
                height: label.height() + (padding << 1),
                fill: resultRectSheme.valueRect.color
            })

            var valueLabel = new Konva.Text({
                x: valueRect.x(),
                y: label.y(),
                text: 0,
                fontFamily: mainFont,
                fontSize: 18,
                //fontStyle: 'bold',
                align: "center",
                fill: "black",
                width: labelRect.width()
            });

            resultRect.setValue = function(val){
                valueLabel.text(val);
            }

            resultRect.height(labelRect.height());

            resultRect.add(labelRect);
            resultRect.add(valueRect);
            resultRect.add(label);
            resultRect.add(valueLabel);

            return resultRect;
        }

        var correctResultRect = createResultRect(40, headerRect.height() + 20, 'Верно');
        var wrongResultRect = createResultRect(correctResultRect.x(), correctResultRect.y() + correctResultRect.height() + 20, 'Неверно');
        var skipResultRect = createResultRect(correctResultRect.x(), wrongResultRect.y() + wrongResultRect.height() + 20, 'Пропущено');

        var resultScreen = new Konva.Group({
            x: 0,
            y: 0
        });

        resultScreen.add(correctResultRect);
        resultScreen.add(wrongResultRect);
        resultScreen.add(skipResultRect);

        resultScreen.height(correctResultRect.height() + 20 + wrongResultRect.height() + 20 + skipResultRect.height());

        resultScreen.setResults = function(rightCount, wrongCount, skipCount){
            correctResultRect.setValue(rightCount);
            wrongResultRect.setValue(wrongCount);
            skipResultRect.setValue(skipCount);
        }

        header.text = function(val){
            headerText.text(val);
            header.height(headerText.height() + headerText.y());
        }

        header.height(headerText.height() + headerText.y());

        header.add(headerRect);
        header.add(headerLabel);
        header.add(headerText);

        var question = new Konva.Text({
            x: headerText.x(),
            y: header.y() + header.height() + 5,
            fontFamily: mainFont,
            fontSize: 12 * mobileScaler,
            text: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
            width: taskPanel.width() - 60,
            fill: 'black'
        });

        var tipLabel = new Konva.Label({
            x: headerText.x(),
            y: 0
        });

        tipLabel.add(new Konva.Tag({
            fill: containerSheme.taskTip.backgroundColor,
            stroke: containerSheme.taskTip.stroke,
            strokeWidth: containerSheme.taskTip.strokeWidth
        }));

        tipLabel.add(new Konva.Text({
            text: 'Покажи один из значков на карте',
            fontFamily: mainFont,
            fontSize: 12,
            //fontStyle: 'bold',
            padding: 5,
            fill: containerSheme.taskTip.fontColor,
            width: taskPanelGroup.width() - (tipLabel.x() << 1) - 20
        }));

        var taskControllsPadding = 10;

        var skipButton = new Konva.Group({
            x: header.x(),
            y: tipLabel.y() + 30
        });

        var skipButtonLabel = new Konva.Text({
            x: 0,
            y: 0,
            text: 'Следующий вопрос',
            fontFamily: mainFont,
            fontSize: 12,
            //fontStyle: 'bold',
            padding: 5,
            fill: containerSheme.skipButton.fontColor
        })

        var skipButtonRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: skipButtonLabel.width(),
            height: skipButtonLabel.height(),
            fill: containerSheme.skipButton.backgroundColor,
            stroke: containerSheme.skipButton.stroke,
            strokeWidth: containerSheme.skipButton.strokeWidth,
            cornerRadius: containerSheme.skipButton.cornerRadius
        });

        skipButton.width(skipButtonRect.width());
        skipButton.height(skipButtonRect.height());

        skipButton.add(skipButtonRect);
        skipButton.add(skipButtonLabel);

        var finishButton = new Konva.Group({
            x: header.x(),
            y: tipLabel.y() + 30
        });

        var finishButtonLabel = new Konva.Text({
            x: 0,
            y: 0,
            text: 'Завершить викторину',
            fontFamily: mainFont,
            fontSize: 12,
            //fontStyle: 'bold',
            padding: 5,
            fill: containerSheme.finishButton.fontColor
        })

        var finishButtonRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: finishButtonLabel.width(),
            height: finishButtonLabel.height(),
            fill: containerSheme.finishButton.backgroundColor,
            stroke: containerSheme.finishButton.stroke,
            strokeWidth: containerSheme.finishButton.strokeWidth,
            cornerRadius: containerSheme.finishButton.cornerRadius
        });

        finishButton.width(finishButtonRect.width());
        finishButton.height(finishButtonRect.height());

        finishButton.add(finishButtonRect);
        finishButton.add(finishButtonLabel);

        finishButton.getText = function(){
            return finishButtonLabel;
        }
        
        var taskControllsWidth = skipButton.width() + finishButton.width() + taskControllsPadding;

        function centerTaskControlls(containerWidth, controllsWidth){
            finishButton.x((containerWidth >> 1) - (controllsWidth >> 1))
            skipButton.x(finishButton.x() + finishButton.width() + taskControllsPadding);
        }

        centerTaskControlls(headerRect.width(), taskControllsWidth);

        /*var resultLabel = new Konva.Label({
            x: 0,
            y: finishButton.y() + finishButton.height() + 8
        });*/

        var resultLabel = new Konva.Group({
            x: 0,
            y: finishButton.y() + finishButton.height() + 8
        });

        var resultLabelText = new Konva.Text({
            x: 0,
            y: 0,
            text: 'Название точки - результат!',
            fontFamily: mainFont,
            fontSize: 12,
            //fontStyle: 'bold',
            padding: 5,
            fill: containerSheme.resultLabel.fontColor
        })

        var resultLabelRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: resultLabelText.width(),
            height: resultLabelText.height(),
            fill: containerSheme.resultLabel.backgroundColor,
            stroke: containerSheme.resultLabel.stroke,
            strokeWidth: containerSheme.resultLabel.strokeWidth,
            cornerRadius: containerSheme.resultLabel.cornerRadius
        });

        resultLabel.width(resultLabelRect.width());
        resultLabel.height(resultLabelRect.height());

        resultLabel.getText = function(){
            return resultLabelText;
        }

        resultLabel.getTag = function(){
            return resultLabelRect;
        }

        resultLabel.add(resultLabelRect);
        resultLabel.add(resultLabelText);

       /* resultLabel.add(new Konva.Tag({
            fill: '#79C22B'
        }));

        resultLabel.add(new Konva.Text({
            text: 'Название точки - результат!',
            fontFamily: mainFont,
            fontSize: 12,
            fontStyle: 'bold',
            padding: 5,
            fill: 'white'
        }));*/

        var bottomGroup = new Konva.Group({
            x: 0,
            y: 0
        });

        bottomGroup.height(tipLabel.height() + 15 + skipButton.height());

        var contentImage = new Konva.Image({
            x: question.x(),
            y: 0
        });

        contentImage.hide();

        var contentHolder = new Konva.Group();
        var contentScrollContainer = new Konva.Group();

        resultLabel.hide();

        taskPanelGroup.add(taskPanel);

        taskPanelGroup.add(header);
        contentHolder.add(question);
        contentHolder.add(contentImage);

        contentScrollContainer.add(contentHolder);

        taskPanelGroup.add(contentScrollContainer);
        taskPanelGroup.add(bottomGroup);

        bottomGroup.add(tipLabel);
        bottomGroup.add(skipButton);
        bottomGroup.add(finishButton);

        skipButton.on(events[isMobile].mouseover, hoverPointer);
        skipButton.on(events[isMobile].mouseout, resetPointer);

        finishButton.on(events[isMobile].mouseover, hoverPointer);
        finishButton.on(events[isMobile].mouseout, resetPointer);

        contentImage.on(events[isMobile].click, function(){
            imageContainer.zoomImage(contentImage.getImage());
        });

        contentImage.on(events[isMobile].mouseover, hoverPointer);
        contentImage.on(events[isMobile].mouseout, resetPointer);

        bottomGroup.add(resultLabel);

        taskPanelGroup.hide();

        taskPanelGroup.currentQuestion = 0;

        taskPanelGroup.scroll = createScrollBar((controllsPanel.width() + taskPanel.width()) - 20, y + headerText.y() + 5, 15, taskPanelGroup.maxHeight - headerText.y() - 15, contentHolder, contentScrollContainer);

        taskPanelGroup.taskMode = false;
        taskPanelGroup.jumpToNext = false;
        taskPanelGroup.isRight = false;
        taskPanelGroup.isSkipped = false;
        taskPanelGroup.isFinished = false;
        taskPanelGroup.isFirst = true;

        taskPanelGroup.leftSidePosition = {
            expanded: controllsPanel.width(),
            regular: - taskPanelGroup.width()
        }

        taskPanelGroup.rightSidePosition = {
            expanded: ($(window).width() / stage.scaler) - controllsPanel.width() - taskPanelGroup.width(),
            regular: ($(window).width() / stage.scaler) + controllsPanel.width()
        }

        taskPanelGroup.sidePosition = taskPanelGroup.leftSidePosition;

        taskPanelGroup.onSideChanged = function(){
            taskPanelGroup.sidePosition = controllsPanel.rightSideMode ? taskPanelGroup.rightSidePosition : taskPanelGroup.leftSidePosition;

            taskPanelGroup.x(!taskPanelGroup.isExpanded ? taskPanelGroup.sidePosition.regular : taskPanelGroup.sidePosition.expanded);

            if(taskPanelGroup.scroll != null){
                taskPanelGroup.scroll.x((taskPanelGroup.sidePosition.expanded + taskPanelGroup.width() - 20) - (controllsPanel.width() + taskPanelGroup.width() - 20));
            }
        }

        taskPanelGroup.initTween = function(){
            taskPanelGroup.defaultX = taskPanelGroup.x();

            //taskPanelGroup.y(stage.height() + (stage.height() >> 1) );
            taskPanelGroup.x(- taskPanelGroup.width());

            taskPanelGroup.outTween = null;
            taskPanelGroup.inTween = null;
        }

        taskPanelGroup.playOutTween = function(callback){
            //if(taskPanelGroup.outTween == null){
                taskPanelGroup.outTween = new Konva.Tween({
                    node: taskPanelGroup,
                    duration: 0.2,
                    x:  taskPanelGroup.sidePosition.regular,
                    easing: Konva.Easings.Linear
                });

                taskPanelGroup.outTween.onFinish = callback;

                taskPanelGroup.outTween.play();

                taskPanelGroup.isExpanded = false;
            /*} else{
                taskPanelGroup.outTween.onFinish = callback;

                taskPanelGroup.outTween.reset();
                taskPanelGroup.outTween.play();
            }*/
        }

        taskPanelGroup.playInTween = function(callback){
            //if(taskPanelGroup.inTween == null){
                taskPanelGroup.inTween = new Konva.Tween({
                    node: taskPanelGroup,
                    duration: 0.2,
                    x:  taskPanelGroup.sidePosition.expanded,
                    easing: Konva.Easings.Linear
                });

                taskPanelGroup.inTween.onFinish = callback;

                taskPanelGroup.inTween.play();

                taskPanelGroup.isExpanded = true;
            /*} else{
                taskPanelGroup.inTween.onFinish = callback;

                taskPanelGroup.inTween.reset();
                taskPanelGroup.inTween.play();
            }*/
        }

        taskPanelGroup.updateState = function(updateFunction){

            if(!taskPanelGroup.taskMode) return;

            taskPanelGroup.scroll.hide();
            contentScrollLayer.draw();

            //taskPanelGroup.scroll.refresh();

            //if(taskPanelGroup.scroll.isVisible) taskPanelGroup.scroll.hide();

            //contentScrollLayer.draw();

            taskPanelGroup.playOutTween(function(){
                updateFunction();

                taskPanelGroup.inTween.reset();
                taskPanelGroup.playInTween(function(){
                    //taskPanelGroup.scroll.refresh(); //contentScrollLayer.draw();
                });
                //controllsLayer.draw();
            });
        }

        taskPanelGroup.showContainer = function(){
            taskPanelGroup.show();
            taskPanelGroup.playInTween(function(){
                //if(taskPanelGroup.isFirst){
                    //taskPanelGroup.isFirst = false;
                    taskPanelGroup.scroll.refresh();
                    contentScrollLayer.draw();
                //}
                /*if(taskPanelGroup.scrollWasHidden){
                    taskPanelGroup.scroll.show();
                    contentScrollLayer.draw();
                    taskPanelGroup.scrollWasHidden = false;
                }*/
            });            
        }

        taskPanelGroup.hideContainer = function(){
            taskPanelGroup.scroll.hide();
            contentScrollLayer.draw();

            taskPanelGroup.playOutTween(function(){
                /*if(taskPanelGroup.scroll.isVisible){
                    taskPanelGroup.scroll.hide();
                    contentScrollLayer.draw();
                    taskPanelGroup.scrollWasHidden = true;
                }*/
                taskPanelGroup.taskMode = false;
                taskPanelGroup.hide();            
            });
              
        }

        taskPanelGroup.showResultLabel = function(callback){
            taskPanel.height(taskPanel.height() + 30);

            resultLabel.opacity(0);
            resultLabel.show();

            var fadeInTween = new Konva.Tween({
                node: resultLabel,
                duration: 0.2,
                opacity: 1,
                easing: Konva.Easings.Linear
            });

            fadeInTween.onFinish = callback;

            fadeInTween.play();
        }

        taskPanelGroup.showQuestion = function(questionIndex){
            contentImage.hide();
            contentHolder.y(0);

            taskPanelGroup.jumpToNext = false;
            taskPanelGroup.isRight = false;
            taskPanelGroup.isSkipped = true;

            header.text('Вопрос ' + (questionIndex + 1) + ' из ' + taskArray.length);
            question.text(taskArray[questionIndex].value);

            question.y(header.y() + header.height() + 10);

            var imageMargin = 0;

            var img = taskArray[questionIndex].image;

            if(img != null){
                contentImage.show();

                contentImage.setImage(img);

                //var maxImageSize = (defaultStageHeight >> 1) - 90;
                //var maxImageSize = (defaultStageHeight >> 1) - 65;

                var maxImageWidth = taskPanelGroup.width() - 20;

                var maxImageSize = Math.min(taskPanelGroup.minHeight - 65, maxImageWidth);

                var scaler = calculateAspectRatioFit(img.width, img.height, maxImageSize, maxImageSize);

                contentImage.scale({
                    x: scaler,
                    y: scaler
                });

                var imageWidth = scaler * contentImage.width();

                //contentImage.y(question.y() + question.height() + 10);
                contentImage.y(header.y() + header.height() + 10);
                if(imageWidth < maxImageWidth) contentImage.x((maxImageWidth >> 1) - (imageWidth >> 1));

                imageMargin = (contentImage.height() * scaler) + 10;


                question.y(question.y() + imageMargin)
            }

            taskPanelGroup.currentQuestion = questionIndex;

            //var contentSize = 72 + header.height() + question.height() + imageMargin;
            var contentSize = question.y() + question.height();

            if(contentSize < (taskPanelGroup.maxHeight - header.height())){
                taskPanel.height(contentSize + bottomGroup.height() + 10);
                bottomGroup.y(contentSize + 10);

                contentHolder.height(contentSize - 10);

                contentScrollContainer.setClip({
                    x: 0,
                    y: 0,
                    width: taskPanel.width(),
                    height: contentSize
                })

                centerTaskControlls(headerRect.width(), taskControllsWidth);

            } else{

                centerTaskControlls(headerRect.width() - 20, taskControllsWidth);

                /*if(!taskPanelGroup.scroll.isVisible){
                    //taskPanelGroup.scroll.resize(contentSize)
                    taskPanelGroup.scroll.show();
                    contentScrollLayer.draw();
                }*/

                contentHolder.height(contentSize - 60);

                //contentSize = taskPanelGroup.maxHeight;
                taskPanel.height(taskPanelGroup.maxHeight);
                bottomGroup.y(taskPanelGroup.maxHeight - (bottomGroup.height()));

                contentScrollContainer.setClip({
                    x: 0,
                    y: header.height() + 5,
                    width: taskPanel.width(),
                    height: taskPanelGroup.maxHeight - 65 - (header.height() + 5)
                });
            }

            if(questionIndex == taskArray.length - 1){
                skipButton.hide();
                finishButton.x((headerRect.width() >> 1) - (finishButton.width() >> 1));
            } else{
                skipButton.show();
            }

            resultLabel.hide();

            taskPanelGroup.scroll.hide();
            contentScrollLayer.draw();

            setTimeout(function(){
                if(!taskPanelGroup.isExpanded) return;
                taskPanelGroup.scroll.refresh();
                contentScrollLayer.draw();
            }, 400);
        }

        taskPanelGroup.checkAnswer = function(name, id){
            if(!resultLabel.visible()) taskPanelGroup.showResultLabel(null);
            //resultLabel.show();
            taskPanelGroup.isSkipped = false;

            var tag = resultLabel.getTag();
            var label = resultLabel.getText();

            var resultSheme = containerSheme.resultLabel;

            if(id == taskArray[taskPanelGroup.currentQuestion].answerID){
                label.text(name + ' - верный ответ!');
                //tag.fill('#79C22B');
                tag.fill(resultSheme.backgroundColor);
                label.fill(resultSheme.fontColor);
                tag.width(label.width());
                //skipButton.getText().text('Следующий вопрос');
                taskPanelGroup.rightCount++;
                taskPanelGroup.jumpToNext = true;
                taskPanelGroup.isRight = true;
            } else{
                label.text(name + ' - неверный ответ!');
                tag.fill(resultSheme.wrong.backgroundColor);
                label.fill(resultSheme.wrong.fontColor);
                tag.width(label.width());
                if(!allowReAnswer) taskPanelGroup.jumpToNext = true;
                taskPanelGroup.wrongCount++;
            }

            resultLabel.x((headerRect.width() >> 1) - (label.width() >> 1))

            controllsLayer.draw();
        }

        taskPanelGroup.reset = function(){

            taskPanelGroup.playOutTween(function(){

                headerLabel.text('Викторина');
                tipLabel.show();

                taskPanelGroup.showQuestion(0);

                finishButton.getText().text('Завершить викторину');
                finishButton.off(events[isMobile].click);

                finishButton.on(events[isMobile].click, finishTask);

                taskPanelGroup.rightCount = 0;
                taskPanelGroup.wrongCount = 0;
                taskPanelGroup.skipCount = 0;

                resultScreen.remove();

                taskPanelGroup.isFinished = false;
                taskPanelGroup.taskMode = false;

                /*if(taskPanelGroup.scroll.isVisible){
                    taskPanelGroup.scroll.hide();
                    contentScrollLayer.draw();
                }*/

                //controllsLayer.draw();
            });

            controllsPanel.unGlow(controllsPanel.taskBtn);
        }

        taskPanelGroup.nextQuestion = function(){

            if(!taskPanelGroup.jumpToNext) taskPanelGroup.skipCount++;

            if(taskPanelGroup.currentQuestion < taskArray.length - 1){
                taskPanelGroup.updateState(function(){ taskPanelGroup.showQuestion(taskPanelGroup.currentQuestion + 1) })
                //taskPanelGroup.showQuestion(taskPanelGroup.currentQuestion + 1);

                //controllsLayer.draw();
            } else{
                finishTask();
            }
        }

        taskPanelGroup.showStats = function(){
            contentImage.hide();
            contentHolder.y(0);

            header.text('');
            headerLabel.text('Результаты викторины: ');
            //question.text('Верно: ' + taskPanelGroup.rightCount + '\nНеверно: ' + taskPanelGroup.wrongCount + '\nПропущено: ' + taskPanelGroup.skipCount);
            question.text('');

            resultScreen.setResults(taskPanelGroup.rightCount, taskPanelGroup.wrongCount, taskPanelGroup.skipCount);

            taskPanelGroup.add(resultScreen);
            
            taskPanel.height(65 + header.height() + resultScreen.height());

            contentHolder.height(header.height() + resultScreen.height());

            question.y(header.height() + 10);

            contentScrollContainer.setClip({
                x: 0,
                y: 0,
                width: taskPanel.width(),
                height: contentHolder.height()
            });

            bottomGroup.y(header.height() + resultScreen.height())

            finishButton.getText().text('Покинуть викторину');
            finishButton.off(events[isMobile].click);

            finishButton.on(events[isMobile].click, taskPanelGroup.reset);

            tipLabel.hide();
            skipButton.hide();
            resultLabel.hide();

            if(taskPanelGroup.scroll.isVisible) taskPanelGroup.scroll.hide(); contentScrollLayer.draw();

            taskPanelGroup.isFinished = true;

            centerTaskControlls(headerRect.width(), taskControllsWidth >> 1);
        }

        taskPanelGroup.showQuestion(0);

        taskPanelGroup.scroll.hide();
        contentScrollLayer.draw();

        skipButton.on(events[isMobile].click, taskPanelGroup.nextQuestion);
        finishButton.on(events[isMobile].click, finishTask);

        return taskPanelGroup;
    }

    function finishTask(){
        if(taskPanel.isSkipped) taskPanel.skipCount++;

        if(taskPanel.currentQuestion < taskArray.length - 1){
            taskPanel.skipCount += (taskArray.length - 1) - taskPanel.currentQuestion;
        }

        taskPanel.updateState( taskPanel.showStats );
    }

    function buildControlls(x, y){
    	var controllsGroup = new Konva.Group({
    		x: x,
    		y: y
    	});

        var rightSideFitGroup = new Konva.Group({
            x: 0,
            y: 0
        });
        var botSideFitGroup = new Konva.Group({
            x: 0,
            y: 0
        });

        controllsPanel = createControllsPanel();

        layerPanel = buildLayerPanel(0, controllsPanel.layerPanelBtn.y());
    	if(isLayersEditable) botSideFitGroup.add( layerPanel );

    	//rightSideFitGroup.add( buildZoomButtonPanel(defaultStageWidth - 36, (defaultStageHeight >> 1) - 50) );

        if(taskEnabled){
            taskPanel = createTaskPanel(40, controllsPanel.taskBtn.y());
            controllsGroup.add( taskPanel );
        }

        //botSideFitGroup.add( createTaskButton(0, 100) );

        //viewContainer = buildViewContainer(10, aspectRatio == 1 ? 742 : 551);
        viewContainer = buildViewContainer(0, controllsPanel.viewPanelBtn.y());

        controllsGroup.add( viewContainer );

        zoomCounter = createZoomCounter(controllsPanel.width(), 5);

        if(showScaleTipFlag) controllsGroup.add(zoomCounter);

        controllsGroup.add(rightSideFitGroup);
        controllsGroup.add(botSideFitGroup);

        controllsGroup.add(controllsPanel);

        controllsPanel.moveToTop();

        controllsGroup.fitToStage = function(dx, dy){

            //botSideFitGroup.y(dy);
            //rightSideFitGroup.x(dx);

            //layerPanel.reallocateScroll(dy);
            //viewContainer.reallocateScroll(dy);
        }

        controllsGroup.fitToStage(stage.dx, stage.dy);
        controllsGroup.fitToStage(stage.dx, stage.dy);

        initFullscreenContainer();

    	return controllsGroup;
    }

    function createZoomCounter(x, y){
        var zoomCounter = new Konva.Text({
            x: x,
            y: y,
            text: '',
            fontFamily: mainFont,
            fontSize: 18,
            fill: 'black'
        });

        //var lineWidth = 37.8;
        var lineWidth = getPPI()/2.54;

        var tipLine = new Konva.Line({
            points: [0, 0, 0, 10, 0, 5, lineWidth, 5, lineWidth, 0, lineWidth, 10],
            stroke: 'black',
            strokeWidth: 3
        });

        if(showScaleTipFlag) tipLayer.add(tipLine);

        zoomCounter.refreshTip = function(){

            tipLine.x(tipLine.defaultX * controllsLayer.scaleX());
            tipLine.y(tipLine.defaultY * controllsLayer.scaleY());

            tipLayer.batchDraw();
        }

        zoomCounter.initZoomCounter = function(){
            zoomCounter.valueCollection = [];

            //var prev = Math.round(2000 / defaultScaler);
            var prev = 2000;

            for(var i = 0; i < maxZoomLevel; i += 0.20){
                //zoomCounter.valueCollection.push(prev);
                //prev = Math.round(prev - prev * 0.20);
                zoomCounter.valueCollection.push(Math.round(prev / (defaultScaler + i)));
            }

            zoomCounter.depth = 0;

            zoomCounter.text(zoomCounter.valueCollection[zoomCounter.depth] + ' км');

            tipLine.x(zoomCounter.x() + ((zoomCounter.width()) >> 1) - 18.4)
            tipLine.y(zoomCounter.y() + zoomCounter.height() + 3);

            tipLine.defaultX = tipLine.x();
            tipLine.defaultY = tipLine.y();

            zoomCounter.refreshTip();

            //tipLayer.draw();
        }

        zoomCounter.rebuild = function(){
            zoomCounter.valueCollection = [];

            //var prev = Math.round(2000 / defaultScaler);
            var prev = 2000;

            for(var i = 0; i < maxZoomLevel; i += 0.20){
                //zoomCounter.valueCollection.push(prev);
                //prev = Math.round(prev - prev * 0.20);
                zoomCounter.valueCollection.push(Math.round(prev / (defaultScaler + i)));
            }

            zoomCounter.depth = 0;

            zoomCounter.text(zoomCounter.valueCollection[zoomCounter.depth] + ' км');

            tipLine.x(zoomCounter.x() + ((zoomCounter.width()) >> 1) - 18.4)
            tipLine.y(zoomCounter.y() + zoomCounter.height() + 3);

            tipLine.defaultX = tipLine.x();
            tipLine.defaultY = tipLine.y();

            zoomCounter.refreshTip();
        }

        zoomCounter.setValue = function(value){
            zoomCounter.depth += (value > 0 ? 1 : - 1);
            zoomCounter.text(zoomCounter.valueCollection[zoomCounter.depth] + ' км');
        }

        zoomCounter.reset = function(){
            zoomCounter.depth = 0;
            zoomCounter.text(zoomCounter.valueCollection[zoomCounter.depth] + ' км');
        }

        return zoomCounter;
    }

    function createPreLoader(x, y){
        var imgObj = new Image();

        var preloaderObject = new Konva.Group({
            x: 0,
            y: 0
        });

        var modalRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: $(window).width(),
            height: $(window).height(),
            fill: 'black'
        });

        modalRect.opacity(0.8);

        preloaderObject.add(modalRect);

        imgObj.onload = function(){
            console.log($(window).width())
            var preLoader = new Konva.Image({
                x: ($(window).width() >> 1) - (imgObj.width >> 1),
                y: ($(window).height() >> 1) - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height
            });

            preloaderObject.showPreloader = function(){
                preloaderObject.show();
            }

            preloaderObject.hidePreloader = function(){
                //preLoader.remove();
                preloaderObject.hide();
                //controllsLayer.batchDraw();
            }

            preloaderObject.add(preLoader);
            imageContainer.add(preloaderObject);
            imageContainer.drawScene();
            //imageContainer.moveToTop();
            //controllsLayer.batchDraw();

            /*setTimeout(function(){
                //controllsLayer.batchDraw();
            }, 1000);*/
        }

        imgObj.src = 'assets/ui/preLoaderWhite.png'

        return preloaderObject;
    }

    function bitmapDrawMap(callback){
        draggableLayer.drawScene();
    }

    function iosDrawMap(callback){
        draggableLayer.drawScene();
        return;
        var mapContainer = contentGroup.sourceGroup;
        var layerCollection = mapContainer.getChildren();
        var pieces = [];

        draggableLayer.clear();

        drawIosLayers(layerCollection, 0, function(){
            console.log('graphic drawed!');

            /*setTimeout(function(){
                location.reload(true);
            }, 5000);*/
        });
    }

    function drawIosLayers(layers, index, onComplete){
        var pieces = layers[index].getChildren();

        drawIosLayer(pieces, 0, function(){
            if(index >= (layers.length - 1)){
                if(onComplete) onComplete();
                return;
            }

            setTimeout(function(){
                index++;
                drawIosLayers(layers, index, onComplete);
            }, 30)
        })
    }

    function drawIosLayer(pieces, index, onComplete){
        //console.log(index);
        pieces[index].draw();

        if(index >= (pieces.length - 1)){
            if(onComplete) onComplete();
            return;
        }

        setTimeout(function(){
            index++;
            drawIosLayer(pieces, index, onComplete);
        }, 40)
    }

    function iosLoadAssets(layerArray){
        var counter = layerArray.length;

        var assetIndex = registerAssets();

        for(var i = 0; i < layerArray.length; i++){
            loadIosLayer(layerArray[i].src, i, function(){
                counter--;
                if(counter == 0) setAssetLoaded(assetIndex);
            });
        }
    }

    function loadIosLayer(imageSrc, index, onLayerLoaded){
        //console.log(layersFolder);
        var croppedImageGroup = new Konva.Group();

        croppedImageGroup.width(0);
        croppedImageGroup.height(0);

        var n = 20;

        var cropLoadCounter = n;

        var piecesCollection = [];

        var dy = 0;

        var addinationIndex = '0';

        for(var i = 0; i < n; i++){
            var imageObj = new Image();

            imageObj.counter = i;

            imageObj.onload = function(){
                var self = this;
                setTimeout(function(){
                    var image = new Konva.Image({
                      x: 0,
                      y: 0,
                      image: self,
                      width: self.width,
                      height: self.height
                    });

                    piecesCollection[self.counter] = image;

                    if(image.width > croppedImageGroup.width()) croppedImageGroup.width(image.width);

                    croppedImageGroup.add(image);

                    cropLoadCounter--;

                    if(cropLoadCounter == 0){
                        piecesLoaded();
                    }
                }, 50);
            }

            if(sliceZeroPath) addinationIndex = i > 8 ? '' : '0';

            imageObj.src = levelManager.getRoute(layersFolder + '/png/ios/' + imageSrc.replace('.svg', '_' + addinationIndex + (i + 1) +'.png')) + '?' + Math.random() * Math.random();
            imageObj = null;
        }

        function piecesLoaded(){
            var dy = 0;

            for(var i = 0; i < piecesCollection.length; i++){
                piecesCollection[i].y(dy);
                dy += piecesCollection[i].height();
            }

            croppedImageGroup.width(piecesCollection[0].width())
            croppedImageGroup.height(dy);

            if(contentWidth < croppedImageGroup.width()) contentWidth = croppedImageGroup.width();
            if(contentHeight < croppedImageGroup.height()) contentHeight = croppedImageGroup.height();

            layerArray[index].asset = croppedImageGroup;

            onLayerLoaded(index);
            /*counter--;
            if(counter == 0) setAssetLoaded(assetIndex);*/
        }
    }

    function setAssetLoaded(assetIndex){
        assetOqueue[assetIndex] = true;
        return checkLoad();
    }

    function checkLoad(){
        var flag = true;
        for(var i = 0; i < assetOqueue.length; i++){
            if(!assetOqueue[i]) return false;
        }

        onAssetsLoad();

        return flag;
    }

    function registerAssets(){
        assetOqueue.push(false);
        return (assetOqueue.length - 1);
    }

    function loadAssets(layerArray){
        console.log('start loading images...')
    	var counter = layerArray.length;

        var assetIndex = registerAssets();

        //cachedSceneCanvas=document.getElementById("cacheCanvas");
        cachedSceneCanvas=document.createElement('canvas');

    	for(var i = 0; i < layerArray.length; i++){
    		loadImage(layerArray[i].src, i);
    	}

    	function loadImage(imageSrc, index){
			var imageObj = document.createElement('img');

            var loadDelay = 50;

            if(ieVersion == '11'){
                hiddenDiv.appendChild(imageObj);
                loadDelay = 300;
            }

	    	imageObj.onload = function(){
                setTimeout(function(){
                    /*var image = new Konva.Image({
                      x: 0,
                      y: 0,
                      image: imageObj,
                      width: imageObj.width,
                      height: imageObj.height
                    });*/

                    imageObj.drawCache = function(){
                        var ctx=cachedSceneCanvas.getContext("2d");
                        ctx.drawImage(this,0,0);
                    }

                    if(ieVersion == '11'){
                        hiddenDiv.removeChild(imageObj);
                    }
                    //console.log('layer size is: ' + image.width(), image.height());
                    //console.log(imageObj.hiddenDiv.width, imageObj.hiddenDiv.height)

                    if(imageObj.width > contentWidth) contentWidth = imageObj.width;
                    if(imageObj.height > contentHeight) contentHeight = imageObj.height;

                    layerArray[index].asset = imageObj;

                    //console.log(counter + ' image is loaded');

    				counter--;
    				if(counter == 0) setAssetLoaded(assetIndex);
                }, loadDelay);
			}

            if(renderType == 0){
               imageObj.src = levelManager.getRoute(layersFolder + '/svg/' + imageSrc);
               //console.log('svg');
            } else{
                imageObj.src = levelManager.getRoute(layersFolder + '/png/' + imageSrc.replace('.svg', '.png'));
                //console.log('png');
            }
	    }
    }

    function onAssetsLoad(){
        console.log('assets loaded');
        buildTitleScreen();

        setTimeout(function(){
            preLoader.hidePreloader();
            //controllsLayer.batchDraw();
            imageContainer.drawScene();
            fullscreenLayer.batchDraw();
        }, 600);
    }

    function drawApp(){
        setTimeout(function(){
            drawMap();

            setTimeout(function(){
                controllsLayer.batchDraw();
                answerPointLayer.batchDraw();
            }, 500)
        }, 300);
    }

    function buildTitleScreen(){
        titleScreenGroup = new Konva.Group({
            x: 0,
            y: 0
        });

        var titleText = root.children('title').text();
        var taskText = root.children('question').text();

        var headerGroup = createHead(titleText, taskText);

        titleScreenGroup.add(headerGroup)

        fullscreenLayer.add(titleScreenGroup);

        //fullscreenLayer.draw();

        titleScreenGroup.playButton = createButton(
            (((($(window).width() / stage.scaler)) >> 1) - 71.5),
            (headerGroup.height() + 5) + ((($(window).height() / stage.scaler) - (headerGroup.height() + 5)) >> 1) - 23.5,
            143,
            47,
            'Открыть',
            function(){
                titleScreenGroup.hideTitle(function(){
                    
                    titleScreenGroup.recyle();
                    titleScreenGroup = null;

                    setTimeout(startApp, 200);
                })
            });

        titleScreenGroup.add(titleScreenGroup.playButton);

        titleScreenGroup.hideTitle = function(callback){
            /*if(isMobile){
                titleScreenGroup.hide();
                return;
            }*/

            titleScreenGroup.playButton.hide();

            var outTween = new Konva.Tween({
                node: headerGroup,
                duration: 0.3,
                y: - headerGroup.height(),
                easing: Konva.Easings.Linear,
                onFinish: function(){
                    titleScreenGroup.remove();
                    callback();
                }
            });

            outTween.play();
        }

        titleScreenGroup.recyle = function(){
            titleScreenGroup.removeChildren();
            titleScreenGroup.destroy();
            titleScreenGroup.playButton = null;
            titleScreenGroup.hideTitle = null;
        }

        titleScreenGroup.resizeTitle = function(){
            titleScreenGroup.recyle();
            titleScreenGroup = null;

            buildTitleScreen();
        }

        /*titleScreenGroup.hideTitle(function(){
            setTimeout(startApp, 200);
        })*/

        //fullscreenLayer.draw();
    }

    function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Konva.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Konva.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Konva.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Konva.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler - 35,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Konva.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: $(window).width() / stage.scaler + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Konva.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width() - padding
            });

            if(questionLabel.height() > 432) questionLabel.height(432);

            var questionRect = new Konva.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitleRect.width(),
                height: questionLabel.height() + questionLabel.fontSize() * 0.2 + (padding << 1),
                stroke: headSheme.questionTitle.stroke
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

    function startApp(){

        var scaler = calculateAspectRatioFit(contentWidth, contentHeight, stage.width(), stage.height());

        if(renderType == 2){
            var iosHDScaler = calculateAspectRatioFit(contentWidth, contentHeight, 2250, 2250);
            var iosScaler = calculateAspectRatioFit(contentWidth * iosHDScaler, contentHeight * iosHDScaler, stage.width(), stage.height());
        }

        defaultContentWidth = contentWidth;
        defaultContentHeight = contentHeight;

        contentWidth *= scaler;
        contentHeight *= scaler;

        defaultScaler = scaler;

        //a large transparent background to make everything draggable
        var background = new Konva.Rect({
            x: -1000,
            y: -1000,
            width: 2000,
            height: 2000,
            fill: "#000000",
            opacity: 0
        });

        draggableLayer.add(background);

        contentGroup.sourceGroup = new Konva.Group();

        cachedSceneCanvas.width = defaultContentWidth;
        cachedSceneCanvas.height = defaultContentHeight;

        for(var i = 0; i < layerArray.length; i++){
            layerArray[i].isAdded = false;
            layerArray[i].asset._depth = i;
                if(layerArray[i].isVisible){
                    //contentGroup.sourceGroup.add(layerArray[i].asset);
                    //_context.drawImage(layerArray[i].asset,layerArray[i].asset.width,layerArray[i].asset.height);
                    layerArray[i].asset.drawCache();
                    layerArray[i].isAdded = true;
                }
        }

        contentGroup.rebuild = function(){
            var ctx = cachedSceneCanvas.getContext('2d');

            ctx.clearRect(0, 0, cachedSceneCanvas.width, cachedSceneCanvas.height);

            for(var i = 0; i < layerArray.length; i++){
                if(layerArray[i].isVisible){
                    layerArray[i].asset.drawCache();
                }
            }
        }

        //console.log(cachedSceneCanvas);

        //$(document).append(cachedSceneCanvas);

        //contentGroup._cache = cachedSceneCanvas;
        /*contentGroup._cache.canvas = {
            scene: cachedSceneCanvas,
            filter: cachedSceneCanvas,
            hit: cachedSceneCanvas,
            x : 0,
            y : 0
        };*/

        draggableLayer.add(contentGroup);

        //Test area

        drawMap = function(){
            draggableLayer.draw();
        }
        
        /*if(renderType == 0){

            //contentGroup.cache.process();

            contentGroup.add(contentGroup.sourceGroup);

            contentGroup.sourceGroup.scale({
                x: scaler,
                y: scaler
            })

            contentGroup.cache({
                width: (defaultContentWidth),
                height: (defaultContentHeight),
                x: 0,
                y: 0
            })            
        } else */
        if(renderType == 1 || renderType == 0){
            //contentGroup.add(contentGroup.sourceGroup);

            /*contentGroup.sourceGroup.cache({
                width: defaultContentWidth,
                height: defaultContentHeight,
                x: 0,
                y: 0
            });*/

            contentGroup.scale({
                x: scaler,
                y: scaler
            });

            //draggableLayer.draw();

            console.log(draggableLayer.getCanvas())

            contentGroup.cacheImage = new Konva.Image({
                image: cachedSceneCanvas,
                width: defaultContentWidth,
                height: defaultContentHeight
            });

            //contentGroup.sourceGroup.remove();

            contentGroup.add(contentGroup.cacheImage);
            //contentGroup.add(cacheShape);

            /*contentGroup.drawScene(cachedSceneCanvas, contentGroup, true);

            contentGroup._cache.canvas = {
                scene: cachedSceneCanvas,
                filter: cachedSceneCanvas,
                hit: cachedSceneCanvas,
                x : 0,
                y : 0
            };*/

            drawMap = bitmapDrawMap;
        } else if(renderType == 2){
            contentGroup.extraGroup = new Konva.Group();
            contentGroup.add(contentGroup.extraGroup);
            contentGroup.extraGroup.add(contentGroup.sourceGroup);

            contentGroup.sourceGroup.scale({
                x: iosHDScaler,
                y: iosHDScaler
            })

            defaultContentWidth *= iosHDScaler;
            defaultContentHeight *= iosHDScaler;

            /*contentGroup.extraGroup.cache({
                width: defaultContentWidth,
                height: defaultContentHeight,
                x: 0,
                y: 0
            });*/

            contentGroup.extraGroup.scale({
                x: iosScaler,
                y: iosScaler
            });

            contentGroup.iosScaler = iosScaler;

            contentWidth = defaultContentWidth * iosScaler;
            contentHeight = defaultContentHeight * iosScaler;

            drawMap = iosDrawMap;
        }

        centerMap();

        //draggableLayer.draw();

        //printer = printer.process();

        map.zoom = regularZoom;
        map.showLayer = regularShowLayer;
        map.hideLayer = regularHideLayer;

        if(renderType == 1 || renderType == 0) map.zoom = bitmapZoom;
        if(renderType == 2) map.zoom = iosZoom;

        map.showLayer.process();
        map.hideLayer.process();

        buildAnswerPoints(points);

        if(renderType == 0 && isMobile){
            contentGroup.cache.process();
            draggableLayer.draw.process();
            draggableLayer.drawScene.process();

            draggableLayer.on('dragend', function(){
                var dx = - draggableLayer.x() - (stage.width() * stage.scaleX());
                var dy = - draggableLayer.y() - (stage.height() * stage.scaleY());

                if(dx < 0) dx = 0;
                if(dy < 0) dy = 0;

                contentGroup.cache({
                    width: (stage.width() * stage.scaleX()) * 3,
                    height: (stage.height() * stage.scaleY()) * 3,
                    x: dx,
                    y: dy
                })

                contentGroup.x(dx);
                contentGroup.y(dy);

                drawMap();
            });
        } else if(!isMobile){            
            draggableLayer.on('dragstart', grabPointer);
            draggableLayer.on('dragend', resetPointer);
        }

        controlls = buildControlls(0, 0);

        controlls.hide();

        controllsLayer.add(controlls);

        viewContainer.initTween();
        if(taskEnabled) taskPanel.initTween();

        zoomCounter.initZoomCounter();

        drawApp();

        map.rebuildContent = function(newWidth, newHeight){
            defaultScaler = calculateAspectRatioFit(contentWidth, contentHeight, newWidth, newHeight);

            var scaler = defaultScaler + zoomLevel;

            preventActions = true;
            draggableLayer.draggable(false);

            contentGroup.clearCache();

            var visibleLayerCollection = contentGroup.children;

            var newWidth = 0;
            var newHeight = 0;

            contentWidth = 0;
            contentHeight = 0;

            for(var i = 0; i < visibleLayerCollection.length; i++){
                visibleLayerCollection[i].scale({
                    x: scaler,
                    y: scaler
                })

                newWidth = visibleLayerCollection[i].width() * visibleLayerCollection[i].scaleX();
                newHeight = visibleLayerCollection[i].height() * visibleLayerCollection[i].scaleY();

                if(newWidth > contentWidth) contentWidth = newWidth;
                if(newHeight > contentHeight) contentHeight = newHeight;
            }

            answerPointLayer.scale({
                x: scaler,
                y: scaler
            })

            contentGroup.cache({
                width: contentWidth,
                height: contentHeight
            });

            answerPointLayer.batchDraw();

            draggableLayer.draw();

            preventActions = false;
            draggableLayer.draggable(true);
        }

        function hideCachedLayer(layer){
            sourceGroup.children[layer.depth].hide();

            sourceGroup.show();
            draggableLayer.draw();

            sourceGroup.toImage({
                callback: function(data){
                    contentGroup.image.setImage(data);

                    if(renderType != 1){

                        contentGroup.image.width(contentWidth);
                        contentGroup.image.height(contentHeight);

                        contentGroup.image.scale({
                            x: 1,
                            y: 1
                        });
                    }

                    draggableLayer.draw();
                },
                width: defaultContentWidth,
                height: defaultContentHeight,
                x: (renderType != 1) ? draggableLayer.x() : 0,
                y: (renderType != 1) ? draggableLayer.y() : 0
            });

            sourceGroup.hide();
            draggableLayer.draw();
        }

        function showCachedLayer(layer){
            sourceGroup.children[layer.depth].show();

            sourceGroup.show();
            draggableLayer.draw();

            sourceGroup.toImage({
                callback: function(data){
                    contentGroup.image.setImage(data);

                    if(renderType != 1){

                        contentGroup.image.width(contentWidth);
                        contentGroup.image.height(contentHeight);

                        contentGroup.image.scale({
                            x: 1,
                            y: 1
                        });
                    }

                    draggableLayer.draw();
                },
                width: defaultContentWidth,
                height: defaultContentHeight,
                x: (renderType != 1) ? draggableLayer.x() : 0,
                y: (renderType != 1) ? draggableLayer.y() : 0
            });

            sourceGroup.hide();
            draggableLayer.draw();
        }

		function regularHideLayer_(layer, invoker){
            console.log('hide');

			layer.asset.hide();

            if(renderType == 0){

                if(!isMobile){
        			contentGroup.cache({
        				width: contentWidth,
        				height: contentHeight
        			})
                } else{
                    var dx = - draggableLayer.x() - (stage.width() * stage.scaleX());
                    var dy = - draggableLayer.y() - (stage.height() * stage.scaleY());

                    if(dx < 0) dx = 0;
                    if(dy < 0) dy = 0;

                    contentGroup.cache({
                        width: (stage.width() * stage.scaleX()) * 3,
                        height: (stage.height() * stage.scaleY()) * 3,
                        x: dx,
                        y: dy
                    })

                    contentGroup.x(dx);
                    contentGroup.y(dy);
                }
            } else if(renderType == 2){
                /*contentGroup.extraGroup.cache({
                    width: defaultContentWidth,
                    height: defaultContentHeight,
                    x: 0,
                    y: 0
                });*/
            } else if(renderType == 1){
                contentGroup.sourceGroup.cache({
                    width: defaultContentWidth,
                    height: defaultContentHeight
                })

                setTimeout(function(){
                    applyLayerChanges(invoker);
                }, 1000);

                return;
            }

            //draggableLayer.drawScene();

            applyLayerChanges(invoker);
		}

        function applyLayerChanges(invoker){
            drawMap();

            setTimeout(function(){
                invoker.listening(true);
                preventActions = false;
                invoker.getLayer().drawScene();

                preLoader.hidePreloader();
                imageContainer.drawScene();

                //draggableLayer.listening(true);
                draggableLayer.draggable(true);
                //console.log('listening: ' + invoker.listening(), preventActions);
                console.log('done changing layers');
            }, 1000); 
        }

        function regularShowLayer(layer, invoker){
            layer.isVisible = true;
            contentGroup.rebuild();

            setTimeout(function(){
                applyLayerChanges(invoker);
            }, 1000);
        }

        function regularHideLayer(layer, invoker){
            layer.isVisible = false;
            contentGroup.rebuild();

            setTimeout(function(){
                applyLayerChanges(invoker);
            }, 1000);
        }

		function regularShowLayer_(layer, invoker){
            console.log('show');

			//Zoom this layer until it fit the others by zoomLevel
            if(!layer.isAdded){
                contentGroup.sourceGroup.add(layer.asset);
                layer.asset.setZIndex(layer.asset._depth);
                layer.isAdded = true;
            }
			layer.asset.show();

			if(renderType == 0){

                if(!isMobile){
                    /*contentGroup.cache({
                        width: contentWidth,
                        height: contentHeight
                    })*/
                } else{
                    var dx = - draggableLayer.x() - (stage.width() * stage.scaleX());
                    var dy = - draggableLayer.y() - (stage.height() * stage.scaleY());

                    if(dx < 0) dx = 0;
                    if(dy < 0) dy = 0;

                    contentGroup.cache({
                        width: (stage.width() * stage.scaleX()) * 3,
                        height: (stage.height() * stage.scaleY()) * 3,
                        x: dx,
                        y: dy
                    })

                    contentGroup.x(dx);
                    contentGroup.y(dy);
                }
            } else if(renderType == 2){
                /*contentGroup.extraGroup.cache({
                    width: defaultContentWidth,
                    height: defaultContentHeight,
                    x: 0,
                    y: 0
                });*/
            } else if(renderType == 1){
                contentGroup.sourceGroup.cache({
                    width: defaultContentWidth,
                    height: defaultContentHeight
                });

                setTimeout(function(){
                    applyLayerChanges(invoker);
                }, 1000);

                return;
            }

            //draggableLayer.drawScene();

            applyLayerChanges(invoker);      
		}

        function iosZoom(zoomAmount){
            var newZoom = zoomLevel + zoomAmount;

            if(newZoom > maxZoomLevel || newZoom < 0) return;

            if(preventActions) return;

            zoomCounter.setValue(zoomAmount);
            controllsLayer.draw();

            preventActions = true;
            draggableLayer.draggable(false);

            performZoomAction = performZoomAction.process();

            var start = new Date().getTime();

            performZoomAction(start);

            function performZoomAction(){
                zoomLevel += zoomAmount;

                //contentGroup.clearCache();

                var visibleLayerCollection = contentGroup.children;
                var newWidth = 0;
                var newHeight = 0;

                var prevWidth = contentWidth;
                var prevHeight = contentHeight;

                contentWidth = 0;
                contentHeight = 0;

                //console.log(prevWidth, prevHeight);

                //console.log(contentGroup.scaleX() * contentGroup.extraGroup.scaleX(), defaultScaler);

                contentGroup.scale({
                    x: +(contentGroup.scaleX() + zoomAmount).toFixed(1),
                    y: +(contentGroup.scaleY() + zoomAmount).toFixed(1)
                });

                /*contentWidth = (defaultContentWidth * contentGroup.iosScaler) + (defaultContentWidth * contentGroup.iosScaler) * zoomAmount;
                contentHeight = (defaultContentHeight * contentGroup.iosScaler) + (defaultContentHeight * contentGroup.iosScaler) * zoomAmount;*/

                contentWidth = (defaultContentWidth * contentGroup.iosScaler) * contentGroup.scaleX();
                contentHeight = (defaultContentHeight * contentGroup.iosScaler) * contentGroup.scaleX();

                //console.log(defaultContentWidth * contentGroup.iosScaler, defaultContentHeight * contentGroup.iosScaler, contentWidth, contentHeight, prevWidth, prevHeight);

                //alert((stage.width() * stage.scaleX()) * 3);

                //alert(contentWidth + ' ---> ' + contentHeight)

                var zoomPoint = {
                    x: (contentWidth - prevWidth) >> 1,
                    y: (contentHeight - prevHeight) >> 1
                }

                if(zoomAmount < 0){
                    zoomPoint.x = (contentWidth - prevWidth);
                    zoomPoint.y = (contentHeight - prevHeight);
                }

                var newPoint = {
                    x: draggableLayer.x() - zoomPoint.x,
                    y: draggableLayer.y() - zoomPoint.y
                }

                if(newPoint.x > 0) newPoint.x = 0;
                if(newPoint.y > 0) newPoint.y = 0;

                var answerCollection = answerPointsGroup.children;

                for(i = 0; i < answerCollection.length; i++){
                    answerCollection[i].x(answerCollection[i].defaultX * defaultScaler * (1 + zoomLevel));
                    answerCollection[i].y(answerCollection[i].defaultY * defaultScaler * (1 + zoomLevel));

                    if(centerPoints){
                        answerCollection[i].x(answerCollection[i].x() - (answerCollection[i].width() >> 1));
                        answerCollection[i].y(answerCollection[i].y() - (answerCollection[i].height() >> 1));
                    }
                }

                draggableLayer.x(newPoint.x);
                draggableLayer.y(newPoint.y);

                answerPointLayer.x(draggableLayer.x());
                answerPointLayer.y(draggableLayer.y());

                var dx = - draggableLayer.x() - (stage.width() * stage.scaleX());
                var dy = - draggableLayer.y() - (stage.height() * stage.scaleY());

                if(dx < 0) dx = 0;
                if(dy < 0) dy = 0;

                /*contentGroup.cache({
                    width: (stage.width() * stage.scaleX()) * 3,
                    height: (stage.height() * stage.scaleY()) * 3,
                    x: dx,
                    y: dy
                })

                contentGroup.x(dx);
                contentGroup.y(dy);*/

                centerMap();

                answerPointLayer.batchDraw();

                //draggableLayer.drawScene();
                drawMap();

                preventActions = false;
                draggableLayer.draggable(true);

                var end = new Date().getTime();
                var time = end - start;
                //console.log('done zooming at zoom level ' + contentGroup.scaleX() + ', ecexution time is: ' + time);
            }
        }

        /*function centerMap(){
            mapMargin = ((stage.width() * stage.scaleX()) >> 1) - (contentWidth >> 1);
            if(mapMargin < 0) mapMargin = 0;

            contentGroup.x(mapMargin);
            answerPointsGroup.x(mapMargin);

            //console.log((stage.width() * stage.scaleX()), contentWidth);

            //console.log(mapMargin);
        }*/

		function regularZoom(zoomAmount){
            var newZoom = zoomLevel + zoomAmount;

            if(newZoom > maxZoomLevel || newZoom < 0) return;

            if(preventActions) return;

            zoomCounter.setValue(zoomAmount);
            controllsLayer.drawScene();

            preventActions = true;
            draggableLayer.draggable(false);

            performZoomAction = performZoomAction.process();

            var start = new Date().getTime();

            performZoomAction(start);

            function performZoomAction(){
                zoomLevel += zoomAmount;

                //contentGroup.clearCache();

                var visibleLayerCollection = contentGroup.children;
                var newWidth = 0;
                var newHeight = 0;

                var prevWidth = contentWidth;
                var prevHeight = contentHeight;

                contentWidth = 0;
                contentHeight = 0;

                contentGroup.sourceGroup.scale({
                    x: +(contentGroup.sourceGroup.scaleX() + zoomAmount).toFixed(1),
                    y: +(contentGroup.sourceGroup.scaleY() + zoomAmount).toFixed(1)
                });

                contentWidth = defaultContentWidth * +(defaultScaler + zoomLevel).toFixed(1);
                contentHeight = defaultContentHeight * +(defaultScaler + zoomLevel).toFixed(1);

                //alert((stage.width() * stage.scaleX()) * 3);

                //alert(contentWidth + ' ---> ' + contentHeight)

                var zoomPoint = {
                    x: (contentWidth - prevWidth) >> 1,
                    y: (contentHeight - prevHeight) >> 1
                }

                if(zoomAmount < 0){
                    zoomPoint.x = (contentWidth - prevWidth);
                    zoomPoint.y = (contentHeight - prevHeight);
                }

                var newPoint = {
                    x: draggableLayer.x() - zoomPoint.x,
                    y: draggableLayer.y() - zoomPoint.y
                }

                if(newPoint.x > 0) newPoint.x = 0;
                if(newPoint.y > 0) newPoint.y = 0;

                var answerCollection = answerPointsGroup.children;

                for(i = 0; i < answerCollection.length; i++){
                    answerCollection[i].x(answerCollection[i].defaultX * +(zoomLevel + defaultScaler).toFixed(1));
                    answerCollection[i].y(answerCollection[i].defaultY * +(zoomLevel + defaultScaler).toFixed(1));

                    if(centerPoints){
                        answerCollection[i].x(answerCollection[i].x() - (answerCollection[i].width() >> 1));
                        answerCollection[i].y(answerCollection[i].y() - (answerCollection[i].height() >> 1));
                    }
                }

                draggableLayer.x(newPoint.x);
                draggableLayer.y(newPoint.y);

                answerPointLayer.x(draggableLayer.x());
                answerPointLayer.y(draggableLayer.y());

                if(isMobile){

                    var dx = - draggableLayer.x() - (stage.width() * stage.scaleX());
                    var dy = - draggableLayer.y() - (stage.height() * stage.scaleY());

                    if(dx < 0) dx = 0;
                    if(dy < 0) dy = 0;

                    contentGroup.cache({
                        width: (stage.width() * stage.scaleX()) * 3,
                        height: (stage.height() * stage.scaleY()) * 3,
                        x: dx,
                        y: dy
                    });

                    contentGroup.x(dx);
                    contentGroup.y(dy);
                } else{
                    contentGroup.cache({
                        width: contentWidth,
                        height: contentHeight
                    });
                }

                centerMap();

                answerPointLayer.batchDraw();

                draggableLayer.drawScene();

                preventActions = false;
                draggableLayer.draggable(true);

                var end = new Date().getTime();
                var time = end - start;
                //console.log('done zooming at zoom level ' + contentGroup.sourceGroup.scaleX() + ', ecexution time is: ' + time);
            }
        }

        function optimizedRegularZoom(zoomAmount){

            var newZoom = zoomLevel + zoomAmount;

            if(newZoom > maxZoomLevel || newZoom < 0) return;

            if(preventActions) return;

            zoomCounter.setValue(zoomAmount);
            controllsLayer.draw();

            preventActions = true;
            draggableLayer.draggable(false);

            zoomLevel += zoomAmount;

            var prevWidth = contentWidth;
            var prevHeight = contentHeight;

            contentWidth = 0;
            contentHeight = 0;

            var visibleLayerCollection = sourceGroup.children;

            for(var i = 0; i < visibleLayerCollection.length; i++){
                visibleLayerCollection[i].scale({
                    x: visibleLayerCollection[i].scaleX() + zoomAmount,
                    y: visibleLayerCollection[i].scaleY() + zoomAmount
                });

                if(visibleLayerCollection[i].width() * visibleLayerCollection[i].scaleX() > contentWidth) contentWidth = visibleLayerCollection[i].width() * visibleLayerCollection[i].scaleX();
                if(visibleLayerCollection[i].height() * visibleLayerCollection[i].scaleY() > contentHeight) contentHeight = visibleLayerCollection[i].height() * visibleLayerCollection[i].scaleY();
            }

            sourceGroup.show();

            sourceGroup.toImage({
                callback: function(data){
                    contentGroup.image.setImage(data);

                    contentGroup.image.width(contentWidth);
                    contentGroup.image.height(contentHeight);

                    contentGroup.image.scale({
                        x: 1,
                        y: 1
                    });

                    var zoomPoint = {
                        x: (contentWidth - prevWidth) >> 1,
                        y: (contentHeight - prevHeight) >> 1
                    }

                    if(zoomAmount < 0){
                        zoomPoint.x = (contentWidth - prevWidth);
                        zoomPoint.y = (contentHeight - prevHeight);
                    }

                    var newPoint = {
                        x: draggableLayer.x() - zoomPoint.x,
                        y: draggableLayer.y() - zoomPoint.y
                    }

                    if(newPoint.x > 0) newPoint.x = 0;
                    if(newPoint.y > 0) newPoint.y = 0;

                    var answerCollection = answerPointsGroup.children;

                    for(i = 0; i < answerCollection.length; i++){
                        answerCollection[i].x(answerCollection[i].defaultX * (zoomLevel + defaultScaler));
                        answerCollection[i].y(answerCollection[i].defaultY * (zoomLevel + defaultScaler));
                    }

                    draggableLayer.x(newPoint.x);
                    draggableLayer.y(newPoint.y);

                    answerPointLayer.x(draggableLayer.x());
                    answerPointLayer.y(draggableLayer.y());

                    centerMap();

                    answerPointLayer.batchDraw();

                    draggableLayer.draw();

                    preventActions = false;
                    draggableLayer.draggable(true);
                },
                width: contentWidth,
                height: contentHeight,
                x: 0,
                y: 0
            });

            sourceGroup.hide();
            draggableLayer.draw();
        }

        function bitmapZoom(zoomAmount){

			var newZoom = zoomLevel + zoomAmount;

            if(newZoom > maxZoomLevel || newZoom < 0) return;

            if(preventActions) return;

            zoomCounter.setValue(zoomAmount);
            controllsLayer.drawScene();

            preventActions = true;
            draggableLayer.draggable(false);

            performZoomAction = performZoomAction.process();

            var start = new Date().getTime();

            performZoomAction(start)

            function performZoomAction(start){
                zoomLevel += zoomAmount;

                var newWidth = 0;
                var newHeight = 0;

                var prevWidth = contentWidth;
                var prevHeight = contentHeight;

                contentGroup.scale({
                    x: +(contentGroup.scaleX() + zoomAmount).toFixed(1),
                    y: +(contentGroup.scaleY() + zoomAmount).toFixed(1)
                });

                var answerCollection = answerPointsGroup.children;

                for(i = 0; i < answerCollection.length; i++){
                    answerCollection[i].x(answerCollection[i].defaultX * +(zoomLevel + defaultScaler).toFixed(1));
                    answerCollection[i].y(answerCollection[i].defaultY * +(zoomLevel + defaultScaler).toFixed(1));

                    if(centerPoints){
                        answerCollection[i].x(answerCollection[i].x() - (answerCollection[i].width() >> 1));
                        answerCollection[i].y(answerCollection[i].y() - (answerCollection[i].height() >> 1));
                    }
                }

                contentWidth = defaultContentWidth * +(defaultScaler + zoomLevel).toFixed(1);
                contentHeight = defaultContentHeight * +(defaultScaler + zoomLevel).toFixed(1);

                /*console.log(defaultContentWidth, defaultContentHeight,
                    contentWidth, contentHeight,
                    prevWidth, prevHeight,
                    defaultScaler, contentGroup.scaleX(),
                    defaultScaler + zoomLevel);*/

                var zoomPoint = {
                    x: (contentWidth - prevWidth) >> 1,
                    y: (contentHeight - prevHeight) >> 1
                }

                if(zoomAmount < 0){
                    zoomPoint.x = (contentWidth - prevWidth);
                    zoomPoint.y = (contentHeight - prevHeight);
                }

                var newPoint = {
                    x: draggableLayer.x() - zoomPoint.x,
                    y: draggableLayer.y() - zoomPoint.y
                }

                if(newPoint.x > 0) newPoint.x = 0;
                if(newPoint.y > 0) newPoint.y = 0;

                draggableLayer.x(newPoint.x);
                draggableLayer.y(newPoint.y);

                answerPointLayer.x(draggableLayer.x());
                answerPointLayer.y(draggableLayer.y());

                centerMap();

                answerPointLayer.batchDraw();

                draggableLayer.batchDraw();

                preventActions = false;
                draggableLayer.draggable(true);

                var end = new Date().getTime();
                var time = end - start;
                //console.log('done zooming at zoom level ' + contentGroup.scaleX() + ', ecexution time is: ' + time);
            }
		}

		var zoom = function(e) {
          if(e.wheelDeltaY > 0){
            zoomIn();
          } else{
            zoomOut();
          }
		}

        controlls.show();

        /*if(answerPointLayer.isLoaded){
            answerPointLayer.scale({
                x: defaultScaler,
                y: defaultScaler
            })

            answerPointLayer.draw();
        } else{
            answerPointLayer.forceScale = true;
        }*/

		document.addEventListener("mousewheel", zoom, false);
    }

    function resetZoom(){

        if(renderType == 2){
            contentGroup.scale({
                x: 1,
                y: 1
            });
        } else if(renderType == 1){
            contentGroup.scale({
                x: defaultScaler,
                y: defaultScaler
            });
        } else{
            contentGroup.sourceGroup.scale({
                x: defaultScaler,
                y: defaultScaler
            });
        }

        zoomLevel = 0;
        zoomCounter.reset();

        var answerCollection = answerPointsGroup.children;

        for(i = 0; i < answerCollection.length; i++){
            answerCollection[i].x(answerCollection[i].defaultX * defaultScaler);
            answerCollection[i].y(answerCollection[i].defaultY * defaultScaler);

            if(centerPoints){
                answerCollection[i].x(answerCollection[i].x() - (answerCollection[i].width() >> 1));
                answerCollection[i].y(answerCollection[i].y() - (answerCollection[i].height() >> 1));
            }
        }

        if(renderType == 2){
            contentWidth = (defaultContentWidth * contentGroup.iosScaler);
            contentHeight = (defaultContentHeight * contentGroup.iosScaler);
        } else{
            contentWidth = (defaultContentWidth * defaultScaler);
            contentHeight = (defaultContentHeight * defaultScaler);
        }

        answerCollection = null;
    }

    function centerMap(containerWidth){
        containerWidth = containerWidth || (stage.width() * stage.scaleX());
        mapMargin = (containerWidth >> 1) - (contentWidth >> 1);
        if(mapMargin < 0) mapMargin = 0;

        contentGroup.x(mapMargin);
        answerPointsGroup.x(mapMargin);

        console.log(containerWidth, stage.width() * stage.scaleX(), contentWidth);
    }

    function preloadAnswerPoints(points){
        var x;
        var y;
        var text;
        var name;
        var image;
        var id;

        var loadCounter = {};

        pointAssets = [];

        dx = +root.children('points').attr('dx');
        dy = +root.children('points').attr('dy');

        var pointScaler = +root.children('points').attr('bitmapScaler');
        if(isNaN(pointScaler)) pointScaler = 1;
        if(renderType == 0) pointScaler = 1;

        if(typeof dx == 'undefined' || dx == '' || !dx) dx = 0;
        if(typeof dy == 'undefined' || dx == '' || !dy) dy = 0;

        $(points).each(function(index){
            x = (+$(this).attr('x') + dx) * pointScaler;
            y = (+$(this).attr('y') + dy) * pointScaler;

            textData = this;
            name = $(this).attr('name');
            image = levelManager.getRoute($(this).attr('img'));
            id = $(this).attr('id');

            if(typeof image != 'undefined' && image != ''){

                var contentImgObj = new Image();

                contentImgObj.isLoaded = false;

                contentImgObj.assetIndex = registerAssets();

                contentImgObj.onload = function(){
                    contentImgObj.isLoaded = true;
                    setAssetLoaded(this.assetIndex)
                }

                contentImgObj.src = image;

            }

            pointAssets.push({
                textData: textData,
                name: name,
                image: contentImgObj,
                id: id,
                x: x,
                y: y
            })

            //createAnswerPoint(x, y, name, textData, id, image, loadCounter);
        });
    }

    function buildAnswerPoints(points, isRebuild){

        if(isRebuild){
            //console.log(answerPointsGroup);
            var answerPointCollection = answerPointsGroup.getChildren();
            for(var j = 0; j < answerPointCollection.length; j++){
                answerPointCollection[j].recyle();
            }
            answerPointsGroup.removeChildren();

            answerPointCollection = null;
        }

        var pointData = null;

        for(var i = 0; i < pointAssets.length; i++){
            pointData = pointAssets[i];
            createAnswerPoint(pointData.x, pointData.y, pointData.name, pointData.textData, pointData.id, pointData.image);
        }


        if(!isRebuild){
            draggableLayer.on('dragmove', function(){
                answerPointLayer.x(draggableLayer.x());
                answerPointLayer.y(draggableLayer.y());

                answerPointLayer.batchDraw();
            })
        }

        //answerPointLayer.batchDraw();
    }

    function xmlPointInformationToData(textData){
        var text = $(textData).children('text');

        text = (typeof text == 'undefined') ? '' : text.text();

        var dataObject = {
            text: text,
            links: []
        };

        var xmlLinkObject = $(textData).children('links');

        var url = '';
        var label = '';

        if(typeof xmlLinkObject != 'undefined'){
            xmlLinkObject = xmlLinkObject.children('link');

            if(typeof xmlLinkObject != 'undefined'){
                $(xmlLinkObject).each(function(index){
                    url = $(this).attr('url');
                    label = $(this).text();

                    dataObject.links.push({
                        url: url,
                        label: label
                    });
                });
            }

            if(dataObject.links.length > maxLinkCount) maxLinkCount = dataObject.links.length;
        }

        return dataObject;
    }

    function createAnswerPoint(x, y, name, textData, id, contentImage, postDraw){
        var answerPoint = new Konva.Group({
            x: x * defaultScaler,
            y: y * defaultScaler
        })

        answerPoint.pointId = id;

        var pointData = xmlPointInformationToData(textData);

        if(pointData.links.length == 0 && pointData.text == '' && typeof contentImage == 'undefined' && typeof name == 'undefined' && typeof id == 'undefined'){
            answerPoint.isEmpty = true;
        }

        answerPoint.pointData = pointData;

        //var imgObj = new Image();

        var imgObj = controllsGraphic.mapAnswerPoint;

        var image = new Konva.Sprite({
            x: 0,
            y: 0,
            image: imgObj,
            animation: 'clear',
            animations: {
                clear: [
                  // x, y, width, height
                  0,0,40,40
                ],
                active: [
                  // x, y, width, height
                  0,40,40,40
                ]
            },
            frameRate: 7,
            frameIndex: 0,
            width: 40,
            height: 40
        });

        answerPoint.width(image.width());
        answerPoint.height(image.height());

        if(centerPoints){
            answerPoint.x(answerPoint.x() - (answerPoint.width() >> 1));
            answerPoint.y(answerPoint.y() - (answerPoint.height() >> 1));
        }

        image.label = name;

        image.pointData = pointData;

        answerPoint.defaultX = x;
        answerPoint.defaultY = y;

        answerPoint.setActive = function(){
            image.animation('active');
            answerPoint.isActive = true;
        }

        answerPoint.setClear = function(){
            if(showHistory) return;
            image.animation('clear');
            answerPoint.isActive = false;
        }

        answerPoint.recyle = function(){
            image.pointData = null;
            image.label = null;
            image.contentImage = null;
            this.pointData = null;
            image.destroy();
            image = null;
            this.destroyChildren();
            this.destroy();
        }

        image.answerID = id;

        image.contentImage = contentImage;

        var hitCircle = new Konva.Circle({
            x: (image.width() >> 1),
            y: (image.height() >> 1),
            radius: (image.width() >> 1) * answerPointHitScaler,
            fill: 'white',
            strokeWidth: 0
        })

        hitCircle.opacity(0);

        answerPoint.add(image);
        answerPoint.add(hitCircle);

        answerPointsGroup.add(answerPoint);

        hitCircle.on(events[isMobile].click, function(){
            if(preventActions) return;
            if(taskPanel.taskMode && !taskPanel.jumpToNext && !taskPanel.isFinished){
                taskPanel.checkAnswer(name, id);
            } else if(taskPanel.taskMode && taskPanel.jumpToNext){
                taskPanel.nextQuestion();
            } else{
                if(answerPoint.isEmpty) return;
                viewContainer.setValue(image.label, image.pointData, image.contentImage, answerPoint);
                if(!viewContainer.isExpanded) viewContainer.toggle();
            }

            controllsLayer.draw();
        });

        hitCircle.on(events[isMobile].mouseover, hoverPointer);
        hitCircle.on(events[isMobile].mouseout, resetPointer);

        imgObj = null;
    }

    function expandContainer(container){
        if(expandedContainer != null && container != expandedContainer) hideContainer(expandedContainer);

        expandedContainer = container;
        container.showContainer();

        controllsPanel.glow(container.btn);

        container.isExpanded = true;
    }

    function hideContainer(container){
        container.hideContainer();
        container.isExpanded = false;
        controllsPanel.unGlow(container.btn);
    }

    function buildViewContainer(x, y){
        var containerWidth = 350;

        var containerMaxHeight = 350;
        var containerMinHeight = 220;

        var containerSheme = colorSheme.controllsPanel.container;

        var containerGroup = new Konva.Group({
            x: - containerWidth,
            y: y
        });

        containerGroup.btn = controllsPanel.viewPanelBtn;

        containerGroup.contentImage = new Konva.Image({
          x: 15,
          y: 0
        });

        containerGroup.contentImage.on(events[isMobile].mouseover, hoverPointer);
        containerGroup.contentImage.on(events[isMobile].mouseout, resetPointer);

        containerGroup.contentImage.hide();

        containerGroup.contentImage.on(events[isMobile].click, function(){
            imageContainer.zoomImage(containerGroup.contentImage.getImage());
        })

        var headerGroup = new Konva.Group();

        var headerRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: containerWidth,
            height: 40 * mobileScaler,
            fill: containerSheme.header.backgroundColor
        });

        var headerLabel = new Konva.Text({
            x: 15,
            y: (headerRect.height() >> 1) - (9 * mobileScaler),
            text: 'Информация',
            fontFamily: mainFont,
            fontSize: 18 * mobileScaler,
            //fontStyle: 'bold',
            fill: containerSheme.header.fontColor
        });

        var rectangle = new Konva.Rect({
            x: 0,
            y: headerRect.height(),
            width: containerWidth,
            height: containerMaxHeight,
            fill: 'white',
            stroke: containerSheme.stroke,
            strokeWidth: containerSheme.strokeWidth
        });

        var header = new Konva.Text({
            x: 15,
            y: headerRect.height() + 5,
            fontSize: 16 * mobileScaler,
            fontFamily: mainFont,
            //fontStyle: 'bold',
            width: containerWidth - 30,
            fill: 'black',
            text: 'Sample header'
        });

        var contentText = new Konva.Text({
            x: header.x(),
            y: headerRect.height() + 30,
            fontSize: 12 * mobileScaler,
            fontFamily: mainFont,
            width: containerWidth - 38,
            fill: 'black',
            text: 'Sample text'
        });

        function updateLinks(position, links){
            var linksHeight = 0;

            for(var i = 0; i < containerGroup.links.length; i++){
                if(i < links.length){
                    containerGroup.links[i].show();
                    containerGroup.links[i].y(position + linksHeight);

                    containerGroup.links[i].setValue(links[i].url, links[i].label);

                    linksHeight += containerGroup.links[i].height() + containerGroup.links[i].fontSize() * 0.2;

                    continue;
                }

                containerGroup.links[i].hide();
            }

            return linksHeight;
        }

        var scrollContainer = new Konva.Group();
        var textHolder = new Konva.Group();

        containerGroup.links = [];

        if(maxLinkCount > 0){
            var link;
            var prevLink;

            for(var i = 0; i < maxLinkCount; i++){
                link = new Konva.Text({
                    x: contentText.x(),
                    y: ((prevLink == null) ? (contentText.height() + 5 * i + contentText.y() + contentText.fontSize() * 0.2) : (prevLink.height() + prevLink.y() + prevLink.fontSize() * 0.2)),
                    text: 'teh link',
                    fontSize: contentText.fontSize(),
                    fontFamily: contentText.fontFamily(),
                    width: contentText.width(),
                    fill: containerSheme.linkColor
                });

                link.url = '';

                link.setValue = function(url, label){
                    label = label == '' ? url : label;

                    this.url = url;
                    this.text('• ' + label);
                }

                link.on(events[isMobile].mouseover, function(){
                    //this.fill('orange');
                    hoverPointer();
                    //textHolder.drawScene();
                });

                link.on(events[isMobile].mouseout, function(){
                    //this.fill('blue');
                    resetPointer();
                    //textHolder.drawScene();
                });

                link.on(events[isMobile].click, function(){
                    window.open(this.url, '_blank');
                });

                link.hide();

                containerGroup.links.push(link);

                textHolder.add(link);

                prevLink = link;
            }
        }

        scrollContainer.setClip({
            x: 0,
            y: headerRect.height() + 2,
            width: rectangle.width(),
            height: rectangle.height() - 12
        })

        containerGroup.isActive = false;

        textHolder.add(header);
        textHolder.add(contentText);
        textHolder.add(containerGroup.contentImage);

        scrollContainer.add(textHolder);

        containerGroup.defaultX = - containerWidth;

        containerGroup.header = header;

        containerGroup.contentText = contentText;

        containerGroup.isExpanded = false;

        containerGroup.expandedPosition = containerWidth;

        containerGroup.linkedPoint = null;

        containerGroup.scroll = createScrollBar((controllsPanel.width() + containerWidth) - 20, containerGroup.y() + headerRect.height() + 10, 15, rectangle.height() - 20, textHolder, scrollContainer);

        containerGroup.scroll.refresh();

        headerGroup.on(events[isMobile].mouseover, hoverPointer);
        headerGroup.on(events[isMobile].mouseout, resetPointer);

        headerGroup.on(events[isMobile].click, function(){
            if(preventActions) return;
            //if(containerGroup.isActive) containerGroup.toggle();
            /*
                titleText,
                headText,
                image,
                contentTextData
            */
            fullscreenContainer.setContent(
                headerLabel.text(),
                containerGroup.header.text(),
                containerGroup.contentImage.getImage(),
                contentText.text(),
                containerGroup.linksData);
        });

        containerGroup.leftSidePosition = {
            expanded: controllsPanel.width(),
            regular: - containerWidth
        }

        containerGroup.rightSidePosition = {
            expanded: ($(window).width() / stage.scaler) - controllsPanel.width() - containerWidth,
            regular: ($(window).width() / stage.scaler) + controllsPanel.width()
        }

        containerGroup.sidePosition = containerGroup.leftSidePosition;

        containerGroup.recalculatePosition = function(){
            containerGroup.rightSidePosition = {
                expanded: ($(window).width() / stage.scaler) - controllsPanel.width() - containerWidth,
                regular: ($(window).width() / stage.scaler) + controllsPanel.width()
            }
        }

        containerGroup.onSideChanged = function(){
            containerGroup.sidePosition = controllsPanel.rightSideMode ? containerGroup.rightSidePosition : containerGroup.leftSidePosition;

            containerGroup.x(!containerGroup.isExpanded ? containerGroup.sidePosition.regular : containerGroup.sidePosition.expanded);

            if(containerGroup.scroll != null){
                containerGroup.scroll.x((containerGroup.sidePosition.expanded + containerWidth - 20) - (controllsPanel.width() + containerWidth - 20));
                //contentScrollLayer.draw();
                /*console.log('scroll ' + (layerPanel.sidePosition.expanded + layerPanelWidth - 20));
                console.log('scroll prev --> ' + (controllsPanel.width() + layerPanelWidth - 20));
                console.log(layerSelectionContainer.scroll.x());*/
            }

            //controllsLayer.draw();
        }

        containerGroup.reallocateScroll = function(dy){
            containerGroup.scroll.reallocateScroll(dy);
            contentScrollLayer.draw();
        }

        containerGroup.setValue = function(header, textData, picture, point){
            if(containerGroup.tween.isPlaying) return;

            if(containerGroup.linkedPoint != null) containerGroup.linkedPoint.setClear();

            containerGroup.linkedPoint = point;

            containerGroup.linkedPoint.setActive();

            answerPointLayer.draw();

            textHolder.y(0);

            if(!containerGroup.isActive){
                containerGroup.isActive = true;
                containerGroup.toggle();
            }

            var text = textData.text;

            containerGroup.header.text(header);
            containerGroup.contentText.text(text);

            containerGroup.contentText.y(containerGroup.header.y() + containerGroup.header.height() + containerGroup.header.fontSize() * 0.2);

            var imageMargin = 0;

            if(picture != null){
                //containerGroup.contentImage.y(containerGroup.contentText.y() + containerGroup.contentText.height() + 20);
                containerGroup.contentImage.x(0);
                containerGroup.contentImage.y(containerGroup.contentText.y());

                containerGroup.contentImage.setImage(picture);

                var scaler = calculateAspectRatioFit(picture.width, picture.height, 150, 150);

                containerGroup.contentImage.scale({
                    x: scaler,
                    y: scaler
                })

                var imageWidth = containerGroup.contentImage.width() * scaler

                imageMargin += containerGroup.contentImage.height() * scaler + 10;

                var avaibleWidth = containerWidth - 20;

                if(imageWidth < avaibleWidth) containerGroup.contentImage.x((avaibleWidth >> 1) - (imageWidth >> 1));

                containerGroup.contentImage.show();
            } else{
                containerGroup.contentImage.hide();
            }

            containerGroup.contentText.y(containerGroup.contentText.y() + imageMargin);

            textHolder.height(containerGroup.header.height() + containerGroup.contentText.height() + 10 + imageMargin);

            textHolder.height(updateLinks(textHolder.height() + containerGroup.header.y() - 10, textData.links) + textHolder.height());

            containerGroup.linksData = textData.links;

            /*if(textHolder.height() < containerMaxHeight && textHolder.height() > containerMinHeight){
                rectangle.height(textHolder.height());
            } else if(textHolder.height() > containerMaxHeight){
                rectangle.height(containerMaxHeight);
            } else{
                rectangle.height(containerMinHeight);
            }*/

            setTimeout(function(){
                if(!containerGroup.isExpanded) return;
                containerGroup.scroll.refresh();
                contentScrollLayer.draw();
            }, 700);
        }

        containerGroup.initTween = function(){
            containerGroup.tween = new Konva.Tween({
                node: containerGroup,
                duration: 0.3,
                x: controllsPanel.width(),
                easing: Konva.Easings.Linear
              });

            containerGroup.tween.onFinish = tweenEndHandler;

            containerGroup.tween.isPlaying = false;

            containerGroup.reverseTween = null;
        }

        containerGroup.showContainer = function(){
            containerGroup.isExpanded = true;

            containerGroup.tween = new Konva.Tween({
                node: containerGroup,
                duration: 0.3,
                x: containerGroup.sidePosition.expanded,
                easing: Konva.Easings.Linear
              });

            containerGroup.tween.onFinish = tweenEndHandler;

            //containerGroup.tween.reset();
            containerGroup.tween.play();
        }

        containerGroup.hideContainer = function(){
            containerGroup.isExpanded = false;

            if(containerGroup.linkedPoint != null){
                /*if(containerGroup.linkedPoint.isActive){
                    containerGroup.linkedPoint.setClear();
                    answerPointLayer.draw();
                }*/
            }

            //if(containerGroup.reverseTween == null){

                containerGroup.reverseTween = new Konva.Tween({
                    node: containerGroup,
                    duration: 0.3,
                    x: containerGroup.sidePosition.regular,
                    easing: Konva.Easings.Linear
                  });

                containerGroup.reverseTween.onFinish = reverseTweenEndHandler;
            /*} else{
                containerGroup.reverseTween.reset();
            }*/

            containerGroup.reverseTween.play();

            containerGroup.scroll.hide();

            contentScrollLayer.draw();
        }

        containerGroup.toggle = function(){
            if(containerGroup.tween.isPlaying) return;

            containerGroup.tween.isPlaying = true;

            if(!containerGroup.isExpanded){

                expandContainer(containerGroup);

                /*containerGroup.isExpanded = false;

                hideScroll();

                contentScrollLayer.draw();

                if(containerGroup.reverseTween == null){

                    containerGroup.reverseTween = new Konva.Tween({
                        node: containerGroup,
                        duration: 0.3,
                        x: containerGroup.defaultX,
                        easing: Konva.Easings.Linear
                      });

                    containerGroup.reverseTween.onFinish = reverseTweenEndHandler;
                } else{
                    containerGroup.reverseTween.reset();
                }

                containerGroup.reverseTween.play();*/

            } else{
                /*containerGroup.isExpanded = true;

                containerGroup.tween.reset();
                containerGroup.tween.play();*/

                hideContainer(containerGroup);
            }

            controllsLayer.draw();
        }

        headerGroup.add(headerRect);
        headerGroup.add(headerLabel);

        var imgObj = new Image();

        var iconPadding = 9 * mobileScaler;

        var iconSize = headerRect.height() - (iconPadding << 1);

        //imgObj.onload = function(){
        var fullscreenIcon = new Konva.Image({
            x: headerRect.width() - iconSize - iconPadding,
            y: iconPadding,
            width: iconSize,
            height: iconSize,
            image: controllsGraphic.fullscreenInIcon
        });

        headerGroup.add(fullscreenIcon);
        //}

        //imgObj.src = skinManager.getGraphic(colorSheme.controllsPanel.fullscreenIcon.in);

        containerGroup.add(headerGroup)
        containerGroup.add(rectangle);
        containerGroup.add(scrollContainer);

        function tweenEndHandler(){
            containerGroup.scroll.refresh();
            containerGroup.tween.isPlaying = false;
        }

        function reverseTweenEndHandler(){
            containerGroup.tween.isPlaying;
            containerGroup.tween.isPlaying = false;
        }

        function hideScroll(){
            containerGroup.scroll.hide();
           // setTimeout(function(){
            //contentScrollLayer.draw();
            //}, 50);

            //console.log('hided! ' + Math.random());
        }

        return containerGroup;
    }

    function initLayerArray(layers){
        layerArray = [];

        $(layers).each(function(index){
            layerArray.push({
                name: $(this).text(),
                src: $(this).attr('src'),
                depth: +$(this).attr('depth'),
                isVisible: $(this).attr('visible') == 'yes'
            });
        })
    }

    function getFileName(url){
       return url.src.substring(url.lastIndexOf('/')+1);
    }

    function updateLayerArray(layers, callback){

        $(layers).each(function(index){
            for(var i = 0; i < layerArray.length; i++){
                if(layerArray[i].src == $(this).attr('src')){
                    //console.log('layer ' + layerArray[i].src + ' being reused!');
                    layerArray[i].reused = true;
                    layerArray[i].name = $(this).text();
                    layerArray[i].depth = index;
                    layerArray[i].isVisible = ($(this).attr('visible') == 'yes');

                    if(layerArray[i].asset.width() > contentWidth) contentWidth = layerArray[i].asset.width();
                    if(layerArray[i].asset.height() > contentHeight) contentHeight = layerArray[i].asset.height();

                    if(layerArray[i].isVisible && !layerArray[i].asset.getParent()){
                        contentGroup.sourceGroup.add(layerArray[i].asset);
                        layerArray[i].isAdded = true;
                        layerArray[i].asset.setZIndex(index);
                    }

                    return;
                }
            }

            layerArray.push({
                name: $(this).text(),
                src: $(this).attr('src'),
                depth: index,
                isVisible: $(this).attr('visible') == 'yes',
                isNewLayer: true
            });

            console.log('new layer ' + $(this).attr('src') + ' was added');
        });

        var loadCounter = 0;
        levelLayerCount = layerArray.length;

        for(var i = 0; i < layerArray.length; i++){
            layerArray[i].levelRelated = true;

            console.log(layerArray[i]);

            if(!layerArray[i].reused && !layerArray[i].isNewLayer){
                layerArray[i].asset.remove();
                layerArray[i].isAdded = false;
                layerArray[i].levelRelated = false;
                levelLayerCount--;
                console.log(layerArray[i].src + ' is being removed');

                continue;
            }

            if(layerArray[i].isNewLayer){
                loadCounter++;
                loadIosLayer(layerArray[i].src, i, onNewLayerLoaded);
                console.log('loading new layer');
                layerArray[i].isNewLayer = false;
            }

            if(layerArray[i].reused){
                if(layerArray[i].isAdded && !layerArray[i].isVisible){ //already added and not visible
                    layerArray[i].asset.remove();
                    layerArray[i].isAdded = false;
                }                
                layerArray[i].reused = false;
            }
        }

        if(loadCounter <= 0) callback();

        function onNewLayerLoaded(index){
            layerArray[index].isAdded = false;
            if(layerArray[index].isVisible){
                contentGroup.sourceGroup.add(layerArray[index].asset);
                layerArray[index].isAdded = true;
                layerArray[index].asset.setZIndex(layerArray[index].depth);
            }

            loadCounter--;
            if(loadCounter <= 0) callback();
        }
    }

    function createControllsPanel(){
        var controllsPanelGroup = new Konva.Group({
            x: 0,
            y: 0
        });

        controllsPanelGroup.rightSideMode = false;

        //controllsPanelGroup.selected = null;

        var panelSheme = colorSheme.controllsPanel;

        controllsPanelGroup.glow = function(panelButton){
            //if(controllsPanelGroup.selected != null) controllsPanelGroup.unGlow(controllsPanelGroup.selected);
            panelButton.rect.fill(panelSheme.glowedBackgroundColor);
            panelButton.rect.stroke("");
            //panelButton.label.fill(panelSheme.glowedFontColor);
            //controllsPanelGroup.selected = panelButton;
            controllsPanelGroup.draw();
        };

        controllsPanelGroup.unGlow = function(panelButton){
            panelButton.rect.fill(panelSheme.regularBackgroundColor);
            panelButton.rect.stroke(panelSheme.stroke);
            //panelButton.label.fill(panelSheme.regularFontColor);
            controllsPanelGroup.draw();
        };

        var controllsPanel = new Konva.Rect({
            x: 0,
            y: 0,
            width: 40 * mobileScaler,
            height: $(window).height() / stage.scaler,
            fill: panelSheme.backgroundColor,
            opacity: 0.65
        });

        var sideModeBtn = new Konva.Group({
            x: 0,
            y: ($(window).height() / stage.scaler) - controllsPanel.width()
        });

        var sideModeRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: controllsPanel.width(),
            height: controllsPanel.width(),
            fill: panelSheme.container.sideButton.backgroundColor
        });

        var sideModeLabel = new Konva.Shape({
            x: ((iconPadding << 1) + 3) * mobileScaler,
            y: (iconPadding + 4) * mobileScaler,
            width: 22 * mobileScaler,
            height: 22 * mobileScaler,
            fill: panelSheme.container.sideButton.arrowColor,
            sceneFunc: function(context) {
              context.beginPath();
              context.moveTo(0, 0);
              context.lineTo(0, this.height());
              context.lineTo(Math.round(this.width() * 0.8), Math.round((this.height() >> 1)));
              context.closePath();
              // KonvaJS specific context method
              context.fillStrokeShape(this);
            }
        });
            
        sideModeBtn.label = sideModeLabel;

        sideModeBtn.add(sideModeRect);
        sideModeBtn.add(sideModeLabel);
        //}

        //imgObj_2.src = skinManager.getGraphic(panelSheme.layerPanelIcon);

        sideModeBtn.rect = sideModeRect;

        controllsPanelGroup.changeSideMode = function(){
            if(preventActions) return;
            //controllsPanelGroup.glow(layerPanelBtn);
            if(controllsPanelGroup.rightSideMode){
                controllsPanelGroup.rightSideMode = false;
                controllsPanelGroup.x(0);

                sideModeBtn.label.x(sideModeBtn.label.x() - (sideModeBtn.label.width() >> 1) - 4);
                sideModeBtn.label.y(sideModeBtn.label.y() - sideModeBtn.label.height());

                //sideModeBtn.label.rotate(180);
                //if(expandedContainer != null) hideContainer(expandedContainer);
                //controllsLayer.draw();
            } else{
                controllsPanelGroup.rightSideMode = true;
                controllsPanelGroup.x($(window).width() / stage.scaler - controllsPanelGroup.width());

                sideModeBtn.label.x(sideModeBtn.label.x() + (sideModeBtn.label.width() >> 1) + 4);
                sideModeBtn.label.y(sideModeBtn.label.y() + sideModeBtn.label.height());
                //sideModeBtn.label.rotate(0);
                //if(expandedContainer != null) hideContainer(expandedContainer);
                //controllsLayer.draw();
            }

            sideModeBtn.label.rotate(180);

            onSideChanged();

            controllsLayer.draw();
            contentScrollLayer.draw();
        }

        sideModeBtn.on(events[isMobile].mouseover, hoverPointer);
        sideModeBtn.on(events[isMobile].mouseout, resetPointer);
        sideModeBtn.on(events[isMobile].click, controllsPanelGroup.changeSideMode);

        function onSideChanged(){
            if(isLayersEditable) layerPanel.onSideChanged();
            viewContainer.onSideChanged();
            if(taskEnabled) taskPanel.onSideChanged();
            //layerPanel.onSideChanged();
            //layerPanel.onSideChanged();
        }

        controllsPanelGroup.sideModeBtn = sideModeBtn;

        var layerPanelBtn = new Konva.Group({
            x: 0,
            y: 40 + controllsPanel.width()
        });

        var layerPanelRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: controllsPanel.width(),
            height: controllsPanel.width(),
            fill: panelSheme.regularBackgroundColor,
            stroke: panelSheme.stroke,
            strokeWidth: panelSheme.strokeWidth
        });

        //var imgObj = new Image();

        //imgObj.onload = function(){

        var layerPanelLabel = new Konva.Image({
            x: iconPadding + 1,
            y: iconPadding,
            image: controllsGraphic.layerPanelIcon,
            width: controllsGraphic.layerPanelIcon.width,
            height: controllsGraphic.layerPanelIcon.height
        });

        var scaler = calculateAspectRatioFit(controllsGraphic.layerPanelIcon.width, controllsGraphic.layerPanelIcon.height, controllsPanel.width() - (iconPadding << 1), controllsPanel.width() - (iconPadding << 1))

        layerPanelLabel.scale({
            x: scaler,
            y: scaler
        });

        layerPanelBtn.add(layerPanelRect);
        
        layerPanelBtn.label = layerPanelLabel;
        layerPanelBtn.add(layerPanelLabel);
        //}

        //imgObj.src = skinManager.getGraphic(panelSheme.layerPanelIcon);

        layerPanelBtn.rect = layerPanelRect;

        layerPanelBtn.on(events[isMobile].mouseover, hoverPointer);
        layerPanelBtn.on(events[isMobile].mouseout, resetPointer);
        layerPanelBtn.on(events[isMobile].click, function(){
            if(preventActions) return;
            //controllsPanelGroup.glow(layerPanelBtn);
            layerPanel.togglePanel();
        });

        controllsPanelGroup.layerPanelBtn = layerPanelBtn;

        var viewPanelBtn = new Konva.Group({
            x: 0,
            y: (isLayersEditable ? layerPanelBtn.y() : (layerPanelBtn.y() - controllsPanel.width())) + controllsPanel.width()
        });

        controllsPanelGroup.viewPanelBtn = viewPanelBtn;

        var viewPanelRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: controllsPanel.width(),
            height: controllsPanel.width(),
            fill: panelSheme.regularBackgroundColor,
            stroke: panelSheme.stroke,
            strokeWidth: panelSheme.strokeWidth
        });

        /*var _imgObj = new Image();

        _imgObj.onload = function(){*/

        var viewPanelLabel = new Konva.Image({
            x: iconPadding + 3,
            y: iconPadding,
            image: controllsGraphic.viewPanelIcon,
            width: controllsGraphic.viewPanelIcon.width,
            height: controllsGraphic.viewPanelIcon.height
        });

        var scaler = calculateAspectRatioFit(controllsGraphic.viewPanelIcon.width, controllsGraphic.viewPanelIcon.height, controllsPanel.width() - (iconPadding << 1), controllsPanel.width() - (iconPadding << 1))

        viewPanelLabel.scale({
            x: scaler,
            y: scaler
        });

        viewPanelBtn.add(viewPanelRect);
        
        viewPanelBtn.label = viewPanelLabel;
        viewPanelBtn.add(viewPanelLabel);
        //}

        //_imgObj.src = skinManager.getGraphic(panelSheme.viewPanelIcon);

        viewPanelBtn.rect = viewPanelRect;
        //viewPanelBtn.label = viewPanelLabel;

        viewPanelBtn.on(events[isMobile].mouseover, hoverPointer);
        viewPanelBtn.on(events[isMobile].mouseout, resetPointer);
        viewPanelBtn.on(events[isMobile].click, function(){
            if(preventActions) return;
            if(viewContainer.isActive){
                viewContainer.toggle();
                //controllsPanelGroup.glow(viewPanelBtn);
            }
        });

        var legendBtn = new Konva.Group({
            x: 0,
            y: (isLayersEditable ? viewPanelBtn.y() : (viewPanelBtn.y())) + controllsPanel.width()
        });

        controllsPanelGroup.legendBtn = legendBtn;

        var legendPanelRect = new Konva.Rect({
            x: 0,
            y: 0,
            width: controllsPanel.width(),
            height: controllsPanel.width(),
            fill: panelSheme.regularBackgroundColor,
            stroke: panelSheme.stroke,
            strokeWidth: panelSheme.strokeWidth
        });

        /*var _imgObj = new Image();

        _imgObj.onload = function(){*/

        var legendPanelLabel = new Konva.Image({
            x: iconPadding + 2,
            y: iconPadding,
            image: controllsGraphic.legendIcon,
            width: controllsGraphic.legendIcon.width,
            height: controllsGraphic.legendIcon.height
        });

        var scaler = calculateAspectRatioFit(controllsGraphic.legendIcon.width, controllsGraphic.legendIcon.height, controllsPanel.width() - (iconPadding << 1), controllsPanel.width() - (iconPadding << 1))

        legendPanelLabel.scale({
            x: scaler,
            y: scaler
        });

        legendBtn.add(legendPanelRect);
        
        legendBtn.label = legendPanelLabel;
        legendBtn.add(legendPanelLabel);
        //}

        //_imgObj.src = skinManager.getGraphic(panelSheme.viewPanelIcon);

        legendBtn.rect = legendPanelRect;
        //viewPanelBtn.label = viewPanelLabel;

        legendBtn.on(events[isMobile].mouseover, hoverPointer);
        legendBtn.on(events[isMobile].mouseout, resetPointer);
        legendBtn.on(events[isMobile].click, function(){
            if(preventActions) return;
            /*if(viewContainer.isActive){
                viewContainer.toggle();
                //controllsPanelGroup.glow(viewPanelBtn);
            }*/
            imageContainer.zoomImage(controllsGraphic.legendGraphic)
        });
        //viewPanelBtn.add(viewPanelLabel);

        controllsPanelGroup.add(controllsPanel);

        controllsPanelGroup.taskBtn = createTaskButton(0, 40);

        var zoomInBtn = createZoomButton(0, sideModeBtn.y() - 119 * mobileScaler, 'zoomIn', zoomIn);
        var zoomOutBtn = createZoomButton(0, sideModeBtn.y() - 80 * mobileScaler, 'zoomOut', zoomOut);

        controllsPanelGroup.add(zoomInBtn);
        controllsPanelGroup.add(zoomOutBtn);

        if(taskEnabled) controllsPanelGroup.add( controllsPanelGroup.taskBtn );
        if(isLayersEditable) controllsPanelGroup.add(layerPanelBtn);
        controllsPanelGroup.add(viewPanelBtn);
        if(legendSource != '') controllsPanelGroup.add(legendBtn);
        controllsPanelGroup.add(sideModeBtn);
        

        controllsPanelGroup.height(controllsPanel.height());
        controllsPanelGroup.width(controllsPanel.width());

        controllsPanelGroup.onSizeChange = function(newWidth, newHeight){
            controllsPanel.height($(window).height() / stage.scaler);

            controllsPanelGroup.height(controllsPanel.height());

            sideModeBtn.y(($(window).height() / stage.scaler) - controllsPanel.width());

            zoomInBtn.y(sideModeBtn.y() - 119 * mobileScaler);
            zoomOutBtn.y(sideModeBtn.y() - 80 * mobileScaler);

            if(controllsPanelGroup.rightSideMode){
                controllsPanelGroup.rightSideMode = false;
                controllsPanelGroup.x(0);

                sideModeBtn.label.x(sideModeBtn.label.x() - (sideModeBtn.label.width() >> 1) - 4);
                sideModeBtn.label.y(sideModeBtn.label.y() - sideModeBtn.label.height());
                sideModeBtn.label.rotate(180);

                onSideChanged();
            }

            layerPanel.recalculatePosition();
            viewContainer.recalculatePosition();
        }

        return controllsPanelGroup;
    }

    function createTaskButton(x, y){
        var taskButtonGroup = new Konva.Group({
            x: x,
            y: y
        });

        //var image = new Image();

        //image.onload = function(){
            /*var buttonImage = new Konva.Image({
                x: 0,
                y: 0,
                width: image.width,
                height: image.height,
                image : image
            });*/

            panelSheme = colorSheme.controllsPanel;

            var buttonImage = new Konva.Rect({
                x: 0,
                y: 0,
                width: 40 * mobileScaler,
                height: 40 * mobileScaler,
                fill: panelSheme.regularBackgroundColor,
                stroke: panelSheme.stroke,
                strokeWidth: panelSheme.strokeWidth
            });

            //var imgObj = new Image();

            //imgObj.onload = function(){

            var label = new Konva.Image({
                x: iconPadding + 2,
                y: iconPadding,
                image: controllsGraphic.taskPanelIcon,
                width: controllsGraphic.taskPanelIcon.width,
                height: controllsGraphic.taskPanelIcon.height
            });

            var scaler = calculateAspectRatioFit(controllsGraphic.taskPanelIcon.width, controllsGraphic.taskPanelIcon.height, buttonImage.width() - (iconPadding << 1), buttonImage.width() - (iconPadding << 1))

            label.scale({
                x: scaler,
                y: scaler
            });


            taskButtonGroup.rect = buttonImage;

            taskButtonGroup.add(buttonImage);
            
            taskButtonGroup.label = label;
            taskButtonGroup.add(label);
            //}

            //imgObj.src = skinManager.getGraphic(panelSheme.taskPanelIcon);

            //taskButtonGroup.label = label;
            //taskButtonGroup.add(label);

            taskButtonGroup.on(events[isMobile].mouseover, hoverPointer);
            taskButtonGroup.on(events[isMobile].mouseout, resetPointer);

            taskButtonGroup.on(events[isMobile].click, function(){
                if(preventActions) return;
                /*if(taskPanel.isFinished){
                    taskPanel.taskMode = false;

                    taskPanel.reset();

                    taskPanel.hideContainer();
                }*/
                if(!taskPanel.taskMode){
                    expandContainer(taskPanel);

                    taskPanel.taskMode = true;

                    //controllsPanel.glow(this);
                } else{
                    hideContainer(taskPanel);

                    taskPanel.taskMode = false;
                    //controllsPanel.unGlow(this);
                }

                controllsLayer.draw();
            })

            //controllsLayer.draw();
        //}

        //image.src = 'assets/ui/taskButton.png'

        return taskButtonGroup;
    }

    function initTaskArray(tasks, isRandom){
        if(isRandom) shuffle(tasks);

        var loadCount = 0;

        var assetIndex = registerAssets();

        $(tasks).each(function(index){
            var taskImage = null;

            var imageSrc = $(this).attr('img');

            if(typeof imageSrc != 'undefined' && imageSrc != ''){

                loadCount++;

                taskImage = new Image();

                taskImage.isLoaded = false;

                taskImage.onload = function(){
                    taskImage.isLoaded = true;
                    loadCount--;
                    if(loadCount == 0) setAssetLoaded(assetIndex);
                }

                taskImage.src = levelManager.getRoute(imageSrc);
            }

            taskArray.push({
                answerID : $(this).attr('answer'),
                value : $(this).text(),
                image : taskImage
            })
        })

        if(loadCount == 0) setAssetLoaded(assetIndex);
    }

    function receiveMessage(event){
        if(event.data == 'destroy'){
            //call destructor
            return;
        }
        updateMap(event.data.replace(/.*\?/, ''));
    }

    function updateMap(newCofig){
        if(!levelManager.updateConfig(newCofig, rebuildContent)){
            //same config request
        }
    }

    function rebuildContent(xml){
        root = $(xml).children('component');
        levelManager.initRoute(root);

        sliceZeroPath = $(root).attr('sliceZeroPath') == 'yes';

        var pointsNode = root.children('points');

        points = pointsNode.children('point');

        onAssetsLoad = onAssetsReloaded;

        preloadAnswerPoints(points); //reload it!

        layersFolder = root.attr('layersFolder');

        centerPoints = pointsNode.attr('center') == 'yes';

        if(typeof layersFolder == 'undefined' || layersFolder == '') layersFolder = defaultLayerFolder;

        showHistory = pointsNode.attr('showHistory') == 'yes';

        legendSource = levelManager.getRoute($(root).children('legend').attr('src'));

        if(typeof legendSource == 'undefined') legendSource = '';

        if(renderType != 0){
            if(legendSource.indexOf('.svg') != -1){
                legendSource = legendSource.replace('.svg', '.png');
            }
        }

        if(typeof legendSource == 'undefined' || !legendSource) legendSource = '';

        var layers = root.children('layers').children('layer');

        var _tasks = root.children('tasks');

        taskEnabled = (_tasks.length != 0);

        if(!taskEnabled){
            taskPanel = {};
            taskPanel.taskMode = false;
            taskPanel.jumpToNext = false;
            taskPanel.isFinished = true;
        }

        var tasks = root.children('tasks').children('task');

        if(!tasks) taskEnabled = false;

        var maxZoom = +$(root).attr('maxZoomLevel');

        var editable = root.children('layers').attr('editable');

        isLayersEditable = (typeof editable != 'undefined' && editable != '' && editable != 'no');

        var scaleTipFlag = root.attr('showScaleTip');

        showScaleTipFlag = (typeof scaleTipFlag != 'undefined' && scaleTipFlag != '' && scaleTipFlag != 'no');

        maxZoomLevel = isNaN(maxZoom) ? 0.5 : Math.round(maxZoom / 5);

        //update this too
        //if(taskEnabled) initTaskArray(tasks, root.children('tasks').attr('randomize') == 'yes');

        if(taskEnabled) allowReAnswer = root.children('tasks').attr('allowReAnswer') == 'yes';
        
        //initLayerArray(layers);

        contentWidth = 0;
        contentHeight = 0;

        var layerAssetIndex = registerAssets();

        updateLayerArray(layers, function(){
            console.log('layers update finished');

            setAssetLoaded(layerAssetIndex);
        });

        layers = null;

        //preloadControllsGraphic(); //we don't need it 

        /*if(renderType != 2){
            loadAssets(layerArray);
        } else{
            iosLoadAssets(layerArray);
        }

        initImageContainer();*/
    }

    function onAssetsReloaded(){
        console.log('assets reloaded');

        if(contentWidth != defaultContentWidth || contentHeight != defaultContentHeight){
            //different sized map
            console.log('different size');

            defaultContentWidth = contentWidth;
            defaultContentHeight = contentHeight;

            var scaler = calculateAspectRatioFit(contentWidth, contentHeight, stage.width(), stage.height());

            var iosHDScaler = calculateAspectRatioFit(contentWidth, contentHeight, 2250, 2250);
            var iosScaler = calculateAspectRatioFit(contentWidth * iosHDScaler, contentHeight * iosHDScaler, stage.width(), stage.height());
        
            defaultScaler = scaler;

            contentWidth *= scaler;
            contentHeight *= scaler;

            contentGroup.sourceGroup.scale({
                x: iosHDScaler,
                y: iosHDScaler
            })

            defaultContentWidth *= iosHDScaler;
            defaultContentHeight *= iosHDScaler;

            contentGroup.extraGroup.scale({
                x: iosScaler,
                y: iosScaler
            });

            contentGroup.iosScaler = iosScaler;

            contentWidth = defaultContentWidth * iosScaler;
            contentHeight = defaultContentHeight * iosScaler;

            zoomCounter.rebuild();
        }

        zoomLevel = 0;
        resetZoom();
        centerMap();
        draggableLayer.x(0);
        draggableLayer.y(0);
        answerPointLayer.x(0);
        answerPointLayer.y(0);

        layerPanel.rebuildContent();

        if(expandedContainer != null) hideContainer(expandedContainer);

        controllsLayer.drawScene();

        drawMap();

        buildAnswerPoints(points, true); //build new answer points
        answerPointLayer.batchDraw(); //synchronyse that with map draw!
    }

    /*Вызывается по окончанию загрузки данных. Инициирует данными глобальные переменные а также элементы интерфейса**/
    function xmlLoadHandler(xml) {

        console.log('level loaded');

        root = $(xml).children('component');

        levelManager.initRoute(root);

        sliceZeroPath = $(root).attr('sliceZeroPath') == 'yes';

        var pointsNode = root.children('points');

        points = pointsNode.children('point');

        preloadAnswerPoints(points);

        console.log('points preloaded')

        layersFolder = root.attr('layersFolder');

        centerPoints = pointsNode.attr('center') == 'yes';

        if(typeof layersFolder == 'undefined' || layersFolder == '') layersFolder = defaultLayerFolder;

        showHistory = pointsNode.attr('showHistory') == 'yes';

        legendSource = levelManager.getRoute($(root).children('legend').attr('src'));

        if(typeof legendSource == 'undefined') legendSource = '';

        if(renderType != 0){
            if(legendSource.indexOf('.svg') != -1){
                legendSource = legendSource.replace('.svg', '.png');
            }
        }

        if(typeof legendSource == 'undefined' || !legendSource) legendSource = '';

        var layers = root.children('layers').children('layer');

        var _tasks = root.children('tasks');

        taskEnabled = (_tasks.length != 0);

        if(!taskEnabled){
            taskPanel = {};
            taskPanel.taskMode = false;
            taskPanel.jumpToNext = false;
            taskPanel.isFinished = true;
        }

        var tasks = root.children('tasks').children('task');

        if(!tasks) taskEnabled = false;

        var maxZoom = +$(root).attr('maxZoomLevel');

        var editable = root.children('layers').attr('editable');

        isLayersEditable = (typeof editable != 'undefined' && editable != '' && editable != 'no');

        var scaleTipFlag = root.attr('showScaleTip');

        showScaleTipFlag = (typeof scaleTipFlag != 'undefined' && scaleTipFlag != '' && scaleTipFlag != 'no');

        maxZoomLevel = isNaN(maxZoom) ? 0.5 : Math.round(maxZoom / 5);

        if(taskEnabled) initTaskArray(tasks, root.children('tasks').attr('randomize') == 'yes');

        if(taskEnabled) allowReAnswer = root.children('tasks').attr('allowReAnswer') == 'yes';

        console.log('task array inited');
        
        initLayerArray(layers);

        console.log('layer array inited');

        preloadControllsGraphic();

        console.log('graphic preloaded');
        console.log(renderType);

        if(renderType != 2){
            loadAssets(layerArray);
        } else{
            iosLoadAssets(layerArray);
        }

        initImageContainer();
	}

    function preloadControllsGraphic(){
        controllsGraphic = [];

        var panelSheme = colorSheme.controllsPanel;

        loadGraphic(skinManager.getGraphic(panelSheme.layerPanelIcon), 'layerPanelIcon');
        loadGraphic(skinManager.getGraphic(panelSheme.taskPanelIcon), 'taskPanelIcon');
        loadGraphic(skinManager.getGraphic(panelSheme.viewPanelIcon), 'viewPanelIcon');
        loadGraphic(skinManager.getGraphic(panelSheme.legendIcon), 'legendIcon');
        loadGraphic(skinManager.getGraphic(panelSheme.zoomIcon['in']), 'zoomInIcon');
        loadGraphic(skinManager.getGraphic(panelSheme.zoomIcon.out), 'zoomOutIcon');
        loadGraphic(skinManager.getGraphic(panelSheme.fullscreenIcon['in']), 'fullscreenInIcon');
        loadGraphic(skinManager.getGraphic(panelSheme.fullscreenIcon.out), 'fullscreenOutIcon');
        loadGraphic('assets/ui/mapAnswerPoint.png', 'mapAnswerPoint');
        loadGraphic(skinManager.getGraphic(colorSheme.controllsPanel.checkBox), 'checkBox');
        if(legendSource != '') loadGraphic(legendSource, 'legendGraphic');
    }

    function loadGraphic(path, key){
        var imgObj = new Image();

        imgObj.assetIndex = registerAssets();
        imgObj.key = key;

        imgObj.onload = function(){
            setAssetLoaded(this.assetIndex);
            controllsGraphic[this.key] = this;
        }

        imgObj.src = path;
    }	

	function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

	    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

	    return ratio;
	 }

	function createPicture(x, y, picture, columnIndex, maxSize, columnWidth){
		var imgObj = new Image();
		var id = picture.attr('id');		

		imgObj.id = 'img_'+id;
		imgObj.onload = function(){
			var image = new Konva.Image({
			  x: x,
			  y: y,
			  image: imgObj,
			  width: imgObj.width,
			  height: imgObj.height,
			  name: 'img_'+columnIndex
			});

			//console.log('columnWidth '+columnWidth+'   maxSize '+maxSize);
			maxSize = Math.min(maxSize, columnWidth);

			var scaler = calculateAspectRatioFit(imgObj.width, imgObj.height, maxSize, maxSize);

			image.scaleX(scaler);
			image.scaleY(scaler);
			image.scaler = scaler;

			if((image.width()*image.scaleX())<columnWidth){
				image.x(x+(columnWidth/2-(image.width()*image.scaleX())/2));
			}

			image.defaultPositionX = image.x();
			image.defaultPositionY = image.y();

			image.on('click', imageZoomHandler);
			image.isZoomed = false;

			backgroundLayer.add(image);

			var leftPosition = [x-20, image.y()+((image.height()*image.scaleY())>>1)];
			var rightPosition = [x+columnWidth-10, image.y()+((image.height()*image.scaleY())>>1)];
			if(columnIndex!=0&&columnIndex!=COLUMN_COUNT-1){ //Узлы с двух сторон
				buildNode(leftPosition[0], leftPosition[1], id, -1, columnIndex);
				buildNode(rightPosition[0], rightPosition[1], id, 1, columnIndex);
			} else{ //Узел с одной из сторон
				var position = (columnIndex==0) ? rightPosition : leftPosition;
				buildNode(position[0], position[1], id, (columnIndex==0)?1:-1, columnIndex);
			}


			backgroundLayer.draw();
		};
		imgObj.src = picture.attr('picture');

		/*Скалирует изображения по клику**/
		function imageZoomHandler(evt){
			var currentImg = evt.targetNode;
			imageContainer.zoomImage(currentImg.getImage());				      
		}
	}

	function getRelativePointerPosition() {
	    var pointer = stage.getPointerPosition();
	    var pos = stage.getPosition();
	    var offset = stage.getOffset();
	    var scale = stage.getScale();

	    return {
	        x : ((pointer.x / scale.x) - (pos.x / scale.x) + offset.x),
	        y : ((pointer.y / scale.y) - (pos.y / scale.y) + offset.y)
	    };
	}
    /*Отображает сообщение-результат игры
    * @param {number} x определяет положение сообщения в пространстве по оси x
    * @param {number} y определяет положение сообщения в пространстве по оси y
    * @param {boolean} isWin флаг определяет результат игры
    * false - проиграл
    * true - выиграл
    */
    function showResultLabel(x,y,isWin){
    	var label = new Konva.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	backgroundLayer.draw();
    }

    /*Перемешивает элементы массива o
    * @param {array} o исходный массив
    * @return {array} o массив с перемешанными элементами
    */
	function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

    function createScrollBar(x, y, width, height, contentGroup, clippedLayer, scrollLayer, drawBackground){
        var scrollObject = new Konva.Group({
            x: x,
            y: y
        });

        scrollLayer = scrollLayer || contentScrollLayer;

        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Konva.Rect({
           x: 0,
           y: 0,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        if(drawBackground) vscrollArea.fill(scrollScheme.backgroundColor);

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11;

        var vscroll = new Konva.Group({
            x: 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= (scrollStart + scrollObject.y()) * contentScrollLayer.scaleY()){
                    newY = (scrollStart + scrollObject.y()) * contentScrollLayer.scaleY();
                }
                else if(newY > (scrollEnd + scrollObject.y()) * contentScrollLayer.scaleY()) {
                    newY = (scrollEnd + scrollObject.y()) * contentScrollLayer.scaleY() ;
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                    }
            }
          });

          vscrollArea.defaultY = vscrollArea.y();
          vscroll.defaultY = vscroll.y();

          var scrollCircle = new Konva.Group({
            x: (width >> 1) - 9.5,
            y: 0
          });

          var backgroundCircle = new Konva.Circle({
            x: 0,
            y: 0,
            radius: width >> 1,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Konva.Circle({
            x: 0,
            y: 0,
            radius: backgroundCircle.radius() * 0.6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          }

          var scrollTopArrowGroup = new Konva.Group({
            x: 0,
            y: 0
          })

          var scrollTopArrow = new Konva.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Konva.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Konva.Group({
            x: 0,
            y: height
          });

          var scrollBotArrow = new Konva.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Konva.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          scrollLayer.add(scrollObject);

          scrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            scrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            scrollLayer.drawScene();
          }

          scrollObject.setHeight = function(height){
            console.log(vscrollArea.height(), height)
            vscrollArea.height(height);

            scrollStart = vscrollArea.y() + height * 0.11;
            scrollEnd = vscrollArea.getHeight() - height * 0.11;

            arrowDy = height * 0.038;

            scrollBotArrowGroup.y(height);

            scrollBotArrow.setPoints([
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ]);

            scrollBotRectangle.y(- (arrowDy + arrowRadius.y));
            scrollBotRectangle.height(arrowDy + arrowRadius.y);

            scrollTopArrow.setPoints([
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ]);

            scrollTopRectangle.height(arrowDy + arrowRadius.y);
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            scrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            scrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
            scrollTopArrowGroup.hide();
            scrollBotArrowGroup.hide();
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
            scrollTopArrowGroup.show();
            scrollBotArrowGroup.show();
          }

          scrollObject.reallocateScroll = function(dy){
            return;
            vscrollArea.y(vscrollArea.defaultY + dy);
            vscroll.y(vscroll.defaultY + dy);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();
          }

          scrollObject.refresh = function(resetScrollGroup){
            var clip = clippedLayer.getClip();

            vscrollArea.y(0);
            //vscrollArea.height(height);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            scrollTopArrowGroup.y(5);
            scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            if(resetScrollGroup) contentGroup.y(0);
            vscroll.y(scrollStart);

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
                scrollTopArrowGroup.hide();
                scrollBotArrowGroup.hide();
            } else{
                scrollObject.show();
                scrollTopArrowGroup.show();
                scrollBotArrowGroup.show();
            }

            scrollLayer.draw();
          }

          //clippedLayer.getParent().batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.getParent().draw();
          }
          return scrollObject;
    }

    function _createScrollBar(x, y, width, height, contentGroup, clippedGroup){
         var vscrollArea = new Konva.Rect({
            x: x,
            y: y,
            width: width,
            height: height,
            fill: '#CCCCCC',
            opacity: 0.3
          });

         var scrollStart = vscrollArea.y();
         var scrollEnd = vscrollArea.getHeight() - height * 0.13 + y;

          var vscroll = new Konva.Rect({
            x: x,
            y: scrollStart,
            width: width,
            height: height * 0.13,
            fill: '#F8DF45',
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart * contentScrollLayer.scaleY()){
                    newY = scrollStart * contentScrollLayer.scaleY();
                }
                else if(newY > scrollEnd * contentScrollLayer.scaleY()) {
                    newY = scrollEnd * contentScrollLayer.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                    }
            },
            strokeWidth: 0
          });

          vscrollArea.defaultY = vscrollArea.y();
          vscroll.defaultY = vscroll.y();

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedGroup.draw()});

          contentScrollLayer.add(vscrollArea);
          contentScrollLayer.add(vscroll);

          contentScrollLayer.draw();

          var scrollObject = {};

          scrollObject.isVisible = true;

          scrollObject.dy = contentGroup.y();

          scrollObject.resize = function(height){
            vscrollArea.height(height)
          }

          scrollObject.reallocateScroll = function(dy){
            vscrollArea.y(vscrollArea.defaultY + dy);
            vscroll.y(vscroll.defaultY + dy);

            scrollStart = vscrollArea.y();
            scrollEnd = vscrollArea.getHeight() - height * 0.13 + vscrollArea.y();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();

            scrollObject.isVisible = false;
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();

            scrollObject.isVisible = true;
          }

          scrollObject.refresh = function(){
            contentGroup.y(0);
            vscroll.y(scrollStart);

            //console.log(clippedGroup.getClip().height, contentGroup.height())

            if(clippedGroup.getClip().height >= contentGroup.height()){
                scrollObject.hide();
            } else{
                scrollObject.show();
            }

            contentScrollLayer.draw();
          }

          function scrollContent(){
            var clip = clippedGroup.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedGroup.getParent().draw()
          }
          return scrollObject;
    }

    function hoverPointer(){
        $('body').removeClass();
        $('body').addClass('hoverPoint');
    }
    function resetPointer(){
        $('body').removeClass();
        $('body').addClass('grab');
    }

    function grabPointer(){
        $('body').removeClass();
        $('body').addClass('grabbing');
    }

	function initImageContainer(){
        var image = new Konva.Image({
          x: 20,
          y: 20
        });

        var modalRect = new Konva.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: stage.width() * stage.scaleX(),
            height: stage.height() * stage.scaleY()
        });

        var modalGroup = new Konva.Group();

        modalRect.opacity(0.8);

        image.on(events[isMobile].mouseover, hoverPointer);
        image.on(events[isMobile].mouseout, resetPointer);

        image.hide();
        modalRect.hide();

        modalGroup.add(modalRect);
        modalGroup.add(image);

        imageContainer.add(modalGroup);

        imageContainer.modalRect = modalRect;

        imageContainer.zoomImage = function(imageObj){

            modalRect.show();

            image.setImage(imageObj);

            var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, stage.width() * stage.scaleX() - 40, stage.height() * stage.scaleY() - 40);

            image.width(imageObj.width * scaler);
            image.height(imageObj.height * scaler);

            if(!image.isVisible()){
                image.x((stage.width() * stage.scaleX()) >> 1);
                image.y((stage.height() * stage.scaleY()) >> 1);

                image.scale({
                    x: 0,
                    y: 0
                })

                var inTween = new Konva.Tween({
                    node: image,
                    scaleX: 1,
                    scaleY: 1,
                    x: ((stage.width() * stage.scaleX()) >> 1)-(image.width() >> 1),
                    y: ((stage.height() * stage.scaleY()) >> 1)-(image.height() >> 1),
                    duration: 0.4,
                    easing: Konva.Easings.EaseInOut
                })
                inTween.onFinish = function(){
                    //modalRect.show();
                }

                image.show();
                inTween.play();
            }

            modalGroup.on(events[isMobile].click, zoomOut);

            imageContainer.draw();
        }

        function zoomOut(evt){

            var outTween = new Konva.Tween({
                node: image,
                scaleX: 0,
                scaleY: 0,
                x: (stage.width() * stage.scaleX())>>1,
                y: (stage.height() * stage.scaleY())>>1,
                duration: 0.4,
                easing: Konva.Easings.EaseInOut
            });

            outTween.onFinish = function(){
                image.hide();
                imageContainer.draw();
            }

            modalRect.hide();

            outTween.play();

            modalGroup.off(events[isMobile].click, zoomOut);
        }
    }

    function getPPI(){
        // create an empty element
        var div = document.createElement("div");
        // give it an absolute size of one inch
        div.style.width="1in";
        // append it to the body
        var body = document.getElementsByTagName("body")[0];
        body.appendChild(div);
        // read the computed width
        var ppi = document.defaultView.getComputedStyle(div, null).getPropertyValue('width');
        // remove it again
        body.removeChild(div);
        // and return the value
        return parseFloat(ppi);
    }
});