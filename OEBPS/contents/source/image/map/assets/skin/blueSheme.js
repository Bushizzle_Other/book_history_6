var colorSheme = {
	"controllsPanel":{
		"container":{
			"header":{
				"backgroundColor": "#00A8D4",
				"fontColor": "white"
			},
			"taskTip":{
				"backgroundColor": "#DFEDF5",
				"fontColor": "black",
				"stroke": "",
				"strokeWidth": 0.8
			},
			"resultLabel":{
				"cornerRadius": 0,
				"stroke": "",
				"backgroundColor": "green",
				"fontColor": "white",
				"wrong":{
					"fontColor": "black",
					"backgroundColor": "red"
				}
			},
			"skipButton":{
				"cornerRadius": 0,
				"stroke": "#46a6d1",
				"backgroundColor": "white",
				"fontColor": "black"
			},
			"finishButton":{
				"cornerRadius": 0,
				"stroke": "#46a6d1",
				"backgroundColor": "white",
				"fontColor": "black"
			},
			"sideButton":{
				"backgroundColor": "#0059A9",
				"arrowColor": "white"
			},
			"stroke": "#46a6d1",
			"strokeWidth": 0.8,
			"linkColor": "#15577b",
			"layerPanel":{
				"hoverRect":{
					"backgroundColor": "#DAEEF6",
					"stroke": "#46a6d1",
					"strokeWidth": 0.8
				}
			}
		},
		"taskPanelIcon": "controllsPanel/taskBlue.png",
		"layerPanelIcon": "controllsPanel/layersBlue.png",
		"viewPanelIcon": "controllsPanel/viewBlue.png",
		"legendIcon": "controllsPanel/legendIcon.png",
		"glowedBackgroundColor": "#0059A9",
		"glowedFontColor": "black",
		"regularBackgroundColor": "white",
		"regularFontColor": "black",
		"backgroundColor": "#111111",
		"stroke": "#46a6d1",
		"strokeWidth": 1.2,
		"zoomIcon":{
			"in": "controllsPanel/zoomInBlue.png",
			"out": "controllsPanel/zoomOutBlue.png"
		},
		"fullscreenIcon":{
			"in": "fullscreenInIcon.png",
			"out": "fullscreenOutIcon.png"
		},
		"checkBox": "mapCheckBoxBlue.png"
	},
	"taskResultRect":{
		"labelRect":{
			"color": "#DEF2F8"
		},
		"valueRect":{
			"color": "#C1E8F3"
		}
	},
	"scrollBar":{
		"trackColor": "#87D0E7",
		"arrowColor": "#87D0E7",
		"innerSliderColor": "#87D0E7",
		"outerSliderColor": "#87D0E7"
	},
	"bottomPanel":{
		"color": "#DCE6D1",
		"stroke":"#C0DEA8"
	},
	"button":{
		"gradientBackgroundColorStart": "white",
		"gradientBackgroundColorEnd": "white",
		"labelColor": "black",
		"stroke": "#46a6d1",
		"strokeWidth": 2.5,
		"cornerRadius": 0
	},
	"resultButton":{
		"gradientBackgroundColorStart": "grey",
		"gradientBackgroundColorEnd": "grey",
		"labelColor": "white",
		"stroke": "",
		"strokeWidth": 0,
		"cornerRadius": 8,
		"correctColor" : "#1ECC1E",
		"wrongColor" :  "#FF4C4C"
	},
	"player":{
		"backgroundRectColor": "#666666",
		"trackBackroundColor": "#DFDFDF",
		"trackColor": ["#D9F0FE", "#99D7FE"]
	},
	"taskContainer":{
		"outerRect":{
			"backgroundColor": "#FFFFFF",
			"strokeColor": "#ACBAAD"
		},
		"innerRect":{
			"backgroundColor": "#829683",
			"strokeColor": "#243A25"
		}
	},
	"header":{
		"mainTitle":{
			"backgroundColor": "#46a6d1",
			"fontColor": "white",
			"stroke": ""
		},
		"sidePanel":{
			"backgroundColor": "#00A8D4",
			"stroke": "",
			"icon": "icon.png"
		},
		"questionTitle":{
			"backgroundColor": "white",
			"fontColor": "black",
			"stroke": "#46a6d1"
		}
	}
}