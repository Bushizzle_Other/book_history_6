$(window).load(function () {

    $(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

    function reorient(e) {
        window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
    events[1].click = 'tap';
    events[0].down = 'mousedown';
    events[1].down = 'touchstart';
    events[0].up = 'mouseup';
    events[1].up = 'touchend';
    events[0].move = 'mousemove';
    events[1].move = 'touchmove';
    events[0].mouseover = 'mouseover';
    events[1].mouseover = 'mouseover';
    events[0].mouseout = 'mouseout';
    events[1].mouseout = 'mouseout';
    events[0].mousedown = 'mousedown';
    events[1].mousedown = 'tap';

	/*Глобальные переменные**/

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var setArray = [];
	var currentSlide = null;

	var controllsPanel = null;

	var currentInterval = null;

	var currentSetIndex;

	var aspectRatio = 0; //widescreen 16:9

    var defaultStageWidth = 1024;
    var defaultStageHeight = 576;

	var tweenMachine = null;

	//Отступ всего контента от сцены
	var marginLeft = 60;
    var marginTop = 60;

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 20;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

	var isHorizontal;

    var root = '';

    var isTitleLoaded, isSlidesLoaded, isSoundLoaded = false;

    var mixer = null;

    var levelManager = new LevelManager();

    var skinManager = null;
    var colorSheme = null;

    var setTitle = null;

    var mainFont = 'mainFont';

    var parseMode = 0;

    var slideManager = null;

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
    var contentLayer = new Kinetic.Layer();
	var controllsLayer = new Kinetic.Layer();
	var imageContainer = new Kinetic.Layer();

	stage.add(backgroundLayer);
    stage.add(contentLayer);
	stage.add(controllsLayer);
	stage.add(imageContainer);

	/*Группы**/
	var tableGroup = new Kinetic.Group();
	var answerGroup = new Kinetic.Group();
	var dropContainerGroup = new Kinetic.Group();

	contentLayer.add(tableGroup);
	contentLayer.add(answerGroup);
	contentLayer.add(dropContainerGroup);

	$(window).on('resize', fitStage);

    function fitStage(){
        scaleScreen($(window).width(),  $(window).height());
    }

    function initAspectRatio(){
        if($(window).width() / $(window).height() < 1.6){
            aspectRatio = 1;
            defaultStageWidth = 1024;
            defaultStageHeight = 768;

            stage.width(defaultStageWidth);
            stage.width(defaultStageHeight);
        }
    }

    function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        stage.draw();
    }

    function initWindowSize(){
        scaleScreen($(window).width(), $(window).height());
    }

    skinManager = new SkinManager('assets/skin', function(){
        initAspectRatio();
        initWindowSize();

        setTimeout(initApplication, 300);
    });

    var preLoader = null;

    function initApplication(){
        preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));

        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    }

    function xmlLoadHandler(xml){
        tweenMachine = buildTweenMachine();

    	root = $(xml).children('component');

        levelManager.initRoute(root);

        //setTitle = createSetTitle();
        //setTitle.hide();
        //contentLayer.add(setTitle);

        slideManager = new buildSlideManager();

        parseMode = root.length == 0 ? 1 : 0;

        if(parseMode == 0){
            buildSlides();
        } else{
            root = $(xml).children('SS');
            buildAdaptedSlides();
        }
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            };

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
            };

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        };

        imgObj.src = 'assets/preLoader.png';

        return preloaderObject;
    }

    function onAssetsLoaded(){
        var mainTitle = root.attr('text');

        if(typeof mainTitle == 'undefined') mainTitle = '';

        //x = (($(window).width() / stage.scaler) >> 1) - 224;
        //y = (($(window).height() / stage.scaler) >> 1) - 279;

        createTitleScreen(mainTitle);
    }

    function buildControllsPanel(x, y, slideCount){
    	var panelGroup = new Kinetic.Group({x: x, y: y});

    	var playButton = createPlayButton(0, 0);

    	//var markerLine = createMarkerLine(5, 15, 0, 15);

		var markerSpacer = 40;

    	var x = markerSpacer;
    	
    	panelGroup.markerArray = [];
    	panelGroup.playButton = playButton;

    	//panelGroup.add(markerLine);
    	panelGroup.add(playButton);

    	for(var i = 0; i<slideCount; i++){
    		var marker = createSetMarker(x + markerSpacer * i, 0, i);

    		panelGroup.add(marker);

    		panelGroup.markerArray.push(marker);
    	}

    	//markerLine.getPoints()[2] = marker.x();

    	controllsLayer.add(panelGroup);
    	controllsLayer.draw();

    	panelGroup.activeMarker = null;

    	panelGroup.setMarker = function(index){

    		if(panelGroup.activeMarker != null) panelGroup.activeMarker.setClear();

    		panelGroup.activeMarker = panelGroup.markerArray[index];

    		panelGroup.activeMarker.setActive();

    		panelGroup.changePlayButtonState('stop');
    	};
		
    	panelGroup.changePlayButtonState = function(state){
    		panelGroup.playButton.setButtonState(state);
    	};

    	return panelGroup;
    }

    function createMarkerLine(fromX, fromY, toX, toY){
    	 var markerLine = new Kinetic.Line({
	        points: [fromX, fromY, toX, toY],
	        stroke: '#EEEEEE',
	        strokeWidth: 4
	      });

    	 return markerLine;
    }

    function createPlayButton(x, y){
    	var imageObj = new Image();
    	var buttonGroup = new Kinetic.Group({x: x, y: y});

    	buttonGroup.width(30);
    	buttonGroup.height(30);

    	buttonGroup.playButton = null;

    	imageObj.onload = function(){
    		var playButton = new Kinetic.Sprite({
				x: 0,
				y: - 0.5,
				image: imageObj,
				animation: 'stop',
				animations: {
					play: [
					  // x, y, width, height
					  0,0,64,64
					],
					stop: [
					  // x, y, width, height
					  64,0,64,64
					]
				},
				frameRate: 7,
				frameIndex: 0,
                scale:{
                    x: 0.49,
                    y: 0.49
                }
			});

			buttonGroup.playButton = playButton;

			buttonGroup.on(events[isMobile].click, function(evt){
				if(buttonGroup.playButton.animation() == 'stop'){
                    slideManager.pause();
					//clearInterval(currentInterval);
					buttonGroup.playButton.animation('play');
                    //mixer.stop();
                    mixer.pause();
				} else{
                    slideManager.resume();
					//currentInterval = setInterval(jumpToNextSlide, currentSlide.time);
					buttonGroup.playButton.animation('stop');
                    mixer.soundCounter = 0;
                    //mixer.play(currentSetIndex);
                    mixer.resume();
				}

				controllsLayer.draw();
			});

			buttonGroup.on('mouseover', hoverPointer);
			buttonGroup.on('mouseout', resetPointer);

			buttonGroup.add(playButton);
			controllsLayer.draw();
    	};

    	imageObj.src = skinManager.getGraphic(colorSheme.slidePlayButton);

    	buttonGroup.setButtonState = function(state){
    		if(buttonGroup.playButton!=null){
    			buttonGroup.playButton.animation(state);
    			controllsLayer.draw();
    		}
    	};

    	return buttonGroup;
    }

    function buildTweenMachine(){
    	var tweener = {};

        var limitX = $(window).width() / stage.scaler;
        var limitY = $(window).height() / stage.scaler;

    	tweener.rightIn = function(node, duration, easing, callback){
    		var dx = node.x();

    		node.x(-limitX);

    		var tween = new Kinetic.Tween({
    			node: node,
    			x: dx,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	}

    	tweener.rightOut = function(node, duration, easing, callback){
    		var tween = new Kinetic.Tween({
    			node: node,
    			x: limitX,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		})

    		tween.play();
    	};

    	tweener.leftIn = function(node, duration, easing, callback){
    		var dx = node.x();

    		node.x(limitX);

    		var tween = new Kinetic.Tween({
    			node: node,
    			x: dx,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};

    	tweener.leftOut = function(node, duration, easing, callback){
    		var tween = new Kinetic.Tween({
    			node: node,
    			x: -limitX,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};

    	tweener.downIn = function(node, duration, easing, callback){
    		var dy = node.y();

    		node.y(limitY);

    		var tween = new Kinetic.Tween({
    			node: node,
    			y: dy,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};

    	tweener.downOut = function(node, duration, easing, callback){
    		var tween = new Kinetic.Tween({
    			node: node,
    			y: -limitY,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};

    	tweener.upIn = function(node, duration, easing, callback){
    		var dy = node.y();

    		node.y( - limitY);

    		var tween = new Kinetic.Tween({
    			node: node,
    			y: dy,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};

    	tweener.upOut = function(node, duration, easing, callback){
    		var tween = new Kinetic.Tween({
    			node: node,
    			y: limitY,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};

    	tweener.fadeIn = function(node, duration, easing, callback){

    		node.opacity(0);

    		var tween = new Kinetic.Tween({
    			node: node,
    			opacity: 1,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};

    	tweener.fadeOut = function(node, duration, easing, callback){
    		var tween = new Kinetic.Tween({
    			node: node,
    			opacity: 0,
    			duration: duration,
    			onFinish: callback,
    			easing: easing
    		});

    		tween.play();
    	};


    	return tweener;
    }

    function createSetMarker(x, y, setIndex){
    	var imageObj = new Image();
    	var buttonGroup = new Kinetic.Group({x: x, y: y});

    	buttonGroup.setMarker = null;
    	buttonGroup.animation = 'clear'
    	buttonGroup.setIndex = setIndex;

        var markerSheme = colorSheme.setMarker;

        var setMarker = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 30,
            height: 30,
            fill: markerSheme.regular.color,
            stroke: markerSheme.regular.stroke,
            strokeWidth: markerSheme.regular.strokeWidth
        });

		var label = new Kinetic.Text({
			x: 0,
			y: 0,
			fontFamily: mainFont,
			fontSize: 20,
			text: setIndex,
			fill: markerSheme.regular.fontColor
		});

        if(label.width() < setMarker.width()) label.x((setMarker.width() >> 1) - (label.width() >> 1));
        if(label.height() < setMarker.height()) label.y((setMarker.height() >> 1) - (label.height() >> 1));

        buttonGroup.setMarker = setMarker;
		buttonGroup.label = label;

		buttonGroup.on(events[isMobile].click, function(){
			if(currentSlide!=setArray[buttonGroup.setIndex].slides[0]){
				showSet(buttonGroup.setIndex);
			}
		});

		buttonGroup.on('mouseover', hoverPointer);
		buttonGroup.on('mouseout', resetPointer);

		buttonGroup.add(setMarker);
		buttonGroup.add(label);

		controllsLayer.draw();
    	//}

    	//imageObj.src = 'assets/setMarker.png';

    	buttonGroup.setActive = function(){
    		if(buttonGroup.setMarker != null){
	    		//buttonGroup.setMarker.animation('active');
                buttonGroup.setMarker.fill(markerSheme.active.color);
                buttonGroup.setMarker.stroke(markerSheme.active.stroke);
                buttonGroup.label.fill(markerSheme.active.fontColor);
	    		backgroundLayer.draw();
	    	} else{
	    		//buttonGroup.animation = 'active';
	    	}
    	};

		buttonGroup.setClear = function(){
			if(buttonGroup.setMarker != null){
	    		//buttonGroup.setMarker.animation('clear');
                buttonGroup.setMarker.fill(markerSheme.regular.color);
                buttonGroup.setMarker.stroke(markerSheme.regular.stroke);
                buttonGroup.label.fill(markerSheme.regular.fontColor);
	    		backgroundLayer.draw();
    		} else{
	    		//buttonGroup.animation = 'clear';
	    	}
    	};

    	return buttonGroup;
    }

    function timeStringToMilliseconds(timeString){
    	var min = +timeString.substr(0, 2) * 60000;
    	var sec = +timeString.substr(3, 2) * 1000;
    	return (min + sec);
    }

    function buildAdaptedSlides(){
        var containerWidth = $(window).width() / stage.scaler;
        var containerHeight = $(window).height() / stage.scaler - 60;

        var slideCounter = {
            counter: 0,
            inc: function(){
                this.counter++;
            },
            dec: function(){
                this.counter--;
                if(this.counter == 0) mixer = buildSoundMixer(setArray, onAssetsLoaded);
            }
        }

        var slidesData = $(root).children('frames').children('frame');
        //var sound = levelManager.getRoute($(root).children('sounds').children('sound').eq(0).attr('File'));
        var sounds = $(root).children('sounds').children('sound');

        var soundCollection = [];

        $(sounds).each(function(soundIndex){
            sound = levelManager.getRoute($(this).attr('File'));
            if(typeof sound == 'undefined') sound = '';

            soundCollection.push(sound);
        });

        var prevTime = 0;

        if(typeof sound == 'undefined') sound = '';

        setArray[0] = {
            slides: [],
            sound: soundCollection
        };

        $(slidesData).each(function(subIndex){
            var picture = levelManager.getRoute($(this).attr('File'));
            var time = $(this).attr('duration');
            //var title = $(this).attr('text');
            var tooltip = $(this).attr('text');

            var title = '';

            if(typeof title == 'undefined') title = '';
            if(typeof tooltip == 'undefined') tooltip = '';

            slideCounter.inc();

            var slide = createSlide(containerWidth, containerHeight, picture, tooltip, title, slideCounter);
            slide.time = time;
            console.log(time);
            slide.slideIndex = setArray[0].slides.length;
            slide.setIndex = 0;
            slide.timeMark = prevTime;

            prevTime += slide.time;

            setArray[0].slides.push(slide);

            slide.hide();
        });
    }

    function buildSlides(){
        var sets = $(root).children('set');

    	var containerWidth = $(window).width() / stage.scaler;
    	var containerHeight = $(window).height() / stage.scaler - 60;

        var slideCounter = {
            counter: 0,
            inc: function(){
                this.counter++;
            },
            dec: function(){
                this.counter--;
                if(this.counter == 0) mixer = buildSoundMixer(setArray, onAssetsLoaded);
            }
        };

    	$(sets).each(function(index){
    		var slidesData = $(this).children('picture');
    		var sound = levelManager.getRoute($(this).attr('sound'));

            var prevTime = 0;

    		if(typeof sound == 'undefined') sound = '';

    		setArray[index] = {
                slides: [],
                sound: sound
            };

    		$(slidesData).each(function(subIndex){
	    		var picture = levelManager.getRoute($(this).attr('img'));
                var time = $(this).attr('time');
	    		var title = $(this).attr('title');
                var tooltip = $(this).text();

                if(typeof title == 'undefined') title = '';

                slideCounter.inc();

    			var slide = createSlide(containerWidth, containerHeight, picture, tooltip, title, slideCounter);
    			slide.time = timeStringToMilliseconds(time);
    			slide.slideIndex = setArray[index].slides.length;
    			slide.setIndex = index;
                slide.timeMark = prevTime;

                prevTime += slide.time;

    			setArray[index].slides.push(slide);

    			slide.hide();
    		});
    	});
    }

    function buildSoundMixer(assetArray, onAssetsLoaded){
        var mixer = {};

        if (!createjs.Sound.initializeDefaultPlugins()) {return null;}

        var audioPath = "";
        var manifest = [];

        if(parseMode == 0){

            for(var i = 0; i < assetArray.length; i++){
                if(assetArray[i].sound == '') continue;

                manifest.push({
                    id: i,
                    src: assetArray[i].sound
                });
            }

            mixer.onComplete = function(){
                //do nothing
            };

        } else{
            for(var i = 0; i < assetArray[0].sound.length; i++){
                if(assetArray[0].sound[i] == '') continue;

                manifest.push({
                    id: i,
                    src: assetArray[0].sound[i]
                });
            }

            mixer.soundCount = assetArray[0].sound.length;
            mixer.soundCounter = 0;

            mixer.onComplete = function(){
                mixer.soundCounter++;
                if(mixer.soundCounter >= mixer.soundCount){
                    mixer.soundCounter = 0;
                    return;
                }
                mixer.play(mixer.soundCounter);
            };
        }

        if(manifest.length < 1){
            onAssetsLoaded();
            return null;
        };
        
        mixer.loadCounter = manifest.length;

        createjs.Sound.alternateExtensions = ["ogg"];
        createjs.Sound.addEventListener("fileload", handleLoad);
        createjs.Sound.registerManifest(manifest, audioPath);

        mixer.targetUI = null;

        mixer.instanceId = -1;

        mixer.play = function(id){
            //if(mixer.instanceId == id) return;

            if(mixer.instance != null) mixer.instance.stop();

            mixer.instance = createjs.Sound.play(id);
            mixer.instanceId = id;

            mixer.instance.addEventListener ("complete", function(instance) {
                if(mixer.onComplete) mixer.onComplete();
                //if(mixer.targetUI != null) mixer.targetUI.reset();
            });
        };

        mixer.stop = function(){
            if(mixer.instance != null) mixer.instance.stop();

            mixer.instanceId = null;

            mixer.instance.removeEventListener("complete", function(instance) {
                //if(mixer.targetUI != null) mixer.targetUI.reset();
            });
        };

        mixer.pause = function(){
            if(mixer.instance != null) mixer.instance.pause();
        };

        mixer.resume = function(){
            if(mixer.instance != null) mixer.instance.resume();
        };

        mixer.playFrom = function(time){
            mixer.instance.setPosition(time);
        };

        function handleLoad(event) {
            mixer.loadCounter--;
            if(mixer.loadCounter == 0 && onAssetsLoaded) onAssetsLoaded()
        };

        return mixer;
    }

    function createTitleScreen(title){
    	var titleGroup = new Kinetic.Group({x: 0, y: 0});

    	var backgroundImage = new Image();

        var titleLoadCounter = {
            counter: 0,
            inc: function(){
                this.counter++;
            },
            dec: function(){
                this.counter--;
                if(this.counter == 0) onAssetsLoaded();
            }
        };

        titleLoadCounter.inc();

    	//backgroundImage.onload = function(){
		/*var image = new Kinetic.Image({
		  x: 150,
		  y: 153,
		  image: backgroundImage,
		  width: 409,
		  height: 335
		});*/

        var padding = 10;

        var headSheme = colorSheme.header;

		var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        if(title == '') mainTitleRect.height(0);

        titleLoadCounter.inc();
        titleLoadCounter.dec();

		//titleGroup.add(image);
        titleGroup.add(mainTitleRect);
		titleGroup.add(mainTitle);
		titleGroup.add(createStartButton(0, mainTitleRect.height(), titleLoadCounter));			
    	//}

    	//backgroundImage.src = 'assets/titleBooks.png'


        function onAssetsLoaded(){
            preLoader.hidePreloader();
            
            createBackground();

            backgroundLayer.add(titleGroup);
            
            titleGroup.on('mouseover', hoverPointer);
            titleGroup.on('mouseout', resetPointer);
            titleGroup.on(events[isMobile].click, startSlideShow)

            backgroundLayer.draw();
        }
    }

    function createBackground(){
    	/*var rect = new Kinetic.Rect({
    		x: 0,
    		y: 0,
    		fill: '#C0C0C0',
    		width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler
    	})

    	backgroundLayer.add(rect);
    	backgroundLayer.draw();*/
    }

    function createStartButton(x, y, titleLoadCounter){
    	var buttonContainer = new Kinetic.Group({x: x, y: y});

    	var startButton = new Image();

    	startButton.onload = function(){
    		var image = new Kinetic.Image({
			  x: 0,
			  y: 0,
			  image: startButton,
			  width: 200,
			  height: 200
			});

            var avaibleHeight = ($(window).height() / stage.scaler) - y - 5;
            var avaibleWidth = ($(window).width() / stage.scaler);

            if(image.height() < avaibleHeight){
                buttonContainer.y(y + (avaibleHeight >> 1) - (image.height() >> 1) + 5);
            }

            if(image.width() < avaibleWidth){
                buttonContainer.x((avaibleWidth >> 1) - (image.width() >> 1));
            }

			buttonContainer.add(image)
			//backgroundLayer.draw();

            titleLoadCounter.dec();
    	}

    	startButton.src = skinManager.getGraphic(colorSheme.header.playButton);

    	return buttonContainer;
    }

    function startSlideShow(evt){        
    	var titleScreen = evt.targetNode.getParent().getParent();

        tweenMachine.downOut(titleScreen, 0.4, Kinetic.Easings.Linear, function(){
            titleScreen.hide();
        });

    	controllsPanel = buildControllsPanel(35, $(window).height() / stage.scaler - 40, setArray.length);

    	showSet(0);
    	//createSlide(defaultStageWidth, defaultStageHeight, '2.jpg', '01   Пожарные и спасатели', 'Запомните телефоны городских служб безопасности:')
    }

    function showSet(index){
    	//if(currentSlide!=null) currentSlide.hide();
    	//if(currentInterval!=null) clearInterval( currentInterval );

        slideManager.stop();

    	controllsPanel.setMarker(index);

    	showSlide(setArray[index].slides[0], currentSlide);

    	currentSlide = setArray[index].slides[0];
    	//currentSlide.show();

        mixer.soundCounter = 0;

        mixer.play(index);

        slideManager.play(currentSlide.time);

    	//currentInterval = setInterval(jumpToNextSlide, currentSlide.time);
    	currentSetIndex = index;
    	contentLayer.draw();
    }

    function buildSlideManager() {
        var timerId, start, remaining, duration;

        this.play = function(duration){
            start = new Date();
            remaining = duration;
            timerId = window.setTimeout(function() {
                jumpToNextSlide();
            }, remaining);
        };

        this.pause = function() {
            window.clearTimeout(timerId);
            remaining -= new Date() - start;
        };

        this.resume = function() {
            start = new Date();
            timerId = window.setTimeout(function() {
                jumpToNextSlide();
            }, remaining);
        };

        this.stop = function(){
            window.clearTimeout(timerId);
        };
    }

    function jumpToNextSlide(){
    	var currentSlideIndex = currentSlide.slideIndex;
    	var currentSetIndex = currentSlide.setIndex;

    	//if(currentInterval!=null) clearInterval( currentInterval );

    	currentSlideIndex++;

    	if(currentSlideIndex<setArray[currentSetIndex].slides.length){
    		/*currentSlide.hide();
    		currentSlide = setArray[currentSetIndex].slides[currentSlideIndex];
    		currentSlide.show();*/

    		showSlide(setArray[currentSetIndex].slides[currentSlideIndex], currentSlide);

    		currentSlide = setArray[currentSetIndex].slides[currentSlideIndex];

    		//currentInterval = setInterval(jumpToNextSlide, currentSlide.time);
            slideManager.play(currentSlide.time);
    	} else if((currentSetIndex+1)<setArray.length){ //jump to next set
    		showSet(currentSetIndex+1);
    	} else{
    		showResultLabel((($(window).width() / stage.scaler) >> 1) - 152, (($(window).height() / stage.scaler) >> 1) - 100); //315
    	}
    	contentLayer.draw();
    }

    function showSlide(nextSlide, prevSlide){

    	nextSlide.show();

    	tweenMachine.rightIn(nextSlide, 0.45, Kinetic.Easings.Linear);

    	if(prevSlide != null){
    		var defaultY = prevSlide.y();
    		tweenMachine.upOut(prevSlide, 0.3, Kinetic.Easings.Linear, function(){
    			prevSlide.y(defaultY);
    			prevSlide.hide();
    		});
    	}

    	/*var defaultX = nextSlide.x();

    	nextSlide.x( - 1000 );

    	nextSlide.show();

    	var nextTween = new Kinetic.Tween({
    		node: nextSlide,
    		duration: 0.5,
    		x: defaultX,
	    	easing: Kinetic.Easings.Linear
    	})

    	if(prevSlide != null){
	    	var prevTwen = new Kinetic.Tween({
	    		node: prevSlide,
	    		duration: 0.5,
	    		x: 1000,
	    		onFinish: function(){
	    			prevSlide.x(defaultX);
	    			prevSlide.hide();
	    		},
	    		easing: Kinetic.Easings.Linear
	    	})

	    	prevTwen.play();
	    }

    	nextTween.play();*/

    }

    function createSetTitle(){
        var titleContainer = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var padding = 10;

        var titleSheme = colorSheme.slideTitle;

        var titleLabel = new Kinetic.Text({
            x: 25,
            y: padding,
            text: '',
            fontSize: 21,
            fontFamily: mainFont,
            fill: titleSheme.fontColor,
            align: 'left'
        });

        var titleHeight = titleLabel.height() + titleLabel.fontSize() * 0.1 + (padding << 1);

        var titleLimiter = new Kinetic.Line({
            points: [
                0, titleHeight,
                $(window).width() / stage.scaler, titleHeight
            ],
            stroke: titleSheme.limiterColor,
            strokeWidth: titleSheme.limiterWidth
        });

        var maxWidth = $(window).width() / stage.scaler - (titleLabel.x() << 1);

        if(titleLabel.width()>maxWidth){
            titleLabel.width(maxWidth);
        } else{
            //titleLabel.x((maxWidth>>1) - (titleLabel.width()>>1));
        }

        //slideContainer.height(slideContainer.height() + titleLabel.height() + slideSpacer + titleLabel.y());

        titleContainer.add(titleLimiter);
        titleContainer.add(titleLabel);

        titleContainer.setValue = function(value){
            if(value == ''){
                titleContainer.hide();
                return;
            }
            titleContainer.show();
            titleLabel.text(value);

            titleHeight = titleLabel.height() + titleLabel.fontSize() * 0.1 + (padding << 1);

            titleLimiter.points[1] = titleHeight;
            titleLimiter.points[3] = titleHeight;
        };

        //slideContainer.add(titleContainer);

        //titleContainer.height(titleHeight + titleLimiter.height())

        //contentY += titleContainer.height() + slideSpacer;

        //tipPostion += titleContainer.height();
        return titleContainer;
    }

    function createSlide(maxWidth, maxHeight, image, tooltip, title, slideCounter){
    	var slideContainer = new Kinetic.Group({x: 0, y: 0});

        var slideSpacer = 10;

    	var contentX = 0;
    	var contentY = 0;

        var tipPostion =  slideSpacer;

    	if(title!=''){
            var titleContainer = new Kinetic.Group({
                x: 0,
                y: 0
            });

            var padding = 10;

            var titleSheme = colorSheme.slideTitle;

	    	var titleLabel = new Kinetic.Text({
				x: 25,
				y: padding,
				text: title,
				fontSize: 21,
		        fontFamily: mainFont,
		        fill: titleSheme.fontColor,
		        align: 'left'
			});

            var titleHeight = titleLabel.height() + titleLabel.fontSize() * 0.1 + (padding << 1);

            var titleLimiter = new Kinetic.Line({
                points: [
                    0, titleHeight,
                    $(window).width() / stage.scaler, titleHeight
                ],
                stroke: titleSheme.limiterColor,
                strokeWidth: titleSheme.limiterWidth
            });

			if(titleLabel.width()>maxWidth){
				titleLabel.width(maxWidth);
			} else{
				//titleLabel.x((maxWidth>>1) - (titleLabel.width()>>1));
			}

			slideContainer.height(slideContainer.height() + titleLabel.height() + slideSpacer + titleLabel.y());

            titleContainer.add(titleLimiter);
            titleContainer.add(titleLabel);

			slideContainer.add(titleContainer);

            titleContainer.height(titleHeight + titleLimiter.height());

			contentY += titleContainer.height() + slideSpacer;

            tipPostion += titleContainer.height();
	    }

	    if(tooltip!=''){
	    	var tipLabel = new Kinetic.Label({
	    		x: 25,
	    		y: 0
	    	});

            var tipSheme = colorSheme.slideTip;
            var maxLabelWidth = ($(window).width() / stage.scaler) - 50;

	    	var tip = new Kinetic.Text({
				x: 0,
				y: 0,
				text: tooltip,
				fontSize: 20,
		        fontFamily: mainFont,
		        fill: tipSheme.fontColor,
		        align: 'left',
                padding: 15
			});

            var maxTipWidth = maxLabelWidth - (tip.padding() << 1);

		    if(tip.width()>maxTipWidth){
                tip.width(maxTipWidth);
            }

			tipLabel.y(tipPostion + slideSpacer);

			tipLabel.add(new Kinetic.Tag({
				fill: tipSheme.color,
				stroke: tipSheme.stroke,
				strokeWidth: tipSheme.strokeWidth
			}));

			tipLabel.add(tip);

			if(tipLabel.width()<maxLabelWidth){
				tipLabel.x(25 + (maxLabelWidth>>1) - (tipLabel.width()>>1));
			}

			slideContainer.add(tipLabel);

			slideContainer.height(slideContainer.height() + tipLabel.height() + slideSpacer);
	    }

        if(image!=''){
            var imageObj = new Image();

            imageObj.onload = function(){
                var slidePicture = new Kinetic.Image({
                  x: contentX,
                  y: contentY,
                  image: imageObj,
                  width: imageObj.width,
                  height: imageObj.height
                });

                var scaler = calculateAspectRatioFit(slidePicture.width(), slidePicture.height(), maxWidth, maxHeight - slideContainer.height())
                slidePicture.scaleX(scaler);
                slidePicture.scaleY(scaler);

                if(slidePicture.width()*scaler<maxWidth){
                    slidePicture.x((maxWidth>>1) - ((slidePicture.width()*scaler)>>1));
                }

                slideContainer.add(slidePicture);
                //contentLayer.draw();

                slideCounter.dec();

                if(tooltip != '') tipLabel.y(slidePicture.y() + slidePicture.height() * scaler + slideSpacer);

                slideContainer.height(slideContainer.height() + slidePicture.height() * scaler);

                if(slideContainer.height() < maxHeight) slideContainer.y(($(window).height() / stage.scaler >> 1) - (slideContainer.height() >> 1));
            }

            imageObj.src = image;
        } else slideCounter.dec();

        if(image == ''){
            if(slideContainer.height() < maxHeight) slideContainer.y(($(window).height() / stage.scaler >> 1) - (slideContainer.height() >> 1));
        }

	    contentLayer.add(slideContainer);
	    //contentLayer.draw();

	    return slideContainer;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function showResultLabel(x, y){
    	var resultLabel = new Kinetic.Group({
    		x: x,
    		y: y
    	});

        var resultSheme = colorSheme.resultScreen;
        
        var padding = 10;

    	var head = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: padding,
	        text: 'Конец слайд-шоу',
	        fontSize: 24,
	        fontFamily: mainFont,
	        fontStyle: 'bold',
	        fill: resultSheme.header.fontColor,
            width: 400,
            align: "center"
	    });

        var headRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: head.width(),
            height: head.height() + (padding << 1),
            fill: resultSheme.header.color
        });

	    var description = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: headRect.height() + padding,
	        text: 'чтобы запустить ещё раз, \nкоснитесь данной надписи \nили нажмите кнопку любого слайда',
	        fontSize: 18,
	        fontFamily: mainFont,
	        align: 'center',
	        fill: resultSheme.fontColor,
            width: head.width()
	    });

        var descriptionRect = new Kinetic.Rect({
            x: 0,
            y: headRect.height(),
            width: headRect.width(),
            height: description.height() + (padding << 2),
            fill: resultSheme.color,
            stroke: resultSheme.stroke,
            strokeWidth: resultSheme.strokeWidth
        })

        resultLabel.add(headRect);
    	resultLabel.add(head);
        resultLabel.add(descriptionRect);
    	resultLabel.add(description);

        resultLabel.on(events[isMobile].mouseover, hoverPointer);
        resultLabel.on(events[isMobile].mouseout, resetPointer);

        resultLabel.on(events[isMobile].click, function(){
            showSet(0);
        });

    	var dy = currentSlide.y();

        backgroundLayer.add(resultLabel);

        var lastSlide = currentSlide;

    	tweenMachine.upOut(currentSlide, 0.5, Kinetic.Easings.Linear, function(){
    		lastSlide.y(dy);
    		lastSlide.hide();
    	});

    	tweenMachine.fadeIn(resultLabel, 0.5, Kinetic.Easings.Linear, function(){
    		currentSlide = resultLabel;
    	});
    }

    function createZoomButton(x, y, linkedImage){
    	var zoomButtonGroup = new Kinetic.Group({x: x, y: y})
		var imageObj = new Image();

        imageObj.onload = function() {
			var zoomButton = new Kinetic.Sprite({
				x: 0,
				y: 0,
				image: imageObj,
				animation: 'zoomIn',
				animations: {
					zoomIn: [
					  // x, y, width, height
					  0,0,23,23
					],
					zoomOut: [
					  // x, y, width, height
					  23,0,23,23
					]
				},
				frameRate: 7,
				frameIndex: 0			
			});;

			zoomButton.on(events[isMobile].click, function(){
				imageContainer.zoomImage(linkedImage);
			});

			zoomButtonGroup.add(zoomButton);

			linkedImage.getParent().add(zoomButtonGroup);
			pictureLayer.draw();
		}
		imageObj.src = 'assets/zoomBtn.png';

		return zoomButtonGroup;
	}

	function initImageContainer(){
    	var image = new Kinetic.Image({
		  x: 20,
		  y: 20
		});

		var label = new Kinetic.Text({
			x: 20,
		  	y: 20
		});

		var rect = new Kinetic.Rect({
			x: 20,
		  	y: 20
		});

		image.hide();
		rect.hide();
		label.hide();

		imageContainer.add(image);
		imageContainer.add(rect);
		imageContainer.add(label);

		imageContainer.zoomImage = function(linkedImage){
			var imageObj = linkedImage.getImage();
			var objGroup = linkedImage.getParent();

			if(!image.isVisible()) image.show();

			image.setImage(imageObj);

			var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight-40);

			image.width(imageObj.width*scaler);
			image.height(imageObj.height*scaler);

			image.x(((defaultStageWidth-40)>>1)-(image.width()>>1))

			image.on(events[isMobile].click, zoomOut);

			if( typeof objGroup.label != 'undefined' ){

				label.setAttrs(objGroup.label.getAttrs());
				rect.setAttrs(objGroup.labelRect.getAttrs());

				label.x(image.x() + 10)
				label.y((image.y() + image.height()*image.scaleX()) - label.height());

				label.width(image.width()*image.scaleX() - 10);

				rect.x(image.x());
				rect.y(label.y());

				rect.width(label.width() + 10)

				rect.show();
				label.show();
			}

			imageContainer.draw();
		}

		function zoomOut(evt){
			image.hide();
			rect.hide();
			label.hide();
			imageContainer.draw();
		}
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

});