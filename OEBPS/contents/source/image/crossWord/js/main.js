$(window).load(function () {

	$(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

	Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    function reorient(e) {
      window.location.href = window.location.href;
    }

    window.onorientationchange = reorient;

	/*Глобальные переменные**/

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

    if(isMobile) Kinetic.pixelRatio = 1;

	var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

    var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'mouseover';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'mouseout';

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var aspectRatio = 0; //widescreen 16:9

	var defaultStageWidth = 1024;
	var defaultStageHeight = 576;

	var symbolBoxWidth = isMobile ? 60 : 35;
	var symbolBoxHeight = isMobile ? 60 : 35;

	var marginLeft = 25;
	var marginTop = 25;

	var spacer = 3;

	var targetBox = null;
	var targetRoot = null;

	var tooltipBox = null;

	var contentMargin = {x: 5, y: 5};

	var crossTileMatrix = [];

	var wordsArray = [];

	var showKeyboard = false;

	//Отступ всего контента от сцены
	var marginLeft = 60;
    var marginTop = 60;

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 20;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var answerGroupSpacer = 50; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var scrollObject = null;

	var xmlFontSize;

	var isHorizontal;

	var autoplay = true;

	var mixer = null;
	var titlePlayer = null;

	var root = null;
	var assetArray = [];

	var tipContainer = null;

    var levelManager = new LevelManager();

    var header = null;

    var containerPadding = 10;

    var resultScreen = null;

    var keyboard = null;

    var mainFont = 'mainFont';

    var playerProportions = {
        x: 250,
        y: 45
    }

	/*Слои**/
	var playerLayer = new Kinetic.Layer();
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
	var arrowLayer = new Kinetic.Layer();
	var tooltipBoxLayer = new Kinetic.Layer();
	var keayboardLayer = new Kinetic.Layer();
	var contentScrollLayer = new Kinetic.Layer();
	var imageContainer = new Kinetic.Layer();
    var crossPreviewLayer = new Kinetic.Layer();
	var resultLayer = new Kinetic.Layer();

	stage.add(playerLayer);
	stage.add(backgroundLayer);
	stage.add(contentLayer);
	stage.add(arrowLayer);
	stage.add(tooltipBoxLayer);
	stage.add(keayboardLayer);
	stage.add(contentScrollLayer);
	stage.add(imageContainer);
    stage.add(crossPreviewLayer);
	stage.add(resultLayer);

	/*Группы**/
	var crossWordGroup = new Kinetic.Group();
	var answerGroup = new Kinetic.Group();

	contentLayer.add(answerGroup);

	$(window).on('resize', fitStage);

	function fitStage(){
		scaleScreen($(window).width(),  $(window).height());
	}

	function initAspectRatio(){
		if($(window).width() / $(window).height() < 1.6){
			aspectRatio = 1;
			defaultStageWidth = 1024;
			defaultStageHeight = 768;

			stage.width(defaultStageWidth);
			stage.width(defaultStageHeight);
		}
	}

	function scaleScreen(width, height){

        width -= screenMarginX;
        height -= screenMarginY;

        var scalerX = width / defaultStageWidth;
        var scalerY = height / defaultStageHeight;
        var scaler = Math.min(scalerX, scalerY);
    
        stage.scaleX(scaler);
        stage.scaleY(scaler);

        stage.scaler = scaler;

        stage.width(width);
        stage.height(height);

        stage.draw();
    }

	function initWindowSize(){		
		scaleScreen($(window).width(), $(window).height());
	}

    var preLoader = null;
    var skinManager = null;
    var colorSheme = null;

    skinManager = new SkinManager('assets/skin', function(){
        initAspectRatio();
        initWindowSize();

        setTimeout(initApplication, 300);
    });

    function initApplication(){
        colorSheme = skinManager.colorSheme;

        preLoader = createPreLoader((defaultStageWidth >> 1), (defaultStageHeight >> 1));

        setTimeout(function(){
            levelManager.loadConfig(xmlLoadHandler);
        }, 50);
    }

    function xmlLoadHandler(xml){

    	root = $(xml).children('component');

        levelManager.initRoute(root);

    	preloadAssets(root.children('words').children('word'), startApp);
    }

    function startApp(){
    	var mainTitle = root.children('title').text();
    	var question = root.children('question').text();

    	preLoader.hidePreloader();

        resultScreen = initResultScreen();

    	//$('#mainTitle').html(mainTitle);

    	tipContainer = buildTipContainer();

    	//attachBackground();

    	header = createHead(mainTitle, question);

        backgroundLayer.add(header);
        backgroundLayer.draw();

    	var words = root.children('words').children('word');

    	showKeyboard = $(root).attr('keyboard') == 'yes' ? true : false;

    	if(isMobile) showKeyboard = true;
        
        var arrowSize = isMobile ? 84 : 42;

        var crossWordContainer = new Kinetic.Rect({
            x: containerPadding,
            y: header.height() + containerPadding,
            width: $(window).width() / stage.scaler - 310 - containerPadding,
            height: (($(window).height() / stage.scaler) - (header.height() + containerPadding) - containerPadding),
            fill: colorSheme.crosswordPanel.backgroundColor,
            stroke: colorSheme.crosswordPanel.stroke,
            cornerRadius: colorSheme.crosswordPanel.cornerRadius
        });

        backgroundLayer.add(crossWordContainer);

        var contentClip = {
		  x: crossWordContainer.x() + arrowSize * 0.7,
		  y: crossWordContainer.y() + arrowSize * 0.7,
		  width: crossWordContainer.width() - arrowSize * 1.4,
		  height: (crossWordContainer.height() - arrowSize * 1.4)
		}

    	contentLayer.setClip(contentClip);

    	tooltipBox = buildTooltipBox();

    	buildCrossWord(symbolBoxWidth, (symbolBoxHeight << 1), words);

    	//createSymbolBox(50, 50, 50, 50);

    	//var checkButton = createButton(680, 0, 260, 45, 'Проверить', answerButtonHandler);

    	//createKeyboardBlock(200, 200, 65, 40, 'А');

    	if(showKeyboard){
           //keyboard = buildKeyBoard(crossWordContainer.x() + (crossWordContainer.width() >> 1) - 220.5, crossWordContainer.y() + crossWordContainer.height() - 160);
           keyboard = buildKeyBoard((($(window).width() / stage.scaler) >> 1) - 279.5, crossWordContainer.y() + crossWordContainer.height() - 200);
    	   resultLayer.add(keyboard);
        }

        //var dy = contentClip.y + contentClip.height + 20;
    	//var dx = contentClip.x + (contentClip.width >> 1) - 10;

        var arrowPadding = 5;

    	createScrollArrow(crossWordContainer.x() + arrowPadding, crossWordContainer.y()+ ((crossWordContainer.height() >> 1) - (arrowSize >> 1)) + arrowSize, 'left'); //42 x 42
    	createScrollArrow(crossWordContainer.x() + crossWordContainer.width() - (arrowSize >> 1) - arrowPadding, crossWordContainer.y() + ((crossWordContainer.height() >> 1) - (arrowSize >> 1)), 'right');
    	createScrollArrow(crossWordContainer.x() + (crossWordContainer.width() >> 1) - (arrowSize >> 1), crossWordContainer.y() + arrowPadding, 'up');
    	createScrollArrow(crossWordContainer.x() + (crossWordContainer.width() >> 1) - (arrowSize >> 1) + arrowSize, crossWordContainer.y() + crossWordContainer.height() - (arrowSize >> 1) - arrowPadding, 'down');
    	
    	$(document).bind('keypress', keyPressHandler);

    	$(document).unbind('keydown').bind('keydown', function (event) {
		    var doPrevent = false;
		    if (event.keyCode === 8) {

		    	deleteSymbolAndGoPrev();

		    	//crossbrowser preventDefault area
		        var d = event.srcElement || event.target;
		        if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD' || d.type.toUpperCase() === 'FILE')) 
		             || d.tagName.toUpperCase() === 'TEXTAREA') {
		            doPrevent = d.readOnly || d.disabled;
		        }
		        else {
		            doPrevent = true;
		        }
		    } else if(event.keyCode === 46){
		    	deleteSymbolAndGoNext();
		    } /*else{
		    	//keyPressHandler(event);
		    }*/

		    if (doPrevent) {
		        event.preventDefault();
		    }
		});

		initImageContainer();

		//createPreviewBox(crossWordGroup);

		/*createPreviewButton(
            crossWordContainer.x() + crossWordContainer.width() - 80,
            crossWordContainer.y() + 10);*/

        var buttonScaler = isMobile ? 1.5 : 1;

        var checkButton = createButton(
            crossWordContainer.x() + crossWordContainer.width() - 150 * buttonScaler,
            crossWordContainer.y() + 3,
            110 * buttonScaler,
            27 * buttonScaler,
            'Проверить',
            answerButtonHandler);

        var previewButton = createIconButton(
            checkButton.x() + checkButton.width() + 10,
            checkButton.y(),
            29 * buttonScaler,
            27 * buttonScaler,
            colorSheme.zoomIcon,
            function(){
                createPreviewBox(crossWordGroup);
            }
        )

        backgroundLayer.add(previewButton);

        if(showKeyboard) {
            var toKeyboardBtn = createIconButton(
                checkButton.x() - 58 * buttonScaler,
                checkButton.y(),
                48 * buttonScaler,
                27 * buttonScaler,
                colorSheme.keyboard.keyboardIcon,
                toggleKeyboard
            );

            backgroundLayer.add(toKeyboardBtn);
            /*var toKeyboardBtn = createButton(
            crossWordContainer.x() + crossWordContainer.width() - 260,
            crossWordContainer.y() + 10,
            50,
            25,
            'key',
            toggleKeyboard);*/
        }
    }

    function createIconButton(x, y, width, height, iconGraph, callback){
        var iconBtn = new Kinetic.Group({
            x: x,
            y: y
        });

        var imgObj = new Image();

        imgObj.onload = function(){
            var img = new Kinetic.Image({
                x: 0,
                y: 0,
                width: width,
                height: height,
                image: imgObj
            });

            iconBtn.add(img);

            iconBtn.getParent().draw();
        }

        imgObj.src = skinManager.getGraphic(iconGraph);

        iconBtn.on(events[isMobile].click, callback);
        iconBtn.on(events[isMobile].mouseover, hoverPointer);
        iconBtn.on(events[isMobile].mouseout, resetPointer);

        return iconBtn;
    }

    function toggleKeyboard(){
        keyboard.toggle();
    }

    function preloadAssets(words, onAssetsLoaded){
    	var imageLoadCounter = 0;
    	var soundArray = [];
    	var assetPartLoadCounter = 2;
    	var question = null;

    	$(words).each(function(index){
    		question = $(this).children('question');
    		var key = $(this).attr('x') + '' + $(this).attr('y') + '' + ($(this).attr('orientation') == 'across' ? 0 : 1);
    		var picture = levelManager.getRoute($(question).attr('picture'));
    		var sound = levelManager.getRoute($(question).attr('sound'));

    		var tip = $(this).children('tip');

    		assetArray[key] = [];

    		if(picture != '' && typeof picture != 'undefined'){
    			imageLoadCounter++;
    			assetArray[key].picture = new Image();

    			assetArray[key].picture.onload = function(){
    				imageLoadCounter--;
    				if(imageLoadCounter == 0){
                        console.log('images loaded');
    					assetPartLoadCounter--;
    					if(assetPartLoadCounter == 0){
    						onAssetsLoaded();
    					}
    				}
    			}
    			assetArray[key].picture.src = picture;
    		} else{
    			assetArray[key].picture = null;
    		}

    		if(sound != '' && typeof sound != 'undefined'){
    			assetArray[key].sound = soundArray.length;
    			soundArray.push(sound);
    		} else{
    			assetArray[key].sound = null;
    		}

    		/*if(tip != '' && typeof tip != 'undefined'){
                console.log('supartip')
    			var tipImage = $(tip).attr('image');

    			var tipObject = {
    				text: $(tip).text(),
    				tipImage: null
    			}

    			if(tipImage != '' && typeof tipImage != 'undefined'){
    				imageLoadCounter++;
	    			tipObject.tipImage = new Image();

	    			tipObject.tipImage.onload = function(){
	    				imageLoadCounter--;
	    				if(imageLoadCounter == 0){
	    					assetPartLoadCounter--;
	    					if(assetPartLoadCounter == 0){
	    						onAssetsLoaded();
	    					}
	    				}
	    			}

	    			tipObject.tipImage.src = tipImage;
    			}

    			if(tipObject.text == '' && tipObject.tipImage == null){
    				assetArray[key].tip = null;
    			} else{
    				assetArray[key].tip = tipObject;
    			}
    		} else{
    			assetArray[key].tip = null;
    		}*/

            assetArray[key].tip = null;
    	});
	
		if(imageLoadCounter == 0){
			assetPartLoadCounter--;
			if(assetPartLoadCounter == 0){
				onAssetsLoaded();
			}
		}

    	if(soundArray.length != 0){
            console.log(soundArray);
    		mixer = buildSoundMixer(soundArray, function(){
                console.log('loaded sound');
	    		assetPartLoadCounter--;
				if(assetPartLoadCounter == 0){
					onAssetsLoaded();
				}
	    	});
    	} else{
    		assetPartLoadCounter--;
			if(assetPartLoadCounter == 0){
				onAssetsLoaded();
			}
    	}
    }

    function createPreviewButton(x, y){
    	var previewButton = new Kinetic.Group({
    		x: x,
    		y: y
    	});

    	var previewRect = new Kinetic.Rect({
    		x: 0,
    		y: 0,
    		width: 50,
    		height: 25,
    		fill: 'red',
    		cornerRadius: 5
    	});

    	previewButtonLabel = new Kinetic.Text({
    		x: 0,
    		y: 3,
    		align: 'center',
    		fontFamily: mainFont,
    		fontSize: 16,
    		fill: 'white',
    		text: 'zoom',
    		width: previewRect.width()
    	})

    	previewButton.on(events[isMobile].click, function(){
    		createPreviewBox(crossWordGroup);
    	});

    	previewButton.on(events[isMobile].mouseover, hoverPointer);
    	previewButton.on(events[isMobile].mouseout, resetPointer);

    	previewButton.add(previewRect);
    	previewButton.add(previewButtonLabel);
    	backgroundLayer.add(previewButton);
    	backgroundLayer.draw();
    }

    function keyPressHandler(evt){
    	if(targetBox!=null){
    		var value = validateCharacter( String.fromCharCode(event.keyCode).toUpperCase() );
    		
    		if(value != '') targetBox.setValue(value);
    	}
    }

    function validateCharacter(character){
    	var ruAlphabeth = 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ';
    	var enAlphabeth = "QWERTYUIOP[]ASDFGHJKL;'`ZXCVBNM,.";

    	if(ruAlphabeth.indexOf(character) == -1){
    		var enIndex = enAlphabeth.indexOf(character);

    		if(enIndex != -1) return ruAlphabeth[enIndex];
    	} else{
    		return character;
    	}

    	return '';
    }

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
	 */

    function createHead(title, question){
        
        var mobileScaler = !isMobile ? 1 : 1.5

        var padding = 10 * mobileScaler;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: 24 * mobileScaler,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: 16 * mobileScaler,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: questionLabel.height() + (padding << 1),
                stroke: headSheme.questionTitle.stroke
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

    function attachQuestion(x, y, text){
    	var questionGroup = new Kinetic.Group({
    		x: x,
    		y: y
    	})

    	var question = new Kinetic.Text({
    		x: 10,
    		y: 2,
    		text: text,
    		fontSize: 26,
    		fontFamily: mainFont,
    		fill: 'white'
    	})

    	var questionRect = new Kinetic.Shape({
    		x: 0,
    		y: 0,
    		sceneFunc: function(context){
    			context.beginPath();
				context.moveTo(0, 0);
				context.lineTo(question.width() + 165, 0);
				context.lineTo(question.width() + 182, 17);
				context.lineTo(question.width() + 165, 34);
				context.lineTo(0, 34);
				context.lineTo(0, 0);
				context.closePath();
				// KineticJS specific context method
				context.fillStrokeShape(this);
    		},
    		fill: '#999999'
    	})

    	questionGroup.add(questionRect);
    	questionGroup.add(question);

    	backgroundLayer.add(questionGroup);
    	backgroundLayer.draw();
    }

    function attachBackground(){
    	var bacgroundGroup = new Kinetic.Group();

    	var imageObj = new Image();

    	imageObj.onload = function(){
    		var background = new Kinetic.Image({
    			x: 0,
    			y: 0,
    			image: imageObj,
    			width: imageObj.width,
    			height: imageObj.height
    		});

    		var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, $(window).width() / stage.scaler, $(window).height() / stage.scaler);

    		background.scale({x: scaler, y: scaler});

    		background.cache();

    		bacgroundGroup.add(background);
    		backgroundLayer.draw();
    	};

    	imageObj.src = 'assets/crossBackground.png'

    	backgroundLayer.add(bacgroundGroup);
    }

    function createSymbolBox(x, y, width, height, rootSymbol, matrixX, matrixY, isConstant){
        var symbolSheme = colorSheme.crossWordSymbol;

    	var boxGroup = new Kinetic.Group({ x:x, y:y });
    	var boxRectangle = new Kinetic.Rect({ //прямоугольная область
	        x: 0,
	        y: 0,
	        width: width,
	        height: height,
            //fill: symbolSheme.clear.backgroundColor,
	        fill: symbolSheme.clear.backgroundColor,
	        stroke: isConstant ? symbolSheme.constant.stroke : symbolSheme.clear.stroke,
	        strokeWidth: isConstant ? symbolSheme.constant.strokeWidth : symbolSheme.clear.strokeWidth,
	        cornerRadius: symbolSheme.clear.cornerRadius
	    });

        boxGroup.clearState = {
            fill: symbolSheme.clear.backgroundColor,
            stroke: isConstant ? symbolSheme.constant.stroke : symbolSheme.clear.stroke,
            strokeWidth: isConstant ? symbolSheme.constant.strokeWidth : symbolSheme.clear.strokeWidth
        }

        boxGroup.activeState = {
            fill: symbolSheme.active.backgroundColor,
            stroke: symbolSheme.active.stroke,
            strokeWidth: symbolSheme.active.strokeWidth
        }

        boxGroup.focusedState = {
            fill: symbolSheme.focused.backgroundColor,
            stroke: symbolSheme.focused.stroke,
            strokeWidth: symbolSheme.focused.strokeWidth
        }

        boxGroup.wrongState = {
            fill: symbolSheme.wrong.backgroundColor,
            stroke: symbolSheme.wrong.stroke,
            strokeWidth: symbolSheme.clear.strokeWidth
        }

        boxGroup.defaultSctroke = boxRectangle.stroke();
        boxGroup.isConstant = isConstant;

    	var symbol = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: 5,
	        text: '',
	        fontSize: symbolBoxWidth - 10,
	        fontFamily: mainFont,
	        fill: symbolSheme.clear.fontColor,
	        align: 'center',
	        width: width
	    });

	    var crossSizeX = x + width;
	    var crossSizeY = y + height;

	    if(crossSizeX > crossWordGroup.width()) crossWordGroup.width(crossSizeX);
	    if(crossSizeY > crossWordGroup.height()) crossWordGroup.height(crossSizeY);

	    boxGroup.add(boxRectangle);
	    boxGroup.add(symbol);

        boxGroup.symbol = symbol;

	    boxGroup.on(events[isMobile].click, onSymbolBoxSelect)

	    boxGroup.isRoot = false;
	    boxGroup.isFocused = false;
	    boxGroup.isActive = false;
	    boxGroup.rootSymbol = rootSymbol;

	    boxGroup.matrixX = matrixX;
	    boxGroup.matrixY = matrixY;

	    boxGroup.setActive = function(){
            /*boxRectangle.fill(symbolSheme.active.backgroundColor);
            if(!boxGroup.isConstant) boxRectangle.stroke(symbolSheme.active.stroke);*/

            boxRectangle.setAttrs(boxGroup.activeState);

            if(boxGroup.isConstant) {
                boxRectangle.stroke(boxGroup.clearState.stroke);
                boxRectangle.strokeWidth(boxGroup.clearState.strokeWidth);
            }

	    	boxGroup.isActive = true;

            boxGroup.symbol.fill(symbolSheme.active.fontColor);
	    	
	        //contentLayer.draw();
	    }

	    boxGroup.focusWord = function(invokerX, invokerY, forceOrientation){

	    	forceOrientation = forceOrientation || -1;

	    	var x = boxGroup.matrixX;
	    	var y = boxGroup.matrixY;

	    	var orientation;

	    	if(forceOrientation!=-1){
	    		orientation = forceOrientation;
	    	} else if(invokerX == x && invokerY == y){
	    		orientation = (typeof boxGroup.linkedWord[0]!='undefined') ? 0 : 1;
	    	} else{
	    		orientation = (invokerX == x) ? 1 : 0;
	    	}

	    	boxGroup.focusedOrientation = orientation;    	

	    	for(var i = 0; i<boxGroup.linkedWord[orientation]; i++){
	    		if(orientation==0){
	    			x = i + boxGroup.matrixX;
	    		} else{
	    			y = i + boxGroup.matrixY;
	    		}
	    		if(!crossTileMatrix[x][y].isFocused){
	    			crossTileMatrix[x][y].setActive();
	    		} else{
	    			crossTileMatrix[x][y].isActive = true;
	    		}
	    	}

	    	boxGroup.tipLink[orientation].selectTip();
	    	boxGroup.tipLink[orientation].scrollToTip();
	    }

	    boxGroup.setFocused = function(focusWordFlag){

            /*boxRectangle.fill(symbolSheme.focused.backgroundColor);

            if(!boxGroup.isConstant) boxRectangle.stroke(symbolSheme.focused.stroke);*/

            boxRectangle.setAttrs(boxGroup.focusedState);

            if(boxGroup.isConstant) {
                boxRectangle.stroke(boxGroup.clearState.stroke);
                boxRectangle.strokeWidth(boxGroup.clearState.strokeWidth);
            }

            boxGroup.symbol.fill(symbolSheme.active.fontColor);

	    	boxGroup.isFocused = true;
	    	if(focusWordFlag){
		    	if(!boxGroup.isRoot){
	    			if(targetRoot!=null) targetRoot.unFocusWord();

	    			boxGroup.rootSymbol.focusWord(boxGroup.matrixX, boxGroup.matrixY);

	    			targetRoot = boxGroup.rootSymbol;
		    	} else{

		    		if(targetRoot!=null) targetRoot.unFocusWord();

		    		boxGroup.focusWord(boxGroup.matrixX, boxGroup.matrixY);

		    		targetRoot = boxGroup;
		    	}
		    }

	        //contentLayer.draw();
	    }

	    function scrollToTile(tile, orientation){
	    	var position = {x: 0, y: 0};
	    	var area = contentLayer.getClip();

	    	position.x = tile.x() + crossWordGroup.x(); //CAUTION!! DANGER TEST ZONE!!!
			position.y = tile.y() + crossWordGroup.y();

			if(orientation == 0){
				if(position.x > (area.x + area.width - symbolBoxWidth)){
					crossWordGroup.x(crossWordGroup.x() - symbolBoxWidth);
				} else if(position.x < (area.x + symbolBoxWidth)){
					crossWordGroup.x(crossWordGroup.x() + symbolBoxWidth);
				}
			} else{
				if(position.y > (area.y + area.height - symbolBoxHeight)){
					crossWordGroup.y(crossWordGroup.y() - symbolBoxHeight);
				} else if(position.y < (area.y + symbolBoxHeight)){
					crossWordGroup.y(crossWordGroup.y() + symbolBoxHeight);
				}
			}
	    }

	    boxGroup.next = function(){
	    	var orientation = targetRoot.focusedOrientation;
	    	var x = orientation == 0 ? boxGroup.matrixX+1 : boxGroup.matrixX;
	    	var y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + 1;

	    	if(typeof crossTileMatrix[x]!='undefined'){
	    		if(typeof crossTileMatrix[x][y]!='undefined'){
	    			setTargetBox(crossTileMatrix[x][y], false);
	    			scrollToTile(crossTileMatrix[x][y], orientation);
	    		}
	    	}
	    }
	    boxGroup.prev = function(){
	    	var orientation = targetRoot.focusedOrientation;
	    	var x = orientation == 0 ? boxGroup.matrixX - 1 : boxGroup.matrixX;
	    	var y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY - 1;

	    	if(!(boxGroup.matrixX == targetRoot.matrixX && boxGroup.matrixY == targetRoot.matrixY)){
	    		setTargetBox(crossTileMatrix[x][y], false);
	    		scrollToTile(crossTileMatrix[x][y], orientation);
	    	}
	    }

	    boxGroup.unFocusWord = function(){
	    	var x = boxGroup.matrixX;
	    	var y = boxGroup.matrixY;

	    	for(var i = 0; i<boxGroup.linkedWord[boxGroup.focusedOrientation]; i++){
	    		if(boxGroup.focusedOrientation==0){
	    			x = i + boxGroup.matrixX;
	    		} else{
	    			y = i + boxGroup.matrixY;
	    		}

    			if(!crossTileMatrix[x][y].isFocused){
    				crossTileMatrix[x][y].setBlank();
    			}
	    	}
	    }

	    boxGroup.setBlank = function(){
	    	boxRectangle.setAttrs(boxGroup.clearState);

	    	isActive = false;
	    	isFocused = false;

            boxGroup.symbol.fill(symbolSheme.clear.fontColor);

	    	//console.log(boxGroup)
	    	
	    	//contentLayer.draw();
	    }

	    boxGroup.setUnfocused = function(){

	    	//console.log(boxGroup);

	    	if(!boxGroup.isActive){
		    	setBlank();
		    } else{
		    	boxGroup.setActive();
		    }
		    boxGroup.isFocused = false;
		    //contentLayer.draw();
	    }
	    boxGroup.setWrong = function(){
	    	boxRectangle.setAttrs(boxGroup.wrongState);

            boxGroup.clearState = boxGroup.wrongState;

            console.log(boxGroup.clearState);
	    	
	        //contentLayer.draw();
	    }

	    boxGroup.setValue = function(value, dontShowNext){
	    	/*var regExp = /[А-Я, A-Z]/
	    	if(regExp.test(value)){*/
	    	symbol.text(value);
	    	if(!dontShowNext) boxGroup.next();
            console.log('key press draw');
    		contentLayer.draw();
	    	//}
	    }

	    boxGroup.getValue = function(){
	    	return symbol.text();
	    }

	    boxGroup.clear = function(value){
	    	symbol.text('');
	    	//contentLayer.draw();
	    }

	    boxGroup.setTipLink = function(orientation, tipLink){
	    	boxGroup.tipLink[orientation] = tipLink;
	    }

	    boxGroup.setRoot = function(index){
	    	boxGroup.isRoot = true;
	    	boxGroup.rootIndex = index;

	    	boxGroup.linkedWord = [];
	    	boxGroup.tipLink = [];
	    	boxGroup.focusedOrientation = 0;

            var mobileScaler = isMobile ? 2 : 1;

	    	var index = new Kinetic.Text({ //текстовая метка
		        x: 2,
		        y: 0,
		        text: index,
		        fontSize: 12 * mobileScaler,
		        fontFamily: mainFont,
		        fill: 'black',
		        align: 'left',
		        width: width
		    });

	    	boxGroup.add(index);
	    	//contentLayer.draw();
	    }

	    boxGroup.getRootIndex = function(){
	    	return boxGroup.rootIndex;
	    }

	    boxGroup.linkWord = function(orientation, length){
	    	boxGroup.linkedWord[orientation] = length;
	    }

        boxGroup.showRandomSymbol = function(orientation, answer, counter){
            var symbolCollection = boxGroup.linkedWord[orientation];
            var symbolPool = [];

            for(var i = 0; i < boxGroup.linkedWord[orientation]; i++){

                x = orientation == 0 ? boxGroup.matrixX + i : boxGroup.matrixX;
                y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + i;

                if(crossTileMatrix[x][y].getValue() != answer[i]){
                    symbolPool.push({
                        tile: crossTileMatrix[x][y],
                        answer: answer[i]
                    });
                }
            }

            if(symbolPool.length > 0){
                var randomIndex = Math.round(Math.random() * (symbolPool.length - 1));

                symbolPool[randomIndex].tile.setValue(symbolPool[randomIndex].answer, true)
            }

            /*x = orientation == 0 ? boxGroup.matrixX + randomIndex : boxGroup.matrixX;
            y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + randomIndex;

            var answerValue = answer[randomIndex];

            if(crossTileMatrix[x][y].getValue() != answerValue){
                crossTileMatrix[x][y].setValue(answerValue, true);
            } else{

            }*/
        }

        boxGroup.setRootWord = function(value, orientation){
            var result = '';

            var x;
            var y;

            for(var i = 0; i < value.length; i++){

                x = orientation == 0 ? boxGroup.matrixX + i : boxGroup.matrixX;
                y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + i;

                crossTileMatrix[x][y].setValue(value[i], true);
            }
        }

	    boxGroup.getRootWord = function(orientation){
	    	var result = '';

	    	var x;
	    	var y;

	    	for(var i = 0; i < boxGroup.linkedWord[orientation]; i++){

	    		x = orientation == 0 ? boxGroup.matrixX + i : boxGroup.matrixX;
	    		y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + i;

	    		result += crossTileMatrix[x][y].getValue();
	    	}

	    	return result;
	    }

        boxGroup.getRootWordLenght = function(orientation){
            return boxGroup.linkedWord[orientation];
        }

        boxGroup.checkRootWord = function(orientation, answer){
            var result = true;

            var x;
            var y;

            for(var i = 0; i < boxGroup.linkedWord[orientation]; i++){

                x = orientation == 0 ? boxGroup.matrixX + i : boxGroup.matrixX;
                y = orientation == 0 ? boxGroup.matrixY : boxGroup.matrixY + i;

                console.log(answer[i] != crossTileMatrix[x][y].getValue());

                if(answer[i] != crossTileMatrix[x][y].getValue()){
                    result = false;
                    crossTileMatrix[x][y].setWrong();
                }
            }

            return result;

        }

	    /*contentLayer.add(boxGroup);

	    contentLayer.draw();*/

	    return boxGroup;
    }

    function createPreviewBox(crosswordGroup){
    	var previewBoxGroup = new Kinetic.Group({
    		x: defaultStageWidth >> 1,
    		y: defaultStageHeight >> 1
    	});

    	previewBoxGroup.scale({
    		x: 0,
    		y: 0
    	});

    	var modalRect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: 'black',
			width: $(window).width() / stage.scaler,
			height: $(window).height() / stage.scaler
		});

		previewBoxGroup.on(events[isMobile].click, function(){

			var outTween = new Kinetic.Tween({
	    		node: previewBoxGroup,
	    		scaleX: 0,
	    		scaleY: 0,
	    		x: defaultStageWidth >> 1,
	    		y: defaultStageHeight >> 1,
				duration: 0.4,
				easing: Kinetic.Easings.EaseInOut
	    	});

	    	outTween.onFinish = function(){
		    	previewBoxGroup.destroy();
		    	crossPreviewLayer.draw();
			}

	    	outTween.play();
		});

		modalRect.opacity(0.8);

    	previewBoxGroup.add(modalRect);

    	//DANGER IOS AREA (if big crossword)

    	crosswordGroup.toImage({
    		x: crossWordGroup.getAbsolutePosition().x + 80 * stage.scaler,
    		y: crossWordGroup.getAbsolutePosition().y + 80 * stage.scaler,
    		width: crossWordGroup.width() * stage.scaler,
    		height: crossWordGroup.height() * stage.scaler,
    		callback: function(img) {
		    	var preview = new Kinetic.Image({
		    		x: 0,
		    		y: 0,
		    		image: img
		    	});

		    	var scaler = calculateAspectRatioFit(img.width, img.height, $(window).width() / stage.scaler, $(window).height() / stage.scaler);

		    	preview.scale({
		    		x: scaler,
		    		y: scaler
		    	})

		    	previewBoxGroup.add(preview);

		    	crossPreviewLayer.draw();
			}
    	})

    	crossPreviewLayer.add(previewBoxGroup);
    	crossPreviewLayer.draw();

    	var inTween = new Kinetic.Tween({
    		node: previewBoxGroup,
    		scaleX: 1,
    		scaleY: 1,
    		x: 0,
    		y: 0,
			duration: 0.4,
			easing: Kinetic.Easings.EaseInOut
    	})

    	inTween.onFinish = function(){
    		//modalRect.show();
    	}

    	inTween.play();

    	previewBoxGroup.on(events[isMobile].mouseover, hoverPointer);
    	previewBoxGroup.on(events[isMobile].mouseout, resetPointer);
    }

    function onSymbolBoxSelect(evt){
    	var currentTarget = evt.target.getParent();
        if(targetBox == currentTarget && isMobile){
            toggleKeyboard();
            return;
        }
    	setTargetBox(currentTarget, true);
        contentLayer.draw();
    }

    function buildCrossWord(x, y, words){
    	crossWordGroup = new Kinetic.Group({
    		x: x, 
    		y: y,
    		width: 0,
    		height: 0
    	});
    	var wordIndex = 1;

    	words.each(function(index){
    		wordIndex = createWord(this, wordIndex);
    	})

    	tooltipBox.buildLabels();

    	contentLayer.add(crossWordGroup);
    	contentLayer.draw();
    }

    function createWord(word, wordIndex){

    	var isHorizontal = $(word).attr('orientation')=='down' ? false : true;

    	var firstSymbolX = +$(word).attr('x');
    	var firstSymbolY = +$(word).attr('y');

    	var questionData = $(word).children('question');

    	var toolTip = $(questionData).text();
    	var tipImage = levelManager.getRoute($(questionData).attr('picture'));
    	var tipSound = levelManager.getRoute($(questionData).attr('sound'));

    	var wordString = $(word).children('string').text();

    	var dx = marginLeft;
    	var dy = marginTop;

    	var matrixX;
    	var matrixY;

    	var rootSymbol = null;

        var constantIndex = wordString.indexOf('~');

        var isConstant = false;

        wordString = wordString.replace('~', '');

    	for(var i = 0; i < wordString.length; i++){

            isConstant = (constantIndex != -1 && i == constantIndex)

    		matrixX = isHorizontal ? firstSymbolX + i : firstSymbolX;
    		matrixY = isHorizontal ? firstSymbolY : firstSymbolY + i;

    		if(typeof crossTileMatrix[matrixX] == 'undefined'){
	    		crossTileMatrix[matrixX] = [];
	    	}

	    	if(typeof crossTileMatrix[matrixX][matrixY] == 'undefined'){
	    		crossTileMatrix[matrixX][matrixY] = createSymbolBox(dx + (symbolBoxWidth + spacer) * matrixX, dy + (symbolBoxHeight + spacer) * matrixY, symbolBoxWidth, symbolBoxHeight, rootSymbol, matrixX, matrixY, isConstant);

	    		crossWordGroup.add(crossTileMatrix[matrixX][matrixY]);
	    	}

	    	if(i == 0){
    			rootSymbol = crossTileMatrix[matrixX][matrixY];
    			if(!rootSymbol.isRoot){
    				rootSymbol.setRoot(wordIndex);
    				wordIndex++;
    			}
    			rootSymbol.linkWord(isHorizontal ? 0 : 1, wordString.length);
    		}
    	}
    	wordsArray[wordString] = {root: rootSymbol, orientation: isHorizontal ? 0 : 1};

    	//toolTip = (isHorizontal ? 'По горизонтали:\n' : 'По вертикали:\n') + rootSymbol.getRootIndex() + '. ' + toolTip;   	
    	toolTip = rootSymbol.getRootIndex() + '. ' + toolTip;   	

    	tooltipBox.pushLabel(toolTip, rootSymbol, isHorizontal ? 0 : 1, 
            firstSymbolX + '' + firstSymbolY + '' + (isHorizontal ? 0 : 1),
            wordString);

    	return wordIndex;
    }

    function createKeyboardBlock(x, y, width, height, label, handler){
        var keyboardBlock = new Kinetic.Group({x: x, y: y});

        handler = handler || keyboardBlockClickHandler;

        var keyboardSheme = colorSheme.keyboard;

        var rect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: height,
            stroke: keyboardSheme.stroke,
            fill: keyboardSheme.backgroundColor,
            cornerRadius: keyboardSheme.cornerRadius,
            strokeWidth: keyboardSheme.strokeWidth
            /*fillLinearGradientStartPoint: {x:width/2, y:0},
            fillLinearGradientEndPoint: {x:width/2,y:height},
            fillLinearGradientColorStops: [
                                             1, '#838383',
                                             0.95, '#515151',
                                             0.8, '#838181',
                                             0.45, '#C2C2C2', 
                                             0.3, '#D9D9D9', 
                                             0.05, '#C2C2C2', 
                                             0, '#E9E9E9'
                                        ]*/
        })

        var symbol = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 7,
            text: label,
            fontSize: 30,
            fontFamily: 'Calibri',
            fill: 'black',
            align: 'center',
            width: width
        });

        keyboardBlock.getValue = function(){
            return symbol.text();
        }

        keyboardBlock.on(events[isMobile].mouseover, hoverPointer);
        keyboardBlock.on(events[isMobile].mouseout, resetPointer);

        keyboardBlock.add(rect);
        keyboardBlock.add(symbol);

        keyboardBlock.on(events[isMobile].click, handler)

        function keyboardBlockClickHandler(evt){
            var target = evt.target.getParent();
            if(targetBox!=null) targetBox.setValue(target.getValue());
        }

        return keyboardBlock;
    }

    function buildKeyBoard(x, y){
        var keyboard = new Kinetic.Group({x: x, y: y});

        var keyboardSheme = colorSheme.keyboard;

        var imgObj = new Image();

        imgObj.onload = function(){

            var closeIcon = new Kinetic.Image({
                x: ($(window).width() / stage.scaler) - x - 50,
                y: 5,
                image: imgObj,
                width: 40,
                height: 40
            });

            keyboard.add(closeIcon);
            
            closeIcon.on(events[isMobile].click, toggleKeyboard)
        }

        imgObj.src = skinManager.getGraphic(colorSheme.keyboard.closeIcon);

        var backgroundRect = new Kinetic.Rect({
            x: - x,
            y: - 5,
            width: $(window).width() / stage.scaler,
            height: 155,
            fill: 'black',
            opacity: 0.8,
            stroke: keyboardSheme.rectStroke,
            strokeWidth: keyboardSheme.rectStrokeWidth
        });

        backgroundRect.on(events[isMobile].click, toggleKeyboard);

        keyboard.add(backgroundRect);

        var alphabetString = 'ЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЁЯЧСМИТЬБЮ';

        var keyWidth = 42;
        var keyHeight = 43;

        var padding = 5;

        var dx = 0;
        var dy = 3;

        for(var i = 0; i<alphabetString.length; i++){
            keyboard.add( createKeyboardBlock(dx, dy, keyWidth, keyHeight, alphabetString[i]) );
            dx += keyWidth + padding;
            if((i+1)%12==0){
                dy+=keyHeight + padding;
                dx = 0;
            }
        }

        keyboard.add( createKeyboardBlock(dx, dy, keyWidth, keyHeight, 'del', deleteSymbolAndGoNext) );

        dx += keyWidth + padding;

        keyboard.add( createKeyboardBlock(dx, dy, (keyWidth<<1) + padding, keyHeight, '←', deleteSymbolAndGoPrev) );

        keyboard.width(12*keyWidth + 11*padding);
        keyboard.height(3*keyHeight + 2*padding);

        keyboard.toggle = function(){
            console.log(keyboard.isVisible());
            if(keyboard.isVisible()){
                keyboard.hide();
            } else{
                keyboard.show();
            }

            resultLayer.draw();
        }

        keyboard.hide();

        return keyboard;
    }

    function deleteSymbolAndGoNext(){
    	targetBox.clear();
    	targetBox.next();

    	contentLayer.batchDraw();
    }
    function deleteSymbolAndGoPrev(){
    	targetBox.clear();
    	targetBox.prev();

    	contentLayer.batchDraw();
    }

    function setTargetBox(currentBox, focusWordFlag){
    	if(!currentBox.isFocused){
	    	if(targetBox != null) targetBox.setUnfocused();

	    	currentBox.setFocused(focusWordFlag);

	    	targetBox = currentBox;
	    }
    }

    function answerButtonHandler(evt){
    	var button = evt.target;
    	var isWin = true;

    	for(var word in wordsArray){
    		if(!wordsArray[word].root.checkRootWord(wordsArray[word].orientation, word.toUpperCase())){
    			isWin = false;
    			//break;
    		}
    	}

        contentLayer.draw();

    	var contentClip = contentLayer.getClip();

        resultScreen.invoke(isWin);

        var button = evt.target.getParent();

        button.setText('Ещё раз');
        button.off(events[isMobile].click);
        button.on(events[isMobile].click, clear);
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback, sheme, dontAppendToLayer){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.width(buttonRectangle.width());
        buttonGroup.height(buttonRectangle.height());

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var mobileScaler = isMobile ? 1.5 : 1;

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 20 * mobileScaler,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);

        if(!dontAppendToLayer){
            backgroundLayer.add(buttonGroup);
            backgroundLayer.draw();
        }
        
        buttonGroup.on(events[isMobile].click, callback);
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].down, resetPointer);

        return buttonGroup;
    }

    function createButton_(x, y, width, height, textLabel, callback){

    	var buttonGroup = new Kinetic.Group({ x:x, y:y });
    	var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
	        x: 0,
	        y: 0,
	        width: width,
	        height: height,
	        fillLinearGradientStartPoint: {x:width/2, y:0},
	        fillLinearGradientEndPoint: {x:width/2,y:height},
	        fillLinearGradientColorStops: [0, '#FFC23F', 1, '#FF9200']
	      });

    	var label = new Kinetic.Text({ //текстовая метка
	        x: 0,
	        y: 4,
	        width: buttonRectangle.width(),
	        height: buttonRectangle.height(),
	        text: textLabel,
	        fontSize: 20,
	        fontFamily: mainFont,
	        fill: 'black',
	        align: 'center'
	      });
    	/*Определяет новую текстовую метку кнопки
    	@param {string} value новое значение метки**/
    	buttonGroup.setText = function(value){
    		label.text(value);
    		backgroundLayer.draw();
    	};

    	buttonGroup.setText = function(value){
    		label.text(value);
    		backgroundLayer.draw();
    	};

    	buttonGroup.add(buttonRectangle);
    	buttonGroup.add(label);
    	backgroundLayer.add(buttonGroup);
    	backgroundLayer.draw();
    	
    	buttonGroup.on(events[isMobile].click, callback);
    	/*label.on("click", function(evt){ //костыль вместо полноценного потока событий
    		evt.targetNode = buttonRectangle;
    		buttonRectangle.fire('click', evt);
    	});*/
		return buttonGroup;
    }

    function showResultLabel(x, y, isWin){
    	var resultGroup = new Kinetic.Group({
    		x: - 500,
    		y: y
    	});

    	var label = new Kinetic.Text({ //текстовая метка
	        x: 100,
	        y: 13,
	        text: isWin ? 'ВЕРНО!' : 'НЕВЕРНО!',
	        fontSize: 24,
	        fontFamily: mainFont,
	        fill: 'white'
	      });

    	var backgroundRectangle = new Kinetic.Rect({
    		x: 0,
    		y: 0,
    		fill: isWin ? 'green' : '#FF4848',
    		width: 480,
    		height: 50
    	})

    	var buttonRect = new Kinetic.Rect({
    		x: 280,
    		y: 10,
    		fill: 'white',
    		cornerRadius: 10,
    		width: 150,
    		height: 30
    	});

    	var buttonLabel = new Kinetic.Text({
    		x: buttonRect.x() + 38,
    		y: buttonRect.y() + 3,
    		fontFamily: mainFont,
    		fontSize: 22,
    		fill: isWin ? 'green' : '#FF4848',
    		text: 'Ещё раз'
    	})

    	resultGroup.on(events[isMobile].click, function(){
    		var outTween = new Kinetic.Tween({
	    		duration: 0.25,
	    		node: resultGroup,
	    		x: - 500,
	    		easing: Kinetic.Easings.Linear
	    	});

	    	outTween.onFinish = function(){
	    		clear();
	    	}

	    	outTween.play();
    	});
    	resultGroup.on(events[isMobile].mouseover, hoverPointer);
    	resultGroup.on(events[isMobile].mouseout, resetPointer);

    	resultGroup.add(backgroundRectangle);
    	resultGroup.add(label);

    	resultGroup.add(buttonRect);
    	resultGroup.add(buttonLabel);

    	backgroundLayer.add(resultGroup);
    	backgroundLayer.draw();

    	var tween = new Kinetic.Tween({
    		duration: 0.25,
    		node: resultGroup,
    		x: x,
    		easing: Kinetic.Easings.Linear
    	});

    	tween.play();
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function createScrollArrow(x, y, type){
		var imageObj = new Image();

        //imageObj.onload = function() {

        var arrowSheme = colorSheme.scrollArrow;

        var arrowSize = isMobile ? 84 : 42;

		var arrow = new Kinetic.Shape({
			x: x,
			y: y,
            width: arrowSize,
            height: arrowSize,
			sceneFunc: function(context) {
              context.beginPath();
              context.moveTo(0, 0);
              context.lineTo(0, this.height());
              context.lineTo(Math.round(this.width() * 0.6), Math.round((this.height() >> 1)));
              context.closePath();
              // KineticJS specific context method
              context.fillStrokeShape(this);
            },
            fill: arrowSheme.color,
            stroke: arrowSheme.stroke,
            strokeWidth: arrowSheme.strokeWidth
        });

		arrow.type = type;

        var angle = 0;

        switch(type){
                case 'left': 
                    angle = 180;
                    arrow.x(x + (arrow.width() >> 1));
                    break;
                case 'right': angle = 0;
                    break;
                case 'up': angle = 270;
                    arrow.y(y + (arrow.height() >> 1));
                    break;
                case 'down': angle = 90;
                    break;
            }

        arrow.rotate(angle);

		arrow.on(events[isMobile].click, scrollContent);
		arrow.on(events[isMobile].mouseover, hoverPointer);
		arrow.on(events[isMobile].mouseout, resetPointer);

		arrowLayer.add(arrow);
		arrowLayer.draw();
		//}

		//imageObj.src = 'assets/scrollArrow_2.0.png';

		function scrollContent(evt){
			var direction = evt.target.type;
			var speed = symbolBoxWidth << 1;
			var dx = 0;
			var dy = 0;

			switch(direction){
				case 'left': dx += speed;
					break;
				case 'right':  dx -= speed;
					break;
				case 'up':  dy += speed;
					break;
				case 'down': dy -= speed;
					break;
			}

			crossWordGroup.x(crossWordGroup.x() + dx);
			crossWordGroup.y(crossWordGroup.y() + dy);

			contentLayer.batchDraw();
		}
	}

	function buildTipContainer(){
		var tipContainer = new Kinetic.Group({
			x: 0,
			y: 0
		});

		var modalRect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: 'black',
			width: $(window).width() / stage.scaler,
			height: $(window).height() / stage.scaler
		});

		modalRect.opacity(0.8);

		contentScrollLayer.add(modalRect);

		modalRect.hide();

		tipContainer.hideTip = function(){
			tipContainer.hide();
			modalRect.hide();
			contentScrollLayer.draw();
		}

		tipContainer.on(events[isMobile].click, tipContainer.hideTip);
		modalRect.on(events[isMobile].click, tipContainer.hideTip);

		tipContainer.image = new Kinetic.Image({
			x: 0,
			y: 0
		});

		tipContainer.label = new Kinetic.Text({
			x: 0,
			y: 0,
			fontSize: 25,
			fill: 'white',
			fontFamily: mainFont
		});

		tipContainer.add(tipContainer.image);
		tipContainer.add(tipContainer.label);
		contentScrollLayer.add(tipContainer);

		tipContainer.showTip = function(tip){
			modalRect.show();

			tipContainer.image.hide();
			tipContainer.label.hide();

			tipContainer.show();

			if(tip.tipImage != null){
				tipContainer.image.setImage(tip.tipImage);
				tipContainer.image.show();

				var scaler;

				var imageWidth = 0;
				var imageHeight = 0;

				if(tip.text == ''){
					scaler = calculateAspectRatioFit(tip.tipImage.width, tip.tipImage.height, defaultStageWidth-40, defaultStageHeight-40);

					imageWidth = tip.tipImage.width * scaler;
					imageHeight = tip.tipImage.height * scaler;

					tipContainer.image.x(((($(window).width() / stage.scaler) - 40)>>1)-(imageWidth>>1));
					tipContainer.image.y(20);
				} else{
					tipContainer.label.show();

					tipContainer.label.show();
					tipContainer.label.text(tip.text);
					if(tipContainer.label.width() > defaultStageWidth - 40) tipContainer.label.width(defaultStageWidth - 40);

					scaler = calculateAspectRatioFit(tip.tipImage.width, tip.tipImage.height, defaultStageWidth-40, defaultStageHeight-40 - tipContainer.label.height());

					imageWidth = tip.tipImage.width * scaler;
					imageHeight = tip.tipImage.height * scaler;

					tipContainer.image.y(20);

					tipContainer.image.x(((($(window).width() / stage.scaler) - 40)>>1)-(imageWidth>>1));

					tipContainer.label.y(20 + imageHeight);
					tipContainer.label.x(((($(window).width() / stage.scaler) - 40)>>1)-(tipContainer.label.width()>>1));


				}

				tipContainer.image.scale({
					x: scaler,
					y: scaler
				});


			} else{
				tipContainer.label.show();
				tipContainer.label.text(tip.text);
				if(tipContainer.label.width() > defaultStageWidth - 40) tipContainer.label.width(defaultStageWidth - 40);
				if(tipContainer.label.width() < defaultStageWidth) tipContainer.label.x((defaultStageWidth >> 1) - (tipContainer.label.width() >> 1));
				if(tipContainer.label.height() < defaultStageHeight) tipContainer.label.y((defaultStageHeight >> 1) - (tipContainer.label.height() >> 1));
			}

			contentScrollLayer.draw();
		}

		return tipContainer;
	}

	function buildTooltipBox(){
        var tipSheme = colorSheme.tipPanel;

        var tipBoxWidth = 300;

        var toolTipContainer = new Kinetic.Rect({
            x:  $(window).width() / stage.scaler - tipBoxWidth,
            y: header.height() + (containerPadding >> 1),
            width: tipBoxWidth - containerPadding,
            height: ($(window).height() / stage.scaler) - header.height() - screenMarginY - (containerPadding << 1),
            fill: tipSheme.backgroundColor,
            stroke: tipSheme.stroke,
            cornerRadius: tipSheme.cornerRadius
        });

        var tipContainerLine = new Kinetic.Line({
            points: [
                    toolTipContainer.x(),
                    header.height(),
                    toolTipContainer.x(),
                    $(window).height() / stage.scaler
                    ],
            stroke: tipSheme.lineColor
        })

        backgroundLayer.add(toolTipContainer);
        backgroundLayer.add(tipContainerLine);

        tooltipBoxLayer.setClip({
          x: toolTipContainer.x(),
          y: toolTipContainer.y() + 10,
          width: toolTipContainer.width() - 3,
          height:  toolTipContainer.height() - 15
        });

		var clip = tooltipBoxLayer.getClip();
		var tooltipBox = new Kinetic.Group({x: clip.x, y: clip.y});
		var horizontalSpacer = 10;
		var labelArray = [[], []];

		var activeTooltip = null;

		tooltipBox.scrollBar = false;
        tooltipBox.labelFontSize = isMobile ? 30 : 18;

		var rectArea = new Kinetic.Rect({
			x: clip.x - 10,
			y: clip.y - 10,
			width: clip.width + 20,
			height: clip.height + 20
		});	

		tooltipBox.pushLabel = function(tipString, rootSymbol, orientation, assetKey, answer){
			labelArray[orientation].push({
				index: rootSymbol.getRootIndex(),
				tipString:tipString,
				rootSymbol:rootSymbol,
				assetKey: assetKey,
                answer: answer
			});
		}
		tooltipBox.buildLabels = function(){

			var label;
			var rect;

			var dy = 10;

			for(var o = 0; o < 2; o++){

				rect = createTooltipRectangle(
                    5,                    
                    dy + (o == 0 ? - 6 : 15),
                    clip.width - 35,
                    o == 0 ? 'По горизонтали' : 'По вертикали'
                    );
				if(labelArray[o].length != 0) tooltipBox.add(rect);

                //rect.x((toolTipContainer.width() >> 1) - (rect.width() >> 1));

				dy += rect.height() + 15 + (o == 0 ? 0 : 15);

				labelArray[o].sort(sortRule);
				for(var i = 0; i<labelArray[o].length; i++){
					label = tooltipBox.createLabel(labelArray[o][i].tipString, labelArray[o][i].rootSymbol, o, labelArray[o][i].assetKey, labelArray[o][i].answer);
					label.y(dy);

					//dy += label.height() + 8;
					dy += label.height();

					labelArray[o][i].rootSymbol.setTipLink(o, label);

					tooltipBox.add(label);
				}
			}

			tooltipBox.height(dy);
			showScrollBar();

			tooltipBoxLayer.draw();

            console.log('another draw occurs only one time!');
		}

		function sortRule(a, b){
			if(a.index > b.index){
				return 1;
			} else if(a.index < b.index){
				return -1;
			} else if(a.index == b.index){
				if(a.orientation == 0){
					return -1;
				} else {
					return 1;
				}
			}

			return 0;
		}

	    tooltipBox.createLabel = function(tipString, rootSymbol, orientation, assetKey, answer){
	    	var toolTip = new Kinetic.Group({x:0, y:0});

            var tipSheme = colorSheme.tipPanel;
            var glowedRectSheme = tipSheme.glowedRect;

	    	var tip = assetArray[assetKey].tip;

	    	var tipMargin = 0;

            toolTip.passedSymbol = '';

	    	//if(tip != null){

                var tipIconGroup = new Kinetic.Group({
                    x: 10,
                    y: 0
                });

                var tipIconRect = new Kinetic.Rect({
                    x: 0,
                    y: 5,
                    width: tooltipBox.labelFontSize,
                    height: tooltipBox.labelFontSize,
                    fill: tipSheme.tipIcon.backgroundColor
                })

	    		var tipIcon = new Kinetic.Text({ //текстовая метка
			        x: isMobile ? 8 : 5.5,
			        y: 6,
			        text: '?',
			        fontSize: tooltipBox.labelFontSize,
			        fontFamily: mainFont,
			        fill: tipSheme.tipIcon.fontColor,
			        width: tooltipBox.labelFontSize,
                    height: tooltipBox.labelFontSize
			    });

                tipIconGroup.add(tipIconRect);
                tipIconGroup.add(tipIcon);

			    tipIconGroup.on(events[isMobile].click, function(event){
                    var obj = {
                        targetNode: toolTip
                    }

                    var randomIndex = Math.round(Math.random() * (answer.length - 1));

                    //toolTip.passedSymbol = answer[randomIndex];

                    console.log('iconHandler');

                    toolTip.selectTip();

                    setTargetBox(toolTip.rootSymbol, false);

                    if(targetRoot!=null) targetRoot.unFocusWord();

                    rootSymbol.focusWord(rootSymbol.matrixX, rootSymbol.matrixY, orientation);

                    targetRoot = rootSymbol;

                    if(toolTip.passedSymbol != ''){
                        rootSymbol.setValue(toolTip.passedSymbol);
                        toolTip.passedSymbol = '';
                    }

                    centerSymbolBox(targetRoot);

                    //toolTipClickHandler(obj);
                    rootSymbol.showRandomSymbol(orientation, answer);
                    //rootSymbol.setValue('А');
			    });

			    tipMargin = tooltipBox.labelFontSize + 4;
	    	//}

	    	var label = new Kinetic.Text({ //текстовая метка
		        x: 10 + tipMargin,
		        y: 5,
		        text: tipString,
		        fontSize: tooltipBox.labelFontSize,
		        fontFamily: mainFont,
		        fill: 'black',
		        width: clip.width - 60 - tipMargin
		    });

		    var labelRect = new Kinetic.Rect({ //текстовая метка
		        x: 5,
		        y: 0,
		        width: clip.width - 40,
		        height: label.y() + label.height() + 5,
		        strokeWidth: 0
		    });

		    //console.log(image + ' ' + labelRect.height());

		    toolTip.add(labelRect);
			toolTip.add(label);

			//if(tip != null) toolTip.add(tipIcon);

            toolTip.add(tipIconGroup);
            //toolTip.add(tipIcon);

			toolTip.height(labelRect.height());

		    var imageObj = assetArray[assetKey].picture;

		    if(imageObj != null){

				var image = new Kinetic.Image({
				  x: 30,
				  y: labelRect.height(),
				  image: imageObj,
				  width: imageObj.width,
				  height: imageObj.height
				});

				var imageSize = labelRect.width() - (image.x() << 1);

				var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, imageSize, imageSize);

				image.scaleX(scaler);
				image.scaleY(scaler);
				image.scaler = scaler;

				image.defaultPositionX = image.x();
				image.defaultPositionY = image.y();

				image.on(events[isMobile].click, imageZoomHandler);
				image.isZoomed = false;

				var totalWidth = image.width() * scaler;
				var totalHeight = image.height() * scaler;

				if(totalWidth < imageSize) image.x(image.x() + (imageSize >> 1) - (totalWidth >> 1));
				if(totalHeight < imageSize) image.y(image.y() + (imageSize >> 1) - (totalHeight >> 1));
			    
			    toolTip.add(image);

		    	labelRect.height(labelRect.height() + totalHeight);

			    //tooltipBoxLayer.draw();

				/*Скалирует изображения по клику**/
				function imageZoomHandler(evt){
					var currentImg = evt.target;
					imageContainer.zoomImage(currentImg.getImage());
				}
		    }

		    var soundData = assetArray[assetKey].sound;

		    if(soundData != null){

			    var playerSize = isMobile ? 200 : 150;

			    titlePlayer = createMusicPlayer(10, labelRect.height() + 10, playerSize, playerSize, soundData, mixer, false);

	        	titlePlayer.x((labelRect.width() >> 1) - (titlePlayer.width() >> 1))

	        	toolTip.add( titlePlayer );

	        	//toolTip.height(toolTip.height() + 40);
	        	labelRect.height(labelRect.height() + titlePlayer.height() + 20)
	        }

		    toolTip.height(labelRect.height());

		    //tooltipBoxLayer.draw();

		    toolTip.rootSymbol = rootSymbol;
		    toolTip.isFocused = false;
		    toolTip.labelRect = labelRect;

		    toolTip.on(events[isMobile].click, toolTipClickHandler);

		    toolTip.focus = function(){
		    	labelRect.setAttrs({
		    		fill: glowedRectSheme.color,
                    stroke: glowedRectSheme.stroke,
                    strokeWidth: glowedRectSheme.strokeWidth
		    	});
		    	//tooltipBoxLayer.draw();

		    	toolTip.isFocused = true;
		    }
		    toolTip.unFocus = function(){
		    	labelRect.setAttrs({
		    		fill: 'white',
                    stroke: '',
                    strokeWidth: 0
		    	});
		    	//tooltipBoxLayer.draw();
		    	toolTip.isFocused = false;
		    }

		    toolTip.scrollToTip = function(){
		    	scrollObject.scrollToContentOn(toolTip.y());
		    }

		    toolTip.selectTip = function(){
		    	if(!toolTip.isFocused){
			    	toolTip.focus();

					if(activeTooltip!=null) activeTooltip.unFocus();

					activeTooltip = toolTip;
				}

                //tooltipBoxLayer.draw();
		    }

		    toolTip.on(events[isMobile].mouseover, hoverPointer);
		    toolTip.on(events[isMobile].mouseout, resetPointer);

		    function toolTipClickHandler(evt){
				var toolTip = evt.target.getParent();
				if(!('selectTip' in toolTip)) return;

                console.log('tipHandler');

				toolTip.selectTip();

		    	setTargetBox(toolTip.rootSymbol, false);

		    	if(targetRoot!=null) targetRoot.unFocusWord();

				rootSymbol.focusWord(rootSymbol.matrixX, rootSymbol.matrixY, orientation);

				targetRoot = rootSymbol;

                if(toolTip.passedSymbol != ''){
                    rootSymbol.setValue(toolTip.passedSymbol);
                    toolTip.passedSymbol = '';
                }

				centerSymbolBox(targetRoot);

                contentLayer.draw();
                tooltipBoxLayer.draw();

                console.log('supar invoke!');
			}

			return toolTip;
	    }

		backgroundLayer.add(rectArea);

		tooltipBoxLayer.add(tooltipBox);
		//tooltipBoxLayer.draw();

		function showScrollBar(){
			if(!tooltipBox.scrollBar){
				scrollObject = createScrollBar(clip.x + clip.width-25, clip.y + 10, isMobile ? 35 : 20, clip.height-20, tooltipBox, tooltipBoxLayer);
				tooltipBox.scrollBar = true;
			}

			scrollObject.refresh();
		}
		

		return tooltipBox;
	}

	function createTooltipRectangle(x, y, width, text){
		var rectGroup = new Kinetic.Group({
			x: x,
			y: y
		});

        var tipRectSheme = colorSheme.toolTipRectangle;

        var mobileScaler = !isMobile ? 1 : 1.5;

		var rect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: tipRectSheme.backgroundColor,
			width: width,
			height: 35 * mobileScaler,
			cornerRadius: tipRectSheme.cornerRadius,
            stroke: tipRectSheme.stroke,
            strokeWidth: tipRectSheme.strokeWidth
		});

		var label = new Kinetic.Text({
			x: !isMobile ? 20 : 8,
			y: (rect.height() >> 1) - ((18 * mobileScaler) >> 1),
			text: text,
			fontFamily: mainFont,
			fontSize: 18 * mobileScaler,
			fill: tipRectSheme.labelColor,
			fontStyle: 'bold'
		});

        rectGroup.width(rect.width());
		rectGroup.height(rect.height());

		rectGroup.add(rect);
		rectGroup.add(label);

		return rectGroup;
	}

	function centerSymbolBox(symbolBox){
		var clip = contentLayer.getClip();

		//console.log(symbolBox.x() + ' --> ' + crossWordGroup.x());

		var position = {x: 0, y: 0};
		position.x = - symbolBox.x() + (clip.width >> 1) + symbolBoxWidth;
		position.y = - symbolBox.y() + (clip.height >> 1) + (symbolBoxHeight << 1);


		crossWordGroup.x(position.x);
		crossWordGroup.y(position.y);
		//contentLayer.draw();
	}

	function createScrollBar(x, y, width, height, contentGroup, clippedLayer){
        var scrollObject = new Kinetic.Group();
        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Kinetic.Rect({
           x: x,
           y: y,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11 + y;

        var vscroll = new Kinetic.Group({
            x: x + 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart*stage.scaleY()){
                    newY = scrollStart*stage.scaleY();
                }
                else if(newY > scrollEnd*stage.scaleY()) {
                    newY = scrollEnd*stage.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                }
            }
          });

            if(isMobile){

                contentGroup._scrollBarDefaultY = contentGroup.y();

                //contentGroup.draggable(true);

                contentGroup.dragBoundFunc(function(pos){
                    var newY = pos.y;
                    //console.log(newY);
                    //console.log(contentGroup.height());

                    if(newY >= scrollStart){
                        newY = scrollStart + 20;
                    }
                    else if(newY < (- contentGroup.height() + contentGroup._scrollBarDefaultY)) {
                        newY = (- contentGroup.height() + contentGroup._scrollBarDefaultY);
                    }
                    return {
                        x: this.getAbsolutePosition().x,
                        y: newY
                    }
                });

                contentGroup.on("dragmove", function(){
                    var clip = clippedLayer.getClip();

                    /*var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
                    var dy = speed * (vscroll.getPosition().y - scrollStart);
                    contentGroup.y(dy + scrollObject.dy);*/

                    var percents =  1 / Math.floor(Math.abs(contentGroup.height() / (contentGroup.y() - contentGroup._scrollBarDefaultY)));

                    if(percents < 1){
                        vscroll.y(scrollStart + percents * clip.height);
                    } else{
                        vscroll.y(scrollEnd);
                    } 
                    console.log(percents, contentGroup.height(), contentGroup.y(), contentGroup._scrollBarDefaultY);
                    contentScrollLayer.batchDraw();
                    //vscroll.y(scrollStart + dy )
                });
            }

          var scrollCircle = new Kinetic.Group({
            x: (width >> 1) - 9.5,
            y: 0
          });

          var backgroundCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: width >> 1,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: backgroundCircle.radius() * 0.6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          }

          var scrollTopArrowGroup = new Kinetic.Group({
            x: x,
            y: y
          })

          var scrollTopArrow = new Kinetic.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Kinetic.Group({
            x: x,
            y: y + height
          });

          var scrollBotArrow = new Kinetic.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Kinetic.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          contentScrollLayer.add(scrollObject);

          contentScrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            contentScrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            contentScrollLayer.drawScene();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollToContentOn = function(dy){
            if(!vscroll.isVisible()) return;
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!

            var scrollPosition = scrollStart - (dy / speed);

            if(scrollPosition > scrollEnd) scrollPosition = scrollEnd;

            vscroll.y(scrollPosition);

            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
            scrollTopArrowGroup.hide();
            scrollBotArrowGroup.hide();
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
            scrollTopArrowGroup.show();
            scrollBotArrowGroup.show();
          }

          scrollObject.refresh = function(){
            var clip = clippedLayer.getClip();

            vscrollArea.y(clip.y + 5);
            vscrollArea.height(clip.height - 10);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            scrollTopArrowGroup.y(clip.y + 5);
            scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            //contentGroup.y(0);
            vscroll.y(scrollStart);

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
            } else{
                scrollObject.show();
            }

            contentScrollLayer.draw();
          }

          clippedLayer.batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.batchDraw()
          }
          return scrollObject;
    }

	function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

    function initImageContainer(){
    	var image = new Kinetic.Image({
		  x: 20,
		  y: 20
		});

		var modalRect = new Kinetic.Rect({
			x: 0,
			y: 0,
			fill: 'black',
			width: $(window).width() / stage.scaler,
			height: $(window).height() / stage.scaler
		});

		var modalGroup = new Kinetic.Group();

		modalRect.opacity(0.8);

		image.on(events[isMobile].mouseover, hoverPointer);
    	image.on(events[isMobile].mouseout, resetPointer);

		image.hide();
		modalRect.hide();

		modalGroup.add(modalRect);
		modalGroup.add(image);

		imageContainer.add(modalGroup);

		imageContainer.zoomImage = function(imageObj){

			image.setImage(imageObj);

			var scaler = calculateAspectRatioFit(imageObj.width, imageObj.height, defaultStageWidth-40, defaultStageHeight-40);

			image.width(imageObj.width*scaler);
			image.height(imageObj.height*scaler);

			if(!image.isVisible()){
				image.x(defaultStageWidth >> 1);
		    	image.y(defaultStageHeight >> 1);

		    	image.scale({
		    		x: 0,
		    		y: 0
		    	})

		    	var inTween = new Kinetic.Tween({
		    		node: image,
		    		scaleX: 1,
		    		scaleY: 1,
		    		x: ((defaultStageWidth-40)>>1)-(image.width()>>1),
		    		y: 20,
					duration: 0.4,
					easing: Kinetic.Easings.EaseInOut
		    	})
		    	inTween.onFinish = function(){
		    		//modalRect.show();
		    	}

		    	modalRect.show();

				image.show();
		    	inTween.play();
			}

			modalGroup.on(events[isMobile].click, zoomOut);

			imageContainer.draw();
		}

		function zoomOut(evt){

	    	var outTween = new Kinetic.Tween({
	    		node: image,
	    		scaleX: 0,
	    		scaleY: 0,
	    		x: defaultStageWidth>>1,
	    		y: defaultStageHeight>>1,
				duration: 0.4,
				easing: Kinetic.Easings.EaseInOut
	    	});

	    	outTween.onFinish = function(){
		    	image.hide();
				imageContainer.draw();
			}

			modalRect.hide();

	    	outTween.play();
		}
    }

    function buildSoundMixer(assetArray, onAssetsLoaded){
        var mixer = {};

        if (!createjs.Sound.initializeDefaultPlugins()) {return;}

        var audioPath = "";
        var manifest = [];

        for(var i = 0; i < assetArray.length; i++){
            if(assetArray[i] == '' || typeof assetArray[i] == 'undefined') continue;
            manifest.push({
                id: i,
                src: assetArray[i]
            });
        }

        if(manifest.length < 1){
            onAssetsLoaded();
            return;
        }
        
        mixer.loadCounter = manifest.length;

        createjs.Sound.alternateExtensions = ["ogg"];
        createjs.Sound.addEventListener("fileload", handleLoad);
        createjs.Sound.registerManifest(manifest, audioPath);

        mixer.targetUI = null;

        mixer.assignUI = function(ui){
            if(mixer.targetUI != null) mixer.targetUI.reset();
            mixer.targetUI = ui;
        }

        mixer.play = function(id){
            mixer.instance = createjs.Sound.play(id);
            mixer.instance.addEventListener ("complete", function(instance) {
                if(mixer.targetUI != null) mixer.targetUI.reset();
            });
        }

        function handleLoad(event) {
            mixer.loadCounter--;
            if(mixer.loadCounter == 0 && onAssetsLoaded) onAssetsLoaded()
        }

        return mixer;
    }

    function createMusicPlayer(x, y, maxWidth, maxHeight, musicID, mixer, autoplay){
        var playerSheme = colorSheme.player;

        var musicPlayer = new Kinetic.Group({
            x: x,
            y: y
        });

        if(maxWidth == 0) maxWidth = playerProportions.x;
        if(maxHeight == 0) maxHeight = playerProportions.y;

        if(isMobile) autoplay = false;

        var uiLoadCounter = 2;

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: playerProportions.x,
            height: playerProportions.y,
            fill: playerSheme.backgroundRectColor,
            //cornerRadius: 4
        });

        var scaler = calculateAspectRatioFit(backgroundRect.width(), backgroundRect.height(), maxWidth, maxHeight);

        musicPlayer.scale({
        	x: scaler,
        	y: scaler
        });

        musicPlayer.width(backgroundRect.width() * scaler);
        musicPlayer.height(backgroundRect.height() * scaler);

        backgroundRect.opacity(0.60);

        musicPlayer.add(backgroundRect);

        musicPlayer.isAssigned = false;

        musicPlayer.drawSlider = true;

        musicPlayer.controllsPadding = 10;

        musicPlayer.controllsSize = backgroundRect.height() - (musicPlayer.controllsPadding << 1);

        var playGraphic = new Image();

        var graphWidth = 20;
        var graphHeight = 26.5;

        playGraphic.onload = function(){
            var playButton = new Kinetic.Sprite({
                x: musicPlayer.controllsPadding,
                y: musicPlayer.controllsPadding,
                image: playGraphic,
                animation: autoplay ? 'pause' : 'play',
                animations: {
                    pause: [
                    // x, y, width, height
                      0, 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
                    ],
                    play: [
                      // x, y, width, height
                      graphWidth * skinManager.getScaleFactor(), 0, graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor()
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            /*playButton.scale({
                x: (1 / skinManager.getScaleFactor()),
                y: (1 / skinManager.getScaleFactor())
            });*/

            var playSize = {
                x: graphWidth * skinManager.getScaleFactor() - (musicPlayer.controllsPadding << 1),
                y: backgroundRect.height() - (musicPlayer.controllsPadding << 1)
            }

            playButton.scaler = calculateAspectRatioFit(graphWidth * skinManager.getScaleFactor(), graphHeight * skinManager.getScaleFactor(), musicPlayer.controllsSize, musicPlayer.controllsSize);

            playButton.scale({
                x: playButton.scaler,
                y: playButton.scaler
            })

            musicPlayer.setStoped = function(){
                if(playButton.animation() != 'play'){
                    playButton.animation('play');
                    musicPlayer.getLayer().draw();
                    musicPlayer.paused = false;
                }
            }

            musicPlayer.assignToMixer = function(){
                mixer.assignUI(musicPlayer);
                musicPlayer.isAssigned = true;
                mixer.play(musicID);

                playButton.animation('pause');

                musicPlayer.paused = false;

                trackTime();
            }

            musicPlayer.play = function(newSoundID){
            	musicID = newSoundID;
            	musicPlayer.assignToMixer();
            }

            musicPlayer.add(playButton);
            musicPlayer.getLayer().draw();

            playButton.on(events[isMobile].mouseover, hoverPointer);
            playButton.on(events[isMobile].mouseout, resetPointer);

            playButton.on(events[isMobile].click, function(){
                if(!musicPlayer.isAssigned){
                    musicPlayer.assignToMixer();
                } else{
                
                    if(playButton.animation() == 'play'){
                        musicPlayer.paused ? mixer.instance.resume() : mixer.instance.play()

                        playButton.animation('pause');

                        musicPlayer.paused = false;
                    } else{
                       mixer.instance.pause();
                       playButton.animation('play');
                       musicPlayer.getLayer().draw();
                       musicPlayer.paused = true;
                    }
                }
            });


            //if(autoplay) musicPlayer.assignToMixer();

            uiLoadCounter--;
            if(uiLoadCounter == 0) onUILoaded();
        }

        musicPlayer.destroyPlayer = function(){
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.remove();
        }

        musicPlayer.isLoaded = false;

        musicPlayer.switchMusic = function(newSoundID){
            if(musicPlayer.isLoaded){
                musicPlayer.play(newSoundID);
            } else{
                musicPlayer.uiLoadCallback = function(){
                    musicPlayer.play(newSoundID);
                }
            }
        }

        musicPlayer.uiLoadCallback = null;

        playGraphic.src = skinManager.getGraphic('player/playButton.png');

        var stopGraphic = new Image();

        stopGraphic.onload = function(){

            var stopButtonSize = musicPlayer.controllsSize - 1;

            var stopButton = new Kinetic.Image({
                x: musicPlayer.controllsSize + musicPlayer.controllsPadding,
                y: musicPlayer.controllsPadding,
                image: stopGraphic,
                width: stopButtonSize,
                height: stopButtonSize
            });

            /*stopButton.scale({
                x: (1 / skinManager.getScaleFactor()),
                y: (1 / skinManager.getScaleFactor())
            })*/

            stopButton.on(events[isMobile].mouseover, hoverPointer);
            stopButton.on(events[isMobile].mouseout, resetPointer);

            stopButton.on(events[isMobile].click, function(){
                if(!musicPlayer.isAssigned) return;
                mixer.instance.stop();
                musicPlayer.setStoped();
            });

            musicPlayer.add(stopButton);
            musicPlayer.getLayer().draw();

            uiLoadCounter--;
            if(uiLoadCounter == 0) onUILoaded();
        }

        function onUILoaded(){
            if(autoplay){
                setTimeout(function(){
                    titlePlayer.switchMusic(currentTask);
                }, 200);
            } else{
                if(musicPlayer.uiLoadCallback){
                    musicPlayer.uiLoadCallback();
                    musicPlayer.uiLoadCallback = null;
                }
            }

            titlePlayer.isLoaded = true;
        }

        stopGraphic.src = skinManager.getGraphic('player/stopButton.png');

        var sliderRect = new Kinetic.Rect({
            x: 71,
            y: 18,
            width: backgroundRect.width() - 84,
            height: 7,
            fillLinearGradientStartPoint: {x: 0, y: 0},
            fillLinearGradientEndPoint: {x: backgroundRect.width() - 84, y: 7},
            fillLinearGradientColorStops: [0, playerSheme.trackColor[0], 1, playerSheme.trackColor[1]],
            scaleX: 0
        });

        var sliderBackground = new Kinetic.Rect({
            x: sliderRect.x(),
            y: sliderRect.y(),
            width: sliderRect.width(),
            height: sliderRect.height(),
            fill: playerSheme.trackBackroundColor
        });

        musicPlayer.instance = null;
        musicPlayer.currentTrack = '';

        musicPlayer.on(events[isMobile].click, function(){
            if(!musicPlayer.isAssigned) return;
            //if(musicPlayer.paused) mixer.instance.resume();
            var pointer = stage.getPointerPosition();
            var rectPos = sliderRect.getAbsolutePosition();

            var area = {
                left: rectPos.x,
                top: rectPos.y,
                right: rectPos.x + sliderRect.width() * stage.scaler * musicPlayer.scaleX(),
                bottom: rectPos.y + sliderRect.height() * stage.scaler * musicPlayer.scaleY()
            };

            if(pointer.x > area.left && pointer.x < area.right && pointer.y > area.top && pointer.y < area.bottom){
                var scaler = (pointer.x - area.left) / (sliderRect.width() * stage.scaler * musicPlayer.scaleX());
                mixer.instance.setPosition(mixer.instance.getDuration() * scaler);
            }
        });
        
        musicPlayer.add(sliderBackground);
        musicPlayer.add(sliderRect);

        musicPlayer.reset = function(){
            sliderRect.scaleX(0);
            musicPlayer.setStoped();
            mixer.instance.stop();
            clearInterval(musicPlayer.positionInterval);
            musicPlayer.isAssigned = false;
        }

        return musicPlayer;

        function trackTime() {
            if(!musicPlayer.layerProcessed){
                musicPlayer.getLayer().drawScene.process();
                musicPlayer.layerProcessed = true;
            }

            musicPlayer.positionInterval = setInterval(function(event) {
                if(musicPlayer == null){
                    clearInterval(this);
                    return;
                }
                sliderRect.scaleX(mixer.instance.getPosition() / mixer.instance.getDuration());
                if(musicPlayer.drawSlider) musicPlayer.getLayer().drawScene();

            }, isMobile ? 100 : 30);
        }
    }

    function createPreLoader(x, y){
        var imgObj = new Image();
        var preloaderObject = {};

        preloaderObject.hidePreloader = function(){

        }

        preloaderObject.showPreloader = function(){

        }

        imgObj.onload = function(){
            var preLoader = new Kinetic.Image({
                x: x - (imgObj.width >> 1),
                y: y - (imgObj.height >> 1),
                image: imgObj,
                width: imgObj.width,
                height: imgObj.height
            });

            preloaderObject.showPreloader = function(){
                preLoader.show();
            }

            preloaderObject.hidePreloader = function(){
                preLoader.hide();
                backgroundLayer.draw();
            }

            backgroundLayer.add(preLoader);
            backgroundLayer.draw();
        }

        imgObj.src = 'assets/preLoader.png'

        return preloaderObject;
    }

    function initResultScreen(){
        var resultScreen = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            opacity: 0.8
        });

        modalRect.hide();

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 340,
            height: 240,
            fill: 'white',
            stroke: "black",
            strokeWidth: 0.7
        });

        var resultLabel = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            text: '',
            fontSize: 40,
            fontFamily: mainFont,
            fill: 'black'
        });

        var imgObj = new Image();

        var resultButton;

        imgObj.onload = function(){

            resultButton = new Kinetic.Sprite({
                x: (backgroundRect.width() >> 1) - 70,
                y: backgroundRect.height() - 160,
                image: imgObj,
                animations: {
                    correct: [
                      // x, y, width, height
                      0,0,140,140
                    ],
                    wrong: [
                      // x, y, width, height
                      140,0,140,140
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            resultScreen.on(events[isMobile].click, function(){

                resultScreen.x((($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1));
                resultScreen.y((($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1));

                resultScreen.scale({
                    x: 1,
                    y: 1
                })

                var outTween = new Kinetic.Tween({
                    node: resultScreen,
                    scaleX: 0,
                    scaleY: 0,
                    x: (defaultStageWidth >> 1),
                    y: ((defaultStageHeight - screenMarginY) >> 1),
                    duration: 0.3,
                    easing: Kinetic.Easings.EaseInOut
                });

                modalRect.hide();

                outTween.onFinish = function(){             
                    resultScreen.hide();
                }

                outTween.play();
            });


            resultScreen.add(resultButton);

            resultLayer.draw();
        }

        imgObj.src = skinManager.getGraphic(colorSheme.resultButton.iconSource);

        resultLayer.add(modalRect);
        resultScreen.add(backgroundRect);
        resultScreen.add(resultLabel);

        resultScreen.width(backgroundRect.width());
        resultScreen.height(backgroundRect.height());

        resultScreen.hide();

        resultLayer.add(resultScreen);
        resultLayer.draw();

        resultScreen.centerLabel = function(){
            resultLabel.x((backgroundRect.width() >> 1) - (resultLabel.width() >> 1));
            resultLabel.y((resultButton.y() >> 1) - ((resultLabel.height() + resultLabel.fontSize() * 0.2) >> 1));
        }

        resultScreen.setWin = function(){
            //backgroundRect.fill('green');
            //resetButton.fill(colorSheme.resultButton.correctColor);
            resultButton.animation("correct");
            resultLabel.text('Правильно!');
        }

        resultScreen.setLoose = function(){
            //backgroundRect.fill('red');
            //resetButton.fill(colorSheme.resultButton.wrongColor);
            resultButton.animation("wrong");
            resultLabel.text('Неправильно!');
        }

        resultScreen.invoke = function(isWin){
            if(isWin){
                resultScreen.setWin();
            } else{
                resultScreen.setLoose();
            }

            resultScreen.centerLabel();

            resultScreen.x(defaultStageWidth >> 1);
            resultScreen.y((defaultStageHeight - screenMarginY) >> 1);

            resultScreen.scale({
                x: 0,
                y: 0
            })

            var inTween = new Kinetic.Tween({
                node: resultScreen,
                scaleX: 1,
                scaleY: 1,
                x: (($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1),
                y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1),
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            modalRect.show();
            resultScreen.show();

            inTween.play();
        }

        return resultScreen;
    }

});