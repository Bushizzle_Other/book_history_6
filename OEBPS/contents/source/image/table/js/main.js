$(window).load(function () {

	hookManager.applyFix('KitkatTouchFix');

	$(document).on('touchmove touch tap',function(e) {
        e.preventDefault();
    });

	Function.prototype.process = function( state ){
        var process = function( ){
            var args = arguments;
            var self = arguments.callee;
            setTimeout( function( ){
                self.handler.apply( self, args );
            }, 0 )
        }
        for( var i in state ) process[ i ] = state[ i ];
        process.handler = this;
        return process;
    }

    //var isLandscape = true;

    function reorient(e) {
		//isLandscape = (window.orientation % 180 != 0);
		//$("body > div").css("-webkit-transform", !landscape ? "rotate(-90deg)" : "");
		window.location.href = window.location.href;
	}

    window.onorientationchange = reorient;
    //window.setTimeout(reorient, 0);

	var isMobile = ( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) ? 1 : 0;
	var events = [{}, {}];

	var isIOS = ( /iPhone|iPad|iPod/i.test(navigator.userAgent));

	var screenMarginX = 0;
    var screenMarginY = isIOS ? 0 : 0;

	events[0].click = 'click';
	events[1].click = 'tap';
	events[0].down = 'mousedown';
	events[1].down = 'touchstart';
	events[0].up = 'mouseup';
	events[1].up = 'touchend';
	events[0].move = 'mousemove';
	events[1].move = 'touchmove';
	events[0].mouseover = 'mouseover';
	events[1].mouseover = 'touchstart';
	events[0].mouseout = 'mouseout';
	events[1].mouseout = 'touchend';
	events[0].mousedown = 'mousedown';
	events[1].mousedown = 'tap';

	/*Глобальные переменные**/

	var stage = new Kinetic.Stage({
		container: 'stageContainer',
		width: 1024,
		height: 576
	});

	var contentMargin = {x: 5, y: 5}

	var aspectRatio = 0; //widescreen 16:9

	var defaultStageWidth = 1024;
	var defaultStageHeight = 576;

	//Отступ всего контента от сцены
	var marginLeft = 10;
    var marginTop = 65;

    //Расстояние между ответами
    var verticalAnswerSpacer = 10;
    var horizontalAnswerSpacer = 10;

    //Отступ текста от границы блока
    var textIndentX = 5;
    var textIndentY = 10;

    var answerGroupSpacer = 30; //Расстояние между таблицей и ответами

	var maxItemWidth;
	var maxItemHeight;

	var colCount;
	var rowCount;

	var answerMode;

	var xmlFontSize;

	var isHorizontal;

	var mainTitle = null;

	var marginBot = 0;

	var skinManager = null;

	var answerScrollBar = null;
	var tableScrollBar = null;

	var colorSheme = null;

	var resultScreen = null;

	var levelManager = new LevelManager();

	var correctCount = 0;

	var mainFont = 'mainFont';

	/*Слои**/
	var backgroundLayer = new Kinetic.Layer();
	var contentLayer = new Kinetic.Layer();
	var answerBoxLayer = new Kinetic.Layer();
	var contentScrollLayer = new Kinetic.Layer();
	var dragLayer = new Kinetic.Layer();
	var resultLayer = new Kinetic.Layer();

	stage.add(backgroundLayer);
	stage.add(contentLayer);
	stage.add(answerBoxLayer);
	stage.add(contentScrollLayer);
	stage.add(dragLayer);
	stage.add(resultLayer);

	/*Группы**/
	var tableGroup = new Kinetic.Group();
	var answerGroup = new Kinetic.Group();

	contentLayer.add(tableGroup);
	answerBoxLayer.add(answerGroup);

	$(window).on('resize', fitStage);

	function fitStage(){
		scaleScreen($(window).width(),  $(window).height());
	}

	function initAspectRatio(){
		//var flag = 
		if($(window).width() / $(window).height() < 1.6){
			aspectRatio = 1;
			defaultStageWidth = 1024;
			defaultStageHeight = 768;

			stage.width(defaultStageWidth);
			stage.width(defaultStageHeight);
		}
	}

	function scaleScreen(width, height){
		var marginX = 0;
		var marginY = isIOS ? 0 : 0;

		width -= marginX;
		height -= marginY;

		var scalerX = width / defaultStageWidth;
		var scalerY = height / defaultStageHeight;
		var scaler = Math.min(scalerX, scalerY);
		
		stage.scaleX(scaler);
		stage.scaleY(scaler);

		stage.scaler = scaler;

		stage.width(width);
		stage.height(height);

		stage.draw();
	}

	function initWindowSize(){		
		scaleScreen($(window).width(), $(window).height());
	}
	function isColliding(firstRectangle, secondRectangle){
		var result = false;
	 
		if(!(firstRectangle.bottom < secondRectangle.top ||
			firstRectangle.top > secondRectangle.bottom ||
			firstRectangle.left > secondRectangle.right ||
			firstRectangle.right < secondRectangle.left))

				result = true;

		return result;
	 }

	function createAnswerBlock(x, y, width, height, label){
		var answerBlock = new Kinetic.Group({x:x, y:y, dragOnTop: false});

		answerBlock.defaultX = x;
		answerBlock.defaultY = y;

		var answerSheme = colorSheme.answerBlock;

		answerBlock.getValue = function(){
			return label.text();
		}

		answerBlock.resetPosition = function(){
			answerBlock.x(answerBlock.defaultX);
			answerBlock.y(answerBlock.defaultY);

			answerBoxLayer.draw();
		}

		x = y = 0;

		var rect = new Kinetic.Rect({
			x: x,
			y: y,
			width: width,
			height: height,
			stroke: answerSheme.stroke,
			strokeWidth: answerSheme.strokeWidth,
			fill: answerSheme.color,
			cornerRadius: answerSheme.cornerRadius
		});

		label.fill(answerSheme.fontColor);

		var labelHeight = label.height() + label.fontSize() * 0.1;

		if(labelHeight < rect.height()) label.y((rect.height() >> 1) - (labelHeight >> 1));
		
		label.x(x);
		//label.y(y);

		answerBlock.on(events[isMobile].mouseover, hoverPointer);
    	answerBlock.on(events[isMobile].mouseout, resetPointer);

		answerBlock.on(events[isMobile].down, function(evt){
			answerBlock.x(answerBlock.getAbsolutePosition().x / stage.scaleX());
            answerBlock.y(answerBlock.getAbsolutePosition().y / stage.scaleY());

			answerBlock.moveTo(dragLayer);

			answerBoxLayer.draw();
			dragLayer.draw();

			answerBlock.startDrag();
		});
		
		answerBlock.on('dragend', function(evt) {
			var node = evt.dragEndNode;
			var firstRectangle = { left:node.x(), right:node.x()+maxItemWidth, top:node.y(), bottom:node.y()+maxItemHeight } //danger height!!!!11
			var secondRectangle;
			var tableBoxArray = tableGroup.getChildren();

			for(var i = 0; i < tableBoxArray.length; i++){
				if(tableBoxArray[i].isLocked) continue;
				secondRectangle = {
					left : tableBoxArray[i].getAbsolutePosition().x / stage.scaleX(),
					right : tableBoxArray[i].getAbsolutePosition().x / stage.scaleX() + maxItemWidth,
					top : tableBoxArray[i].getAbsolutePosition().y / stage.scaleY(),
					bottom : tableBoxArray[i].getAbsolutePosition().y / stage.scaleY() + maxItemHeight
				}

				//if(isColliding(firstRectangle, secondRectangle)){

				if(isMouseColliding(secondRectangle)){

					tableBoxArray[i].setValue(node);

					answerBlock.moveTo(answerGroup);

					if(answerMode=='copy'){
						answerBlock.resetPosition();
					} else{
						answerBlock.hide();
					}
					
					answerBoxLayer.draw();
					dragLayer.drawScene();

					return;
				}
			}

			answerBlock.moveTo(answerGroup);

			answerBlock.resetPosition();

			answerBoxLayer.draw();
			dragLayer.drawScene();
		});

	    answerBlock.add(rect);
	    answerBlock.add(label);
	    answerGroup.add(answerBlock);

	    //answerBoxLayer.draw();
	}

	function isMouseColliding(boundRectangle){
		var pointer = getRelativePointerPosition();

		return (pointer.x > boundRectangle.left && pointer.x < boundRectangle.right && pointer.y > boundRectangle.top && pointer.y < boundRectangle.bottom);
	}

	function getRelativePointerPosition() {
	    var pointer = stage.getPointerPosition();
	    var pos = stage.getPosition();
	    var offset = stage.getOffset();
	    var scale = stage.getScale();

	    return {
	        //x : ((pointer.x / scale.x) - (pos.x / scale.x) + offset.x),
	        //y : ((pointer.y / scale.y) - (pos.y / scale.y) + offset.y)
	        x : pointer.x / scale.x,
	        y : pointer.y / scale.y
	    };
	}

	function createTableBlock(x, y, width, height, type, value, answer){
		var tableBlock = new Kinetic.Group({x: x, y: y});
		//var fontStyle = 'italic';
		var fontStyle = '';

		var tableBlockSheme = (value == 'active' ? colorSheme.tableBlock : colorSheme.staticTableBlock);

		if(value=='active'){
			tableBlock.answer = answer;
			tableBlock.isLocked = false;
			value = '';
		} else{
			//fontStyle = 'bold'
			tableBlock.isLocked = true;
		}

		x = y = 0;

		var radius = 15;
		var drawFunction = center;

		var rect = new Kinetic.Rect({
			x: x,
			y: y,
			width: width,
			height: height,
		    fill: tableBlockSheme.color,
		    stroke: tableBlockSheme.stroke,
		    strokeWidth: tableBlockSheme.strokeWidth,
		    cornerRadius: tableBlockSheme.cornerRadius
		});

		var blockLabel = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: value,
	        fontSize: xmlFontSize,
	        fontFamily: mainFont,
	        fill: tableBlockSheme.fontColor,
	        align: 'center',
	        fontStyle: fontStyle,
	        width: width
	    });

	    tableBlock.centerText = function(label){
	    	var labelHeight = label.height() + label.fontSize() * 0.1;

			if(labelHeight < height) label.y((height >> 1) - (labelHeight >> 1));
	    }

	    tableBlock.check = function(){
	    	return (answer.indexOf(tableBlock.value)!=-1)
	    }

		tableBlock.setValue = function(answerBlock){
			if(answerMode != 'copy'){

				if('answerBlock' in tableBlock){
					tableBlock.answerBlock.show();
					tableBlock.answerBlock.resetPosition();
				}

			tableBlock.answerBlock = answerBlock;

			}

			blockLabel.text(answerBlock.getValue());
			tableBlock.name('answered');
			tableBlock.value = answerBlock.getValue();

			tableBlock.centerText(blockLabel);

			contentLayer.draw();
		}

		tableBlock.setWrong = function(){
			blockLabel.fill('red');
			contentLayer.draw();
		}

		tableBlock.centerText(blockLabel);

		tableBlock.add(rect);
		tableBlock.add(blockLabel);
		tableGroup.add(tableBlock);
		//contentLayer.draw();

		function topRight(context) {
			context.beginPath();
			context.moveTo(x, y);
			context.lineTo(x + width - radius, y);
			context.quadraticCurveTo(x + width, y, x + width, y + radius, radius);
			context.lineTo(x + width, y + height);
			context.lineTo(x, y + height);
			context.lineTo(x, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function topLeft(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x + radius, y);
			context.quadraticCurveTo(x, y, x, y + radius, radius);
			context.lineTo(x, y + height);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
		function botLeft(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);			
			context.lineTo(x, y + height-radius);
			context.quadraticCurveTo(x, y + height, x+radius, y + height, radius);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function botRight(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);			
			context.lineTo(x, y + height);
			context.lineTo(x+width-radius, y + height);			
			context.quadraticCurveTo(x + width, y + height, x + width, y + height - radius, radius);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }
	    function center(context) {
			context.beginPath();
			context.moveTo(x+width, y);
			context.lineTo(x, y);			
			context.lineTo(x, y + height);
			context.lineTo(x+width, y + height);
			context.lineTo(x+width, y);
			context.lineWidth = 5;
			context.stroke();

			context.fillStrokeShape(this);
	    }

	}
	
	skinManager = new SkinManager('assets/skin', function(){
        colorSheme = skinManager.colorSheme;
        levelManager.loadConfig(xmlLoadHandler);
    });

    function xmlLoadHandler(xml){
    	initAspectRatio();

    	initWindowSize();

    	resultScreen = initResultScreen();

    	var root = $(xml).children('component');

    	levelManager.initRoute(root);

    	fontSize = $(root).attr('font');

    	xmlFontSize = (typeof fontSize !== 'undefined' && fontSize !== false) ? fontSize : 16;

    	var mode = $(root).attr('mode');

    	answerMode = (typeof mode !== 'undefined' && mode !== false) ? mode : 'move';

    	var tableOrientation = $(root).attr('orientation');

    	tableOrientation = (typeof tableOrientation !== 'undefined' && tableOrientation !== false) ? tableOrientation : 'vertical';

    	//isHorizontal = tableOrientation == 'vertical' ? false : true;
    	isHorizontal = false;

    	var title = root.children('title').text();
    	var question = root.children('question').text();

    	//$('#mainTitle').html(mainTitle);
    	//$('#question').html(question);

    	mainTitle = createHead(title, question);

    	backgroundLayer.add(mainTitle);

    	var columns = root.children('table').children('column');
    	var wrongWords = root.children('wrongWords').text();

    	marginBot = (35 + screenMarginY);

    	buildColumns(columns, wrongWords);

    	var resetButton = createButton((($(window).width() / stage.scaler) >> 1) - 122.5, $(window).height() / stage.scaler - marginBot, 120, 30, 'Сброс', clear);
    	resetButton.id('reset');

    	createButton(resetButton.x() + 125,  resetButton.y(), 120, 30, 'Ответить', answerButtonHandler);
    	
    	//backgroundLayer.draw();

    	drawApp();
    }

    function drawApp(){
    	setTimeout(function(){
    		contentLayer.batchDraw();
    		answerBoxLayer.batchDraw();
    		contentScrollLayer.batchDraw();
    		backgroundLayer.batchDraw();
    		//resultLayer.batchDraw();
    	}, 300); 
    }

    /*Инициирует xml-данными заголовки
    * @param {string} title главный заголовок
    * @param {string} question заголовок-вопрос
    * @param {element} labels xml набор узлов-меток
     */
    function createHead(title, question){

        var padding = 10;

        var headSheme = colorSheme.header;

        var titleGroup = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanel = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var sidePanelRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 43,
            height: 40,
            fill: headSheme.sidePanel.backgroundColor,
            stroke: headSheme.sidePanel.stroke
        });

        //sidePanel.add(sidePanelRect);

        var imgObj = new Image();

        imgObj.onload = function(){
            var appIcon = new Kinetic.Image({
                x: 1,
                y: 1,
                width: 40,
                height: 40,
                image: imgObj
            });

            //sidePanel.add(appIcon);

            //titleGroup.draw();
        }

        //imgObj.src = skinManager.getGraphic(headSheme.sidePanel.icon);

        var mainTitle = new Kinetic.Text({
            x: 35,
            y: padding,
            text: title,
            fontFamily: mainFont,
            fontSize: aspectRatio == 1 ? 28 : 24,
            width: $(window).width() / stage.scaler,
            fill: headSheme.mainTitle.fontColor,
            fontStyle: 'bold'
        });

        var mainTitleRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: headSheme.mainTitle.backgroundColor,
            width: mainTitle.width() + 4,
            height: mainTitle.height() + mainTitle.fontSize() * 0.2 + mainTitle.y() + padding,
            stroke: headSheme.mainTitle.stroke
        });

        titleGroup.add(mainTitleRect);
        titleGroup.add(mainTitle);

        titleGroup.height(mainTitleRect.height());

        if(question != ''){

            var questionLabel = new Kinetic.Text({
                x: mainTitle.x(),
                y: mainTitleRect.y() + mainTitleRect.height() + padding,
                text: question,
                fontFamily: mainFont,
                fontSize: aspectRatio == 1 ? 18 : 16,
                fill: headSheme.questionTitle.fontColor,
                width: mainTitle.width()
            });

            var questionRect = new Kinetic.Rect({
                x: mainTitleRect.x(),
                y: mainTitleRect.y() + mainTitleRect.height(),
                fill: headSheme.questionTitle.backgroundColor,
                width: mainTitle.width() + 4,
                height: questionLabel.height() + questionLabel.fontSize() * 0.1 + (padding << 1),
                stroke: headSheme.questionTitle.stroke,
                strokeWidth: headSheme.questionTitle.strokeWidth
            });

            //mainTitleRect.height(mainTitle.y() + mainTitleRect.height() + questionLabel.height() + questionLabel.fontSize() * 0.2 + 10 + padding);

            titleGroup.add(questionRect);
            titleGroup.add(questionLabel);

            titleGroup.height(titleGroup.height() + questionRect.height());
        }

        sidePanelRect.height(titleGroup.height());

        titleGroup.add(sidePanel);

        return titleGroup;
    }

    function buildColumns(columns, wrongWords){
    	var wordArray = [];
    	var titles = [];
    	var answers = [];

    	var containerPadding = 10;

    	var currentRow;
    	var currentString;
    	var premadeLabel;
    	var rowStrArray = [];

    	marginTop = mainTitle.height() + 5;

    	colCount = columns.length;
    	rowCount = columns.eq(0).children('row').length;

    	var answerColCount = 1;

    	var horizontalElementsCount = isHorizontal ? colCount : colCount + answerColCount;

    	var horizontalAnswerIndentSpace = (answerColCount - 1) * horizontalAnswerSpacer;

    	var maxWidth = (($(window).width() / stage.scaler - 70) - horizontalElementsCount * textIndentX - answerGroupSpacer - marginLeft * 2 - (isHorizontal ? (60 + (containerPadding << 2)) : horizontalAnswerIndentSpace)) / horizontalElementsCount;
    	var maxHeight = 0;

    	//if(isHorizontal) answerColCount = Math.floor(colCount - ((colCount - 1) * verticalAnswerSpacer) / maxWidth);
    	if(isHorizontal) answerColCount = 4;

    	var premadeLabel;
    	var premadeTitle;

    	var actualMaxWidth = 0;

    	correctCount = 0;

    	var wordCollection = [];

    	columns.each(function(colIndex){

    		currentRow = $(this).children('row');
    		answers[colIndex] = [];
    		titles[colIndex] = [];

    		currentRow.each(function(rowIndex){

    			currentString = $(this).text();

    			if(currentString[0]=='#'){

    				currentString = currentString.substr(1, currentString.length-1);
    				//console.log(currentString);
    				
					rowStrArray = currentString.split('~');
					rowStrArray = shuffle(rowStrArray);
					//var strCount = rowStrArray.length>1?rowStrArray.length-1:1;

    				answers[colIndex][rowIndex] = rowStrArray;

					for(var i = 0; i<rowStrArray.length; i++){
						if(!(rowStrArray[i] in wordCollection)){
							if(rowStrArray[i] == '') continue;
							premadeLabel = new Kinetic.Text({ //текстовая метка
						        x: 0,
						        y: 0,
						        text: rowStrArray[i],
						        fontSize: xmlFontSize,
						        fontFamily: mainFont,
						        fill: 'black',
						        align: 'center'
						    });

						    if(premadeLabel.width() > actualMaxWidth) actualMaxWidth = premadeLabel.width();

						    //if(premadeLabel.height() > maxHeight) maxHeight = premadeLabel.height();


							wordArray.push(premadeLabel);
							wordCollection[rowStrArray[i]] = premadeLabel;
							correctCount++;
						}
					}

					titles[colIndex][rowIndex] = 'active';

    			} else{
    				premadeTitle = new Kinetic.Text({ //ОПТИМИЗИРУЙ ЭТО (не создавай по десять раз а используй уже созданные)!!
				        x: 0,
				        y: 0,
				        text: currentString,
				        fontSize: xmlFontSize,
				        fontFamily: mainFont,
				        fill: 'black',
				        align: 'center'
				    });

				    if(premadeTitle.width() > actualMaxWidth) actualMaxWidth = premadeTitle.width();

    				titles[colIndex][rowIndex] = premadeTitle;
    			}
    		})
    	});;

    	if(wrongWords!=''){
    		wrongWords = wrongWords.substr(1, wrongWords.length-1);
    		wrongArray = wrongWords.split('#');
    		for(i = 0; i<wrongArray.length; i++){
    			if(!(wrongArray[i] in wordCollection)){
					premadeLabel = new Kinetic.Text({ //текстовая метка
				        x: 0,
				        y: 0,
				        text: wrongArray[i],
				        fontSize: xmlFontSize,
				        fontFamily: mainFont,
				        fill: 'black',
				        align: 'center'/*,
				        width: maxWidth*/
				    });

				    //if(premadeLabel.height()>maxHeight) maxHeight = premadeLabel.height();
				    if(premadeLabel.width() > actualMaxWidth) actualMaxWidth = premadeLabel.width();

					wordCollection[wrongArray[i]] = premadeLabel;
					wordArray.push(premadeLabel);
				}
    		}
    	}

    	wordArray = shuffle(wordArray);

    	actualMaxWidth += actualMaxWidth * 0.1;

    	if(actualMaxWidth < maxWidth) maxWidth = actualMaxWidth;

    	//counter = 0;

    	for(var i = 0; i < wordArray.length; i++){
    		wordArray[i].width(maxWidth);
    		if(wordArray[i].height()>maxHeight) maxHeight = wordArray[i].height();
    		//counter++;
    	}

    	for(i = 0; i < titles.length; i++){
    		for(var j = 0; j < titles[i].length; j++){
    			if(titles[i][j] == 'active') continue;
    			titles[i][j].width(maxWidth);
    			if(titles[i][j].height()>maxHeight) maxHeight = titles[i][j].height();
    			titles[i][j] = titles[i][j].text();
    		}
    	}

    	maxItemWidth = maxWidth + textIndentX;
    	maxItemHeight = maxHeight + textIndentY;

    	counter = wordArray.length;

    	//var centerPositionY = (($(window).height() / stage.scaler) >> 1) - ((rowCount * maxItemHeight) >> 1);

    	/*for(label in wordArray){
    		counter++;
    	}*/

    	var answerScrollSpace = 0;
    	var tableScrollSpace = 0;
    	var totalScrollSpace = 0;

    	var avaibleH = ($(window).height() / stage.scaler) - marginTop - marginBot - 5 - (containerPadding << 1);
    	var avaibleW = ($(window).width() / stage.scaler) - (marginLeft << 1);

    	var answerRowCount = 4;

    	var HO_answerHeight = answerRowCount * maxHeight + (answerRowCount - 1)* verticalAnswerSpacer;
    	var HO_tableHeight = avaibleH - HO_answerHeight - answerGroupSpacer;

    	var answerRowCount = Math.ceil(counter / answerColCount);

    	var filledVerticalAnswerSpace = (answerRowCount * maxItemHeight + (answerRowCount - 1) * verticalAnswerSpacer);
    	var filledVerticalTableSpace = rowCount * maxItemHeight + (rowCount - 1) * verticalAnswerSpacer;

    	answerGroup.height(filledVerticalAnswerSpace);
    	tableGroup.height(filledVerticalTableSpace);

    	var tableScrollFlag = isHorizontal ? (tableGroup.height() > HO_tableHeight) : (tableGroup.height() > avaibleH);
    	var answerScrollFlag = isHorizontal ? (answerGroup.height() > HO_answerHeight) : (answerGroup.height() > avaibleH);

    	if(tableScrollFlag) tableScrollSpace = 30;
    	if(answerScrollFlag) answerScrollSpace = isHorizontal ? 40 : 30;

    	totalScrollSpace = answerScrollSpace + tableScrollSpace;

    	avaibleW -= totalScrollSpace;

    	var filledVerticalSpace = isHorizontal ? (filledVerticalAnswerSpace + filledVerticalTableSpace + answerGroupSpacer) : Math.max(filledVerticalAnswerSpace, filledVerticalTableSpace);

    	var filledHorizontalAnswerSpace = answerColCount * maxItemWidth + (answerColCount - 1) * horizontalAnswerSpacer;
    	var filledHorizontalTableSpace = colCount * maxItemWidth;

    	var filledHorizontalSpace = isHorizontal ? Math.max(filledHorizontalAnswerSpace, filledHorizontalTableSpace) : (filledHorizontalAnswerSpace + filledHorizontalTableSpace + answerGroupSpacer);

    	//var centerPositionY = isHorizontal ? ((avaibleH > filledVerticalSpace) ? ((avaibleH >> 1) - (filledVerticalSpace >> 1) + marginTop) : (marginTop + containerPadding)) : (marginTop + containerPadding);
    	var centerPositionY = isHorizontal ? ((avaibleH > filledVerticalSpace) ? ((avaibleH >> 1) - (filledVerticalSpace >> 1) + marginTop) : (marginTop + containerPadding)) : (marginTop + containerPadding);
    	var centerPositionX = (avaibleW > filledHorizontalSpace) ? ((avaibleW >> 1) - (filledHorizontalSpace >> 1) + marginLeft) : marginLeft;

    	var tablePosition = {
    		x:marginLeft,
    		y:marginTop + containerPadding
    	}

    	var answerBoxesPosition = {
    		x:marginLeft,
    		y:marginTop
    	};

    	if(!isHorizontal){
    		tablePosition.x = centerPositionX;
    		tablePosition.y = centerPositionY;

    		answerBoxesPosition.x = (centerPositionX + colCount * maxItemWidth + answerGroupSpacer + tableScrollSpace);
    		answerBoxesPosition.y = centerPositionY;
    	} else{

    		answerBoxesPosition.y = (centerPositionY + HO_tableHeight + answerGroupSpacer);

    		if(filledHorizontalTableSpace < avaibleW){
    			tablePosition.x = ((avaibleW >> 1) - (filledHorizontalTableSpace >> 1)) + marginLeft + tableScrollSpace / 2;
    		}

    		if(filledHorizontalAnswerSpace < avaibleW){
    			answerBoxesPosition.x = ((avaibleW >> 1) - (filledHorizontalAnswerSpace >> 1)) + marginLeft + tableScrollSpace / 2;
    		}
    	}

    	//createTable(marginLeft + (horizontalAnswerIndentSpace >> 1), centerPositionY, titles, answers);
    	createTable(tablePosition.x, tablePosition.y, titles, answers);

    	var tableContainerSheme = colorSheme.tableContainer;

    	var tableBackgroundRect = new Kinetic.Rect({
    		x: tablePosition.x - containerPadding,
    		y: tablePosition.y - containerPadding,
    		width: filledHorizontalTableSpace + (tableScrollFlag ? (tableScrollSpace) : 0) + (containerPadding << 1),
    		height: isHorizontal ? ((containerPadding << 1) + HO_tableHeight) : (avaibleH + (containerPadding << 1)),
    		fill: tableContainerSheme.color,
    		stroke: tableContainerSheme.stroke,
    		strokeWidth: tableContainerSheme.strokeWidth,
    		cornerRadius: tableContainerSheme.cornerRadius
    	});

    	var answerContainerSheme = colorSheme.answerContainer;

		var answerBackgroundRect = new Kinetic.Rect({
    		x: answerBoxesPosition.x - containerPadding,
    		y: answerBoxesPosition.y - containerPadding,
    		width: (maxWidth * answerColCount + horizontalAnswerSpacer * (answerColCount - 1) + 6) + (answerScrollFlag ? (answerScrollSpace) : 0) + (containerPadding << 1),
    		height: isHorizontal ? (HO_answerHeight + (containerPadding << 1)) : (avaibleH + (containerPadding << 1)),
    		fill: answerContainerSheme.color,
    		stroke: answerContainerSheme.stroke,
    		strokeWidth: answerContainerSheme.strokeWidth,
    		cornerRadius: answerContainerSheme.cornerRadius
    	});

    	backgroundLayer.add(tableBackgroundRect);
    	backgroundLayer.add(answerBackgroundRect);
    	//backgroundLayer.draw();

    	var answerClip = {
    		x: answerBoxesPosition.x,
    		y: answerBoxesPosition.y,
    		width: (maxWidth * answerColCount + horizontalAnswerSpacer * (answerColCount - 1) + 20),
    		height: isHorizontal ? HO_answerHeight : avaibleH
    	};

    	var tableClip = {
    		x: tablePosition.x,
    		y: tablePosition.y,
    		width: maxWidth * colCount + 15,
    		height: isHorizontal ? HO_tableHeight : avaibleH
    	};

    	answerBoxLayer.setClip(answerClip);
    	contentLayer.setClip(tableClip);

    	//contentLayer.draw();

    	if(answerScrollFlag) answerScrollBar = createScrollBar(answerBackgroundRect.x() + answerBackgroundRect.width() - 30, answerClip.y + 5, 20, answerClip.height - 10, answerGroup, answerBoxLayer);
    	if(tableScrollFlag) tableScrollBar = createScrollBar(tableClip.x + tableClip.width + 10, tableClip.y + 5, 20, tableClip.height - 10, tableGroup, contentLayer);

    	buildAnswerBoxes(answerBoxesPosition.x, answerBoxesPosition.y, wordArray, answerColCount);
    }

    function buildAnswerBoxes(x, y, wordArray, boxColCount){
    	var dy = y;
    	var dx = x;
    	var counter = 0;

    	for(var i = 0; i < wordArray.length; i++){

    		createAnswerBlock(dx, dy, maxItemWidth, maxItemHeight, wordArray[i]);

    		counter++;

    		if(counter != boxColCount){
    			dx += maxItemWidth + horizontalAnswerSpacer;
    		} else{
    			counter = 0;
    			dx = x;
    			dy += maxItemHeight + verticalAnswerSpacer;
    		}
    	}
    }

    function createTable(x, y, titles, answers){

    	for(var i = 0; i<colCount; i++){
    		for(var j = 0; j<rowCount; j++){

    			if(typeof titles[i][j] == 'undefined' && typeof answers[i][j] == 'undefined'){
    				titles[i][j] = '-';
    			}

    			createTableBlock(x+maxItemWidth*i, y+maxItemHeight*j + verticalAnswerSpacer * j, maxItemWidth, maxItemHeight, calculateBoxType(i, j, colCount-1, rowCount-1), titles[i][j], answers[i][j])

    		}
    	}
    }

    function calculateBoxType(i, j, colCount, rowCount){
    	if(i==0&&j==0){
    		return 'topLeft';
    	}
    	if(j==0&&i==colCount){
    		return 'topRight'
    	}
    	if(j==rowCount&&i==0){
    		return 'botLeft';
    	}
    	if(j==rowCount&&i==colCount){
    		return 'botRight';
    	}

    	return 'center';
    }

    function answerButtonHandler(evt){
    	var answered = tableGroup.find('.answered');
    	//var isCorrect = (colCount * rowCount) - colCount == answered.length ? true : false;
    	var isCorrect = true;

    	if(answered.length != 0){

	    	for(var i = 0; i<answered.length; i++){
	    		if(!answered[i].check()){
	    			answered[i].setWrong();
	    			isCorrect = false;
	    		}
	    	}

	    	if(answered.length < correctCount) isCorrect = false;
	    } else{
	    	isCorrect = false;
	    }

    	//showResultLabel($(window).width() / stage.scaler - 385, $(window).height() / stage.scaler - 70, isCorrect);

    	resultScreen.invoke(isCorrect);

    	var button = evt.targetNode.getParent();

    	backgroundLayer.find('#reset')[0].hide();

    	button.x(button.x() - 60);

    	button.setText('Ещё раз');
    	button.off(events[isMobile].click);
    	button.on(events[isMobile].click, clear);
    }

    /*Очищает экран (можно сделать и без перезагрузки экрана, сбросив несколько состояний и вызвав lineLayer.clear())**/
    function clear(evt){
    	window.location.href = window.location.href;
    }

    function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

		var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

		return ratio;
	}

    function createButton(x, y, width, height, textLabel, callback, sheme){
        var buttonSheme = colorSheme[sheme ? sheme : 'button'];

        var buttonGroup = new Kinetic.Group({ x:x, y:y });
        var buttonRectangle = new Kinetic.Rect({ //прямоугольная область
            x: 0,
            y: 0,
            width: width,
            height: height,
            fillLinearGradientStartPoint: {x: width >> 1, y: 0},
            fillLinearGradientEndPoint: {x: width >> 1, y: height},
            fillLinearGradientColorStops: [0, buttonSheme.gradientBackgroundColorStart, 1, buttonSheme.gradientBackgroundColorEnd],
            cornerRadius: buttonSheme.cornerRadius,
            stroke: buttonSheme.stroke,
            strokeWidth: buttonSheme.strokeWidth
          });

        buttonGroup.fill = function(val){
            buttonRectangle.fill(val);
        }

        var label = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            width: buttonRectangle.width(),
            text: textLabel,
            fontSize: 26,
            fontFamily: mainFont,
            fill: buttonSheme.labelColor,
            align: 'center'
          });

        var dy = (buttonRectangle.height() >> 1) - (label.height() >> 1);

        if(dy > 0) label.y(dy - 1);

        label.height(buttonRectangle.height());
        /*Определяет новую текстовую метку кнопки
        @param {string} value новое значение метки**/
        buttonGroup.setText = function(value){
            label.text(value);
            backgroundLayer.draw();
        };

        buttonGroup.add(buttonRectangle);
        buttonGroup.add(label);
        backgroundLayer.add(buttonGroup);
        //backgroundLayer.draw();
        
        buttonGroup.on(events[isMobile].click, callback);
        buttonGroup.on(events[isMobile].mouseover, hoverPointer);
        buttonGroup.on(events[isMobile].mousedown, resetPointer);

        return buttonGroup;
    }

    function showResultLabel(x, y, isWin){
    	var label = new Kinetic.Text({ //текстовая метка
	        x: x,
	        y: y,
	        text: isWin ? 'Правильно' : 'Неправильно',
	        fontSize: 30,
	        fontFamily: mainFont,
	        fill: isWin ? 'green' : 'red'
	      });
    	backgroundLayer.add(label);
    	backgroundLayer.draw();
    }

    function initResultScreen(){
        var resultScreen = new Kinetic.Group({
            x: 0,
            y: 0
        });

        var modalRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            fill: 'black',
            width: $(window).width() / stage.scaler,
            height: $(window).height() / stage.scaler,
            opacity: 0.8
        });

        modalRect.hide();

        var backgroundRect = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: 340,
            height: 240,
            fill: 'white',
            stroke: "black",
            strokeWidth: 0.7
        });

        var resultLabel = new Kinetic.Text({ //текстовая метка
            x: 0,
            y: 0,
            text: '',
            fontSize: 40,
            fontFamily: mainFont,
            fill: 'black'
        });

        var imgObj = new Image();

        var resultButton;

        imgObj.onload = function(){

            resultButton = new Kinetic.Sprite({
                x: (backgroundRect.width() >> 1) - 70,
                y: backgroundRect.height() - 160,
                image: imgObj,
                animations: {
                    correct: [
                      // x, y, width, height
                      0,0,140,140
                    ],
                    wrong: [
                      // x, y, width, height
                      140,0,140,140
                    ]
                },
                frameRate: 7,
                frameIndex: 0
            });

            resultScreen.on(events[isMobile].click, function(){

                resultScreen.x((($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1));
                resultScreen.y((($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1));

                resultScreen.scale({
                    x: 1,
                    y: 1
                })

                var outTween = new Kinetic.Tween({
                    node: resultScreen,
                    scaleX: 0,
                    scaleY: 0,
                    x: (defaultStageWidth >> 1),
                    y: ((defaultStageHeight - screenMarginY) >> 1),
                    duration: 0.3,
                    easing: Kinetic.Easings.EaseInOut
                });

                modalRect.hide();

                outTween.onFinish = function(){             
                    resultScreen.hide();
                }

                outTween.play();
            });


            resultScreen.add(resultButton);

            //resultLayer.draw();
        }

        imgObj.src = skinManager.getGraphic(colorSheme.resultButton.iconSource);

        resultLayer.add(modalRect);
        resultScreen.add(backgroundRect);
        resultScreen.add(resultLabel);

        resultScreen.width(backgroundRect.width());
        resultScreen.height(backgroundRect.height());

        resultScreen.hide();

        resultLayer.add(resultScreen);
        //resultLayer.draw();

        resultScreen.centerLabel = function(){
            resultLabel.x((backgroundRect.width() >> 1) - (resultLabel.width() >> 1));
            resultLabel.y((resultButton.y() >> 1) - ((resultLabel.height() + resultLabel.fontSize() * 0.2) >> 1));
        }

        resultScreen.setWin = function(){
            //backgroundRect.fill('green');
            //resetButton.fill(colorSheme.resultButton.correctColor);
            resultButton.animation("correct");
            resultLabel.text('Правильно!');
        }

        resultScreen.setLoose = function(){
            //backgroundRect.fill('red');
            //resetButton.fill(colorSheme.resultButton.wrongColor);
            resultButton.animation("wrong");
            resultLabel.text('Неправильно!');
        }

        resultScreen.invoke = function(isWin){
            if(isWin){
                resultScreen.setWin();
            } else{
                resultScreen.setLoose();
            }

            resultScreen.centerLabel();

            resultScreen.x(defaultStageWidth >> 1);
            resultScreen.y((defaultStageHeight - screenMarginY) >> 1);

            resultScreen.scale({
                x: 0,
                y: 0
            })

            var inTween = new Kinetic.Tween({
                node: resultScreen,
                scaleX: 1,
                scaleY: 1,
                x: (($(window).width() / stage.scaler) >> 1) - (resultScreen.width() >> 1),
                y: (($(window).height() / stage.scaler - screenMarginY) >> 1) - (resultScreen.height() >> 1),
                duration: 0.4,
                easing: Kinetic.Easings.EaseInOut
            });

            modalRect.show();
            resultScreen.show();

            inTween.play();
        }

        return resultScreen;
    }

    function shuffle(o){
	    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
	    return o;
	};

	function hoverPointer(){
    	$('body').css('cursor', 'pointer');
    }
    function resetPointer(){
    	$('body').css('cursor', 'default');
    }

    function createScrollBar(x, y, width, height, contentGroup, clippedLayer){
        var scrollObject = new Kinetic.Group();

        var scrollScheme = colorSheme.scrollBar;

        var vscrollArea = new Kinetic.Rect({
           x: x,
           y: y,
           width: width,
           height: height,
           stroke: scrollScheme.trackColor,
           strokeWidth: 1,
           opacity: 0.7
        });

        var scrollStart = vscrollArea.y() + height * 0.11;
        var scrollEnd = vscrollArea.getHeight() - height * 0.11 + y;

        var vscroll = new Kinetic.Group({
            x: x + 10,
            y: scrollStart,
            draggable: true,
            dragBoundFunc: function(pos) {
                var newY = pos.y;

                if(newY <= scrollStart*stage.scaleY()){
                    newY = scrollStart*stage.scaleY();
                }
                else if(newY > scrollEnd*stage.scaleY()) {
                    newY = scrollEnd*stage.scaleY();
                }
                return {
                    x: this.getAbsolutePosition().x,
                    y: newY
                }
            }
          });

          var scrollCircle = new Kinetic.Group();

          var backgroundCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 10,
            stroke: scrollScheme.outerSliderColor,
            strokeWidth: 1,
            opacity: 0.7
          });

          var innerCircle = new Kinetic.Circle({
            x: 0,
            y: 0,
            radius: 6,
            fill: scrollScheme.innerSliderColor,
            strokeWidth: 0,
            opacity: 0.7
          });

          scrollCircle.add(backgroundCircle);
          scrollCircle.add(innerCircle);

          vscroll.add(scrollCircle);

          scrollCircle.on(events[isMobile].mouseover, hoverScrollBar);
          scrollCircle.on(events[isMobile].mouseout, unHoverScrollBar);

          var arrowDy = height * 0.038;
          var arrowDx = width * 0.28;
          var arrowRadius = {
            x: width * 0.23,
            y: (width * 0.23) * 0.68
          }

          var scrollTopArrowGroup = new Kinetic.Group({
            x: x,
            y: y
          })

          var scrollTopArrow = new Kinetic.Line({
            points: [
                arrowDx,
                arrowDy + arrowRadius.y,

                arrowRadius.x + arrowDx,
                arrowDy,

                arrowRadius.x * 2 + arrowDx,
                arrowDy + arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });


          var scrollTopRectangle = new Kinetic.Rect({
            x: 0,
            y: 0,
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollTopArrowGroup.add(scrollTopArrow);
          scrollTopArrowGroup.add(scrollTopRectangle);

          var scrollBotArrowGroup = new Kinetic.Group({
            x: x,
            y: y + height
          });

          var scrollBotArrow = new Kinetic.Line({
            points: [
                arrowDx,
                - arrowDy - arrowRadius.y,

                arrowRadius.x + arrowDx,
                - arrowDy,

                arrowRadius.x * 2 + arrowDx,
                - arrowDy - arrowRadius.y
            ],
            strokeWidth: 1,
            stroke: scrollScheme.arrowColor,
            opacity: 0.7
          });

          var scrollBotRectangle = new Kinetic.Rect({
            x: 0,
            y: - (arrowDy + arrowRadius.y),
            width: width,
            height: arrowDy + arrowRadius.y,
            fill: 'red',
            opacity: 0
          });

          scrollBotArrowGroup.add(scrollBotArrow);
          scrollBotArrowGroup.add(scrollBotRectangle);

          scrollBotArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollBotArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].mouseover, hoverScrollBar);
          scrollTopArrowGroup.on(events[isMobile].mouseout, unHoverScrollBar);

          scrollTopArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(-30);
          });

          scrollBotArrowGroup.on(events[isMobile].click, function(){
            scrollObject.scrollBy(30);
          });

          scrollTopArrowGroup.on('mouseover', hoverPointer);
          scrollTopArrowGroup.on('mouseout', resetPointer);
          scrollBotArrowGroup.on('mouseover', hoverPointer);
          scrollBotArrowGroup.on('mouseout', resetPointer);

          vscroll.on('mouseover', hoverPointer);
          vscroll.on('mouseout', resetPointer);
          vscroll.on('dragmove', scrollContent);
          vscroll.on('dragend', function(){clippedLayer.draw()});

          scrollObject.add(vscrollArea);
          scrollObject.add(vscroll);
          scrollObject.add(scrollBotArrowGroup);
          scrollObject.add(scrollTopArrowGroup);

          contentScrollLayer.add(scrollObject);

          //contentScrollLayer.draw();

          //var scrollObject = {};

          scrollObject.dy = contentGroup.y();

          function hoverScrollBar(){
            if(innerCircle.opacity() == 1) return;
            innerCircle.opacity(1);
            backgroundCircle.opacity(1);
            scrollTopArrow.opacity(1);
            scrollBotArrow.opacity(1);

            contentScrollLayer.drawScene();
          }

          function unHoverScrollBar(){
            if(innerCircle.opacity() == 0.7) return;
            innerCircle.opacity(0.7);
            backgroundCircle.opacity(0.7);
            scrollTopArrow.opacity(0.7);
            scrollBotArrow.opacity(0.7);

            contentScrollLayer.drawScene();
          }

          scrollObject.resetScroll = function(){
            vscroll.y(scrollStart);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.scrollBy = function(delta){
            var dy = vscroll.y()+delta;
            if(dy<scrollStart){
                dy = scrollStart;
            } else if(dy > scrollEnd){
                dy = scrollEnd;
            }

            vscroll.y(dy);
            contentScrollLayer.draw();
            vscroll.fire('dragmove');
          }

          scrollObject.hide = function(){
            vscroll.hide();
            vscrollArea.hide();
          }

          scrollObject.show = function(){
            vscroll.show();
            vscrollArea.show();
          }

          scrollObject.refresh = function(){
            var clip = clippedLayer.getClip();

            vscrollArea.y(clip.y + 5);
            vscrollArea.height(clip.height - 10);

            scrollStart = vscrollArea.y() + vscrollArea.height() * 0.11;
            scrollEnd = vscrollArea.getHeight() - vscrollArea.height() * 0.11 + vscrollArea.y();

            scrollTopArrowGroup.y(clip.y + 5);
            scrollBotArrowGroup.y(vscrollArea.y() + vscrollArea.height());

            //contentGroup.y(0);
            vscroll.y(scrollStart);

            if(clippedLayer.getClip().height >= contentGroup.height()){
                scrollObject.hide();
            } else{
                scrollObject.show();
            }

            contentScrollLayer.draw();
          }

          clippedLayer.batchDraw.process();

          function scrollContent(){
            var clip = clippedLayer.getClip();

            var speed = (clip.height - contentGroup.height())/(scrollEnd - scrollStart); //ATTENTION!!!
            var dy = speed * (vscroll.getPosition().y - scrollStart);
            contentGroup.y(dy + scrollObject.dy);
            clippedLayer.batchDraw()
          }
          return scrollObject;
    }

});