$(function(){

    var currScroll = 0;

    $('.js-task').click(function() {
        $('.media-object-overlay').show();
        currScroll = $('body').scrollTop();
        $('html').addClass('overhide');
    });

    //$('.media-object-remove').click(function(){
    //    $('#media-object').removeAttr('style').html('');
    //    $('.media-object-overlay').hide();
    //})

    $('#close-media-object')
        .click(function (e) {
            e.stopPropagation();
            $('.media-object-overlay').hide();
            $('html').removeClass('overhide');

            setTimeout(function(){
                $('body').scrollTop(currScroll);
            }, 300);

            var frame = $('#media-object').children('.activeFrame');

            //media-object recyler
            //frame[0].contentWindow.postMessage('destroy', "*");

            $(frame).removeClass('activeFrame');

            //map cache
            /*var frameUrl = $(frame).attr('src');
             if(frameUrl.indexOf('map') != -1){
             console.log('map being cached');

             $('#media-object').fadeOut();
             $(frame).hide();

             $(this).fadeOut();
             controll_elements.show();
             $('nav').css('pointer-events', 'auto');

             if($(frame).attr('isOrientationChanged')){
             oc3book.update_variant();
             if (typeof oc3book.allign_notes == 'function')
             {
             oc3book.allign_notes();
             }
             }

             return;
             }*/

            $(frame).attr('src', 'blank.html');

            setTimeout(function(){
                $(frame).remove();
            }, 1000)

            $('#media-object').fadeOut();

            $(this).fadeOut();
            //controll_elements.show();
            //$('nav').css('pointer-events', 'auto');

            //if($(frame).attr('isOrientationChanged')){
            //
            //    oc3book.resize();
            //
            //    oc3book.update_variant();
            //    if (typeof oc3book.allign_notes == 'function')
            //    {
            //        oc3book.allign_notes();
            //    }
            //}
        });

});

$(function () {

    function open_media_object (el, isHelp)
    {
        var frameContainer = $('#media-object');

        if($(frameContainer).children('iframe').length > 0) return;

        var mo = $(el).find('[data-type="media-object"]'),
            url = mo.attr('data-src');


        var w = $(window).width();
        var h = $(window).height();

        var controllsPanelHeight = $('#close-media-object').height() + (parseInt($('#close-media-object').css('margin-top')) * 3);


        //create new iframe and set it's prop

        var newFrame = $('<iframe />');
        newFrame.width(w);
        newFrame.height(h - controllsPanelHeight); //magic number, change it!

        frameContainer.width(newFrame.width());
        frameContainer.height(newFrame.height());

        $(newFrame).one('load', function () {
            $(frameContainer).fadeIn(function()
            {
                setTimeout(function() {
                    newFrame[0].contentWindow.focus();
                }, 100);
            });
        });

        newFrame.attr('src', url);

        frameContainer.append(newFrame);

        $(newFrame).addClass('activeFrame');

        $('#close-media-object').fadeIn();
        $('nav').css('pointer-events', 'none');
        $('#close-media-object').css('pointer-events', 'auto');
        return false;
    }

    window.open_media_object = open_media_object;

    $('[data-type="media-object"]').click(function(e){
        var el = $(this).parent();
        open_media_object (el);
        e.stopPropagation();
        return false;
    });

    $('.info-object').click(function (e) {
        open_media_object (this);
        e.stopPropagation();
    });

    // Resize

    window.onresize = function(){ //multiple call fix - looks if size changed since last 100ms
        if(window.skipNextResize){
            window.skipNextResize = false;
            return;
        }
        var activeElement = $(document.activeElement);
        var elementTagName = activeElement[0].tagName;
        //console.log(elementTagName);
        if(elementTagName === "TEXTAREA"){
            window.skipNextResize = true;
            return;
        }
        if(elementTagName === 'INPUT'){
            if(activeElement.attr('type') === 'text'){
                window.skipNextResize = true;
                return;
            }
        }

        if(window.resizeInProcess){
            clearTimeout(window.resizeInProcess);
        } else{
            $('#page-preloader').show();
        }

        window.resizeInProcess = setTimeout(function(){
            window.resizeInProcess = null;
            onResize();
        }, 100);
    }

    function onResize(){
        var mediaObject = $('#media-object').children('.activeFrame');
        if(mediaObject.length > 0){
            $('#page-preloader').show();
            var mediaObjectSource = $(mediaObject).attr('src');

            if(mediaObjectSource && mediaObjectSource != ''){
                var controllsPanelHeight = $('#close-media-object').height() + (parseInt($('#close-media-object').css('margin-top')) * 3);

                $(mediaObject).width($(window).width());
                $(mediaObject).height($(window).height()  - controllsPanelHeight);
                $('#media-object').width($(mediaObject).width());
                $('#media-object').height($(mediaObject).height());
                mediaObject[0].contentWindow.postMessage('resize', "*");
            }

            $(mediaObject).attr('isOrientationChanged', !$(mediaObject).attr('isOrientationChanged'));

            setTimeout(function(){
                $('#page-preloader').hide();
            }, 200);

            return;
        }

        //oc3book.resize();
        //
        //oc3book.update_variant();
        //if (typeof oc3book.allign_notes == 'function')
        //{
        //    oc3book.allign_notes();
        //}

    }

});